<?php

function parameter_type($variable, $error_msg, $variable_name, $param1, $param2, $nullable) {
    if ($nullable == 'NotNull') {
        if ($variable == '') {
            $error_msg = "Field [".$variable_name."] Cannot be Empty!";
        } else {
            /*hapus if (!is_int($parameter)) {
                $invalid_input = true;
                $error_msg = "Field [".$variable_name."] Must be a Number!";
            } else { hapus*/
                if ($variable < $param1 || $variable > $param2) {
                    $error_msg = "Invalid [".$variable_name."]. [".$variable_name."] Must be Between ".$param1." and ".$param2."!";
                }
            //hapus }
        }
    } elseif ($variable != '') {
        /*hapus if (!is_int($parameter)) {
            $invalid_input = true;
            $error_msg = "Field [".$variable_name."] Must be a Number!";
        } else { hapus*/
            if ($variable < $param1 || $variable > $param2) {
                $error_msg = "Invalid [".$variable_name."]. [".$variable_name."] Must be Between ".$param1." and ".$param2."!";
            }
        //hapus }
    }

    return $error_msg;
}

function string_type($variable, $error_msg, $variable_name, $nullable) {
    if ($nullable == 'NotNull') {
        if ($variable == '') {
            $error_msg = "Field [".$variable_name."] Cannot be Empty!";
        } else {
            if (!is_string($variable)) {
                $error_msg = "Field [".$variable_name."] Must be a String!";
            } elseif (!(strlen($variable) == 6) && ($variable_name == 'Periode' || $variable_name == 'Periode Setuju' || $variable_name == 'Periode Transaksi' || $variable_name == 'Kd Periode')) {
                $error_msg = "Invalid [".$variable_name."]!";
            }
        }
    } elseif ($variable != '') {
        if (!is_string($variable)) {
            $error_msg = "Field [".$variable_name."] Must be a String!";
        } elseif (!(strlen($variable) == 6) && ($variable_name == 'Periode' || $variable_name == 'Periode Setuju' || $variable_name == 'Periode Transaksi' || $variable_name == 'Kd Periode')) {
            $error_msg = "Invalid [".$variable_name."]!";
        }
    }

    return $error_msg;
}

function int_type($variable, $error_msg, $variable_name, $nullable) {
    if ($nullable == 'NotNull') {
        if ($variable == '') {
            $error_msg = "Field [".$variable_name."] Cannot be Empty!";
        } else {
            /*hapus if (!is_int($parameter)) {
                $error_msg = "Field [".$variable_name."] Must be a Number!";
            } hapus*/
        }
    } elseif ($variable != '') {
        /*hapus if (!is_int($parameter)) {
            $error_msg = "Field [".$variable_name."] Must be a Number!";
        } hapus*/
    }

    return $error_msg;
}

function check_param_multiple_filled($param, $param_count) {
    if ($param != '') {
        $param_count = $param_count + 1;
    }

    return $param_count;
}

?>