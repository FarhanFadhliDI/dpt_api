<?php
include "connection.php";

ini_set('MAX_EXECUTION_TIME', '-1');

function selectJmlhPmpAll($param, $prm_type, $param_name, $kd_periode) {
    $conn = getConnection();
    
    $array = array();
    $sum2 = 0;
    $where_query = "";
    $name = "";

    for ($i = 0; $i < count($param); $i++) {
        if ($prm_type == "JENIS_AW") { //1
            if ($param[$i] == "NULL") {
                $where_query = " AND CALON_PENERIMA_MP_API.Get_Jenis_Ahli_Waris(a.company, a.n_i_k, a.no_ahli_waris) IS NULL";
            } else {
                $where_query = " AND INITCAP(CALON_PENERIMA_MP_API.Get_Jenis_Ahli_Waris(a.company, a.n_i_k, a.no_ahli_waris)) = INITCAP('".$param[$i]."')";
            }

            $name = $param_name.str_replace(" ", "_", $param[$i]);
        }
        elseif ($prm_type == "KD_JNS_PENSIUN") { //2
            if ($param[$i] == "NULL") {
                $where_query = " AND a.kd_jenis_pensiun IS NULL";
            } else {
                $where_query = " AND a.kd_jenis_pensiun = '".$param[$i]."'";
            }

            $name = $param_name.str_replace(" ", "_", $param[$i]);
        }
        elseif ($prm_type == "DAPEN") { //3
            if ($param[$i] == "NULL") {
                $where_query = " AND KELOMPOK_PESERTA_API.Get_Kelompok_Dapen(a.company, PESERTA_API.Get_Kd_Kelompok(a.company, a.n_i_k)) IS NULL";

                $name = $param_name."DAPEN_".str_replace(" ", "_", $param[$i]);
            } else {
                $where_query = " AND UPPER(KELOMPOK_PESERTA_API.Get_Kelompok_Dapen(a.company, PESERTA_API.Get_Kd_Kelompok(a.company, a.n_i_k))) = UPPER('".$param[$i]."')";

                $name = $param_name.str_replace(" ", "_", $param[$i]);
            }
        }
        elseif ($prm_type == "KD_KLMPK") { //4
            if ($param[$i] == "NULL") {
                $where_query = " AND KELOMPOK_PESERTA_API.Get_Nama_Kelompok(a.company, PESERTA_API.Get_Kd_Kelompok(a.company, a.n_i_k)) IS NULL";
            } else {
                $where_query = " AND UPPER(KELOMPOK_PESERTA_API.Get_Nama_Kelompok(a.company, PESERTA_API.Get_Kd_Kelompok(a.company, a.n_i_k))) LIKE UPPER('%".$param[$i]."%')";
            }

            $name = $param_name.str_replace(" ", "_", $param[$i]);
        }
        elseif ($prm_type == "JK") { //6
            if ($param[$i] == "NULL") {
                $where_query = " AND CALON_PENERIMA_MP_API.Get_Jenis_Kelamin2(a.COMPANY, a.N_I_K, a.no_ahli_waris) IS NULL";
            } else {
                $where_query = " AND INITCAP(CALON_PENERIMA_MP_API.Get_Jenis_Kelamin2(a.COMPANY, a.N_I_K, a.no_ahli_waris)) = INITCAP('".$param[$i]."')";
            }

            $name = $param_name.str_replace(" ", "_", $param[$i]);
        }
        elseif ($prm_type == "AGAMA") { //7
            if ($param[$i] == "NULL") {
                $where_query = " AND CALON_PENERIMA_MP_API.Get_Agama(a.COMPANY, a.N_I_K, a.no_ahli_waris) IS NULL";
            } else {
                $where_query = " AND INITCAP(CALON_PENERIMA_MP_API.Get_Agama(a.COMPANY, a.N_I_K, a.no_ahli_waris)) = INITCAP('".$param[$i]."')";
            }

            $name = $param_name.str_replace(" ", "_", $param[$i]);
        }
        elseif ($prm_type == "GOLDAR") { //8
            if (strpos($param[$i], 'MIN') !== false) {
                $name = $param_name.str_replace("MIN", "_MIN", strtoupper($param[$i]));
                $data_goldar = str_replace("MIN", "-", $param[$i]);
            } elseif (strpos($param[$i], 'PLUS') !== false) {
                $name = $param_name.str_replace("PLUS", "_PLUS", strtoupper($param[$i]));
                $data_goldar = str_replace("PLUS", "+", $param[$i]);
            } else {
                $name = $param_name.str_replace(" ", "_", $param[$i]);
                $data_goldar = $param[$i];
            }
            if ($param[$i] == "NULL") {
                $where_query = " AND CALON_PENERIMA_MP_API.Get_Gol_Darah(a.COMPANY, a.N_I_K, a.no_ahli_waris) IS NULL";
            } else {
                $where_query = " AND UPPER(CALON_PENERIMA_MP_API.Get_Gol_Darah(a.COMPANY, a.N_I_K, a.no_ahli_waris)) = UPPER('".$data_goldar."')";
            }
        }
        elseif ($prm_type == "KD_BAND") { //9
            if ($param[$i] == "NULL") {
                $where_query = " AND PESERTA_API.Get_Kd_Band(a.COMPANY, a.N_I_K) IS NULL";
            } else {
                $where_query = " AND UPPER(PESERTA_API.Get_Kd_Band(a.COMPANY, a.N_I_K)) = UPPER('".$param[$i]."')";
            }

            $name = $param_name.str_replace(" ", "_", $param[$i]);
        }
        
        $query2 = "SELECT COUNT(a.n_i_k) JMLH_$name FROM PENERIMAAN_MP a WHERE a.kd_periode = '".$kd_periode."' AND a.kd_jenis_proses = '1' $where_query";
        
        $sql2 = oci_parse($conn, $query2);
        oci_execute($sql2);
        $data2 = oci_fetch_array($sql2, OCI_ASSOC+OCI_RETURN_NULLS);
        $count2 = (int)$data2['JMLH_'.$name];
        $sum2 += $count2;
            
        $array['JMLH_'.$name] = $count2;
    }

    $array['TOTAL_PMP_'.$prm_type] = $sum2;

    return $array;
}

function selectPsngnPmpAll($param, $param_name, $kd_periode) {
    $conn = getConnection();
    
    $array = array();
    $sum2 = 0;

    for ($i = 0; $i < count($param); $i++) {
        if ($param[$i] == "NULL") {
            //$prm_pst = $pst." IS NULL";
            $prm_name = "JMLH_PMP_PSNGN_NULL";
            $query2 = "SELECT COUNT(a.n_i_k) JMLH_PMP_PSNGN_NULL FROM PENERIMAAN_MP a WHERE a.kd_periode = '".$kd_periode."' AND a.kd_jenis_proses = '1' AND PESERTA_API.Get_N_I_K_Pasangan(a.company, a.n_i_k) IS NULL";
        } elseif ($param[$i] == "PMP") {
            $prm_name = "JMLH_PMP_PSNGN_PMP";
            $query2 = "SELECT COUNT(a.n_i_k) JMLH_PMP_PSNGN_PMP FROM PENERIMAAN_MP a WHERE a.kd_periode = '".$kd_periode."' AND a.kd_jenis_proses = '1' AND PESERTA_API.Get_N_I_K_Pasangan(a.company, a.n_i_k) IS NOT NULL AND PESERTA_API.Get_N_I_K_Pasangan(a.company, a.n_i_k) IN (SELECT b.n_i_k FROM PENERIMAAN_MP b)";
        } elseif ($param[$i] == "MPS") {
            $prm_name = "JMLH_PMP_PSNGN_MPS";
            $query2 = "SELECT COUNT(a.n_i_k) JMLH_PMP_PSNGN_MPS FROM PENERIMAAN_MP a WHERE a.kd_periode = '".$kd_periode."' AND a.kd_jenis_proses = '1' AND PESERTA_API.Get_N_I_K_Pasangan(a.company, a.n_i_k) IS NOT NULL AND PESERTA_API.Get_N_I_K_Pasangan(a.company, a.n_i_k) IN (SELECT b.n_i_k FROM PENERIMAAN_MP b) AND a.mps IS NOT NULL";
            /*$query2 = "SELECT COUNT(a.n_i_k) JMLH_PMP_PSNGN_MPS FROM PENERIMAAN_MP a WHERE a.kd_periode = '".$kd_periode."' AND b.kd_jenis_proses = '1' AND PESERTA_API.Get_N_I_K_Pasangan(a.company, a.n_i_k) IS NOT NULL AND PESERTA_API.Get_N_I_K_Pasangan(a.company, a.n_i_k) IN (SELECT b.n_i_k FROM PENERIMAAN_MP b, PENERIMA_MP2 c WHERE b.n_i_k = c.n_i_k AND NVL(PERIODE_API.Get_Tanggal_Awal_Periode('DPT02', 'null'), SYSDATE) BETWEEN c.valid_from AND c.valid_to AND b.mps IS NOT NULL)";*/
        } elseif ($param[$i] == "AKTIF") {
            $prm_name = "JMLH_PMP_PSNGN_AKTIF";
            $query2 = "SELECT COUNT(a.n_i_k) JMLH_PMP_PSNGN_AKTIF FROM PENERIMAAN_MP a WHERE a.kd_periode = '".$kd_periode."' AND a.kd_jenis_proses = '1' AND PESERTA_API.Get_N_I_K_Pasangan(a.company, a.n_i_k) IS NOT NULL AND PESERTA_API.Get_N_I_K_Pasangan(a.company, a.n_i_k) IN (SELECT a.n_i_k FROM PESERTA_TAB a WHERE a.status_peserta = 'AKTIF' AND a.n_i_k NOT IN (SELECT DISTINCT(b.n_i_k) FROM PENERIMAAN_MP b WHERE TO_NUMBER(b.kd_periode) <= TO_NUMBER('".$kd_periode."')) AND a.n_i_k NOT IN (SELECT DISTINCT(c.n_i_k) FROM HAPUS_PENERIMA_MP c WHERE TO_NUMBER(c.kd_periode) <= TO_NUMBER('".$kd_periode."')))";
        } elseif ($param[$i] == "HAPUS") {
            $prm_name = "JMLH_PMP_PSNGN_HAPUS";
            $query2 = "SELECT COUNT(a.n_i_k) JMLH_PMP_PSNGN_HAPUS FROM PENERIMAAN_MP a WHERE a.kd_periode = '".$kd_periode."' AND a.kd_jenis_proses = '1' AND PESERTA_API.Get_N_I_K_Pasangan(a.company, a.n_i_k) IS NOT NULL AND PESERTA_API.Get_N_I_K_Pasangan(a.company, a.n_i_k) IN (SELECT b.n_i_k FROM HAPUS_PENERIMA_MP b)";
        }
        
        $sql2 = oci_parse($conn, $query2);
        oci_execute($sql2);
        $data2 = oci_fetch_array($sql2, OCI_ASSOC+OCI_RETURN_NULLS);
        $count2 = (int)$data2[$prm_name];
        $sum2 += $count2;
        
        $array[$prm_name] = $count2;
    }
    $array['TOTAL_PMP_PSNGN'] = $sum2;

    return $array;
}

function jmlhPmp($periode, $jmlh_aw, $param_jenis_aw, $kd_jenis_pensiun, $param_dapen, $param_kd_kelompok, $param_jenis_kelamin, $param_agama, $param_gol_darah, $n_i_k, $range_tgl_kerja, $range_tgl_lahir, $range_tgl_capeg, $range_tgl_berhenti, $range_tgl_pensiun, $range_tgl_mulai_pmp, $param_kd_band, $param_pasangan_mp, $jns_api) {
    $conn = getConnection();
    $where_query = "";

        $conn = getConnection();
        
        if ($periode != '') {
            $where_query = $where_query." AND a.kd_periode = '".$periode."'";
        }
        if ($param_jenis_aw != '') {
            if ($param_jenis_aw == 1) { //Peserta
                $where_query = $where_query." AND INITCAP(CALON_PENERIMA_MP_API.Get_Jenis_Ahli_Waris(a.company, a.n_i_k, a.no_ahli_waris)) = INITCAP('Peserta')";
            } elseif ($param_jenis_aw == 2) { //Suami
                $where_query = $where_query." AND INITCAP(CALON_PENERIMA_MP_API.Get_Jenis_Ahli_Waris(a.company, a.n_i_k, a.no_ahli_waris)) = INITCAP('Suami')";
            } elseif ($param_jenis_aw == 3) { //Istri
                $where_query = $where_query." AND INITCAP(CALON_PENERIMA_MP_API.Get_Jenis_Ahli_Waris(a.company, a.n_i_k, a.no_ahli_waris)) = INITCAP('Istri')";
            } elseif ($param_jenis_aw == 4) { //Anak
                $where_query = $where_query." AND INITCAP(CALON_PENERIMA_MP_API.Get_Jenis_Ahli_Waris(a.company, a.n_i_k, a.no_ahli_waris)) = INITCAP('Anak')";
            } elseif ($param_jenis_aw == 5) { //Wali
                $where_query = $where_query." AND INITCAP(CALON_PENERIMA_MP_API.Get_Jenis_Ahli_Waris(a.company, a.n_i_k, a.no_ahli_waris)) = INITCAP('Wali')";
            } elseif ($param_jenis_aw == 6) { //Null
                $where_query = $where_query." AND CALON_PENERIMA_MP_API.Get_Jenis_Ahli_Waris(a.company, a.n_i_k, a.no_ahli_waris) IS NULL";
            }
        }
        if ($kd_jenis_pensiun != '') {
            $where_query = $where_query." AND a.kd_jenis_pensiun = '".$kd_jenis_pensiun."'";
        }
        if ($param_dapen != '') {
            if ($param_dapen == 1) { //DAPEN I
                $where_query = $where_query." AND UPPER(KELOMPOK_PESERTA_API.Get_Kelompok_Dapen(a.company, PESERTA_API.Get_Kd_Kelompok(a.company, a.n_i_k))) = UPPER('DAPEN I')";
            } elseif ($param_dapen == 2) { //DAPEN II
                $where_query = $where_query." AND UPPER(KELOMPOK_PESERTA_API.Get_Kelompok_Dapen(a.company, PESERTA_API.Get_Kd_Kelompok(a.company, a.n_i_k))) = UPPER('DAPEN II')";
            } elseif ($param_dapen == 3) { //NULL
                $where_query = $where_query." AND KELOMPOK_PESERTA_API.Get_Kelompok_Dapen(a.company, PESERTA_API.Get_Kd_Kelompok(a.company, a.n_i_k)) IS NULL";
            }
        }
        if ($param_kd_kelompok != '') {
            if ($param_kd_kelompok == 1) { //PRA
                $where_query = $where_query." AND UPPER(KELOMPOK_PESERTA_API.Get_Nama_Kelompok(a.company, PESERTA_API.Get_Kd_Kelompok(a.company, a.n_i_k))) LIKE UPPER('%PRA%')";
            } elseif ($param_kd_kelompok == 2) { //PASCA
                $where_query = $where_query." AND UPPER(KELOMPOK_PESERTA_API.Get_Nama_Kelompok(a.company, PESERTA_API.Get_Kd_Kelompok(a.company, a.n_i_k))) LIKE UPPER('%PASCA%')";
            } elseif ($param_kd_kelompok == 3) { //NULL
                $where_query = $where_query." AND KELOMPOK_PESERTA_API.Get_Nama_Kelompok(a.company, PESERTA_API.Get_Kd_Kelompok(a.company, a.n_i_k)) IS NULL";
            }
        }
        if ($param_jenis_kelamin != '') {
            if ($param_jenis_kelamin == 1) { //Pria
                $where_query = $where_query." AND INITCAP(CALON_PENERIMA_MP_API.Get_Jenis_Kelamin2(a.COMPANY, a.N_I_K, a.no_ahli_waris)) = INITCAP('Pria')";
            } elseif ($param_jenis_kelamin == 2) { //Wanita
                $where_query = $where_query." AND INITCAP(CALON_PENERIMA_MP_API.Get_Jenis_Kelamin2(a.COMPANY, a.N_I_K, a.no_ahli_waris)) = INITCAP('Wanita')";
            } elseif ($param_jenis_kelamin == 3) { //NULL
                $where_query = $where_query." AND CALON_PENERIMA_MP_API.Get_Jenis_Kelamin2(a.COMPANY, a.N_I_K, a.no_ahli_waris) IS NULL";
            }
        }
        if ($param_agama != '') {
            if ($param_agama == 1) { //Islam
                $where_query = $where_query." AND INITCAP(CALON_PENERIMA_MP_API.Get_Agama(a.COMPANY, a.N_I_K, a.no_ahli_waris)) = INITCAP('Islam')";
            } elseif ($param_agama == 2) { //Katolik
                $where_query = $where_query." AND INITCAP(CALON_PENERIMA_MP_API.Get_Agama(a.COMPANY, a.N_I_K, a.no_ahli_waris)) = INITCAP('Katolik')";
            } elseif ($param_agama == 3) { //Budha
                $where_query = $where_query." AND INITCAP(CALON_PENERIMA_MP_API.Get_Agama(a.COMPANY, a.N_I_K, a.no_ahli_waris)) = INITCAP('Budha')";
            } elseif ($param_agama == 4) { //Kristen
                $where_query = $where_query." AND INITCAP(CALON_PENERIMA_MP_API.Get_Agama(a.COMPANY, a.N_I_K, a.no_ahli_waris)) = INITCAP('Kristen')";
            } elseif ($param_agama == 5) { //Hindu
                $where_query = $where_query." AND INITCAP(CALON_PENERIMA_MP_API.Get_Agama(a.COMPANY, a.N_I_K, a.no_ahli_waris)) = INITCAP('Hindu')";
            } elseif ($param_agama == 6) { //NULL
                $where_query = $where_query." AND CALON_PENERIMA_MP_API.Get_Agama(a.COMPANY, a.N_I_K, a.no_ahli_waris) IS NULL";
            }
        }
        if ($param_gol_darah != '') {
            if ($param_gol_darah == 1) { //O
                $where_query = $where_query." AND UPPER(CALON_PENERIMA_MP_API.Get_Gol_Darah(a.COMPANY, a.N_I_K, a.no_ahli_waris)) = UPPER('O')";
            } elseif ($param_gol_darah == 2) { //A
                $where_query = $where_query." AND UPPER(CALON_PENERIMA_MP_API.Get_Gol_Darah(a.COMPANY, a.N_I_K, a.no_ahli_waris)) = UPPER('A')";
            } elseif ($param_gol_darah == 3) { //B
                $where_query = $where_query." AND UPPER(CALON_PENERIMA_MP_API.Get_Gol_Darah(a.COMPANY, a.N_I_K, a.no_ahli_waris)) = UPPER('B')";
            } elseif ($param_gol_darah == 4) { //AB
                $where_query = $where_query." AND UPPER(CALON_PENERIMA_MP_API.Get_Gol_Darah(a.COMPANY, a.N_I_K, a.no_ahli_waris)) = UPPER('AB')";
            } elseif ($param_gol_darah == 5) { //O-
                $where_query = $where_query." AND UPPER(CALON_PENERIMA_MP_API.Get_Gol_Darah(a.COMPANY, a.N_I_K, a.no_ahli_waris)) = UPPER('O-')";
            } elseif ($param_gol_darah == 6) { //A-
                $where_query = $where_query." AND UPPER(CALON_PENERIMA_MP_API.Get_Gol_Darah(a.COMPANY, a.N_I_K, a.no_ahli_waris)) = UPPER('A-')";
            } elseif ($param_gol_darah == 7) { //B-
                $where_query = $where_query." AND UPPER(CALON_PENERIMA_MP_API.Get_Gol_Darah(a.COMPANY, a.N_I_K, a.no_ahli_waris)) = UPPER('B-')";
            } elseif ($param_gol_darah == 8) { //AB-
                $where_query = $where_query." AND UPPER(CALON_PENERIMA_MP_API.Get_Gol_Darah(a.COMPANY, a.N_I_K, a.no_ahli_waris)) = UPPER('AB-')";
            } elseif ($param_gol_darah == 9) { //O+
                $where_query = $where_query." AND UPPER(CALON_PENERIMA_MP_API.Get_Gol_Darah(a.COMPANY, a.N_I_K, a.no_ahli_waris)) = UPPER('O+')";
            } elseif ($param_gol_darah == 10) { //A+
                $where_query = $where_query." AND UPPER(CALON_PENERIMA_MP_API.Get_Gol_Darah(a.COMPANY, a.N_I_K, a.no_ahli_waris)) = UPPER('A+')";
            } elseif ($param_gol_darah == 11) { //B+
                $where_query = $where_query." AND UPPER(CALON_PENERIMA_MP_API.Get_Gol_Darah(a.COMPANY, a.N_I_K, a.no_ahli_waris)) = UPPER('B+')";
            } elseif ($param_gol_darah == 12) { //AB+
                $where_query = $where_query." AND UPPER(CALON_PENERIMA_MP_API.Get_Gol_Darah(a.COMPANY, a.N_I_K, a.no_ahli_waris)) = UPPER('AB+')";
            } elseif ($param_gol_darah == 13) { //NULL
                $where_query = $where_query." AND CALON_PENERIMA_MP_API.Get_Gol_Darah(a.COMPANY, a.N_I_K, a.no_ahli_waris) IS NULL";
            }
        }
        if ($range_tgl_kerja != '') {
            $tgl_kerja = explode("..", $range_tgl_kerja);
            $where_query = $where_query." AND (CALON_PENERIMA_MP_API.Get_Tgl_Bekerja(a.COMPANY, a.N_I_K, a.no_ahli_waris) BETWEEN TO_DATE('".$tgl_kerja[0]."','MM/DD/YYYY') AND TO_DATE('".$tgl_kerja[1]."','MM/DD/YYYY'))";
        }
        if ($range_tgl_lahir != '') {
            $tgl_lahir = explode("..", $range_tgl_lahir);
            $where_query = $where_query." AND (CALON_PENERIMA_MP_API.Get_Tgl_Lahir(a.COMPANY, a.N_I_K, a.no_ahli_waris) BETWEEN TO_DATE('".$tgl_lahir[0]."','MM/DD/YYYY') AND TO_DATE('".$tgl_lahir[1]."','MM/DD/YYYY'))";
        }
        if ($range_tgl_capeg != '') {
            $tgl_capeg = explode("..", $range_tgl_capeg);
            $where_query = $where_query." AND (PESERTA_API.Get_Tgl_Capeg(a.COMPANY, a.N_I_K) BETWEEN TO_DATE('".$tgl_capeg[0]."','MM/DD/YYYY') AND TO_DATE('".$tgl_capeg[1]."','MM/DD/YYYY'))";
        }
        if ($range_tgl_berhenti != '') {
            $tgl_berhenti = explode("..", $range_tgl_berhenti);
            $where_query = $where_query." AND (PESERTA_API.Get_Tgl_Berhenti(a.COMPANY, a.N_I_K) BETWEEN TO_DATE('".$tgl_berhenti[0]."','MM/DD/YYYY') AND TO_DATE('".$tgl_berhenti[1]."','MM/DD/YYYY'))";
        }
        if ($range_tgl_pensiun != '') {
            $tgl_pensiun = explode("..", $range_tgl_pensiun);
            $where_query = $where_query." AND (PENERIMA_MP_API.Get_Tanggal_Pensiun(a.company,a.n_i_k) BETWEEN TO_DATE('".$tgl_pensiun[0]."','MM/DD/YYYY') AND TO_DATE('".$tgl_pensiun[1]."','MM/DD/YYYY'))";
        }
        if ($range_tgl_mulai_pmp != '') {
            $tgl_mulai_pmp = explode("..", $range_tgl_mulai_pmp);
            $where_query = $where_query." AND (PESERTA_API.Get_Tgl_Berhenti(a.COMPANY, a.N_I_K) BETWEEN TO_DATE('".$tgl_mulai_pmp[0]."','MM/DD/YYYY') AND TO_DATE('".$tgl_mulai_pmp[1]."','MM/DD/YYYY'))";
        }
        if ($param_kd_band != '') {
            if ($param_kd_band == 1) { //I
                $where_query = $where_query." AND UPPER(PESERTA_API.Get_Kd_Band(a.COMPANY, a.N_I_K)) = UPPER('I')";
            } elseif ($param_kd_band == 2) { //II
                $where_query = $where_query." AND UPPER(PESERTA_API.Get_Kd_Band(a.COMPANY, a.N_I_K)) = UPPER('II')";
            } elseif ($param_kd_band == 3) { //III
                $where_query = $where_query." AND UPPER(PESERTA_API.Get_Kd_Band(a.COMPANY, a.N_I_K)) = UPPER('III')";
            } elseif ($param_kd_band == 4) { //IV
                $where_query = $where_query." AND UPPER(PESERTA_API.Get_Kd_Band(a.COMPANY, a.N_I_K)) = UPPER('IV')";
            } elseif ($param_kd_band == 5) { //V
                $where_query = $where_query." AND UPPER(PESERTA_API.Get_Kd_Band(a.COMPANY, a.N_I_K)) = UPPER('V')";
            } elseif ($param_kd_band == 6) { //VI
                $where_query = $where_query." AND UPPER(PESERTA_API.Get_Kd_Band(a.COMPANY, a.N_I_K)) = UPPER('VI')";
            } elseif ($param_kd_band == 7) { //VII
                $where_query = $where_query." AND UPPER(PESERTA_API.Get_Kd_Band(a.COMPANY, a.N_I_K)) = UPPER('VII')";
            } elseif ($param_kd_band == 8) { //NULL
                $where_query = $where_query." AND PESERTA_API.Get_Kd_Band(a.COMPANY, a.N_I_K) IS NULL";
            }
        }
        if ($param_pasangan_mp != '') {
            if ($param_pasangan_mp == 1) { //PMP
                $where_query = " AND PESERTA_API.Get_N_I_K_Pasangan(a.company, a.n_i_k) IS NOT NULL AND PESERTA_API.Get_N_I_K_Pasangan(a.company, a.n_i_k) IN (SELECT b.n_i_k FROM PENERIMAAN_MP b)";
            } elseif ($pasangan_mp == 2) { //MPS
                $where_query = " AND PESERTA_API.Get_N_I_K_Pasangan(a.company, a.n_i_k) IS NOT NULL AND PESERTA_API.Get_N_I_K_Pasangan(a.company, a.n_i_k) IN (SELECT b.n_i_k FROM PENERIMAAN_MP b) AND a.mps IS NOT NULL";
                /*$query2 = "SELECT COUNT(a.n_i_k) JMLH_PMP_PSNGN_MPS FROM PENERIMAAN_MP a WHERE a.kd_periode = '".$periode."' AND b.kd_jenis_proses = '1' AND PESERTA_API.Get_N_I_K_Pasangan(a.company, a.n_i_k) IS NOT NULL AND PESERTA_API.Get_N_I_K_Pasangan(a.company, a.n_i_k) IN (SELECT b.n_i_k FROM PENERIMAAN_MP b, PENERIMA_MP2 c WHERE b.n_i_k = c.n_i_k AND NVL(PERIODE_API.Get_Tanggal_Awal_Periode('DPT02', 'null'), SYSDATE) BETWEEN c.valid_from AND c.valid_to AND b.mps IS NOT NULL)";*/
            } elseif ($pasangan_mp == 3) { //AKTIF
                $where_query = " AND PESERTA_API.Get_N_I_K_Pasangan(a.company, a.n_i_k) IS NOT NULL AND PESERTA_API.Get_N_I_K_Pasangan(a.company, a.n_i_k) IN (SELECT a.n_i_k FROM PESERTA_TAB a WHERE a.status_peserta = 'AKTIF' AND a.n_i_k NOT IN (SELECT DISTINCT (b.n_i_k) FROM PENERIMAAN_MP b WHERE TO_NUMBER(b.kd_periode) <= TO_NUMBER('".$periode."')) AND b.kd_jenis_proses = '1' AND a.n_i_k NOT IN (SELECT DISTINCT (c.n_i_k) FROM HAPUS_PENERIMA_MP c WHERE TO_NUMBER(c.kd_periode) <= TO_NUMBER('".$periode."')))";
            } elseif ($pasangan_mp == 4) { //HAPUS
                $where_query = " AND valid_to AND PESERTA_API.Get_N_I_K_Pasangan(a.company, a.n_i_k) IS NOT NULL AND PESERTA_API.Get_N_I_K_Pasangan(a.company, a.n_i_k) IN (SELECT b.n_i_k FROM HAPUS_PENERIMA_MP b)";
            }
        }

        if (strtoupper($n_i_k) == 'TRUE') {
            if ($jns_api == 'jmlh') {
                if ($jmlh_aw != '') { //cek lagi
                    $query2 = "SELECT COUNT(DISTINCT(n_i_k)) JMLH_PMP FROM (SELECT b.n_i_k FROM PENERIMAAN_MP a, SUSUNAN_AHLI_WARIS2 b WHERE a.company = b.company AND a.n_i_k = b.n_i_k AND a.kd_periode = '".$periode."' AND a.kd_jenis_proses = '1' AND b.tgl_meninggal IS NULL AND b.tgl_nikah_lagi IS NULL AND b.tgl_cerai IS NULL AND ((b.jenis_ahli_waris = 'Anak' AND TRUNC((months_between(sysdate, b.tgl_lahir) / 12), 0) <= 25) OR b.jenis_ahli_waris = 'Istri' OR b.jenis_ahli_waris = 'Suami' OR b.jenis_ahli_waris = 'Peserta') $where_query GROUP BY b.n_i_k HAVING COUNT(b.no_ahli_waris) = $jmlh_aw)";
                }
                else {
                    $query2 = "SELECT COUNT(DISTINCT(a.n_i_k)) JMLH_PMP FROM PENERIMAAN_MP a WHERE a.kd_periode = '".$periode."' AND a.kd_jenis_proses = '1' $where_query";
                }
            }
            elseif ($jns_api == 'list') {
                $query2 = "SELECT a.n_i_k, a.no_ahli_waris, CALON_PENERIMA_MP_API.Get_Nama_Ahli_Waris(a.company,a.n_i_k,a.no_ahli_waris) nama, CALON_PENERIMA_MP_API.Get_Jenis_Ahli_Waris(a.company,a.n_i_k,a.no_ahli_waris) jenis_aw, a.kd_jenis_pensiun, JENIS_PENSIUN_API.Get_Nama_Jenis_Pensiun(a.company,a.kd_jenis_pensiun) jenis_pensiun, PESERTA_API.Get_Kd_Kelompok(a.company,a.n_i_k) kd_kelompok, KELOMPOK_PESERTA_API.Get_Nama_Kelompok(a.company,PESERTA_API.Get_Kd_Kelompok(a.company,a.n_i_k)) nama_kelompok, KELOMPOK_PESERTA_API.Get_Kelompok_Dapen(a.company,PESERTA_API.Get_Kd_Kelompok(a.company,a.n_i_k)) kelompok_dapen, CALON_PENERIMA_MP_API.Get_Jenis_Kelamin2(a.company,a.n_i_k,a.no_ahli_waris) jenis_kelamin, CALON_PENERIMA_MP_API.Get_Agama(a.company,a.n_i_k,a.no_ahli_waris) agama, CALON_PENERIMA_MP_API.Get_Gol_Darah(a.company,a.n_i_k,a.no_ahli_waris) gol_darah, CALON_PENERIMA_MP_API.Get_Tgl_Bekerja(a.company,a.n_i_k,a.no_ahli_waris) tgl_kerja, CALON_PENERIMA_MP_API.Get_Tgl_Lahir(a.company,a.n_i_k,a.no_ahli_waris) tgl_lahir, PESERTA_API.Get_Tgl_Capeg(a.company,a.n_i_k) tgl_capeg, PENERIMA_MP_API.GET_TANGGAL_PENSIUN(a.company,a.n_i_k) tgl_pensiun, PESERTA_API.Get_Tgl_Berhenti(a.company,a.n_i_k) tgl_berhenti, PESERTA_API.Get_Kd_Band(a.company,a.n_i_k) kd_band, PESERTA_API.Get_N_I_K_Pasangan(a.company,a.n_i_k) pasangan_pmp FROM PENERIMAAN_MP a WHERE a.kd_jenis_proses = '1' AND a.no_ahli_waris = 1 $where_query";
            }
        }
        elseif (strtoupper($n_i_k) == 'FALSE' || strtoupper($n_i_k) == '') {
            if ($jns_api == 'jmlh') {
                if ($jmlh_aw != '') {
                    $query2 = "SELECT COUNT(*) JMLH_PMP FROM (SELECT b.n_i_k FROM PENERIMAAN_MP a, SUSUNAN_AHLI_WARIS2 b WHERE a.company = b.company AND a.n_i_k = b.n_i_k AND a.kd_periode = '".$periode."' AND a.kd_jenis_proses = '1' AND b.tgl_meninggal IS NULL AND b.tgl_nikah_lagi IS NULL AND b.tgl_cerai IS NULL AND ((b.jenis_ahli_waris = 'Anak' AND TRUNC((months_between(sysdate, b.tgl_lahir) / 12), 0) <= 25) OR b.jenis_ahli_waris = 'Istri' OR b.jenis_ahli_waris = 'Suami' OR b.jenis_ahli_waris = 'Peserta') $where_query GROUP BY b.n_i_k HAVING COUNT(b.no_ahli_waris) = $jmlh_aw)";
                }
                else {
                    $query2 = "SELECT COUNT(a.n_i_k) JMLH_PMP FROM PENERIMAAN_MP a WHERE a.kd_periode = '".$periode."' AND a.kd_jenis_proses = '1' $where_query";
                }
            }
            elseif ($jns_api == 'list') {
                $query2 = "SELECT a.n_i_k, a.no_ahli_waris, CALON_PENERIMA_MP_API.Get_Nama_Ahli_Waris(a.company,a.n_i_k,a.no_ahli_waris) nama, CALON_PENERIMA_MP_API.Get_Jenis_Ahli_Waris(a.company, a.n_i_k, a.no_ahli_waris) jenis_aw, a.kd_jenis_pensiun, JENIS_PENSIUN_API.Get_Nama_Jenis_Pensiun(a.company,a.kd_jenis_pensiun) jenis_pensiun, PESERTA_API.Get_Kd_Kelompok(a.company,a.n_i_k) kd_kelompok, KELOMPOK_PESERTA_API.Get_Nama_Kelompok(a.company,PESERTA_API.Get_Kd_Kelompok(a.company,a.n_i_k)) nama_kelompok, KELOMPOK_PESERTA_API.Get_Kelompok_Dapen(a.company, PESERTA_API.Get_Kd_Kelompok(a.company,a.n_i_k))kelompok_dapen, CALON_PENERIMA_MP_API.Get_Jenis_Kelamin2(a.company,a.n_i_k,a.no_ahli_waris) jenis_kelamin, CALON_PENERIMA_MP_API.Get_Agama(a.company,a.n_i_k,a.no_ahli_waris) agama, CALON_PENERIMA_MP_API.Get_Gol_Darah(a.company,a.n_i_k,a.no_ahli_waris) gol_darah, CALON_PENERIMA_MP_API.Get_Tgl_Bekerja(a.company,a.n_i_k,a.no_ahli_waris) tgl_kerja, CALON_PENERIMA_MP_API.Get_Tgl_Lahir(a.company,a.n_i_k,a.no_ahli_waris) tgl_lahir, PESERTA_API.Get_Tgl_Capeg(a.company,a.n_i_k) tgl_capeg, PENERIMA_MP_API.GET_TANGGAL_PENSIUN(a.company,a.n_i_k) tgl_pensiun, PESERTA_API.Get_Tgl_Berhenti(a.company,a.n_i_k) tgl_berhenti, PESERTA_API.Get_Kd_Band(a.company,a.n_i_k) kd_band, PESERTA_API.Get_N_I_K_Pasangan(a.company,a.n_i_k) pasangan_pmp FROM PENERIMAAN_MP a WHERE a.kd_periode = '".$periode."' AND a.kd_jenis_proses = '1' $where_query";
            }
        }

        if ($jns_api == 'jmlh') {
            $sql2 = oci_parse($conn, $query2);
            oci_execute($sql2);
            $data2 = oci_fetch_array($sql2, OCI_ASSOC+OCI_RETURN_NULLS);
            $count2 = (int)$data2['JMLH_PMP'];

            return $count2;
        }
        elseif ($jns_api == 'list') {
            $sql2 = oci_parse($conn, $query2);
            oci_execute($sql2);
        
            $count2 = array();
            while ($dt = oci_fetch_assoc($sql2)) {
                $count2[] = $dt;
            }
            
            return $count2;
        }
}

function selectJmlhPstAll($param, $prm_type, $pst, $pst_type, $periode) { //exp: {PRA, PASCA, NULL}, KD_KLMPK, "... AND ..."
    $conn = getConnection();
    
    $array = array();
    $sum2 = 0;
    $pst_alias = strtoupper($pst_type);

    for ($i = 0; $i < count($param); $i++) {
        if ($param[$i] == "NULL") {
            $prm_pst = $pst." IS NULL";
            $prm_pst2 = $prm_pst;
            $prm_name = $pst_alias."_".$prm_type."_NULL";
        } else {
            $prm_pst = $pst." LIKE '%".$param[$i]."%'";
            $prm_pst2 = $pst." = '".$param[$i]."'";
            if ($param[$i] == "O-") {
                $prm_name = $pst_alias."_O_MIN";
            } else {
                $prm_name = $pst_alias."_".strtoupper($param[$i]);
            }

            if ($prm_type == "DAPEN") {
                $prm_name = $pst_alias."_".str_replace(" ", "_", $param[$i]);
            }
        }

        if ($prm_type == "KD_KLMPK") {
            if ($pst_type == "Aktif") {
                $query2 = "SELECT COUNT(a.n_i_k) JMLH_PST_$prm_name FROM PESERTA_AKTIF a WHERE a.status_peserta = '".$pst_type."' AND a.n_i_k NOT IN (SELECT DISTINCT(b.n_i_k) FROM PENERIMAAN_MP b WHERE TO_NUMBER(b.kd_periode) <= TO_NUMBER('".$periode."') AND b.kd_jenis_proses = '1') AND a.n_i_k NOT IN (SELECT DISTINCT(c.n_i_k) FROM HAPUS_PENERIMA_MP c WHERE TO_NUMBER(c.kd_periode) <= TO_NUMBER('".$periode."')) AND $prm_pst";
            } elseif ($pst_type == "Mantan") {
                $query2 = "SELECT COUNT(a.n_i_k) JMLH_PST_$prm_name FROM PENERIMA_MP2 a WHERE PERIODE_API.Get_Tanggal_Awal_Periode(a.company,'".$periode."') BETWEEN a.valid_from AND a.valid_to AND a.kd_jenis_pensiun = '99' AND $prm_pst";
            }
        } else {
            if ($pst_type == "Aktif") {
                $query2 = "SELECT COUNT(a.n_i_k) JMLH_PST_$prm_name FROM PESERTA_AKTIF a WHERE a.status_peserta = '".$pst_type."' AND a.n_i_k NOT IN (SELECT DISTINCT(b.n_i_k) FROM PENERIMAAN_MP b WHERE TO_NUMBER(b.kd_periode) <= TO_NUMBER('".$periode."') AND b.kd_jenis_proses = '1') AND a.n_i_k NOT IN (SELECT DISTINCT(c.n_i_k) FROM HAPUS_PENERIMA_MP c WHERE TO_NUMBER(c.kd_periode) <= TO_NUMBER('".$periode."')) AND $prm_pst2";
            } elseif ($pst_type == "Mantan") {
                $query2 = "SELECT COUNT(a.n_i_k) JMLH_PST_$prm_name FROM PENERIMA_MP2 a WHERE PERIODE_API.Get_Tanggal_Awal_Periode(a.company,'".$periode."') BETWEEN a.valid_from AND a.valid_to AND a.kd_jenis_pensiun = '99' AND $prm_pst2";
            }
        }
        
        $sql2 = oci_parse($conn, $query2);
        oci_execute($sql2);
        $data2 = oci_fetch_array($sql2, OCI_ASSOC+OCI_RETURN_NULLS);
        $count2 = (int)$data2['JMLH_PST_'.$prm_name];
        $sum2 += $count2;
        
        $array['JMLH_PST_'.$prm_name] = $count2;
    }
    $array['TOTAL_PST_'.$prm_type] = $sum2;

    return $array;
}

function jmlhPst($periode, $jns_api, $pst_type, $param_kd_kelompok, $param_dapen, $param_jenis_kelamin, $param_agama, $param_gol_darah) {
    $conn = getConnection();
    $pst_alias = strtolower($pst_type);
    $pst_upper = strtoupper($pst_type);
    $query_where = "";
    
    if ($param_kd_kelompok != '') {
        if ($param_kd_kelompok == 1) { //PRA
            $query_where = $query_where." AND UPPER(KELOMPOK_PESERTA_API.Get_Nama_Kelompok(a.company, PESERTA_API.Get_Kd_Kelompok(a.company, a.n_i_k))) LIKE UPPER('%PRA%')";
        } elseif ($param_kd_kelompok == 2) { //PASCA
            $query_where = $query_where." AND UPPER(KELOMPOK_PESERTA_API.Get_Nama_Kelompok(a.company, PESERTA_API.Get_Kd_Kelompok(a.company, a.n_i_k))) LIKE UPPER('%PASCA%')";
        } elseif ($param_kd_kelompok == 3) { //NULL
            $query_where = $query_where." AND KELOMPOK_PESERTA_API.Get_Nama_Kelompok(a.company, PESERTA_API.Get_Kd_Kelompok(a.company, a.n_i_k)) IS NULL";
        }
    }
    if ($param_dapen != '') {
        if ($param_dapen == 1) { //DAPEN I
            $query_where = $query_where." AND UPPER(KELOMPOK_PESERTA_API.Get_Kelompok_Dapen(a.company, PESERTA_API.Get_Kd_Kelompok(a.company, a.n_i_k))) = UPPER('DAPEN I')";
        } elseif ($param_dapen == 2) { //DAPEN II
            $query_where = $query_where." AND UPPER(KELOMPOK_PESERTA_API.Get_Kelompok_Dapen(a.company, PESERTA_API.Get_Kd_Kelompok(a.company, a.n_i_k))) = UPPER('DAPEN II')";
        } elseif ($param_dapen == 3) { //NULL
            $query_where = $query_where." AND KELOMPOK_PESERTA_API.Get_Kelompok_Dapen(a.company, PESERTA_API.Get_Kd_Kelompok(a.company, a.n_i_k)) IS NULL";
        }
    }
    /*
        if ($kd_klmpk != '') {
            $query_where = $query_where." AND a.kd_kelompok = '".$kd_klmpk."'";
        }
        if ($dapen != '') {
            $query_where = $query_where." AND KELOMPOK_PESERTA_API.Get_Kelompok_Dapen(a.company, a.kd_kelompok) = UPPER('".$dapen."')";
        }
        if ($jk != '') {
            $query_where = $query_where." AND CALON_PENERIMA_MP_API.Get_Jenis_Kelamin2(a.COMPANY, a.N_I_K, 1) = INITCAP('".$jk."')";
        }
        if ($agama != '') {
            $query_where = $query_where." AND CALON_PENERIMA_MP_API.Get_Agama(a.COMPANY, a.N_I_K, 1) = INITCAP('".$agama."')";
        }
        if ($goldar != '') {
            $query_where = $query_where." AND CALON_PENERIMA_MP_API.Get_Gol_Darah(a.COMPANY, a.N_I_K, 1) = UPPER('".$goldar."')";
        }
    */
    if ($param_jenis_kelamin != '') {
        if ($param_jenis_kelamin == 1) { //Pria
            $query_where = $query_where." AND INITCAP(CALON_PENERIMA_MP_API.Get_Jenis_Kelamin2(a.COMPANY, a.N_I_K, 1)) = INITCAP('Pria')";
        } elseif ($param_jenis_kelamin == 2) { //Wanita
            $query_where = $query_where." AND INITCAP(CALON_PENERIMA_MP_API.Get_Jenis_Kelamin2(a.COMPANY, a.N_I_K, 1)) = INITCAP('Wanita')";
        } elseif ($param_jenis_kelamin == 3) { //NULL
            $query_where = $query_where." AND CALON_PENERIMA_MP_API.Get_Jenis_Kelamin2(a.COMPANY, a.N_I_K, 1) IS NULL";
        }
    }
    if ($param_agama != '') {
        if ($param_agama == 1) { //Islam
            $query_where = $query_where." AND INITCAP(CALON_PENERIMA_MP_API.Get_Agama(a.COMPANY, a.N_I_K, 1)) = INITCAP('Islam')";
        } elseif ($param_agama == 2) { //Katolik
            $query_where = $query_where." AND INITCAP(CALON_PENERIMA_MP_API.Get_Agama(a.COMPANY, a.N_I_K, 1)) = INITCAP('Katolik')";
        } elseif ($param_agama == 3) { //Budha
            $query_where = $query_where." AND INITCAP(CALON_PENERIMA_MP_API.Get_Agama(a.COMPANY, a.N_I_K, 1)) = INITCAP('Budha')";
        } elseif ($param_agama == 4) { //Kristen
            $query_where = $query_where." AND INITCAP(CALON_PENERIMA_MP_API.Get_Agama(a.COMPANY, a.N_I_K, 1)) = INITCAP('Kristen')";
        } elseif ($param_agama == 5) { //Hindu
            $query_where = $query_where." AND INITCAP(CALON_PENERIMA_MP_API.Get_Agama(a.COMPANY, a.N_I_K, 1)) = INITCAP('Hindu')";
        } elseif ($param_agama == 6) { //NULL
            $query_where = $query_where." AND CALON_PENERIMA_MP_API.Get_Agama(a.COMPANY, a.N_I_K, 1) IS NULL";
        }
    }
    if ($param_gol_darah != '') {
        if ($param_gol_darah == 1) { //O
            $query_where = $query_where." AND UPPER(CALON_PENERIMA_MP_API.Get_Gol_Darah(a.COMPANY, a.N_I_K, 1)) = UPPER('O')";
        } elseif ($param_gol_darah == 2) { //A
            $query_where = $query_where." AND UPPER(CALON_PENERIMA_MP_API.Get_Gol_Darah(a.COMPANY, a.N_I_K, 1)) = UPPER('A')";
        } elseif ($param_gol_darah == 3) { //B
            $query_where = $query_where." AND UPPER(CALON_PENERIMA_MP_API.Get_Gol_Darah(a.COMPANY, a.N_I_K, 1)) = UPPER('B')";
        } elseif ($param_gol_darah == 4) { //AB
            $query_where = $query_where." AND UPPER(CALON_PENERIMA_MP_API.Get_Gol_Darah(a.COMPANY, a.N_I_K, 1)) = UPPER('AB')";
        } elseif ($param_gol_darah == 5) { //O-
            $query_where = $query_where." AND UPPER(CALON_PENERIMA_MP_API.Get_Gol_Darah(a.COMPANY, a.N_I_K, 1)) = UPPER('O-')";
        } elseif ($param_gol_darah == 6) { //A-
            $query_where = $query_where." AND UPPER(CALON_PENERIMA_MP_API.Get_Gol_Darah(a.COMPANY, a.N_I_K, 1)) = UPPER('A-')";
        } elseif ($param_gol_darah == 7) { //B-
            $query_where = $query_where." AND UPPER(CALON_PENERIMA_MP_API.Get_Gol_Darah(a.COMPANY, a.N_I_K, 1)) = UPPER('B-')";
        } elseif ($param_gol_darah == 8) { //AB-
            $query_where = $query_where." AND UPPER(CALON_PENERIMA_MP_API.Get_Gol_Darah(a.COMPANY, a.N_I_K, 1)) = UPPER('AB-')";
        } elseif ($param_gol_darah == 9) { //O+
            $query_where = $query_where." AND UPPER(CALON_PENERIMA_MP_API.Get_Gol_Darah(a.COMPANY, a.N_I_K, 1)) = UPPER('O+')";
        } elseif ($param_gol_darah == 10) { //A+
            $query_where = $query_where." AND UPPER(CALON_PENERIMA_MP_API.Get_Gol_Darah(a.COMPANY, a.N_I_K, 1)) = UPPER('A+')";
        } elseif ($param_gol_darah == 11) { //B+
            $query_where = $query_where." AND UPPER(CALON_PENERIMA_MP_API.Get_Gol_Darah(a.COMPANY, a.N_I_K, 1)) = UPPER('B+')";
        } elseif ($param_gol_darah == 12) { //AB+
            $query_where = $query_where." AND UPPER(CALON_PENERIMA_MP_API.Get_Gol_Darah(a.COMPANY, a.N_I_K, 1)) = UPPER('AB+')";
        } elseif ($param_gol_darah == 13) { //NULL
            $query_where = $query_where." AND CALON_PENERIMA_MP_API.Get_Gol_Darah(a.COMPANY, a.N_I_K, 1) IS NULL";
        }
    }

    if ($jns_api == "Jumlah") {
        if ($pst_type == "Aktif") {
            //$query = "SELECT SUM(a.jumlah_pst) JMLH_PST_AKTIF FROM PESERTA_AKTIF a WHERE a.kd_periode = '".$periode."' $query_where";
            $query = "SELECT COUNT(a.n_i_k) JMLH_PST_$pst_upper FROM PESERTA_AKTIF a WHERE a.status_peserta = '".$pst_type."' AND a.n_i_k NOT IN (SELECT DISTINCT (b.n_i_k) FROM PENERIMAAN_MP b WHERE TO_NUMBER(b.kd_periode) <= TO_NUMBER('".$periode."') AND b.kd_jenis_proses = '1') AND a.n_i_k NOT IN (SELECT DISTINCT (c.n_i_k) FROM HAPUS_PENERIMA_MP c WHERE TO_NUMBER(c.kd_periode) <= TO_NUMBER('".$periode."')) $query_where";
        } elseif ($pst_type == "Mantan") {
            $query = "SELECT COUNT(a.n_i_k) JMLH_PST_MANTAN FROM PENERIMA_MP2 a WHERE PERIODE_API.Get_Tanggal_Awal_Periode(a.company,'".$periode."') BETWEEN a.valid_from AND a.valid_to AND a.kd_jenis_pensiun = '99' $query_where";
        }

        $sql = oci_parse($conn, $query);
        oci_execute($sql);
        $data1 = oci_fetch_array($sql, OCI_ASSOC+OCI_RETURN_NULLS);
        $result = (int)$data1['JMLH_PST_'.strtoupper($pst_alias)];
        
    } elseif ($jns_api == "List") {
        if ($pst_type == "Aktif") {
            $query = "SELECT a.n_i_k, 1 no_ahli_waris, a.nama_pegawai nama, a.kd_kelompok, KELOMPOK_PESERTA_API.Get_Nama_Kelompok(a.company, a.kd_kelompok) nama_kelompok, CALON_PENERIMA_MP_API.Get_Jenis_Kelamin2(a.COMPANY, a.N_I_K, 1) jenis_kelamin, CALON_PENERIMA_MP_API.Get_Agama(a.COMPANY, a.N_I_K, 1) agama, CALON_PENERIMA_MP_API.Get_Gol_Darah(a.COMPANY, a.N_I_K, 1) gol_darah FROM PESERTA_AKTIF a WHERE a.status_peserta = INITCAP('".$pst_type."') AND a.n_i_k NOT IN (SELECT DISTINCT(b.n_i_k) FROM PENERIMAAN_MP b WHERE TO_NUMBER(b.kd_periode) <= TO_NUMBER('".$periode."')) AND a.n_i_k NOT IN (SELECT DISTINCT(c.n_i_k) FROM HAPUS_PENERIMA_MP c WHERE TO_NUMBER(c.kd_periode) <= TO_NUMBER('".$periode."')) $query_where";
        } elseif($pst_type == "Mantan") {
            $query = "SELECT a.n_i_k, 1 no_ahli_waris, PESERTA_API.Get_Nama_Pegawai(a.company,a.n_i_k) nama, PESERTA_API.Get_Kd_Kelompok(a.company,a.n_i_k) kd_kelompok, KELOMPOK_PESERTA_API.Get_Nama_Kelompok(a.company, PESERTA_API.Get_Kd_Kelompok(a.company,a.n_i_k)) nama_kelompok, KELOMPOK_PESERTA_API.Get_Kelompok_Dapen(a.company, PESERTA_API.Get_Kd_Kelompok(a.company,a.n_i_k)) kelompok_dapen, CALON_PENERIMA_MP_API.Get_Jenis_Kelamin2(a.COMPANY, a.N_I_K, 1) jenis_kelamin, CALON_PENERIMA_MP_API.Get_Agama(a.COMPANY, a.N_I_K, 1) agama, CALON_PENERIMA_MP_API.Get_Gol_Darah(a.COMPANY, a.N_I_K, 1) gol_darah FROM PENERIMA_MP2 a WHERE PERIODE_API.Get_Tanggal_Awal_Periode(a.company,'".$periode."') BETWEEN a.valid_from AND a.valid_to AND a.kd_jenis_pensiun = '99' $query_where";
        }

        $sql = oci_parse($conn, $query);
        oci_execute($sql);
        
        $result = array();
        while ($dt = oci_fetch_assoc($sql)) {
            $result[] = $dt;
        }
    }

    return $result;
}

function pmpTangguh($jns_api, $periode, $param_usia_tangguh, $param_tipe_tangguh) {
    //, $usia_tangguh, $saldo_awal, $penambahan, $sls_bln_brjln, $sls_periode_sblm, $total_penyelesaian, $saldo_akhir) {
    $conn = getConnection();
    $query_where = "";
    /*
        if ($periode != '') {
            $query_where = $query_where." AND a.periode_tangguh = '".$periode."'";
        }
        if ($usia_tangguh != '') {
            $query_where = $query_where." AND a.usia_tangguhan_daftar = '".$usia_tangguh."'";
        }
        if ($saldo_awal != '') {
            $query_where = $query_where." AND a.saldo_awal_tangguhan = '".$saldo_awal."'";
        }
        if ($penambahan != '') {
            $query_where = $query_where.""; // -- //
        }
        if ($sls_bln_brjln != '') {
            $query_where = $query_where.""; // -- //
        }
        if ($sls_periode_sblm != '') {
            $query_where = $query_where.""; // -- //
        }
        if ($total_penyelesaian != '') {
            $query_where = $query_where." AND a.dibayarkan = '".$total_penyelesaian."'"; // -- //
        }
        if ($saldo_akhir != '') {
            $query_where = $query_where." AND a.total_tangguhan = '".$saldo_akhir."'"; // -- //
        }
    */ //parameter
    if ($jns_api == "Jumlah") {
        $query = "SELECT '".$periode."' periode, SUM(saldo_awal) jmlh_pmp_awal, SUM(penambahan) penambahan, SUM(selesai_periode_sblm) selesai_periode_sblm, SUM(selesai_bln_berjalan) selesai_bln_berjalan, (SUM(selesai_periode_sblm)+SUM(selesai_bln_berjalan)) total_penyelesaian, (SUM(saldo_awal)+SUM(penambahan)-SUM(selesai_periode_sblm)-SUM(selesai_bln_berjalan)) jmlh_pmp_akhir FROM (
        /* SALDO-AWAL */
        SELECT COUNT(*) saldo_awal, 0 penambahan, 0 selesai_periode_sblm, 0 selesai_bln_berjalan FROM (SELECT a.n_i_k, a.no_ahli_waris FROM DAFTAR_PST_TANGGUH a, PENERIMAAN_MP b WHERE a.company = b.company AND a.n_i_k = b.n_i_k AND a.no_ahli_waris = b.no_ahli_waris AND a.periode_tangguh < '".$periode."' AND (a.periode_selesai IS NULL OR a.periode_selesai >= '".$periode."') AND b.kd_periode < '".$periode."' AND b.tangguh = 'TRUE' AND (a.state = 'Disetujui' OR a.state = 'Diproses') GROUP BY a.n_i_k, a.no_ahli_waris)
        /**/
        UNION ALL
        /* PENAMBAHAN */
        SELECT 0 saldo_awal, COUNT(*) penambahan, 0 selesai_periode_sblm, 0 selesai_bln_berjalan FROM (SELECT a.n_i_k, a.no_ahli_waris FROM DAFTAR_PST_TANGGUH a, PENERIMAAN_MP b WHERE a.company = b.company AND a.n_i_k = b.n_i_k AND a.no_ahli_waris = b.no_ahli_waris AND (a.state = 'Disetujui' OR a.state = 'Diproses') /*AND a.periode_tangguh = */ AND b.kd_periode = '".$periode."' AND b.tangguh = 'TRUE' AND (PENERIMAAN_MP_API.Get_Tangguh(a.company,a.n_i_k,a.no_ahli_waris,PERIODE_API.Get_Prev_Period(b.kd_periode),b.kd_jenis_proses) <> 'TRUE' OR PENERIMAAN_MP_API.Get_Tangguh(a.company,a.n_i_k,a.no_ahli_waris,PERIODE_API.Get_Prev_Period(b.kd_periode),b.kd_jenis_proses) IS NULL) GROUP BY a.n_i_k, a.no_ahli_waris HAVING MIN(b.kd_periode) = '".$periode."')
        /**/
        UNION ALL
        /* SELESAI_PERIODE_SBLM */
        SELECT 0 saldo_awal, 0 penambahan, COUNT(*) selesai_periode_sblm, 0 selesai_bln_berjalan FROM (SELECT a.n_i_k, a.no_ahli_waris FROM DAFTAR_PST_TANGGUH a, PENERIMAAN_MP b WHERE a.company = b.company AND a.n_i_k = b.n_i_k AND a.no_ahli_waris = b.no_ahli_waris /*AND a.periode_tangguh = */ AND a.periode_selesai = '".$periode."' AND b.kd_periode = PERIODE_API.Get_Prev_Period('".$periode."') AND b.tangguh = 'TRUE' AND (a.state = 'Disetujui' OR a.state = 'Diproses') AND (PENERIMAAN_MP_API.Get_Tangguh(a.company,a.n_i_k,a.no_ahli_waris,PERIODE_API.Get_Prev_Period('".$periode."'),b.kd_jenis_proses) = 'TRUE' OR PENERIMAAN_MP_API.Get_Tangguh(a.company,a.n_i_k,a.no_ahli_waris,PERIODE_API.Get_Prev_Period('".$periode."'),b.kd_jenis_proses) IS NOT NULL) AND b.kd_jenis_proses = '1' AND a.dibayarkan > 0 AND (a.dihapuskan = 0 OR a.dihapuskan IS NULL) GROUP BY a.n_i_k, a.no_ahli_waris)
        /**/
        UNION ALL
        /* SELESAI_BLN_BERJALAN */
        SELECT 0 saldo_awal, 0 penambahan, 0 selesai_periode_sblm, COUNT(*) selesai_bln_berjalan FROM (SELECT a.n_i_k, a.no_ahli_waris FROM DAFTAR_PST_TANGGUH a, PENERIMAAN_MP b WHERE a.company = b.company AND a.n_i_k = b.n_i_k AND a.no_ahli_waris = b.no_ahli_waris AND (a.state = 'Disetujui' OR a.state = 'Diproses') /*AND a.periode_tangguh = */ AND b.kd_periode = '".$periode."' AND b.tangguh = 'TRUE' AND a.periode_selesai = '".$periode."' AND (PENERIMAAN_MP_API.Get_Tangguh(a.company,a.n_i_k,a.no_ahli_waris,PERIODE_API.Get_Prev_Period(b.kd_periode),b.kd_jenis_proses) <> 'TRUE' OR PENERIMAAN_MP_API.Get_Tangguh(a.company,a.n_i_k,a.no_ahli_waris,PERIODE_API.Get_Prev_Period(b.kd_periode),b.kd_jenis_proses) IS NULL) AND b.kd_jenis_proses = '1' AND a.dibayarkan > 0 GROUP BY a.n_i_k, a.no_ahli_waris HAVING MIN(b.kd_periode) = '".$periode."'))
        /**/";

        $sql = oci_parse($conn, $query);
        oci_execute($sql);
        
        $rows = array();
        while ($dt = oci_fetch_assoc($sql)) {
            $result[] = $dt;
        }

    } elseif ($jns_api == "List") {
        $result = array();

        /*
            $query = "SELECT a.n_i_k, a.no_ahli_waris, CALON_PENERIMA_MP_API.Get_Nama_Ahli_Waris(a.company,a.n_i_k,a.no_ahli_waris) nama,  REKENING_PENERIMA_MP_API.Get_Kd_Bank_By_Param(a.company,a.n_i_k,a.no_ahli_waris,SYSDATE) kd_bank, PAYMENT_INSTITUTE_API.Get_Description(a.company,REKENING_PENERIMA_MP_API.Get_Kd_Bank_By_Param(a.company,a.n_i_k,a.no_ahli_waris,SYSDATE)) nama_bank, REKENING_PENERIMA_MP_API.Get_Kd_Cab_By_Param(a.company,a.n_i_k,a.no_ahli_waris,SYSDATE) kd_cabang, PAYMENT_INSTITUTE_OFFICE_API.Get_Description(a.company,REKENING_PENERIMA_MP_API.Get_Kd_Bank_By_Param(a.company,a.n_i_k,a.no_ahli_waris,SYSDATE),REKENING_PENERIMA_MP_API.Get_Kd_Cab_By_Param(a.company,a.n_i_k,a.no_ahli_waris,SYSDATE)) nama_cabang, REKENING_PENERIMA_MP_API.Get_No_Rek_By_Param(a.company,a.n_i_k,a.no_ahli_waris,SYSDATE) no_rekening_bank, REKENING_PENERIMA_MP_API.Get_Nama_Rek_By_Param(a.company,a.n_i_k,a.no_ahli_waris,SYSDATE) nama_pemilik_rekening, '' no_sppb FROM  DAFTAR_PST_TANGGUH a WHERE (a.state = 'Disetujui' OR a.state = 'Diproses') AND a.periode_selesai IS NULL $query_where";
        */ // unused query

        if ($param_tipe_tangguh >= 1 && $param_tipe_tangguh <= 6) {
            if ($param_tipe_tangguh == 1) { //LIST PMP AWAL
                $query = "SELECT n_i_k, no_ahli_waris, CALON_PENERIMA_MP_API.Get_Nama_Ahli_Waris(company,n_i_k,no_ahli_waris) nama, REKENING_PENERIMA_MP_API.Get_Kd_Bank_By_Param(company,n_i_k,no_ahli_waris,PERIODE_API.Get_Tanggal_Akhir(company,'".$periode."')) kd_bank, PAYMENT_INSTITUTE_API.Get_Description(company,REKENING_PENERIMA_MP_API.Get_Kd_Bank_By_Param(company,n_i_k,no_ahli_waris,PERIODE_API.Get_Tanggal_Akhir(company,'".$periode."'))) nama_bank, REKENING_PENERIMA_MP_API.Get_Kd_Cab_By_Param(company,n_i_k,no_ahli_waris,PERIODE_API.Get_Tanggal_Akhir(company,'".$periode."')) kd_cabang, PAYMENT_INSTITUTE_OFFICE_API.Get_Description(company,REKENING_PENERIMA_MP_API.Get_Kd_Bank_By_Param(company,n_i_k,no_ahli_waris,PERIODE_API.Get_Tanggal_Akhir(company,'".$periode."')),REKENING_PENERIMA_MP_API.Get_Kd_Cab_By_Param(company,n_i_k,no_ahli_waris,PERIODE_API.Get_Tanggal_Akhir(company,'".$periode."'))) nama_cabang, REKENING_PENERIMA_MP_API.Get_No_Rek_By_Param(company,n_i_k,no_ahli_waris,PERIODE_API.Get_Tanggal_Akhir(company,'".$periode."')) no_rekening_bank, REKENING_PENERIMA_MP_API.Get_Nama_Rek_By_Param(company,n_i_k,no_ahli_waris,PERIODE_API.Get_Tanggal_Akhir(company,'".$periode."'))  nama_pemilik_rekening FROM (SELECT a.n_i_k, a.no_ahli_waris, a.company FROM DAFTAR_PST_TANGGUH a, PENERIMAAN_MP b WHERE a.company = b.company AND a.n_i_k = b.n_i_k AND a.no_ahli_waris = b.no_ahli_waris AND a.periode_tangguh < '".$periode."' AND (a.periode_selesai IS NULL OR a.periode_selesai >= '".$periode."') AND b.kd_periode < '".$periode."' AND b.tangguh = 'TRUE' AND b.kd_jenis_proses = 1 AND (a.state = 'Disetujui' OR a.state = 'Diproses') GROUP BY a.n_i_k, a.no_ahli_waris, a.company)";

                $sql = oci_parse($conn, $query);
                oci_execute($sql);
                
                $rows = array();
                while ($dt = oci_fetch_assoc($sql)) {
                    $result[] = $dt;
                }
            }
            if ($param_tipe_tangguh == 2) {//PENAMBAHAN
                $query = "SELECT n_i_k, no_ahli_waris, CALON_PENERIMA_MP_API.Get_Nama_Ahli_Waris(company,n_i_k,no_ahli_waris) nama, REKENING_PENERIMA_MP_API.Get_Kd_Bank_By_Param(company,n_i_k,no_ahli_waris,PERIODE_API.Get_Tanggal_Akhir(company,'".$periode."')) kd_bank, PAYMENT_INSTITUTE_API.Get_Description(company,REKENING_PENERIMA_MP_API.Get_Kd_Bank_By_Param(company,n_i_k,no_ahli_waris,PERIODE_API.Get_Tanggal_Akhir(company,'".$periode."'))) nama_bank, REKENING_PENERIMA_MP_API.Get_Kd_Cab_By_Param(company,n_i_k,no_ahli_waris,PERIODE_API.Get_Tanggal_Akhir(company,'".$periode."')) kd_cabang, PAYMENT_INSTITUTE_OFFICE_API.Get_Description(company,REKENING_PENERIMA_MP_API.Get_Kd_Bank_By_Param(company,n_i_k,no_ahli_waris,PERIODE_API.Get_Tanggal_Akhir(company,'".$periode."')),REKENING_PENERIMA_MP_API.Get_Kd_Cab_By_Param(company,n_i_k,no_ahli_waris,PERIODE_API.Get_Tanggal_Akhir(company,'".$periode."'))) nama_cabang, REKENING_PENERIMA_MP_API.Get_No_Rek_By_Param(company,n_i_k,no_ahli_waris,PERIODE_API.Get_Tanggal_Akhir(company,'".$periode."')) no_rekening_bank, REKENING_PENERIMA_MP_API.Get_Nama_Rek_By_Param(company,n_i_k,no_ahli_waris,PERIODE_API.Get_Tanggal_Akhir(company,'".$periode."')) nama_pemilik_rekening FROM (SELECT a.n_i_k, a.no_ahli_waris, a.company FROM DAFTAR_PST_TANGGUH a, PENERIMAAN_MP b WHERE a.company = b.company AND a.n_i_k = b.n_i_k AND a.no_ahli_waris = b.no_ahli_waris AND (a.state = 'Disetujui' OR a.state = 'Diproses') AND a.periode_tangguh = '".$periode."' AND b.kd_periode = '".$periode."' AND b.tangguh = 'TRUE' GROUP BY a.n_i_k, a.no_ahli_waris, a.company HAVING MIN(b.kd_periode) = '".$periode."')";

                $sql = oci_parse($conn, $query);
                oci_execute($sql);
                
                $rows = array();
                while ($dt = oci_fetch_assoc($sql)) {
                    $result[] = $dt;
                }
            }
            if ($param_tipe_tangguh == 3 || $param_tipe_tangguh == 5) {//SELESAI_PERIODE_SBLM
                $query = "SELECT n_i_k, no_ahli_waris, CALON_PENERIMA_MP_API.Get_Nama_Ahli_Waris(company,n_i_k,no_ahli_waris) nama, REKENING_PENERIMA_MP_API.Get_Kd_Bank_By_Param(company,n_i_k,no_ahli_waris,PERIODE_API.Get_Tanggal_Akhir(company,'".$periode."')) kd_bank, PAYMENT_INSTITUTE_API.Get_Description(company,REKENING_PENERIMA_MP_API.Get_Kd_Bank_By_Param(company,n_i_k,no_ahli_waris,PERIODE_API.Get_Tanggal_Akhir(company,'".$periode."'))) nama_bank, REKENING_PENERIMA_MP_API.Get_Kd_Cab_By_Param(company,n_i_k,no_ahli_waris,PERIODE_API.Get_Tanggal_Akhir(company,'".$periode."')) kd_cabang, PAYMENT_INSTITUTE_OFFICE_API.Get_Description(company,REKENING_PENERIMA_MP_API.Get_Kd_Bank_By_Param(company,n_i_k,no_ahli_waris,PERIODE_API.Get_Tanggal_Akhir(company,'".$periode."')),REKENING_PENERIMA_MP_API.Get_Kd_Cab_By_Param(company,n_i_k,no_ahli_waris,PERIODE_API.Get_Tanggal_Akhir(company,'".$periode."'))) nama_cabang, REKENING_PENERIMA_MP_API.Get_No_Rek_By_Param(company,n_i_k,no_ahli_waris,PERIODE_API.Get_Tanggal_Akhir(company,'".$periode."')) no_rekening_bank, REKENING_PENERIMA_MP_API.Get_Nama_Rek_By_Param(company,n_i_k,no_ahli_waris,PERIODE_API.Get_Tanggal_Akhir(company,'".$periode."')) nama_pemilik_rekening FROM (SELECT a.n_i_k, a.no_ahli_waris, a.company FROM DAFTAR_PST_TANGGUH a, PENERIMAAN_MP b WHERE a.company = b.company AND a.n_i_k = b.n_i_k AND a.no_ahli_waris = b.no_ahli_waris /*AND a.periode_tangguh = */ AND a.periode_selesai = '".$periode."' AND b.kd_periode < '".$periode."' AND b.tangguh = 'TRUE' AND (a.state = 'Disetujui' OR a.state = 'Diproses') AND (PENERIMAAN_MP_API.Get_Tangguh(a.company,a.n_i_k,a.no_ahli_waris,PERIODE_API.Get_Prev_Period('".$periode."'),b.kd_jenis_proses) = 'TRUE' OR PENERIMAAN_MP_API.Get_Tangguh(a.company,a.n_i_k,a.no_ahli_waris,PERIODE_API.Get_Prev_Period('".$periode."'),b.kd_jenis_proses) IS NOT NULL) AND b.kd_jenis_proses = '1' AND a.dibayarkan > 0 GROUP BY a.n_i_k, a.no_ahli_waris, a.company)";

                $sql = oci_parse($conn, $query);
                oci_execute($sql);
                
                $rows = array();
                while ($dt = oci_fetch_assoc($sql)) {
                    $result[] = $dt;
                }
            }
            if ($param_tipe_tangguh == 4 || $param_tipe_tangguh == 5) {//SELESAI_BLN_BERJALAN
                $query = "SELECT n_i_k, no_ahli_waris, CALON_PENERIMA_MP_API.Get_Nama_Ahli_Waris(company,n_i_k,no_ahli_waris) nama, REKENING_PENERIMA_MP_API.Get_Kd_Bank_By_Param(company,n_i_k,no_ahli_waris,PERIODE_API.Get_Tanggal_Akhir(company,'".$periode."')) kd_bank, PAYMENT_INSTITUTE_API.Get_Description(company,REKENING_PENERIMA_MP_API.Get_Kd_Bank_By_Param(company,n_i_k,no_ahli_waris,PERIODE_API.Get_Tanggal_Akhir(company,'".$periode."'))) nama_bank, REKENING_PENERIMA_MP_API.Get_Kd_Cab_By_Param(company,n_i_k,no_ahli_waris,PERIODE_API.Get_Tanggal_Akhir(company,'".$periode."')) kd_cabang, PAYMENT_INSTITUTE_OFFICE_API.Get_Description(company,REKENING_PENERIMA_MP_API.Get_Kd_Bank_By_Param(company,n_i_k,no_ahli_waris,PERIODE_API.Get_Tanggal_Akhir(company,'".$periode."')),REKENING_PENERIMA_MP_API.Get_Kd_Cab_By_Param(company,n_i_k,no_ahli_waris,PERIODE_API.Get_Tanggal_Akhir(company,'".$periode."'))) nama_cabang, REKENING_PENERIMA_MP_API.Get_No_Rek_By_Param(company,n_i_k,no_ahli_waris,PERIODE_API.Get_Tanggal_Akhir(company,'".$periode."')) no_rekening_bank, REKENING_PENERIMA_MP_API.Get_Nama_Rek_By_Param(company,n_i_k,no_ahli_waris,PERIODE_API.Get_Tanggal_Akhir(company,'".$periode."')) nama_pemilik_rekening FROM (SELECT a.n_i_k, a.no_ahli_waris, a.company FROM DAFTAR_PST_TANGGUH a, PENERIMAAN_MP b WHERE a.company = b.company AND a.n_i_k = b.n_i_k AND a.no_ahli_waris = b.no_ahli_waris AND (a.state = 'Disetujui' OR a.state = 'Diproses') /*AND a.periode_tangguh = */ AND b.kd_periode = '".$periode."' AND b.tangguh = 'TRUE' AND a.periode_selesai = '".$periode."' AND (PENERIMAAN_MP_API.Get_Tangguh(a.company,a.n_i_k,a.no_ahli_waris,PERIODE_API.Get_Prev_Period(b.kd_periode),b.kd_jenis_proses) <> 'TRUE' OR PENERIMAAN_MP_API.Get_Tangguh(a.company,a.n_i_k,a.no_ahli_waris,PERIODE_API.Get_Prev_Period(b.kd_periode),b.kd_jenis_proses) IS NULL) GROUP BY a.n_i_k, a.no_ahli_waris, a.company HAVING MIN(b.kd_periode) = '".$periode."')";

                $sql = oci_parse($conn, $query);
                oci_execute($sql);
                
                $rows = array();
                while ($dt = oci_fetch_assoc($sql)) {
                    $result[] = $dt;
                }
            }
            if ($param_tipe_tangguh == 6) { //LIST PMP AKHIR
                $query = "SELECT n_i_k, no_ahli_waris, CALON_PENERIMA_MP_API.Get_Nama_Ahli_Waris(company,n_i_k,no_ahli_waris) nama, REKENING_PENERIMA_MP_API.Get_Kd_Bank_By_Param(company,n_i_k,no_ahli_waris,PERIODE_API.Get_Tanggal_Akhir(company,'".$periode."')) kd_bank, PAYMENT_INSTITUTE_API.Get_Description(company,REKENING_PENERIMA_MP_API.Get_Kd_Bank_By_Param(company,n_i_k,no_ahli_waris,PERIODE_API.Get_Tanggal_Akhir(company,'".$periode."'))) nama_bank, REKENING_PENERIMA_MP_API.Get_Kd_Cab_By_Param(company,n_i_k,no_ahli_waris,PERIODE_API.Get_Tanggal_Akhir(company,'".$periode."')) kd_cabang, PAYMENT_INSTITUTE_OFFICE_API.Get_Description(company,REKENING_PENERIMA_MP_API.Get_Kd_Bank_By_Param(company,n_i_k,no_ahli_waris,PERIODE_API.Get_Tanggal_Akhir(company,'".$periode."')),REKENING_PENERIMA_MP_API.Get_Kd_Cab_By_Param(company,n_i_k,no_ahli_waris,PERIODE_API.Get_Tanggal_Akhir(company,'".$periode."'))) nama_cabang, REKENING_PENERIMA_MP_API.Get_No_Rek_By_Param(company,n_i_k,no_ahli_waris,PERIODE_API.Get_Tanggal_Akhir(company,'".$periode."')) no_rekening_bank, REKENING_PENERIMA_MP_API.Get_Nama_Rek_By_Param(company,n_i_k,no_ahli_waris,PERIODE_API.Get_Tanggal_Akhir(company,'".$periode."'))  nama_pemilik_rekening FROM ((SELECT a.n_i_k, a.no_ahli_waris, a.company FROM DAFTAR_PST_TANGGUH a, PENERIMAAN_MP b WHERE a.company = b.company AND a.n_i_k = b.n_i_k AND a.no_ahli_waris = b.no_ahli_waris AND a.periode_tangguh < '".$periode."' AND (a.periode_selesai IS NULL OR a.periode_selesai >= '".$periode."') AND b.kd_periode < '".$periode."' AND b.tangguh = 'TRUE' AND b.kd_jenis_proses = 1 AND (a.state = 'Disetujui' OR a.state = 'Diproses') GROUP BY a.n_i_k, a.no_ahli_waris, a.company
                UNION
                SELECT a.n_i_k, a.no_ahli_waris, a.company FROM DAFTAR_PST_TANGGUH a, PENERIMAAN_MP b WHERE a.company = b.company AND a.n_i_k = b.n_i_k AND a.no_ahli_waris = b.no_ahli_waris AND (a.state = 'Disetujui' OR a.state = 'Diproses') AND a.periode_tangguh = '".$periode."' AND b.kd_periode = '".$periode."' AND b.tangguh = 'TRUE' GROUP BY a.n_i_k, a.no_ahli_waris, a.company HAVING MIN(b.kd_periode) = '".$periode."')
                MINUS
                (SELECT a.n_i_k, a.no_ahli_waris, a.company FROM DAFTAR_PST_TANGGUH a, PENERIMAAN_MP b WHERE a.company = b.company AND a.n_i_k = b.n_i_k AND a.no_ahli_waris = b.no_ahli_waris AND a.periode_tangguh < '".$periode."' AND a.periode_selesai = '".$periode."' AND b.kd_periode < '".$periode."' AND b.tangguh = 'TRUE' AND (a.state = 'Disetujui' OR a.state = 'Diproses') GROUP BY a.n_i_k, a.no_ahli_waris, a.company
                UNION
                SELECT a.n_i_k, a.no_ahli_waris, a.company FROM DAFTAR_PST_TANGGUH a, PENERIMAAN_MP b WHERE a.company = b.company AND a.n_i_k = b.n_i_k AND a.no_ahli_waris = b.no_ahli_waris AND (a.state = 'Disetujui' OR a.state = 'Diproses') AND a.periode_tangguh = '".$periode."' AND b.kd_periode = '".$periode."' AND b.tangguh = 'TRUE' AND a.periode_selesai = '".$periode."' GROUP BY a.n_i_k, a.no_ahli_waris, a.company HAVING MIN(b.kd_periode) = '".$periode."')
                )";

                $sql = oci_parse($conn, $query);
                oci_execute($sql);
                
                $rows = array();
                while ($dt = oci_fetch_assoc($sql)) {
                    $result[] = $dt;
                }
            }
        }
    }

    return $result;
}

function jmlhPmpP2tel($periode, $cab_p2tel) {
    $conn = getConnection();
    $prm_where = "";
    $rows = array();
    
    if ($cab_p2tel != '') {
        $prm_where = $prm_where." AND ALAMAT_PENERIMA_MP_API.Get_P2TEL_default(a.company,a.n_i_k,a.no_ahli_waris,NVL(PERIODE_API.Get_Tanggal_Awal_Periode(a.company,'".$periode."'),SYSDATE)) = '".$cab_p2tel."'";
    }

    $query1 = "SELECT ALAMAT_PENERIMA_MP_API.Get_P2TEL_default(a.company,a.n_i_k,a.no_ahli_waris,NVL(PERIODE_API.Get_Tanggal_Awal_Periode(a.company,'".$periode."'),SYSDATE)) KD_CABANG, MITRA_KERJA_API.Get_Nama_Mitra_Kerja(a.company,ALAMAT_PENERIMA_MP_API.Get_P2TEL_default(a.company,a.n_i_k,a.no_ahli_waris,NVL(PERIODE_API.Get_Tanggal_Awal_Periode(a.company,'".$periode."'),SYSDATE))) NAMA_CABANG, COUNT(a.n_i_k) jmlh_pmp FROM PENERIMAAN_MP a WHERE a.kd_periode = '".$periode."' $prm_where GROUP BY ALAMAT_PENERIMA_MP_API.Get_P2TEL_default(a.company,a.n_i_k,a.no_ahli_waris,NVL(PERIODE_API.Get_Tanggal_Awal_Periode(a.company,'".$periode."'),SYSDATE)), MITRA_KERJA_API.Get_Nama_Mitra_Kerja(a.company,ALAMAT_PENERIMA_MP_API.Get_P2TEL_default(a.company,a.n_i_k,a.no_ahli_waris,NVL(PERIODE_API.Get_Tanggal_Awal_Periode(a.company,'".$periode."'),SYSDATE))) ORDER BY ALAMAT_PENERIMA_MP_API.Get_P2TEL_default(a.company,a.n_i_k,a.no_ahli_waris,NVL(PERIODE_API.Get_Tanggal_Awal_Periode(a.company,'".$periode."'),SYSDATE))";

    $sql1 = oci_parse($conn, $query1);
    oci_execute($sql1);

    $rows = array();
    while ($dt = oci_fetch_assoc($sql1)) {
        $rows[] = $dt;
    }

    return $rows;
}

function jmlhPengurusP2tel($periode, $kd_cabang) {
    $conn = getConnection();
    
    $cabang_query = "";
    $total_count = "";

    if ($kd_cabang != '') {
        $cabang_query = $cabang_query." AND a.kd_mitra_kerja = UPPER('".$kd_cabang."')";
    }

    $ketua = "SELECT COUNT(a.n_i_k) KETUA FROM KONTAK_MITRA_KERJA a WHERE NVL(PERIODE_API.Get_Tanggal_Awal_Periode('DPT02', '".$periode."'), SYSDATE) BETWEEN a.valid_from AND a.valid_to $cabang_query AND UPPER(a.jabatan) LIKE 'KETUA%'";
    
    $ketua_sql = oci_parse($conn, $ketua);
    oci_execute($ketua_sql);
    $ketua_data = oci_fetch_array($ketua_sql, OCI_ASSOC+OCI_RETURN_NULLS);
    $ketua_count = (int)$ketua_data['KETUA'];

    $wakil_ketua = "SELECT COUNT(a.n_i_k) WAKIL_KETUA FROM KONTAK_MITRA_KERJA a WHERE NVL(PERIODE_API.Get_Tanggal_Awal_Periode('DPT02', '".$periode."'), SYSDATE) BETWEEN a.valid_from AND a.valid_to $cabang_query AND UPPER(a.jabatan) LIKE 'WAKIL KETUA%'";
    
    $wakil_ketua_sql = oci_parse($conn, $wakil_ketua);
    oci_execute($wakil_ketua_sql);
    $wakil_ketua_data = oci_fetch_array($wakil_ketua_sql, OCI_ASSOC+OCI_RETURN_NULLS);
    $wakil_ketua_count = (int)$wakil_ketua_data['WAKIL_KETUA'];

    $bendahara = "SELECT COUNT(a.n_i_k) BENDAHARA FROM KONTAK_MITRA_KERJA a WHERE NVL(PERIODE_API.Get_Tanggal_Awal_Periode('DPT02', '".$periode."'), SYSDATE) BETWEEN a.valid_from AND a.valid_to $cabang_query AND UPPER(a.jabatan) LIKE 'BENDAHARA%'";
    
    $bendahara_sql = oci_parse($conn, $bendahara);
    oci_execute($bendahara_sql);
    $bendahara_data = oci_fetch_array($bendahara_sql, OCI_ASSOC+OCI_RETURN_NULLS);
    $bendahara_count = (int)$bendahara_data['BENDAHARA'];

    $sekretaris = "SELECT COUNT(a.n_i_k) SEKRETARIS FROM KONTAK_MITRA_KERJA a WHERE NVL(PERIODE_API.Get_Tanggal_Awal_Periode('DPT02', '".$periode."'), SYSDATE) BETWEEN a.valid_from AND a.valid_to $cabang_query AND UPPER(a.jabatan) LIKE 'SEKRETARIS%'";
    
    $sekretaris_sql = oci_parse($conn, $sekretaris);
    oci_execute($sekretaris_sql);
    $sekretaris_data = oci_fetch_array($sekretaris_sql, OCI_ASSOC+OCI_RETURN_NULLS);
    $sekretaris_count = (int)$sekretaris_data['SEKRETARIS'];

    $bdn_pengawas = "SELECT COUNT(a.n_i_k) BADAN_PENGAWAS FROM KONTAK_MITRA_KERJA a WHERE NVL(PERIODE_API.Get_Tanggal_Awal_Periode('DPT02', '".$periode."'), SYSDATE) BETWEEN a.valid_from AND a.valid_to $cabang_query AND UPPER(a.jabatan) LIKE 'BADAN PENGAWAS%'";
    
    $bdn_pengawas_sql = oci_parse($conn, $bdn_pengawas);
    oci_execute($bdn_pengawas_sql);
    $bdn_pengawas_data = oci_fetch_array($bdn_pengawas_sql, OCI_ASSOC+OCI_RETURN_NULLS);
    $bdn_pengawas_count = (int)$bdn_pengawas_data['BADAN_PENGAWAS'];
    
    $komisariat = "SELECT COUNT(a.n_i_k) KOMISARIAT FROM KONTAK_MITRA_KERJA a WHERE NVL(PERIODE_API.Get_Tanggal_Awal_Periode('DPT02', '".$periode."'), SYSDATE) BETWEEN a.valid_from AND a.valid_to $cabang_query AND UPPER(a.jabatan) LIKE 'KOMISARIAT%'";
    
    $komisariat_sql = oci_parse($conn, $komisariat);
    oci_execute($komisariat_sql);
    $komisariat_data = oci_fetch_array($komisariat_sql, OCI_ASSOC+OCI_RETURN_NULLS);
    $komisariat_count = (int)$komisariat_data['KOMISARIAT'];

    $other = "SELECT COUNT(a.n_i_k) OTHER FROM KONTAK_MITRA_KERJA a WHERE NVL(PERIODE_API.Get_Tanggal_Awal_Periode('DPT02', '".$periode."'), SYSDATE) BETWEEN a.valid_from AND a.valid_to $cabang_query AND (UPPER(a.jabatan) NOT LIKE 'KETUA%' OR UPPER(a.jabatan) NOT LIKE 'WAKIL KETUA%' OR UPPER(a.jabatan) NOT LIKE 'BENDAHARA%' OR UPPER(a.jabatan) NOT LIKE 'SEKRETARIS%' OR UPPER(a.jabatan) NOT LIKE 'BADAN PENGAWAS%' OR UPPER(a.jabatan) NOT LIKE 'KOMISARIAT%')";
    
    $other_sql = oci_parse($conn, $other);
    oci_execute($other_sql);
    $other_data = oci_fetch_array($other_sql, OCI_ASSOC+OCI_RETURN_NULLS);
    $other_count = (int)$other_data['OTHER'];

    $total_count = $ketua_count + $wakil_ketua_count + $bendahara_count + $sekretaris_count + $bdn_pengawas_count + $komisariat_count + $other_count;
    
    $query1 = "SELECT '".$periode."' periode, a.kd_mitra_kerja kd_cabang, MITRA_KERJA_API.Get_Nama_Mitra_Kerja(a.company, a.kd_mitra_kerja) nama_cabang, $ketua_count ketua, $wakil_ketua_count wakil_ketua, $bendahara_count bendahara, $sekretaris_count sekretaris, $bdn_pengawas_count badan_pengawas, $komisariat_count komisariat, $other_count other, $total_count total_pengurus FROM KONTAK_MITRA_KERJA a WHERE NVL(PERIODE_API.Get_Tanggal_Awal_Periode('DPT02', '".$periode."'), SYSDATE) BETWEEN a.valid_from AND a.valid_to $cabang_query";
    
    $sql1 = oci_parse($conn, $query1);
    oci_execute($sql1);
    
    $rows = array();
    while ($dt = oci_fetch_assoc($sql1)) {
        $rows[] = $dt;
    }

    return $rows;
}

function jmlhPmpMeninggal($periode, $periode_trnsksi, $periode_setuju, $range_tgl_trans, $range_tgl_setuju, $jenis_trnsksi, $cab_p2tel, $sk_non_sk, $pmp_non_pmp, $jenis_aw, $jns_api) {
    $conn = getConnection();
    $prm_where = "";
    $prm_where_sk_non_sk = "";

        $conn = getConnection();
        
        if ($periode != '') {
            $prm_where = $prm_where." AND a.kd_periode = '".$periode."'";
        }
        if ($periode_trnsksi != '') {
            $prm_where = $prm_where." AND (PERIODE_API.Get_Periode(a.company,a.valid_from) = '".$periode_trnsksi."')";
        }
        if ($periode_setuju != '') {
            $prm_where = $prm_where." AND INITCAP(a.state) = 'Disetujui' AND PERIODE_API.Get_Kd_Periode(a.company,TO_CHAR(DPT_AUDITTRAIL_API.Get_Tgl('HapusPenerimaMp',a.objid,a.state),'YYYY'),TO_CHAR(DPT_AUDITTRAIL_API.Get_Tgl('HapusPenerimaMp',a.objid,a.state),'MM')) = '".$periode_setuju."'";
        }
        if ($cab_p2tel != '') {
            $prm_where = $prm_where." AND UPPER(KELURAHAN_API.Get_P2tel_Id2(ALAMAT_PENERIMA_MP_API.Get_Kelurahan_Id(a.company,a.no_ahli_waris,a.n_i_k,ALAMAT_PENERIMA_MP_API.Get_No_Urut_Default(a.company,a.n_i_k,a.no_ahli_waris)))) = UPPER('".$cab_p2tel."')";
        }
        
    /*
        if ($pmp_non_pmp != '') {
            if ($pmp_non_pmp == 1) { // PMP
                $prm_where = $prm_where." AND a.n_i_k IN (SELECT n_i_k FROM PENERIMA_MP2) AND a.no_ahli_waris IN (SELECT no_ahli_waris FROM PENERIMA_MP2)";
            }
            elseif ($pmp_non_pmp == 2) { // NON PMP
                $prm_where = $prm_where." AND a.n_i_k NOT IN (SELECT n_i_k FROM PENERIMA_MP2) AND a.no_ahli_waris NOT IN (SELECT no_ahli_waris FROM PENERIMA_MP2)";
            }
        }
    */ //pmp_non_pmp

        if ($jenis_aw != '') {
            if ($jenis_aw == 1) {
                $jenis_aw = "Peserta";
            } elseif ($jenis_aw == 2) {
                $jenis_aw = "Suami";
            } elseif ($jenis_aw == 3) {
                $jenis_aw = "Istri";
            } elseif ($jenis_aw == 4) {
                $jenis_aw = "Anak";
            } elseif ($jenis_aw == 5) {
                $jenis_aw = "Wali";
            }

            $prm_where = $prm_where." AND INITCAP(a.jenis_ahli_waris) = INITCAP('".$jenis_aw."')";
        }

        if ($jns_api == 'jmlh') {
            $count2 = 0;
        }
        elseif ($jns_api == 'list') {
            $count2 = array();
        }
        
        if (strpos($jenis_trnsksi, '1') !== false && ($pmp_non_pmp == '' || $pmp_non_pmp == 1) && ($sk_non_sk == '' || $sk_non_sk == 2)) { //Hapus
            if ($range_tgl_trans != '') {
                $tgl_transaksi = explode("..", $range_tgl_trans);
                $prm_where = $prm_where." AND (a.valid_from BETWEEN TO_DATE('".$tgl_transaksi[0]."','MM/DD/YYYY') AND TO_DATE('".$tgl_transaksi[1]."','MM/DD/YYYY'))";
            }
            if ($range_tgl_setuju != '') {
                $tgl_setuju = explode("..", $range_tgl_setuju);
                $prm_where = $prm_where." AND INITCAP(a.state) = 'Disetujui' AND (DPT_AUDITTRAIL_API.Get_Tgl('HapusPenerimaMp',a.objid,a.state) BETWEEN TO_DATE('".$tgl_setuju[0]."','MM/DD/YYYY') AND TO_DATE('".$tgl_setuju[1]."','MM/DD/YYYY'))";
            }

            if ($jns_api == 'jmlh') {
                $query2 = "SELECT COUNT(a.n_i_k) JMLH_HAPUS_PMP FROM HAPUS_PENERIMA_MP a WHERE a.state NOT IN ('BatalHapus','Dibatalkan') AND a.tgl_meninggal IS NOT NULL $prm_where";

                $sql2 = oci_parse($conn, $query2);
                oci_execute($sql2);
                $data2 = oci_fetch_array($sql2, OCI_ASSOC+OCI_RETURN_NULLS);
                $count2 = $count2 + (int)$data2['JMLH_HAPUS_PMP'];
            }
            elseif ($jns_api == 'list') {
                $query2 = "SELECT a.n_i_k, a.no_ahli_waris, CALON_PENERIMA_MP_API.Get_Nama_Ahli_Waris(a.company,a.n_i_k,a.no_ahli_waris) nama, a.last_update tgl_transaksi, TRUNC(DPT_AUDITTRAIL_API.Get_Tgl('HapusPenerimaMp',a.objid,a.state)) tgl_disetujui, 'HAPUS PMP' jenis_transaksi, KELURAHAN_API.Get_P2tel_Id2(ALAMAT_PENERIMA_MP_API.Get_Kelurahan_Id(a.company,a.no_ahli_waris,a.n_i_k,ALAMAT_PENERIMA_MP_API.Get_No_Urut_Default(a.company,a.n_i_k,a.no_ahli_waris))) cab_p2tel, MITRA_KERJA_API.Get_Nama_Mitra_Kerja(a.company,KELURAHAN_API.Get_P2tel_Id2(ALAMAT_PENERIMA_MP_API.Get_Kelurahan_Id(a.company,a.no_ahli_waris,a.n_i_k,ALAMAT_PENERIMA_MP_API.Get_No_Urut_Default(a.company,a.n_i_k,a.no_ahli_waris)))) nama_cab_p2tel, 'NON SK' SK_NON_SK, 'PMP' PMP_NON_PMP, a.jenis_ahli_waris jenis_aw FROM HAPUS_PENERIMA_MP a WHERE a.state NOT IN ('BatalHapus','Dibatalkan') AND a.tgl_meninggal IS NOT NULL $prm_where";

                $sql2 = oci_parse($conn, $query2);
                oci_execute($sql2);
                
                while ($dt = oci_fetch_assoc($sql2)) {
                    $count2[] = $dt;
                }
            }
        }
        if (strpos($jenis_trnsksi, '2') !== false && ($pmp_non_pmp == '' || $pmp_non_pmp == 1)) { //Mutasi PMP
            if ($range_tgl_trans != '') {
                $tgl_transaksi = explode("..", $range_tgl_trans);
                $prm_where = $prm_where." AND (a.valid_from BETWEEN TO_DATE('".$tgl_transaksi[0]."','MM/DD/YYYY') AND TO_DATE('".$tgl_transaksi[1]."','MM/DD/YYYY'))";
            }
            if ($range_tgl_setuju != '') {
                $prm_where = $prm_where." AND INITCAP(a.state) = 'Disetujui' AND (DPT_AUDITTRAIL_API.Get_Tgl('MutasiPenerimaMp',a.objid,a.state) BETWEEN TO_DATE('".$tgl_setuju[0]."','MM/DD/YYYY') AND TO_DATE('".$tgl_setuju[1]."','MM/DD/YYYY'))";
            }
            
            if ($sk_non_sk != '') {
                if ($sk_non_sk == 1) { //SK
                    $prm_where_sk_non_sk = $prm_where." AND a.non_create_sk = 'FALSE'";
                }
                elseif ($sk_non_sk == 2) { //Non SK
                    $prm_where_sk_non_sk = $prm_where." AND a.non_create_sk = 'TRUE'";
                }
            }
            elseif ($sk_non_sk == '') {
                $prm_where_sk_non_sk = $prm_where;
            }

            if ($jns_api == 'jmlh') {
                $query2 = "SELECT COUNT(a.n_i_k) JMLH_MUTASI_PMP FROM MUTASI_PENERIMA_MP a WHERE a.state = 'Disetujui' AND a.tgl_meninggal IS NOT NULL AND NVL(a.gabung_mp,'FALSE') != 'TRUE' $prm_where_sk_non_sk";

                $sql2 = oci_parse($conn, $query2);
                oci_execute($sql2);
                $data2 = oci_fetch_array($sql2, OCI_ASSOC+OCI_RETURN_NULLS);
                $count2 = $count2 + (int)$data2['JMLH_MUTASI_PMP'];
            }
            elseif ($jns_api == 'list') {
                $query2 = "SELECT a.n_i_k, a.no_ahli_waris, CALON_PENERIMA_MP_API.Get_Nama_Ahli_Waris(a.company,a.n_i_k,a.no_ahli_waris) nama, a.last_update tgl_transaksi, TRUNC(DPT_AUDITTRAIL_API.Get_Tgl('MutasiPenerimaMp',a.objid,a.state)) tgl_disetujui, 'Mutasi PMP' jenis_transaksi, KELURAHAN_API.Get_P2tel_Id2(ALAMAT_PENERIMA_MP_API.Get_Kelurahan_Id(a.company,a.no_ahli_waris,a.n_i_k,ALAMAT_PENERIMA_MP_API.Get_No_Urut_Default(a.company,a.n_i_k,a.no_ahli_waris))) cab_p2tel, MITRA_KERJA_API.Get_Nama_Mitra_Kerja(a.company,KELURAHAN_API.Get_P2tel_Id2(ALAMAT_PENERIMA_MP_API.Get_Kelurahan_Id(a.company,a.no_ahli_waris,a.n_i_k,ALAMAT_PENERIMA_MP_API.Get_No_Urut_Default(a.company,a.n_i_k,a.no_ahli_waris)))) nama_cab_p2tel, CASE a.non_create_sk WHEN 'TRUE' THEN 'SK' ELSE 'NON_SK' END SK_NON_SK, 'PMP' PMP_NON_PMP, a.jenis_ahli_waris jenis_aw FROM MUTASI_PENERIMA_MP a WHERE a.state = 'Disetujui' AND a.tgl_meninggal IS NOT NULL AND NVL(a.gabung_mp,'FALSE') != 'TRUE' $prm_where_sk_non_sk";

                $sql2 = oci_parse($conn, $query2);
                oci_execute($sql2);
                
                while ($dt = oci_fetch_assoc($sql2)) {
                    $count2[] = $dt;
                }
            }
        }
        if (strpos($jenis_trnsksi, '3') !== false && ($pmp_non_pmp == '' || $pmp_non_pmp == 1)) { //Mutasi AW
            if ($range_tgl_trans != '') {
                $tgl_transaksi = explode("..", $range_tgl_trans);
                $prm_where = $prm_where." AND (a.valid_from BETWEEN TO_DATE('".$tgl_transaksi[0]."','MM/DD/YYYY') AND TO_DATE('".$tgl_transaksi[1]."','MM/DD/YYYY'))";
            }
            if ($range_tgl_setuju != '') {
                $tgl_setuju = explode("..", $range_tgl_setuju);
                $prm_where = $prm_where." AND INITCAP(a.state) = 'Disetujui' AND (DPT_AUDITTRAIL_API.Get_Tgl('MutasiPenerimaMp',a.objid,a.state) BETWEEN TO_DATE('".$tgl_setuju[0]."','MM/DD/YYYY') AND TO_DATE('".$tgl_setuju[1]."','MM/DD/YYYY'))";
            }
            
            if ($sk_non_sk != '') {
                if ($sk_non_sk == 1) { //SK
                    $prm_where_sk_non_sk = $prm_where." AND a.non_create_sk = 'FALSE'";
                }
                elseif ($sk_non_sk == 2) { //Non SK
                    $prm_where_sk_non_sk = $prm_where." AND a.non_create_sk = 'TRUE'";
                }
            }
            elseif ($sk_non_sk == '') {
                $prm_where_sk_non_sk = $prm_where;
            }

            if ($jns_api == 'jmlh') {
                $query2 = "SELECT COUNT(a.n_i_k) JMLH_MUTASI_PMP_AW FROM MUTASI_PENERIMA_MP_AW a WHERE a.state = 'Disetujui' AND a.tgl_meninggal IS NOT NULL $prm_where_sk_non_sk";

                $sql2 = oci_parse($conn, $query2);
                oci_execute($sql2);
                $data2 = oci_fetch_array($sql2, OCI_ASSOC+OCI_RETURN_NULLS);
                $count2 = $count2 + (int)$data2['JMLH_MUTASI_PMP_AW'];
            }
            elseif ($jns_api == 'list') {
                $query2 = "SELECT a.n_i_k, a.no_ahli_waris, CALON_PENERIMA_MP_API.Get_Nama_Ahli_Waris(a.company,a.n_i_k,a.no_ahli_waris) nama, a.last_update tgl_transaksi, TRUNC(DPT_AUDITTRAIL_API.Get_Tgl('MutasiPenerimaMp',a.objid,a.state)) tgl_disetujui, 'Mutasi AW' jenis_transaksi, KELURAHAN_API.Get_P2tel_Id2(ALAMAT_PENERIMA_MP_API.Get_Kelurahan_Id(a.company,a.no_ahli_waris,a.n_i_k,ALAMAT_PENERIMA_MP_API.Get_No_Urut_Default(a.company,a.n_i_k,a.no_ahli_waris))) cab_p2tel, MITRA_KERJA_API.Get_Nama_Mitra_Kerja(a.company,KELURAHAN_API.Get_P2tel_Id2(ALAMAT_PENERIMA_MP_API.Get_Kelurahan_Id(a.company,a.no_ahli_waris,a.n_i_k,ALAMAT_PENERIMA_MP_API.Get_No_Urut_Default(a.company,a.n_i_k,a.no_ahli_waris)))) nama_cab_p2tel, CASE a.non_create_sk WHEN 'TRUE' THEN 'SK' ELSE 'NON_SK' END SK_NON_SK, 'PMP' PMP_NON_PMP, a.jenis_ahli_waris jenis_aw FROM MUTASI_PENERIMA_MP_AW a WHERE a.state = 'Disetujui' AND a.tgl_meninggal IS NOT NULL $prm_where_sk_non_sk";

                $sql2 = oci_parse($conn, $query2);
                oci_execute($sql2);
                
                while ($dt = oci_fetch_assoc($sql2)) {
                    $count2[] = $dt;
                }
            }
        }
        if (strpos($jenis_trnsksi, '4') !== false && ($pmp_non_pmp == '' || $pmp_non_pmp == 1)) { //Mutasi Gabung
            if ($range_tgl_trans != '') {
                $tgl_transaksi = explode("..", $range_tgl_trans);
                $prm_where = $prm_where." AND (a.valid_from BETWEEN TO_DATE('".$tgl_transaksi[0]."','MM/DD/YYYY') AND TO_DATE('".$tgl_transaksi[1]."','MM/DD/YYYY'))";
            }
            if ($range_tgl_setuju != '') {
                $prm_where = $prm_where." AND INITCAP(a.state) = 'Disetujui' AND (DPT_AUDITTRAIL_API.Get_Tgl('MutasiPenerimaMp',a.objid,a.state) BETWEEN TO_DATE('".$tgl_setuju[0]."','MM/DD/YYYY') AND TO_DATE('".$tgl_setuju[1]."','MM/DD/YYYY'))";
            }
            
            if ($sk_non_sk != '') {
                if ($sk_non_sk == 1) { //SK
                    $prm_where_sk_non_sk = $prm_where." AND a.non_create_sk = 'FALSE'";
                }
                elseif ($sk_non_sk == 2) { //Non SK
                    $prm_where_sk_non_sk = $prm_where." AND a.non_create_sk = 'TRUE'";
                }
            }
            elseif ($sk_non_sk == '') {
                $prm_where_sk_non_sk = $prm_where;
            }

            if ($jns_api == 'jmlh') {
                $query2 = "SELECT COUNT(a.n_i_k) JMLH_GABUNG_PMP FROM MUTASI_PENGGABUNGAN_MP a WHERE a.state = 'Disetujui' AND a.tgl_meninggal IS NOT NULL AND NVL(a.gabung_mp,'FALSE') = 'TRUE' $prm_where_sk_non_sk";

                $sql2 = oci_parse($conn, $query2);
                oci_execute($sql2);
                $data2 = oci_fetch_array($sql2, OCI_ASSOC+OCI_RETURN_NULLS);
                $count2 = $count2 + (int)$data2['JMLH_GABUNG_PMP'];
            }
            elseif ($jns_api == 'list') {
                $query2 = "SELECT a.n_i_k, a.no_ahli_waris, CALON_PENERIMA_MP_API.Get_Nama_Ahli_Waris(a.company,a.n_i_k,a.no_ahli_waris) nama, a.last_update tgl_transaksi, TRUNC(DPT_AUDITTRAIL_API.Get_Tgl('MutasiPenerimaMp',a.objid,a.state)) tgl_disetujui, 'Mutasi Gabung' jenis_transaksi, KELURAHAN_API.Get_P2tel_Id2(ALAMAT_PENERIMA_MP_API.Get_Kelurahan_Id(a.company,a.no_ahli_waris,a.n_i_k,ALAMAT_PENERIMA_MP_API.Get_No_Urut_Default(a.company,a.n_i_k,a.no_ahli_waris))) cab_p2tel, MITRA_KERJA_API.Get_Nama_Mitra_Kerja(a.company,KELURAHAN_API.Get_P2tel_Id2(ALAMAT_PENERIMA_MP_API.Get_Kelurahan_Id(a.company,a.no_ahli_waris,a.n_i_k,ALAMAT_PENERIMA_MP_API.Get_No_Urut_Default(a.company,a.n_i_k,a.no_ahli_waris)))) nama_cab_p2tel, CASE a.non_create_sk WHEN 'TRUE' THEN 'SK' ELSE 'NON_SK' END SK_NON_SK, 'PMP' PMP_NON_PMP, a.jenis_ahli_waris jenis_aw FROM MUTASI_PENGGABUNGAN_MP a WHERE a.state = 'Disetujui' AND a.tgl_meninggal IS NOT NULL AND NVL(a.gabung_mp,'FALSE') = 'TRUE' $prm_where_sk_non_sk";

                $sql2 = oci_parse($conn, $query2);
                oci_execute($sql2);
                
                while ($dt = oci_fetch_assoc($sql2)) {
                    $count2[] = $dt;
                }
            }
        }
        if (strpos($jenis_trnsksi, '5') !== false && ($pmp_non_pmp == '' || $pmp_non_pmp == 2) && ($sk_non_sk == '' || $sk_non_sk == 2)) { //Ubah AW
            if ($range_tgl_trans != '') {
                $tgl_transaksi = explode("..", $range_tgl_trans);
                $prm_where = $prm_where." AND (DPT_AUDITTRAIL_API.Get_Tgl('TrUbahAhliWaris',a.objid,'Dipersiapkan') BETWEEN TO_DATE('".$tgl_setuju[0]."','MM/DD/YYYY') AND TO_DATE('".$tgl_setuju[1]."','MM/DD/YYYY'))";
            }
            if ($range_tgl_setuju != '') {
                $prm_where = $prm_where." AND INITCAP(a.state) = 'Disetujui' AND (DPT_AUDITTRAIL_API.Get_Tgl('TrUbahAhliWaris',a.objid,a.state) BETWEEN TO_DATE('".$tgl_setuju[0]."','MM/DD/YYYY') AND TO_DATE('".$tgl_setuju[1]."','MM/DD/YYYY'))";
            }

            if ($jns_api == 'jmlh') {
                $query2 = "SELECT COUNT(a.n_i_k) JMLH_UBAH_AW FROM TR_UBAH_AHLI_WARIS a WHERE a.state = 'Disetujui' AND a.tgl_meninggal IS NOT NULL $prm_where";

                $sql2 = oci_parse($conn, $query2);
                oci_execute($sql2);
                $data2 = oci_fetch_array($sql2, OCI_ASSOC+OCI_RETURN_NULLS);
                $count2 = $count2 + (int)$data2['JMLH_UBAH_AW'];
            }
            elseif ($jns_api == 'list') {
                $query2 = "SELECT a.n_i_k, a.no_ahli_waris, CALON_PENERIMA_MP_API.Get_Nama_Ahli_Waris(a.company,a.n_i_k,a.no_ahli_waris) nama, a.last_update tgl_transaksi, TRUNC(DPT_AUDITTRAIL_API.Get_Tgl('MutasiPenerimaMp',a.objid,a.state)) tgl_disetujui, 'Ubah AW' jenis_transaksi, KELURAHAN_API.Get_P2tel_Id2(ALAMAT_PENERIMA_MP_API.Get_Kelurahan_Id(a.company,1,a.n_i_k,ALAMAT_PENERIMA_MP_API.Get_No_Urut_Default(a.company,a.n_i_k,1))) cab_p2tel, MITRA_KERJA_API.Get_Nama_Mitra_Kerja(a.company,KELURAHAN_API.Get_P2tel_Id2(ALAMAT_PENERIMA_MP_API.Get_Kelurahan_Id(a.company,1,a.n_i_k,ALAMAT_PENERIMA_MP_API.Get_No_Urut_Default(a.company,a.n_i_k,1)))) nama_cab_p2tel, 'NON SK' SK_NON_SK, 'NON PMP' PMP_NON_PMP, a.jenis_ahli_waris jenis_aw FROM TR_UBAH_AHLI_WARIS a WHERE a.state = 'Disetujui' AND a.tgl_meninggal IS NOT NULL $prm_where";

                $sql2 = oci_parse($conn, $query2);
                oci_execute($sql2);
                
                while ($dt = oci_fetch_assoc($sql2)) {
                    $count2[] = $dt;
                }
            }
        }
        
        /*$sql2 = oci_parse($conn, $query2);
        oci_execute($sql2);
        $data2 = oci_fetch_array($sql2, OCI_ASSOC+OCI_RETURN_NULLS);
        $count2 = (int)$data2['JMLH_PMP_MENINGGAL'];*/

    return $count2;
}

function jmlhPmpMeninggalAll($tahun, $param) {
    $conn = getConnection();
    
    $array = array();
    $bulan_sum = 0;
    $trwln_sum = 0;
    $smt_sum = 0;
    $jns_aw_sum = 0;
    $sns_sum = 0;
    $pnp_sum = 0;

    if ($param == 'bulan') {
        // Per Bulan //
        for ($i = 1; $i <= 12; $i++) {
            if ($i < 10) {
                $bln = '0'.$i;
                $prd = $tahun.$bln;
            }
            else {
                $bln = $i;
                $prd = $tahun.$bln;
            }

            $bln_count = 0;

        // $bulan //
            if ($i == 1) {
                $bulan = 'JANUARI';
            } elseif ($i == 2) {
                $bulan = 'FEBRUARI';
            } elseif ($i == 3) {
                $bulan = 'MARET';
            } elseif ($i == 4) {
                $bulan = 'APRIL';
            } elseif ($i == 5) {
                $bulan = 'MEI';
            } elseif ($i == 6) {
                $bulan = 'JUNI';
            } elseif ($i == 7) {
                $bulan = 'JULI';
            } elseif ($i == 8) {
                $bulan = 'AGUSTUS';
            } elseif ($i == 9) {
                $bulan = 'SEPTEMBER';
            } elseif ($i == 10) {
                $bulan = 'OKTOBER';
            } elseif ($i == 11) {
                $bulan = 'NOVEMBER';
            } elseif ($i == 12) {
                $bulan = 'DESEMVER';
            }
        // -- //

        // Hapus //
            $bln_hapus_query = "SELECT COUNT(a.n_i_k) JMLH_PMP_MD_BULAN_HAPUS FROM HAPUS_PENERIMA_MP a WHERE a.state = 'Disetujui' AND a.tgl_meninggal IS NOT NULL AND a.kd_periode = '".$prd."'";
            
            $bln_hapus_sql = oci_parse($conn, $bln_hapus_query);
            oci_execute($bln_hapus_sql);
            $bln_hapus_data = oci_fetch_array($bln_hapus_sql, OCI_ASSOC+OCI_RETURN_NULLS);
            $bln_count = $bln_count + (int)$bln_hapus_data['JMLH_PMP_MD_BULAN_HAPUS'];

        // Mutasi PMP //
            $bln_mpmp_query = "SELECT COUNT(a.n_i_k) JMLH_PMP_MD_BULAN_MPMP FROM MUTASI_PENERIMA_MP a WHERE a.state = 'Disetujui' AND a.tgl_meninggal IS NOT NULL AND NVL(a.gabung_mp,'FALSE') != 'TRUE' AND a.kd_periode = '".$prd."'";
            
            $bln_mpmp_sql = oci_parse($conn, $bln_mpmp_query);
            oci_execute($bln_mpmp_sql);
            $bln_mpmp_data = oci_fetch_array($bln_mpmp_sql, OCI_ASSOC+OCI_RETURN_NULLS);
            $bln_count = $bln_count + (int)$bln_mpmp_data['JMLH_PMP_MD_BULAN_MPMP'];

        // Mutasi AW //
            $bln_maw_query = "SELECT COUNT(a.n_i_k) JMLH_PMP_MD_BULAN_MAW FROM MUTASI_PENERIMA_MP_AW a WHERE a.state = 'Disetujui' AND a.tgl_meninggal IS NOT NULL AND a.kd_periode = '".$prd."'";
            
            $bln_maw_sql = oci_parse($conn, $bln_maw_query);
            oci_execute($bln_maw_sql);
            $bln_maw_data = oci_fetch_array($bln_maw_sql, OCI_ASSOC+OCI_RETURN_NULLS);
            $bln_count = $bln_count + (int)$bln_maw_data['JMLH_PMP_MD_BULAN_MAW'];

        // Mutasi Gabung //
            $bln_mgbng_query = "SELECT COUNT(a.n_i_k) JMLH_PMP_MD_BULAN_MGBNG FROM MUTASI_PENGGABUNGAN_MP a WHERE a.state = 'Disetujui' AND a.tgl_meninggal IS NOT NULL AND NVL(a.gabung_mp,'FALSE') = 'TRUE' AND a.kd_periode = '".$prd."'";
            
            $bln_mgbng_sql = oci_parse($conn, $bln_mgbng_query);
            oci_execute($bln_mgbng_sql);
            $bln_mgbng_data = oci_fetch_array($bln_mgbng_sql, OCI_ASSOC+OCI_RETURN_NULLS);
            $bln_count = $bln_count + (int)$bln_mgbng_data['JMLH_PMP_MD_BULAN_MGBNG'];

        // Ubah AW //
            $bln_ubah_aw_query = "SELECT COUNT(a.n_i_k) JMLH_PMP_MD_BULAN_UBAH_AW FROM TR_UBAH_AHLI_WARIS a WHERE a.state = 'Disetujui' AND a.tgl_meninggal IS NOT NULL AND a.kd_periode = '".$prd."'";
            
            $bln_ubah_aw_sql = oci_parse($conn, $bln_ubah_aw_query);
            oci_execute($bln_ubah_aw_sql);
            $bln_ubah_aw_data = oci_fetch_array($bln_ubah_aw_sql, OCI_ASSOC+OCI_RETURN_NULLS);
            $bln_count = $bln_count + (int)$bln_ubah_aw_data['JMLH_PMP_MD_BULAN_UBAH_AW'];
        // -- //
            
            $array['JMLH_PMP_MD_'.$bulan] = $bln_count;
            $bulan_sum = $bulan_sum + $bln_count;
        }
        $array['TOTAL_PMP_MD_BULAN'] = $bulan_sum;
    }
    elseif ($param == 'triwulan') {
        // Per Triwulan //
        for ($i = 1; $i <= 4; $i++) {
            if ($i == 1) {
                $triwulan = "AND a.kd_periode IN ('".$tahun."01', '".$tahun."02', '".$tahun."03')";
                $trwln = "TRIWULAN_I";
            }
            elseif ($i == 2) {
                $triwulan = "AND a.kd_periode IN ('".$tahun."04', '".$tahun."05', '".$tahun."06')";
                $trwln = "TRIWULAN_II";
            }
            elseif ($i == 3) {
                $triwulan = "AND a.kd_periode IN ('".$tahun."07', '".$tahun."08', '".$tahun."09')";
                $trwln = "TRIWULAN_III";
            }
            elseif ($i == 4) {
                $triwulan = "AND a.kd_periode IN ('".$tahun."10', '".$tahun."11', '".$tahun."12')";
                $trwln = "TRIWULAN_IV";
            }

            $triwln_count = 0;

        // Hapus //
            $triwln_hapus_query = "SELECT COUNT(a.n_i_k) JMLH_PMP_MD_TRIWLN_HAPUS FROM HAPUS_PENERIMA_MP a WHERE a.state = 'Disetujui' AND a.tgl_meninggal IS NOT NULL $triwulan";
                
            $triwln_hapus_sql = oci_parse($conn, $triwln_hapus_query);
            oci_execute($triwln_hapus_sql);
            $triwln_hapus_data = oci_fetch_array($triwln_hapus_sql, OCI_ASSOC+OCI_RETURN_NULLS);
            $triwln_count = $triwln_count + (int)$triwln_hapus_data['JMLH_PMP_MD_TRIWLN_HAPUS'];

        // Mutasi PMP //
            $triwln_mpmp_query = "SELECT COUNT(a.n_i_k) JMLH_PMP_MD_TRIWLN_MPMP FROM MUTASI_PENERIMA_MP a WHERE a.state = 'Disetujui' AND a.tgl_meninggal IS NOT NULL AND NVL(a.gabung_mp,'FALSE') != 'TRUE' $triwulan";
            
            $triwln_mpmp_sql = oci_parse($conn, $triwln_mpmp_query);
            oci_execute($triwln_mpmp_sql);
            $triwln_mpmp_data = oci_fetch_array($triwln_mpmp_sql, OCI_ASSOC+OCI_RETURN_NULLS);
            $triwln_count = $triwln_count + (int)$triwln_mpmp_data['JMLH_PMP_MD_TRIWLN_MPMP'];

        // Mutasi AW //
            $triwln_maw_query = "SELECT COUNT(a.n_i_k) JMLH_PMP_MD_TRIWLN_MAW FROM MUTASI_PENERIMA_MP_AW a WHERE a.state = 'Disetujui' AND a.tgl_meninggal IS NOT NULL $triwulan";
            
            $triwln_maw_sql = oci_parse($conn, $triwln_maw_query);
            oci_execute($triwln_maw_sql);
            $triwln_maw_data = oci_fetch_array($triwln_maw_sql, OCI_ASSOC+OCI_RETURN_NULLS);
            $triwln_count = $triwln_count + (int)$triwln_maw_data['JMLH_PMP_MD_TRIWLN_MAW'];

        // Mutasi Gabung //
            $triwln_mgbng_query = "SELECT COUNT(a.n_i_k) JMLH_PMP_MD_TRIWLN_MGBNG FROM MUTASI_PENGGABUNGAN_MP a WHERE a.state = 'Disetujui' AND a.tgl_meninggal IS NOT NULL AND NVL(a.gabung_mp,'FALSE') = 'TRUE' $triwulan";
            
            $triwln_mgbng_sql = oci_parse($conn, $triwln_mgbng_query);
            oci_execute($triwln_mgbng_sql);
            $triwln_mgbng_data = oci_fetch_array($triwln_mgbng_sql, OCI_ASSOC+OCI_RETURN_NULLS);
            $triwln_count = $triwln_count + (int)$triwln_mgbng_data['JMLH_PMP_MD_TRIWLN_MGBNG'];

        // Ubah AW //
            $triwln_ubah_aw_query = "SELECT COUNT(a.n_i_k) JMLH_PMP_MD_TRIWLN_UBAH_AW FROM TR_UBAH_AHLI_WARIS a WHERE a.state = 'Disetujui' AND a.tgl_meninggal IS NOT NULL $triwulan";
            
            $triwln_ubah_aw_sql = oci_parse($conn, $triwln_ubah_aw_query);
            oci_execute($triwln_ubah_aw_sql);
            $triwln_ubah_aw_data = oci_fetch_array($triwln_ubah_aw_sql, OCI_ASSOC+OCI_RETURN_NULLS);
            $triwln_count = $triwln_count + (int)$triwln_ubah_aw_data['JMLH_PMP_MD_TRIWLN_UBAH_AW'];
        // -- //

            $array['JMLH_PMP_MD_'.$trwln] = $triwln_count;
            $trwln_sum = $trwln_sum + $triwln_count;
        }
        $array['TOTAL_PMP_MD_TRIWULAN'] = $trwln_sum;
    }
    elseif ($param == 'semester') {
        // Per Semester //
        for ($i = 1; $i <= 2; $i++) {
            if ($i == 1) {
                $semester = "AND a.kd_periode IN ('".$tahun."01', '".$tahun."02', '".$tahun."03', '".$tahun."04', '".$tahun."05', '".$tahun."06')";
                $smt = "SEMESTER_I";
            }
            elseif ($i == 2) {
                $semester = "AND a.kd_periode IN ('".$tahun."07', '".$tahun."08', '".$tahun."09', '".$tahun."10', '".$tahun."11', '".$tahun."12')";
                $smt = "SEMESTER_II";
            }

            $smt_count = 0;

        // Hapus //
            $smt_hapus_query = "SELECT COUNT(a.n_i_k) JMLH_PMP_MD_TRIWLN_HAPUS FROM HAPUS_PENERIMA_MP a WHERE a.state = 'Disetujui' AND a.tgl_meninggal IS NOT NULL $semester";
                
            $smt_hapus_sql = oci_parse($conn, $smt_hapus_query);
            oci_execute($smt_hapus_sql);
            $smt_hapus_data = oci_fetch_array($smt_hapus_sql, OCI_ASSOC+OCI_RETURN_NULLS);
            $smt_count = $smt_count + (int)$smt_hapus_data['JMLH_PMP_MD_TRIWLN_HAPUS'];

        // Mutasi PMP //
            $smt_mpmp_query = "SELECT COUNT(a.n_i_k) JMLH_PMP_MD_TRIWLN_MPMP FROM MUTASI_PENERIMA_MP a WHERE a.state = 'Disetujui' AND a.tgl_meninggal IS NOT NULL AND NVL(a.gabung_mp,'FALSE') != 'TRUE' $semester";
            
            $smt_mpmp_sql = oci_parse($conn, $smt_mpmp_query);
            oci_execute($smt_mpmp_sql);
            $smt_mpmp_data = oci_fetch_array($smt_mpmp_sql, OCI_ASSOC+OCI_RETURN_NULLS);
            $smt_count = $smt_count + (int)$smt_mpmp_data['JMLH_PMP_MD_TRIWLN_MPMP'];

        // Mutasi AW //
            $smt_maw_query = "SELECT COUNT(a.n_i_k) JMLH_PMP_MD_TRIWLN_MAW FROM MUTASI_PENERIMA_MP_AW a WHERE a.state = 'Disetujui' AND a.tgl_meninggal IS NOT NULL $semester";
            
            $smt_maw_sql = oci_parse($conn, $smt_maw_query);
            oci_execute($smt_maw_sql);
            $smt_maw_data = oci_fetch_array($smt_maw_sql, OCI_ASSOC+OCI_RETURN_NULLS);
            $smt_count = $smt_count + (int)$smt_maw_data['JMLH_PMP_MD_TRIWLN_MAW'];

        // Mutasi Gabung //
            $smt_mgbng_query = "SELECT COUNT(a.n_i_k) JMLH_PMP_MD_TRIWLN_MGBNG FROM MUTASI_PENGGABUNGAN_MP a WHERE a.state = 'Disetujui' AND a.tgl_meninggal IS NOT NULL AND NVL(a.gabung_mp,'FALSE') = 'TRUE' $semester";
            
            $smt_mgbng_sql = oci_parse($conn, $smt_mgbng_query);
            oci_execute($smt_mgbng_sql);
            $smt_mgbng_data = oci_fetch_array($smt_mgbng_sql, OCI_ASSOC+OCI_RETURN_NULLS);
            $smt_count = $smt_count + (int)$smt_mgbng_data['JMLH_PMP_MD_TRIWLN_MGBNG'];

        // Ubah AW //
            $smt_ubah_aw_query = "SELECT COUNT(a.n_i_k) JMLH_PMP_MD_TRIWLN_UBAH_AW FROM TR_UBAH_AHLI_WARIS a WHERE a.state = 'Disetujui' AND a.tgl_meninggal IS NOT NULL $semester";
            
            $smt_ubah_aw_sql = oci_parse($conn, $smt_ubah_aw_query);
            oci_execute($smt_ubah_aw_sql);
            $smt_ubah_aw_data = oci_fetch_array($smt_ubah_aw_sql, OCI_ASSOC+OCI_RETURN_NULLS);
            $smt_count = $smt_count + (int)$smt_ubah_aw_data['JMLH_PMP_MD_TRIWLN_UBAH_AW'];
        // -- //

            $array['JMLH_PMP_MD_'.$smt] = $smt_count;
            $smt_sum = $smt_sum + $smt_count;
        }
        $array['TOTAL_PMP_MD_SEMESTER'] = $smt_sum;
    }
    elseif ($param == 'tahun') {
        // Per Tahun //
        $thn_count = 0;

        // Hapus //
            $thn_hapus_query = "SELECT COUNT(a.n_i_k) JMLH_PMP_MD_TRIWLN_HAPUS FROM HAPUS_PENERIMA_MP a WHERE a.state = 'Disetujui' AND a.tgl_meninggal IS NOT NULL AND a.kd_periode LIKE '".$tahun."%'";
                
            $thn_hapus_sql = oci_parse($conn, $thn_hapus_query);
            oci_execute($thn_hapus_sql);
            $thn_hapus_data = oci_fetch_array($thn_hapus_sql, OCI_ASSOC+OCI_RETURN_NULLS);
            $thn_count = $thn_count + (int)$thn_hapus_data['JMLH_PMP_MD_TRIWLN_HAPUS'];

        // Mutasi PMP //
            $thn_mpmp_query = "SELECT COUNT(a.n_i_k) JMLH_PMP_MD_TRIWLN_MPMP FROM MUTASI_PENERIMA_MP a WHERE a.state = 'Disetujui' AND a.tgl_meninggal IS NOT NULL AND NVL(a.gabung_mp,'FALSE') != 'TRUE' AND a.kd_periode LIKE '".$tahun."%'";
            
            $thn_mpmp_sql = oci_parse($conn, $thn_mpmp_query);
            oci_execute($thn_mpmp_sql);
            $thn_mpmp_data = oci_fetch_array($thn_mpmp_sql, OCI_ASSOC+OCI_RETURN_NULLS);
            $thn_count = $thn_count + (int)$thn_mpmp_data['JMLH_PMP_MD_TRIWLN_MPMP'];

        // Mutasi AW //
            $thn_maw_query = "SELECT COUNT(a.n_i_k) JMLH_PMP_MD_TRIWLN_MAW FROM MUTASI_PENERIMA_MP_AW a WHERE a.state = 'Disetujui' AND a.tgl_meninggal IS NOT NULL AND a.kd_periode LIKE '".$tahun."%'";
            
            $thn_maw_sql = oci_parse($conn, $thn_maw_query);
            oci_execute($thn_maw_sql);
            $thn_maw_data = oci_fetch_array($thn_maw_sql, OCI_ASSOC+OCI_RETURN_NULLS);
            $thn_count = $thn_count + (int)$thn_maw_data['JMLH_PMP_MD_TRIWLN_MAW'];

        // Mutasi Gabung //
            $thn_mgbng_query = "SELECT COUNT(a.n_i_k) JMLH_PMP_MD_TRIWLN_MGBNG FROM MUTASI_PENGGABUNGAN_MP a WHERE a.state = 'Disetujui' AND a.tgl_meninggal IS NOT NULL AND NVL(a.gabung_mp,'FALSE') = 'TRUE' AND a.kd_periode LIKE '".$tahun."%'";
            
            $thn_mgbng_sql = oci_parse($conn, $thn_mgbng_query);
            oci_execute($thn_mgbng_sql);
            $thn_mgbng_data = oci_fetch_array($thn_mgbng_sql, OCI_ASSOC+OCI_RETURN_NULLS);
            $thn_count = $thn_count + (int)$thn_mgbng_data['JMLH_PMP_MD_TRIWLN_MGBNG'];

        // Ubah AW //
            $thn_ubah_aw_query = "SELECT COUNT(a.n_i_k) JMLH_PMP_MD_TRIWLN_UBAH_AW FROM TR_UBAH_AHLI_WARIS a WHERE a.state = 'Disetujui' AND a.tgl_meninggal IS NOT NULL AND a.kd_periode LIKE '".$tahun."%'";
            
            $thn_ubah_aw_sql = oci_parse($conn, $thn_ubah_aw_query);
            oci_execute($thn_ubah_aw_sql);
            $thn_ubah_aw_data = oci_fetch_array($thn_ubah_aw_sql, OCI_ASSOC+OCI_RETURN_NULLS);
            $thn_count = $thn_count + (int)$thn_ubah_aw_data['JMLH_PMP_MD_TRIWLN_UBAH_AW'];
        // -- //
        
        $array['JMLH_PMP_MD_TAHUN'] = $thn_count;
    }
    elseif ($param == 'jns_trnsksi') {
        // Hapus //
            $hapus_query = "SELECT COUNT(a.n_i_k) JMLH_PMP_MD_HAPUS FROM HAPUS_PENERIMA_MP a WHERE a.state = 'Disetujui' AND a.tgl_meninggal IS NOT NULL AND a.kd_periode LIKE '".$tahun."%'";
                
            $hapus_sql = oci_parse($conn, $hapus_query);
            oci_execute($hapus_sql);
            $hapus_data = oci_fetch_array($hapus_sql, OCI_ASSOC+OCI_RETURN_NULLS);

            $array['JMLH_PMP_MD_HAPUS'] = (int)$hapus_data['JMLH_PMP_MD_HAPUS'];

        // Mutasi PMP //
            $mpmp_query = "SELECT COUNT(a.n_i_k) JMLH_PMP_MD_MPMP FROM MUTASI_PENERIMA_MP a WHERE a.state = 'Disetujui' AND a.tgl_meninggal IS NOT NULL AND NVL(a.gabung_mp,'FALSE') != 'TRUE' AND a.kd_periode LIKE '".$tahun."%'";
            
            $mpmp_sql = oci_parse($conn, $mpmp_query);
            oci_execute($mpmp_sql);
            $mpmp_data = oci_fetch_array($mpmp_sql, OCI_ASSOC+OCI_RETURN_NULLS);
            $array['JMLH_PMP_MD_MUTASI_PMP'] = (int)$mpmp_data['JMLH_PMP_MD_MPMP'];

        // Mutasi AW //
            $maw_query = "SELECT COUNT(a.n_i_k) JMLH_PMP_MD_MAW FROM MUTASI_PENERIMA_MP_AW a WHERE a.state = 'Disetujui' AND a.tgl_meninggal IS NOT NULL AND a.kd_periode LIKE '".$tahun."%'";
            
            $maw_sql = oci_parse($conn, $maw_query);
            oci_execute($maw_sql);
            $maw_data = oci_fetch_array($maw_sql, OCI_ASSOC+OCI_RETURN_NULLS);
            $array['JMLH_PMP_MD_MUTASI_AW'] = (int)$maw_data['JMLH_PMP_MD_MAW'];

        // Mutasi Gabung //
            $mgbng_query = "SELECT COUNT(a.n_i_k) JMLH_PMP_MD_MGBNG FROM MUTASI_PENGGABUNGAN_MP a WHERE a.state = 'Disetujui' AND a.tgl_meninggal IS NOT NULL AND NVL(a.gabung_mp,'FALSE') = 'TRUE' AND a.kd_periode LIKE '".$tahun."%'";
            
            $mgbng_sql = oci_parse($conn, $mgbng_query);
            oci_execute($mgbng_sql);
            $mgbng_data = oci_fetch_array($mgbng_sql, OCI_ASSOC+OCI_RETURN_NULLS);
            $array['JMLH_PMP_MD_MUTASI_GABUNG'] = (int)$mgbng_data['JMLH_PMP_MD_MGBNG'];

        // Ubah AW //
            $ubah_aw_query = "SELECT COUNT(a.n_i_k) JMLH_PMP_MD_UBAH_AW FROM TR_UBAH_AHLI_WARIS a WHERE a.state = 'Disetujui' AND a.tgl_meninggal IS NOT NULL AND a.kd_periode LIKE '".$tahun."%'";
            
            $ubah_aw_sql = oci_parse($conn, $ubah_aw_query);
            oci_execute($ubah_aw_sql);
            $ubah_aw_data = oci_fetch_array($ubah_aw_sql, OCI_ASSOC+OCI_RETURN_NULLS);
            $array['JMLH_PMP_MD_UBAH_AW'] = (int)$ubah_aw_data['JMLH_PMP_MD_UBAH_AW'];
        // -- //

        $array['TOTAL_PMP_MD_JNS_TRANSAKSI'] = (int)$hapus_data['JMLH_PMP_MD_HAPUS'] + (int)$mpmp_data['JMLH_PMP_MD_MPMP'] + (int)$maw_data['JMLH_PMP_MD_MAW'] + (int)$mgbng_data['JMLH_PMP_MD_MGBNG'] + (int)$ubah_aw_data['JMLH_PMP_MD_UBAH_AW'];
    }
    elseif ($param == 'sk_non_sk') {
        // SK NON SK //
        for ($i = 1; $i <= 2; $i++) {
            if ($i == 1) {
                $sns_count = 0;

                // Mutasi PMP //
                    $sns_mpmp_query = "SELECT COUNT(a.n_i_k) JMLH_PMP_MD_SNS_MPMP FROM MUTASI_PENERIMA_MP a WHERE a.state = 'Disetujui' AND a.tgl_meninggal IS NOT NULL AND NVL(a.gabung_mp,'FALSE') != 'TRUE' AND a.kd_periode LIKE '".$tahun."%' AND a.non_create_sk = 'FALSE'";
                    
                    $sns_mpmp_sql = oci_parse($conn, $sns_mpmp_query);
                    oci_execute($sns_mpmp_sql);
                    $sns_mpmp_data = oci_fetch_array($sns_mpmp_sql, OCI_ASSOC+OCI_RETURN_NULLS);
                    $sns_count = $sns_count + (int)$sns_mpmp_data['JMLH_PMP_MD_SNS_MPMP'];

                // Mutasi AW //
                    $sns_maw_query = "SELECT COUNT(a.n_i_k) JMLH_PMP_MD_SNS_MAW FROM MUTASI_PENERIMA_MP_AW a WHERE a.state = 'Disetujui' AND a.tgl_meninggal IS NOT NULL AND a.kd_periode LIKE '".$tahun."%' AND a.non_create_sk = 'FALSE'";
                    
                    $sns_maw_sql = oci_parse($conn, $sns_maw_query);
                    oci_execute($sns_maw_sql);
                    $sns_maw_data = oci_fetch_array($sns_maw_sql, OCI_ASSOC+OCI_RETURN_NULLS);
                    $sns_count = $sns_count + (int)$sns_maw_data['JMLH_PMP_MD_SNS_MAW'];

                // Mutasi Gabung //
                    $sns_mgbng_query = "SELECT COUNT(a.n_i_k) JMLH_PMP_MD_SNS_MGBNG FROM MUTASI_PENGGABUNGAN_MP a WHERE a.state = 'Disetujui' AND a.tgl_meninggal IS NOT NULL AND NVL(a.gabung_mp,'FALSE') = 'TRUE' AND a.kd_periode LIKE '".$tahun."%' AND a.non_create_sk = 'FALSE'";
                    
                    $sns_mgbng_sql = oci_parse($conn, $sns_mgbng_query);
                    oci_execute($sns_mgbng_sql);
                    $sns_mgbng_data = oci_fetch_array($sns_mgbng_sql, OCI_ASSOC+OCI_RETURN_NULLS);
                    $sns_count = $sns_count + (int)$sns_mgbng_data['JMLH_PMP_MD_SNS_MGBNG'];

                $array['JMLH_PMP_MD_SK'] = $sns_count;
                $sns_sum = $sns_sum + $sns_count;
            }
            elseif ($i == 2) {
                $sns_count = 0;

                // Hapus //
                    $sns_hapus_query = "SELECT COUNT(a.n_i_k) JMLH_PMP_MD_SNS_HAPUS FROM HAPUS_PENERIMA_MP a WHERE a.state = 'Disetujui' AND a.tgl_meninggal IS NOT NULL AND a.kd_periode LIKE '".$tahun."%'";
                            
                    $sns_hapus_sql = oci_parse($conn, $sns_hapus_query);
                    oci_execute($sns_hapus_sql);
                    $sns_hapus_data = oci_fetch_array($sns_hapus_sql, OCI_ASSOC+OCI_RETURN_NULLS);
                    $sns_count = $sns_count + (int)$sns_hapus_data['JMLH_PMP_MD_SNS_HAPUS'];

                // Mutasi PMP //
                    $sns_mpmp_query = "SELECT COUNT(a.n_i_k) JMLH_PMP_MD_SNS_MPMP FROM MUTASI_PENERIMA_MP a WHERE a.state = 'Disetujui' AND a.tgl_meninggal IS NOT NULL AND NVL(a.gabung_mp,'FALSE') != 'TRUE' AND a.kd_periode LIKE '".$tahun."%' AND a.non_create_sk = 'TRUE'";
                        
                    $sns_mpmp_sql = oci_parse($conn, $sns_mpmp_query);
                    oci_execute($sns_mpmp_sql);
                    $sns_mpmp_data = oci_fetch_array($sns_mpmp_sql, OCI_ASSOC+OCI_RETURN_NULLS);
                    $sns_count = $sns_count + (int)$sns_mpmp_data['JMLH_PMP_MD_SNS_MPMP'];

                // Mutasi AW //
                    $sns_maw_query = "SELECT COUNT(a.n_i_k) JMLH_PMP_MD_SNS_MAW FROM MUTASI_PENERIMA_MP_AW a WHERE a.state = 'Disetujui' AND a.tgl_meninggal IS NOT NULL AND a.kd_periode LIKE '".$tahun."%' AND a.non_create_sk = 'TRUE'";
                    
                    $sns_maw_sql = oci_parse($conn, $sns_maw_query);
                    oci_execute($sns_maw_sql);
                    $sns_maw_data = oci_fetch_array($sns_maw_sql, OCI_ASSOC+OCI_RETURN_NULLS);
                    $sns_count = $sns_count + (int)$sns_maw_data['JMLH_PMP_MD_SNS_MAW'];

                // Mutasi Gabung //
                    $sns_mgbng_query = "SELECT COUNT(a.n_i_k) JMLH_PMP_MD_SNS_MGBNG FROM MUTASI_PENGGABUNGAN_MP a WHERE a.state = 'Disetujui' AND a.tgl_meninggal IS NOT NULL AND NVL(a.gabung_mp,'FALSE') = 'TRUE' AND a.kd_periode LIKE '".$tahun."%' AND a.non_create_sk = 'TRUE'";
                    
                    $sns_mgbng_sql = oci_parse($conn, $sns_mgbng_query);
                    oci_execute($sns_mgbng_sql);
                    $sns_mgbng_data = oci_fetch_array($sns_mgbng_sql, OCI_ASSOC+OCI_RETURN_NULLS);
                    $sns_count = $sns_count + (int)$sns_mgbng_data['JMLH_PMP_MD_SNS_MGBNG'];

                // Ubah AW //
                    $sns_ubah_aw_query = "SELECT COUNT(a.n_i_k) JMLH_PMP_MD_SNS_UBAH_AW FROM TR_UBAH_AHLI_WARIS a WHERE a.state = 'Disetujui' AND a.tgl_meninggal IS NOT NULL AND a.kd_periode LIKE '".$tahun."%'";
                    
                    $sns_ubah_aw_sql = oci_parse($conn, $sns_ubah_aw_query);
                    oci_execute($sns_ubah_aw_sql);
                    $sns_ubah_aw_data = oci_fetch_array($sns_ubah_aw_sql, OCI_ASSOC+OCI_RETURN_NULLS);
                    $sns_count = $sns_count + (int)$sns_ubah_aw_data['JMLH_PMP_MD_SNS_UBAH_AW'];

                $array['JMLH_PMP_MD_NON_SK'] = $sns_count;
                $sns_sum = $sns_sum + $sns_count;
            }
        }
        $array['TOTAL_PMP_MD_SK_NON_SK'] = $sns_sum;
    }
    elseif ($param == 'pmp_non_pmp') {
        // PMP NON PMP //
        for ($i = 1; $i <= 2; $i++) {
            if ($i == 1) {
                $pnp_count = 0;
                
                // Hapus //
                    $pnp_hapus_query = "SELECT COUNT(a.n_i_k) JMLH_PMP_MD_PNP_HAPUS FROM HAPUS_PENERIMA_MP a WHERE a.state = 'Disetujui' AND a.tgl_meninggal IS NOT NULL AND a.kd_periode LIKE '".$tahun."%'";
                        
                    $pnp_hapus_sql = oci_parse($conn, $pnp_hapus_query);
                    oci_execute($pnp_hapus_sql);
                    $pnp_hapus_data = oci_fetch_array($pnp_hapus_sql, OCI_ASSOC+OCI_RETURN_NULLS);
                    $pnp_count = $pnp_count + (int)$pnp_hapus_data['JMLH_PMP_MD_PNP_HAPUS'];

                // Mutasi PMP //
                    $pnp_mpmp_query = "SELECT COUNT(a.n_i_k) JMLH_PMP_MD_PNP_MPMP FROM MUTASI_PENERIMA_MP a WHERE a.state = 'Disetujui' AND a.tgl_meninggal IS NOT NULL AND NVL(a.gabung_mp,'FALSE') != 'TRUE' AND a.kd_periode LIKE '".$tahun."%'";
                    
                    $pnp_mpmp_sql = oci_parse($conn, $pnp_mpmp_query);
                    oci_execute($pnp_mpmp_sql);
                    $pnp_mpmp_data = oci_fetch_array($pnp_mpmp_sql, OCI_ASSOC+OCI_RETURN_NULLS);
                    $pnp_count = $pnp_count + (int)$pnp_mpmp_data['JMLH_PMP_MD_PNP_MPMP'];

                // Mutasi AW //
                    $pnp_maw_query = "SELECT COUNT(a.n_i_k) JMLH_PMP_MD_PNP_MAW FROM MUTASI_PENERIMA_MP_AW a WHERE a.state = 'Disetujui' AND a.tgl_meninggal IS NOT NULL AND a.kd_periode LIKE '".$tahun."%'";
                    
                    $pnp_maw_sql = oci_parse($conn, $pnp_maw_query);
                    oci_execute($pnp_maw_sql);
                    $pnp_maw_data = oci_fetch_array($pnp_maw_sql, OCI_ASSOC+OCI_RETURN_NULLS);
                    $pnp_count = $pnp_count + (int)$pnp_maw_data['JMLH_PMP_MD_PNP_MAW'];

                // Mutasi Gabung //
                    $pnp_mgbng_query = "SELECT COUNT(a.n_i_k) JMLH_PMP_MD_PNP_MGBNG FROM MUTASI_PENGGABUNGAN_MP a WHERE a.state = 'Disetujui' AND a.tgl_meninggal IS NOT NULL AND NVL(a.gabung_mp,'FALSE') = 'TRUE' AND a.kd_periode LIKE '".$tahun."%'";
                    
                    $pnp_mgbng_sql = oci_parse($conn, $pnp_mgbng_query);
                    oci_execute($pnp_mgbng_sql);
                    $pnp_mgbng_data = oci_fetch_array($pnp_mgbng_sql, OCI_ASSOC+OCI_RETURN_NULLS);
                    $pnp_count = $pnp_count + (int)$pnp_mgbng_data['JMLH_PMP_MD_PNP_MGBNG'];

                $array['JMLH_PMP_MD_PMP'] = $pnp_count;
                $pnp_sum = $pnp_sum + $pnp_count;
            }
            elseif ($i == 2) {
                $pnp_count = 0;

                // Ubah AW //
                    $pnp_ubah_aw_query = "SELECT COUNT(a.n_i_k) JMLH_PMP_MD_PNP_UBAH_AW FROM TR_UBAH_AHLI_WARIS a WHERE a.state = 'Disetujui' AND a.tgl_meninggal IS NOT NULL AND a.kd_periode LIKE '".$tahun."%'";
                    
                    $pnp_ubah_aw_sql = oci_parse($conn, $pnp_ubah_aw_query);
                    oci_execute($pnp_ubah_aw_sql);
                    $pnp_ubah_aw_data = oci_fetch_array($pnp_ubah_aw_sql, OCI_ASSOC+OCI_RETURN_NULLS);
                    $pnp_count = $pnp_count + (int)$pnp_ubah_aw_data['JMLH_PMP_MD_PNP_UBAH_AW'];

                $array['JMLH_PMP_MD_NON_PMP'] = $pnp_count;
                $pnp_sum = $pnp_sum + $pnp_count;
            }
        }
        $array['TOTAL_PMP_MD_PMP_NON_PMP'] = $pnp_sum;
    }
    elseif ($param == 'jns_aw') {
        for ($i = 1; $i <= 5; $i++) {
            $jns_aw_count = 0;

        // $jns_aw //
            if ($i == 1) {
                $jns_aw = 'PESERTA';
                $where_jns_aw = "AND a.jenis_ahli_waris = 'Peserta'";
            } elseif ($i == 2) {
                $jns_aw = 'SUAMI';
                $where_jns_aw = "AND a.jenis_ahli_waris = 'Suami'";
            } elseif ($i == 3) {
                $jns_aw = 'ISTRI';
                $where_jns_aw = "AND a.jenis_ahli_waris = 'Istri'";
            } elseif ($i == 4) {
                $jns_aw = 'ANAK';
                $where_jns_aw = "AND a.jenis_ahli_waris = 'Anak'";
            } elseif ($i == 5) {
                $jns_aw = 'WALI';
                $where_jns_aw = "AND a.jenis_ahli_waris = 'Wali'";
            }
        // -- //

        // Hapus //
            $jns_aw_hapus_query = "SELECT COUNT(a.n_i_k) JMLH_PMP_MD_BULAN_HAPUS FROM HAPUS_PENERIMA_MP a WHERE a.state = 'Disetujui' AND a.tgl_meninggal IS NOT NULL AND a.kd_periode LIKE '".$tahun."%' $where_jns_aw";
            
            $jns_aw_hapus_sql = oci_parse($conn, $jns_aw_hapus_query);
            oci_execute($jns_aw_hapus_sql);
            $jns_aw_hapus_data = oci_fetch_array($jns_aw_hapus_sql, OCI_ASSOC+OCI_RETURN_NULLS);
            $jns_aw_count = $jns_aw_count + (int)$jns_aw_hapus_data['JMLH_PMP_MD_BULAN_HAPUS'];

        // Mutasi PMP //
            $jns_aw_mpmp_query = "SELECT COUNT(a.n_i_k) JMLH_PMP_MD_BULAN_MPMP FROM MUTASI_PENERIMA_MP a WHERE a.state = 'Disetujui' AND a.tgl_meninggal IS NOT NULL AND NVL(a.gabung_mp,'FALSE') != 'TRUE' AND a.kd_periode LIKE '".$tahun."%' $where_jns_aw";
            
            $jns_aw_mpmp_sql = oci_parse($conn, $jns_aw_mpmp_query);
            oci_execute($jns_aw_mpmp_sql);
            $jns_aw_mpmp_data = oci_fetch_array($jns_aw_mpmp_sql, OCI_ASSOC+OCI_RETURN_NULLS);
            $jns_aw_count = $jns_aw_count + (int)$jns_aw_mpmp_data['JMLH_PMP_MD_BULAN_MPMP'];

        // Mutasi AW //
            $jns_aw_maw_query = "SELECT COUNT(a.n_i_k) JMLH_PMP_MD_BULAN_MAW FROM MUTASI_PENERIMA_MP_AW a WHERE a.state = 'Disetujui' AND a.tgl_meninggal IS NOT NULL AND a.kd_periode LIKE '".$tahun."%' $where_jns_aw";
            
            $jns_aw_maw_sql = oci_parse($conn, $jns_aw_maw_query);
            oci_execute($jns_aw_maw_sql);
            $jns_aw_maw_data = oci_fetch_array($jns_aw_maw_sql, OCI_ASSOC+OCI_RETURN_NULLS);
            $jns_aw_count = $jns_aw_count + (int)$jns_aw_maw_data['JMLH_PMP_MD_BULAN_MAW'];

        // Mutasi Gabung //
            $jns_aw_mgbng_query = "SELECT COUNT(a.n_i_k) JMLH_PMP_MD_BULAN_MGBNG FROM MUTASI_PENGGABUNGAN_MP a WHERE a.state = 'Disetujui' AND a.tgl_meninggal IS NOT NULL AND NVL(a.gabung_mp,'FALSE') = 'TRUE' AND a.kd_periode LIKE '".$tahun."%' $where_jns_aw";
            
            $jns_aw_mgbng_sql = oci_parse($conn, $jns_aw_mgbng_query);
            oci_execute($jns_aw_mgbng_sql);
            $jns_aw_mgbng_data = oci_fetch_array($jns_aw_mgbng_sql, OCI_ASSOC+OCI_RETURN_NULLS);
            $jns_aw_count = $jns_aw_count + (int)$jns_aw_mgbng_data['JMLH_PMP_MD_BULAN_MGBNG'];

        // Ubah AW //
            $jns_aw_ubah_aw_query = "SELECT COUNT(a.n_i_k) JMLH_PMP_MD_BULAN_UBAH_AW FROM TR_UBAH_AHLI_WARIS a WHERE a.state = 'Disetujui' AND a.tgl_meninggal IS NOT NULL AND a.kd_periode LIKE '".$tahun."%' $where_jns_aw";
            
            $jns_aw_ubah_aw_sql = oci_parse($conn, $jns_aw_ubah_aw_query);
            oci_execute($jns_aw_ubah_aw_sql);
            $jns_aw_ubah_aw_data = oci_fetch_array($jns_aw_ubah_aw_sql, OCI_ASSOC+OCI_RETURN_NULLS);
            $jns_aw_count = $jns_aw_count + (int)$jns_aw_ubah_aw_data['JMLH_PMP_MD_BULAN_UBAH_AW'];
        // -- //
            
            $array['JMLH_PMP_MD_'.$jns_aw] = $jns_aw_count;
            $jns_aw_sum = $jns_aw_sum + $jns_aw_count;
        }
        $array['TOTAL_PMP_MD_JNS_AW'] = $jns_aw_sum;
    }

    return $array;
} //[] jmlhPmpMeninggalAll []//

function jmlhPmpByrMp($periode, $bank, $p2tel, $kategori, $jns_api) {
    $conn = getConnection();
    $query_sum = "";
    $where_query = "";
    $dtl_rekap_mp_join = "";

    if ($bank != '') {
        $where_query = $where_query." AND b.kd_bank = '".$bank."'";
    }

    /*
        //if ($status != '') {
        //    $query_where = $query_where."";
        //}
        //if ($mp_netto != '') {
        //    $query_where = $query_where." AND (NVL(a.mpb,0) + NVL(a.rapel,0)) = '".$mp_netto."'";
        //}
    */

    if ($p2tel != '') {
        $where_query = $where_query." AND KELURAHAN_API.Get_P2tel_Id2(ALAMAT_PENERIMA_MP_API.Get_Kelurahan_Id(a.company,a.no_ahli_waris,a.n_i_k,ALAMAT_PENERIMA_MP_API.Get_No_Urut_Default(a.company,a.n_i_k,a.no_ahli_waris))) = '".$p2tel."'";
    }
    if ($kategori != '') {
        if (strtoupper($kategori) == 1) { //MPB
            $where_query = $where_query." AND a.kd_jenis_proses = '1'";
        }
        elseif (strtoupper($kategori) == 2) { //THT/MP20
            $where_query = $where_query." AND (a.tht <> 0 OR a.mp20 <> 0)";
        }
        elseif (strtoupper($kategori) == 3) { //MPS
            $where_query = $where_query." AND a.kd_jenis_proses LIKE '5%'";
        }
    }

    /*
        //if ($pph21_dibayar != '') {
        //    $query_where = $query_where." AND NVL(a.pajak,0) = '".$pph21_dibayar."'";
        //}
        //if ($hasil_bruto != '') {
        //    $query_where = $query_where." AND (NVL(a.mpb,0) + NVL(a.rapel,0) + NVL(a.pajak,0)) = '".$hasil_bruto."'";
        //}
        //if ($no_sppb != '') {
        //    $query_where = $query_where." AND c.no_spp = '".$no_sppb."'";
        //    $dtl_rekap_mp_join = $dtl_rekap_mp_join." JOIN DTL_REKAP_MP c ON b.company = c.company AND b.tipe_proses = c.tipe_proses AND b.kd_periode = c.kd_periode";
        //}

        //$query = "SELECT SUM(a.nominal_transfer) JMLH_NILAI_BYR_MP FROM PENERIMAAN_MP_OV a JOIN REKAP_MP b ON a.company = b.company AND  a.kd_periode = b.kd_periode AND a.kd_jenis_proses = b.tipe_proses $dtl_rekap_mp_join WHERE a.kd_periode = '".$periode."' $query_where";
    */

    if ($jns_api == 'jmlh') {
        $query = "SELECT COUNT(a.n_i_k) JMLH_PMP, SUM(a.nominal_transfer) JMLH_NILAI_BYR_MP FROM PENERIMAAN_MP a, REKENING_PENERIMA_MP b WHERE a.company = b.company AND a.n_i_k = b.n_i_k AND a.no_ahli_waris = b.no_ahli_waris AND PERIODE_API.Get_Tanggal_Awal(a.company,a.kd_periode) BETWEEN b.valid_from AND b.valid_to AND a.kd_periode = '".$periode."' $where_query";

        $sql = oci_parse($conn, $query);
        oci_execute($sql);
        
        while ($dt = oci_fetch_assoc($sql)) {
            $result[] = $dt;
        }
    }
    elseif ($jns_api == 'list') {
        $query = "SELECT a.n_i_k, a.no_ahli_waris, CALON_PENERIMA_MP_API.Get_Nama_Ahli_Waris(a.company,a.n_i_k,a.no_ahli_waris) nama, CALON_PENERIMA_MP_API.Get_Jenis_Ahli_Waris(a.company,a.n_i_k,a.no_ahli_waris) JENIS_AW, a.kd_jenis_pensiun, PESERTA_API.Get_Kd_Kelompok(a.company,a.n_i_k) kd_kelompok, KELOMPOK_PESERTA_API.Get_Nama_Kelompok(a.company,PESERTA_API.Get_Kd_Kelompok(a.company,a.n_i_k)) nama_kelompok, KELOMPOK_PESERTA_API.Get_Kelompok_Dapen(a.company,PESERTA_API.Get_Kd_Kelompok(a.company,a.n_i_k)) kelompok_dapen, b.kd_bank, PAYMENT_INSTITUTE_API.Get_Description(b.company,b.kd_bank) nama_bank, b.kd_cab kd_cabang, PAYMENT_INSTITUTE_OFFICE_API.Get_Description(b.company,b.kd_bank,b.kd_cab) nama_cabang, a.nominal_transfer nilai_bayar_mp FROM PENERIMAAN_MP a, REKENING_PENERIMA_MP b WHERE a.company = b.company AND a.n_i_k = b.n_i_k AND a.no_ahli_waris = b.no_ahli_waris AND PERIODE_API.Get_Tanggal_Awal(a.company,a.kd_periode) BETWEEN b.valid_from AND b.valid_to AND a.kd_periode = '".$periode."' $where_query";

        $sql = oci_parse($conn, $query);
        oci_execute($sql);
        
        while ($dt = oci_fetch_assoc($sql)) {
            $result[] = $dt;
        }
    }
    
    return $result;
} //[] jmlhPmpByrMp []//

function jmlhPmpByrMpAll($periode, $parameter) {
    $conn = getConnection();
    $where_query = "";
    $count1 = array();

    if ($parameter == 1) { //KD_BANK
        $bank_array = array("BRI", "MANDIRI", "BNI", "BTPN", "BHS", "TUNAI"); //6

        for ($i = 0; $i < 6; $i++) {
            if ($i == 0) { //BRI
                $where_query = " AND b.kd_bank = '002'";
            }
            if ($i == 1) { //MANDIRI
                $where_query = " AND b.kd_bank = '008'";
            }
            if ($i == 2) { //BNI
                $where_query = " AND b.kd_bank = '009'";
            }
            if ($i == 3) { //BTPN
                $where_query = " AND b.kd_bank = '213'";
            }
            if ($i == 4) { // BHS
                $where_query = " AND b.kd_bank NOT IN ('002','008','009','213','998','999')";
            }
            if ($i == 5) { // TUNAI
                $where_query = " AND b.kd_bank IN ('998','999')";
            }

            $query2 = "SELECT COUNT(a.n_i_k) JMLH_PMP, SUM(a.nominal_transfer) JMLH_NILAI_BYR_MP FROM PENERIMAAN_MP a, REKENING_PENERIMA_MP b WHERE a.company = b.company AND a.n_i_k = b.n_i_k AND a.no_ahli_waris = b.no_ahli_waris AND PERIODE_API.Get_Tanggal_Awal(a.company,a.kd_periode) BETWEEN b.valid_from AND b.valid_to AND a.kd_periode = '".$periode."' $where_query";
            
            $sql2 = oci_parse($conn, $query2);
            oci_execute($sql2);
            
            $count = array();
            while ($dt = oci_fetch_assoc($sql2)) {
                $count[] = $dt;
            }
            
            $count1[$bank_array[$i]] = $count;
        }
    }
    elseif ($parameter == 2) { //P2TEL
        
        $p2tel_query = "SELECT t.p2tel_id P2TEL_ID FROM KELURAHAN_TAB t GROUP BY t.p2tel_id ORDER BY t.p2tel_id";
        
        $p2tel_sql = oci_parse($conn, $p2tel_query);
        oci_execute($p2tel_sql);
        
        $p2tel_count = array();
        while ($bank_dt = oci_fetch_assoc($p2tel_sql)) {
            $p2tel_count[] = $bank_dt;
        }
        
        $p2tel_row = oci_num_rows($p2tel_sql);
        $p2tel_array = $p2tel_count;
        
        /*
            $p2tel_array2 = array("ACEH", "AMBON", "BALEENDAH", "BALIKPAPAN", "BANJARMASIN", "BANYUWANGI", "BATURAJA", "BDG1", "BDG2", "BDG3", "BDG4", "BDG5", "BENGKULU", "BIAK", "BINJAI", "BKSBAR", "BKSTIM", "BLITAR", "BOGOR", "BOJONEGORO", "BONDOWOSO", "BUKITTINGGI", "CEPU", "CIANJUR", "CIMAHI", "CIREBON", "DAPEN151", "DENPASAR", "DEPOK", "ENDE", "GARUT", "GORONTALO", "JAMBI", "JAYAPURA", "JEMBER", "JKT1", "JKT3", "JKT4", "JOMBANG", "KEDIRI", "KENDARI", "KISARAN", "KUDUS", "KUPANG", "LAMPUNG", "LHOKSEU", "LUARNEGERI", "MADIUN", "MAGELANG", "MAKASSAR", "MALANG", "MANADO", "MANOKWARI", "MATARAM", "MEDAN1", "MEDAN2", "MERAUKE", "MOJOKERTO", "PADANG", "PALEMBANG", "PALU", "PAMEKASAN", "PARE", "PASURUAN", "PEKALONGAN", "PONTIANAK", "PROBOLINGGO", "PURWOKERTO", "RIDAR", "RIKEP", "SAMARINDA", "SBY3", "SBY5", "SERANG", "SIANTAR", "SIBOLGA", "SIDOARJO", "SINGARAJA", "SMG2", "SMG4", "SOLO", "SORONG", "SUBANG", "SUKABUMI", "SUMEDANG", "TANGERANG", "TANGSEL", "TASIKMALAYA", "TEGAL", "TERNATE", "TULUNGAGUNG", "UBER", "YOGYAKARTA");
        */

        for ($i = 0; $i < count($p2tel_array); $i++) {
            //$where_query = " AND UPPER(ALAMAT_PENERIMA_MP_API.Get_P2TEL_default(a.company,a.n_i_k,a.no_ahli_waris,PERIODE_API.Get_Tanggal_Awal(a.company,a.kd_periode))) = UPPER('".$p2tel_array2[$i]."')";

            $query2 = "SELECT COUNT(a.n_i_k) JMLH_PMP, SUM(a.nominal_transfer) JMLH_NILAI_BYR_MP FROM PENERIMAAN_MP a, REKENING_PENERIMA_MP b WHERE a.company = b.company AND a.n_i_k = b.n_i_k AND a.no_ahli_waris = b.no_ahli_waris AND (PERIODE_API.Get_Tanggal_Awal(a.company, a.kd_periode) BETWEEN b.valid_from AND b.valid_to) AND a.kd_periode = '".$periode."' AND UPPER(ALAMAT_PENERIMA_MP_API.Get_P2TEL_default(a.company,a.n_i_k,a.no_ahli_waris,PERIODE_API.Get_Tanggal_Awal(a.company,a.kd_periode))) = UPPER('".$p2tel_array[$i]['P2TEL_ID']."')";
            
            $sql2 = oci_parse($conn, $query2);
            oci_execute($sql2);
            
            $count = array();
            while ($dt = oci_fetch_assoc($sql2)) {
                $count[] = $dt;
            }
            
            $count1[$p2tel_array[$i]['P2TEL_ID']] = $count;
        }
    }
    elseif ($parameter == 3) { //KATEGORI
        $kategori_array = array("MPB", "THT/MP20", "MPS"); //3

        for ($i = 0; $i < 3; $i++) {
            if ($i == 0) { //MPB
                $where_query = " AND a.kd_jenis_proses = '1'";
            }
            if ($i == 1) { //THT/MP20
                $where_query = " AND (a.tht <> 0 OR a.mp20 <> 0)";
            }
            if ($i == 2) { //MPS
                $where_query = " AND a.kd_jenis_proses LIKE '5%'";
            }

            $query2 = "SELECT COUNT(a.n_i_k) JMLH_PMP, SUM(a.nominal_transfer) JMLH_NILAI_BYR_MP FROM PENERIMAAN_MP a, REKENING_PENERIMA_MP b WHERE a.company = b.company AND a.n_i_k = b.n_i_k AND a.no_ahli_waris = b.no_ahli_waris AND PERIODE_API.Get_Tanggal_Awal(a.company,a.kd_periode) BETWEEN b.valid_from AND b.valid_to AND a.kd_periode = '".$periode."' $where_query";
            
            $sql2 = oci_parse($conn, $query2);
            oci_execute($sql2);
            
            $count = array();
            while ($dt = oci_fetch_assoc($sql2)) {
                $count[] = $dt;
            }
            
            $count1[$kategori_array[$i]] = $count;
        }
    }

    return $count1;
} //[] jmlhPmpByrMpAll []//

function jmlhPmpMp($periode, $bank, $status, $p2tel, $kategori, $jns_api) {
    $conn = getConnection();
    $query_sum = "";
    $query_where = "";
    $dtl_rekap_mp_join = "";
    $query_select = "";

    if ($bank != '') {
        $query_where = $query_where." AND a.kode_bank = '".$bank."'";
    }
    if ($status != '') {
        $query_where = $query_where."";
    }
    if ($p2tel != '') {
        $query_where = $query_where." AND KELURAHAN_API.Get_P2tel_Id2(ALAMAT_PENERIMA_MP_API.Get_Kelurahan_Id(a.company,a.no_ahli_waris,a.n_i_k,ALAMAT_PENERIMA_MP_API.Get_No_Urut_Default(a.company,a.n_i_k,a.no_ahli_waris))) = '".$p2tel."'";
    }
    if ($kategori != '') {
        if (strtoupper($kategori) == 'MPB') {
            $query_where = $query_where." AND a.kd_jenis_proses = '1'";

            if ($jns_api == 'jmlh') {
                $query_select = $query_select."SUM((NVL(a.mpb,0) + NVL(a.rapel,0))) mp_netto, SUM(NVL(a.pajak,0)) pph21_dibayar, SUM((NVL(a.mpb,0) + NVL(a.rapel,0) + NVL(a.pajak,0))) hasil_bruto";
            } else {
                $query_select = $query_select."(NVL(a.mpb,0) + NVL(a.rapel,0)) mp_netto, NVL(a.pajak,0) pph21_dibayar, (NVL(a.mpb,0) + NVL(a.rapel,0) + NVL(a.pajak,0)) hasil_bruto";
            }
        }
        elseif (strtoupper($kategori) == 'THT/MP20') {
            $query_where = $query_where." AND (a.tht <> 0 OR a.mp20 <> 0)";
            $dtl_rekap_mp_join = $dtl_rekap_mp_join." JOIN DTL_REKAP_MP c ON b.company = c.company AND b.tipe_proses = c.tipe_proses AND b.kd_periode = c.kd_periode";

            if ($jns_api == 'jmlh') {
                $query_select = $query_select."SUM((NVL(NVL(a.tht,a.mp20),0))) mp_netto, SUM(NVL(a.pajak,0)) pph21_dibayar, SUM((NVL(NVL(a.tht,a.mp20),0) + NVL(a.pajak,0))) hasil_bruto"; //, c.no_spp no_sppb
            } else {
                $query_select = $query_select."(NVL(NVL(a.tht,a.mp20),0)) mp_netto, NVL(a.pajak,0) pph21_dibayar, (NVL(NVL(a.tht,a.mp20),0) + NVL(a.pajak,0)) hasil_bruto, c.no_spp no_sppb";
            }
        }
        elseif (strtoupper($kategori) == 'MPS') {
            $query_where = $query_where." AND a.kd_jenis_proses LIKE '5%'";
            $dtl_rekap_mp_join = $dtl_rekap_mp_join." JOIN DTL_REKAP_MP c ON b.company = c.company AND b.tipe_proses = c.tipe_proses AND b.kd_periode = c.kd_periode";

            if ($jns_api == 'jmlh') {
                $query_select = $query_select."SUM((NVL(a.mps,0))) mp_netto, SUM(NVL(a.pajak,0)) pph21_dibayar, SUM((NVL(a.mps,0) + NVL(a.pajak,0))) hasil_bruto"; //, c.no_spp no_sppb
            } else {
                $query_select = $query_select."(NVL(a.mps,0)) mp_netto, NVL(a.pajak,0) pph21_dibayar, (NVL(a.mps,0) + NVL(a.pajak,0)) hasil_bruto, c.no_spp no_sppb";
            }
        }
    }

    if ($jns_api == 'jmlh') {
        $query = "SELECT a.kd_periode, COUNT(a.n_i_k) jmlh_pmp, $query_select FROM PENERIMAAN_MP_OV a JOIN REKAP_MP b ON a.company = b.company AND a.kd_periode = b.kd_periode AND a.kd_jenis_proses = b.tipe_proses $dtl_rekap_mp_join WHERE a.kd_periode LIKE '".$periode."%' $query_where GROUP BY a.kd_periode ORDER BY a.kd_periode";

        $sql = oci_parse($conn, $query);
        oci_execute($sql);
        
        while ($dt = oci_fetch_assoc($sql)) {
            $result[] = $dt;
        }
    }
    elseif ($jns_api == 'list') {
        $query = "SELECT a.n_i_k, a.no_ahli_waris, CALON_PENERIMA_MP_API.Get_Nama_Ahli_Waris(a.company,a.n_i_k,a.no_ahli_waris) nama, CALON_PENERIMA_MP_API.Get_Jenis_Ahli_Waris(a.company,a.n_i_k,a.no_ahli_waris) JENIS_AW, a.kd_jenis_pensiun, PESERTA_API.Get_Kd_Kelompok(a.company,a.n_i_k) kd_kelompok, KELOMPOK_PESERTA_API.Get_Nama_Kelompok(a.company,PESERTA_API.Get_Kd_Kelompok(a.company,a.n_i_k)) nama_kelompok, KELOMPOK_PESERTA_API.Get_Kelompok_Dapen(a.company, PESERTA_API.Get_Kd_Kelompok(a.company,a.n_i_k)) kelompok_dapen, a.kode_bank, a.nama_bank, a.kode_cabang, a.nama_cabang, $query_select FROM PENERIMAAN_MP_OV a JOIN REKAP_MP b ON a.company = b.company AND a.kd_periode = b.kd_periode AND a.kd_jenis_proses = b.tipe_proses $dtl_rekap_mp_join WHERE a.kd_periode = '".$periode."' $query_where";

        $sql = oci_parse($conn, $query);
        oci_execute($sql);
        
        while ($dt = oci_fetch_assoc($sql)) {
            $result[] = $dt;
        }
    }
    
    return $result;
} //[] jmlhPmpMp []//

function jmlhPmpByrMt($jns_api, $periode, $param_bank, $p2tel) {
    $conn = getConnection();

    $where_query = "";

    if ($param_bank != '') {
        if ($param_bank == 1) { //BRI
            $where_query = $where_query." AND b.kd_bank = '002'";
        }
        elseif ($param_bank == 2) { //MANDIRI
            $where_query = $where_query." AND b.kd_bank = '008'";
        }
        elseif ($param_bank == 3) { //BNI
            $where_query = $where_query." AND b.kd_bank = '009'";
        }
        elseif ($param_bank == 4) { //BTPN
            $where_query = $where_query." AND d.kd_bank = '213'";
        }
        elseif ($param_bank == 5) { //BHS
            $where_query = $where_query." AND d.kd_koordinator = 'BHS'";
        }
        elseif ($param_bank == 6) { //TUNAI
            $where_query = $where_query." AND d.kd_koordinator = 'TUNAI'";
        }
    }
    if ($p2tel != '') {
        $where_query = $where_query." AND UPPER(KELURAHAN_API.Get_P2tel_Id2(ALAMAT_PENERIMA_MP_API.Get_Kelurahan_Id(a.company,a.no_ahli_waris,a.n_i_k,ALAMAT_PENERIMA_MP_API.Get_No_Urut_Default(a.company,a.n_i_k,a.no_ahli_waris)))) = UPPER('".$p2tel."')";
    }

    if ($jns_api == 'jmlh') {
        $query = "SELECT COUNT(a.n_i_k) JMLH_PENERIMA_MT, SUM(a.nominal) JMLH_NILAI_BAYAR_MT FROM MANFAAT_TAMBAHAN a, rekening_penerima_mp_tab b, bank_koordinator_tab c, dtl_bank_koordinator_tab d WHERE a.company = b.company AND a.n_i_k = b.n_i_k AND a.no_ahli_waris = b.no_ahli_waris AND b.company = c.company AND b.company = d.company and b.institute_id = d.kd_bank AND d.company = c.company and d.kd_koordinator = c.kd_koordinator AND CASE WHEN PENERIMAAN_MP_API.Get_Kd_Jenis_Pensiun(a.company,a.n_i_k,a.no_ahli_waris,a.kd_periode,a.kd_jenis_manfaat) = '1' THEN PERIODE_API.Get_Tanggal_Awal(a.company,a.kd_periode) ELSE PERIODE_API.Get_Tanggal_Akhir(a.company,a.kd_periode) END BETWEEN b.valid_from AND b.valid_to AND a.kd_periode = '".$periode."' $where_query";
    }
    elseif ($jns_api == 'list') {
        $query = "SELECT a.n_i_k, a.no_ahli_waris, PESERTA_API.Get_Nama_Pegawai(a.company,a.n_i_k) NAMA, CALON_PENERIMA_MP_API.Get_Jenis_Ahli_Waris(a.company,a.n_i_k,a.no_ahli_waris) JENIS_AW, PENERIMA_MP_API.Get_KdJenisPensiun(a.company,a.n_i_k,a.no_ahli_waris) KD_JENIS_PENSIUN, KELOMPOK_PESERTA_API.Get_Kelompok_Dapen(a.company,PESERTA_API.Get_Kd_Kelompok(a.company,a.n_i_k)) Kelompok_Dapen, KELOMPOK_PESERTA_API.Get_Nama_Kelompok(a.company,PESERTA_API.Get_Kd_Kelompok(a.company,a.n_i_k)) NAMA_KELOMPOK, PESERTA_API.Get_Kd_Kelompok(a.company,a.n_i_k) KD_KELOMPOK, a.nominal JMLH_NILAI_BAYAR_MT FROM MANFAAT_TAMBAHAN a, rekening_penerima_mp_tab b, bank_koordinator_tab c, dtl_bank_koordinator_tab d WHERE a.company = b.company AND a.n_i_k = b.n_i_k AND a.no_ahli_waris = b.no_ahli_waris AND b.company = c.company AND b.company = d.company and b.institute_id = d.kd_bank AND d.company = c.company and d.kd_koordinator = c.kd_koordinator AND CASE WHEN PENERIMAAN_MP_API.Get_Kd_Jenis_Pensiun(a.company,a.n_i_k,a.no_ahli_waris,a.kd_periode,a.kd_jenis_manfaat) = '1' THEN PERIODE_API.Get_Tanggal_Awal(a.company,a.kd_periode) ELSE PERIODE_API.Get_Tanggal_Akhir(a.company,a.kd_periode) END BETWEEN b.valid_from AND b.valid_to AND a.kd_periode = '".$periode."' $where_query";
    }

    $sql = oci_parse($conn, $query);
    oci_execute($sql);
    
    while ($dt = oci_fetch_assoc($sql)) {
        $result[] = $dt;
    }

    return $result;
} //[] jmlhByrMt []//

function jmlhPmpByrMtAll($periode, $parameter) {
    $conn = getConnection();
    $where_query = "";
    $count1 = array();

    if ($parameter == 1) { //KD_BANK
        $bank_array = array("BRI", "MANDIRI", "BNI", "BTPN", "BHS", "TUNAI"); //6

        for ($i = 0; $i < 6; $i++) {
            if ($i == 0) { //BRI
                $where_query = " AND b.kd_bank = '002'";
            }
            if ($i == 1) { //MANDIRI
                $where_query = " AND b.kd_bank = '008'";
            }
            if ($i == 2) { //BNI
                $where_query = " AND b.kd_bank = '009'";
            }
            if ($i == 3) { //BTPN
                $where_query = " AND b.kd_bank = '213'";
            }
            if ($i == 4) { // BHS
                $where_query = " AND d.kd_koordinator = 'BHS'";
            }
            if ($i == 5) { // TUNAI
                $where_query = " AND d.kd_koordinator = 'TUNAI'";
            }

            $query2 = "SELECT COUNT(a.n_i_k) JMLH_PENERIMA_MT, SUM(a.nominal) JMLH_NILAI_BAYAR_MT FROM MANFAAT_TAMBAHAN a, rekening_penerima_mp_tab b, bank_koordinator_tab c, dtl_bank_koordinator_tab d WHERE a.company = b.company AND a.n_i_k = b.n_i_k AND a.no_ahli_waris = b.no_ahli_waris AND b.company = c.company AND b.company = d.company and b.institute_id = d.kd_bank AND d.company = c.company and d.kd_koordinator = c.kd_koordinator AND CASE WHEN PENERIMAAN_MP_API.Get_Kd_Jenis_Pensiun(a.company,a.n_i_k,a.no_ahli_waris,a.kd_periode,a.kd_jenis_manfaat) = '1' THEN PERIODE_API.Get_Tanggal_Awal(a.company,a.kd_periode) ELSE PERIODE_API.Get_Tanggal_Akhir(a.company,a.kd_periode) END BETWEEN b.valid_from AND b.valid_to AND a.kd_periode = '".$periode."' $where_query";
            
            $sql2 = oci_parse($conn, $query2);
            oci_execute($sql2);
            
            $count = array();
            while ($dt = oci_fetch_assoc($sql2)) {
                $count[] = $dt;
            }
            
            $count1[$bank_array[$i]] = $count;
        }
    }
    elseif ($parameter == 2) { //P2TEL
        $p2tel_query = "SELECT t.p2tel_id P2TEL_ID FROM KELURAHAN_TAB t GROUP BY t.p2tel_id ORDER BY t.p2tel_id";

        $p2tel_sql = oci_parse($conn, $p2tel_query);
        oci_execute($p2tel_sql);
        
        $p2tel_count = array();
        while ($bank_dt = oci_fetch_assoc($p2tel_sql)) {
            $p2tel_count[] = $bank_dt;
        }
        
        $p2tel_row = oci_num_rows($p2tel_sql);
        $p2tel_array = $p2tel_count;

        for ($i = 0; $i < $p2tel_row; $i++) {
            $where_query = " AND UPPER(KELURAHAN_API.Get_P2tel_Id2(ALAMAT_PENERIMA_MP_API.Get_Kelurahan_Id(a.company,a.no_ahli_waris,a.n_i_k,ALAMAT_PENERIMA_MP_API.Get_No_Urut_Default(a.company,a.n_i_k,a.no_ahli_waris)))) = UPPER('".$p2tel_array[$i]['P2TEL_ID']."')";

            $query2 = "SELECT COUNT(a.n_i_k) JMLH_PENERIMA_MT, SUM(a.nominal) JMLH_NILAI_BAYAR_MT FROM MANFAAT_TAMBAHAN a, rekening_penerima_mp_tab b, bank_koordinator_tab c, dtl_bank_koordinator_tab d WHERE a.company = b.company AND a.n_i_k = b.n_i_k AND a.no_ahli_waris = b.no_ahli_waris AND b.company = c.company AND b.company = d.company and b.institute_id = d.kd_bank AND d.company = c.company and d.kd_koordinator = c.kd_koordinator AND CASE WHEN PENERIMAAN_MP_API.Get_Kd_Jenis_Pensiun(a.company,a.n_i_k,a.no_ahli_waris,a.kd_periode,a.kd_jenis_manfaat) = '1' THEN PERIODE_API.Get_Tanggal_Awal(a.company,a.kd_periode) ELSE PERIODE_API.Get_Tanggal_Akhir(a.company,a.kd_periode) END BETWEEN b.valid_from AND b.valid_to AND a.kd_periode = '".$periode."' $where_query";
            
            $sql2 = oci_parse($conn, $query2);
            oci_execute($sql2);
            
            $count = array();
            while ($dt = oci_fetch_assoc($sql2)) {
                $count[] = $dt;
            }
            
            $count1[$p2tel_array[$i]['P2TEL_ID']] = $count;
        }
    }

    return $count1;
} //[] jmlhByrMtAll []//

function jmlhSaldoReturMp($periode, $nik, $nominal_retur, $periode_terima, $periode_dibayar) {
    $conn = getConnection();

    $where_query = "";
    $nik_where_query = "";

    if ($nik != '') {
        $where_query = $where_query." AND b.n_i_k = '".$nik."'";
        $nik_where_query = $nik_where_query." AND n_i_k = '".$nik."'";
    }
    if ($nominal_retur != '') {
        $where_query = $where_query." AND b.nominal_retur = '".$nominal_retur."'";
    }
    if ($periode_terima != '') {
        $where_query = $where_query."";
    }
    if ($periode_dibayar != '') {
        $where_query = $where_query." AND TO_CHAR(b.tgl_transaksi,'YYYYMM') = '".$periode_dibayar."'";
    }
    
    $query = "SELECT SUM(b.nominal_transfer) jmlh_saldo_retur_mp FROM RETUR_MP a, DETIL_RETUR_MP b WHERE a.company = b.company AND a.no_referensi = b.no_referensi AND a.kd_periode = '".$periode."' AND b.n_i_k NOT IN (SELECT n_i_k FROM PENYELESAIAN_RETUR WHERE kd_periode = '".$periode."' $nik_where_query) AND b.n_i_k NOT IN (SELECT n_i_k FROM DTL_BAYAR_RETUR WHERE kd_periode = '".$periode."' $nik_where_query) AND UPPER(b.status) <> 'DIKOREKSI' $where_query";
    
    $sql = oci_parse($conn, $query);
    oci_execute($sql);
    $data = oci_fetch_array($sql, OCI_ASSOC+OCI_RETURN_NULLS);
    $count = (int)$data['JMLH_SALDO_RETUR_MP'];

    return $count;
} //[] jmlhSaldoReturMp []//

function jmlhPmpLbhByr($periode, $periode_setuju, $jenis_sbb_lbh_byr, $cab_p2tel, $bank_koor, $nama_bank, $jns_api) {
    $prm_where = "";
    $count2 = 0;
    $periode_bank = "";

    $conn = getConnection();

    if ($periode != '') {
        $prm_where = $prm_where." AND a.state NOT IN ('BatalHapus','Dibatalkan') AND b.kd_periode = '".$periode."'";

        $periode_bank = $periode;
    }
    if ($periode_setuju != '') {
        $prm_where = $prm_where." AND INITCAP(a.state) = 'Disetujui' AND (TO_CHAR(DPT_AUDITTRAIL_API.Get_Tgl('HapusPenerimaMp',a.objid,a.state),'YYYY') || TO_CHAR(DPT_AUDITTRAIL_API.Get_Tgl('HapusPenerimaMp',a.objid,a.state),'MM')) = '".$periode_setuju."'";

        $periode_bank = $periode_setuju;
    }
    if ($jenis_sbb_lbh_byr != '') {
        if ($jenis_sbb_lbh_byr == 1) { //meninggal
            $prm_where = $prm_where." AND a.tgl_meninggal IS NOT NULL";
        }
        elseif ($jenis_sbb_lbh_byr == 2) { //anak
            $prm_where = $prm_where." AND (a.tgl_berhenti_sekolah IS NOT NULL OR a.tgl_menikah IS NOT NULL OR a.tgl_bekerja IS NOT NULL OR a.dewasa = 'TRUE')";
        }
        elseif ($jenis_sbb_lbh_byr == 3) { //menikah lagi
            $prm_where = $prm_where." AND a.tgl_menikah_lagi IS NOT NULL";
        }
    }
    if ($cab_p2tel != '') {
        $prm_where = $prm_where." AND a.mitra_kerja = '".$cab_p2tel."'";
    }
    if ($bank_koor != '') {
        if ($bank_koor == 1) { //BRI
            $prm_where = $prm_where." AND REKENING_PENERIMA_MP_API.Get_Kd_Bank_By_Param(a.company,a.n_i_k,a.no_ahli_waris,PERIODE_API.Get_Tanggal_Awal_Periode(a.company,a.kd_periode)) = '002'";
        }
        elseif ($bank_koor == 2) { //MANDIRI
            $prm_where = $prm_where." AND REKENING_PENERIMA_MP_API.Get_Kd_Bank_By_Param(a.company,a.n_i_k,a.no_ahli_waris,PERIODE_API.Get_Tanggal_Awal_Periode(a.company,a.kd_periode)) = '008'";
        }
        elseif ($bank_koor == 3) { //BNI
            $prm_where = $prm_where." AND REKENING_PENERIMA_MP_API.Get_Kd_Bank_By_Param(a.company,a.n_i_k,a.no_ahli_waris,PERIODE_API.Get_Tanggal_Awal_Periode(a.company,a.kd_periode)) = '009'";
        }
        elseif ($bank_koor == 4) { //BTPN
            $prm_where = $prm_where." AND REKENING_PENERIMA_MP_API.Get_Kd_Bank_By_Param(a.company,a.n_i_k,a.no_ahli_waris,PERIODE_API.Get_Tanggal_Awal_Periode(a.company,a.kd_periode)) = '213'";
        }
        elseif ($bank_koor == 5) { // BHS
            $prm_where = $prm_where." AND REKENING_PENERIMA_MP_API.Get_Kd_Bank_By_Param(a.company,a.n_i_k,a.no_ahli_waris,PERIODE_API.Get_Tanggal_Awal_Periode(a.company,a.kd_periode)) NOT IN ('002','008','009','213','998','999')";
        }
        elseif ($bank_koor == 6) { // TUNAI
            $prm_where = $prm_where." AND REKENING_PENERIMA_MP_API.Get_Kd_Bank_By_Param(a.company,a.n_i_k,a.no_ahli_waris,PERIODE_API.Get_Tanggal_Awal_Periode(a.company,a.kd_periode)) IN ('998','999')";
        }
    }
    if ($nama_bank != '') {
        $prm_where = $prm_where." AND REKENING_PENERIMA_MP_API.Get_Kd_Bank_By_Param(a.company, a.n_i_k, a.no_ahli_waris, PERIODE_API.Get_Tanggal_Awal(a.company, '".$periode_bank."')) = '".$nama_bank."'";
    }

    if ($jns_api == 'jmlh_nilai' && ($periode != '' || $periode_setuju != '')) {
        $query2 = "SELECT SUM(JMLH_PMP_LEBIH_BAYAR) JMLH_PMP_LEBIH_BAYAR, SUM(JMLH_NILAI_LEBIH_BAYAR) JMLH_NILAI_LEBIH_BAYAR FROM (
        -- // -- // -- // -- // -- // --
        SELECT COUNT(*) JMLH_PMP_LEBIH_BAYAR, 0 JMLH_NILAI_LEBIH_BAYAR FROM (SELECT a.n_i_k, a.no_ahli_waris, b.kd_periode FROM HAPUS_PENERIMA_MP a, PENERIMAAN_MP b WHERE a.company = b.company AND a.n_i_k = b.n_i_k AND a.no_ahli_waris = b.no_ahli_waris AND a.kd_periode = b.kd_periode AND b.kd_periode > PERIODE_API.Get_Kd_Periode(a.company,TO_CHAR(a.tgl_berhenti,'YYYY'),TO_CHAR(a.tgl_berhenti,'MM')) $prm_where GROUP BY a.n_i_k, a.no_ahli_waris, b.kd_periode ORDER BY a.n_i_k)
        -- // -- // -- // -- // -- // --
        UNION ALL
        -- // -- // -- // -- // -- // --
        SELECT 0 JMLH_PMP_LEBIH_BAYAR, SUM(nominal_transfer) JMLH_NILAI_LEBIH_BAYAR FROM (SELECT a.n_i_k, a.no_ahli_waris, b.kd_periode, b.nominal_transfer FROM HAPUS_PENERIMA_MP a, PENERIMAAN_MP b WHERE a.company = b.company AND a.n_i_k = b.n_i_k AND a.no_ahli_waris = b.no_ahli_waris AND a.kd_periode = b.kd_periode AND b.kd_periode > PERIODE_API.Get_Kd_Periode(a.company,TO_CHAR(a.tgl_berhenti,'YYYY'),TO_CHAR(a.tgl_berhenti,'MM')) $prm_where GROUP BY a.n_i_k, a.no_ahli_waris, b.kd_periode, b.nominal_transfer ORDER BY a.n_i_k)
        )";

        $sql2 = oci_parse($conn, $query2);
        oci_execute($sql2);

        $count2 = array();
        while ($dt = oci_fetch_assoc($sql2)) {
            $count2[] = $dt;
        }
    }
    elseif ($jns_api == 'list') {
        $query2 = "SELECT n_i_k, no_ahli_waris, CALON_PENERIMA_MP_API.Get_Nama_Ahli_Waris(company,n_i_k,no_ahli_waris) nama, DPT_AUDITTRAIL_API.Get_Tgl('HapusPenerimaMp',objid,'Disetujui') tgl_disetujui, CASE WHEN tgl_meninggal IS NOT NULL THEN 'Meninggal' WHEN tgl_berhenti_sekolah IS NOT NULL OR tgl_menikah IS NOT NULL OR tgl_bekerja IS NOT NULL OR dewasa = 'TRUE' THEN 'Anak' WHEN tgl_menikah_lagi IS NOT NULL THEN 'Janda/Duda Menikah Lagi' END PENYEBAB_LEBIH_BAYAR, KELURAHAN_API.Get_P2tel_Id2(ALAMAT_PENERIMA_MP_API.Get_Kelurahan_Id(company,no_ahli_waris,n_i_k,ALAMAT_PENERIMA_MP_API.Get_No_Urut_Default(company,n_i_k,no_ahli_waris))) CABANG_P2TEL, BANK_KOORDINATOR_API.Get_Keterangan(company,REKENING_PENERIMA_MP_API.Get_Kd_Bank_By_Param(company,n_i_k,no_ahli_waris,PERIODE_API.Get_Tanggal_Awal_Periode(company,kd_periode))) BANK_KOOR, PAYMENT_INSTITUTE_API.Get_Description(company,REKENING_PENERIMA_MP_API.Get_Kd_Bank_By_Param(company,n_i_k,no_ahli_waris,PERIODE_API.Get_Tanggal_Awal_Periode(company,kd_periode))) NAMA_BANK, nominal_transfer JMLH_KELEBIHAN_BAYAR FROM (
        SELECT a.n_i_k, a.no_ahli_waris, b.kd_periode, b.nominal_transfer, a.company, a.objid, a.tgl_meninggal, a.tgl_berhenti_sekolah, a.tgl_menikah, a.tgl_bekerja, a.dewasa, a.tgl_menikah_lagi FROM HAPUS_PENERIMA_MP a, PENERIMAAN_MP b WHERE a.company = b.company AND a.n_i_k = b.n_i_k AND a.no_ahli_waris = b.no_ahli_waris AND a.kd_periode = b.kd_periode AND b.kd_periode > PERIODE_API.Get_Kd_Periode(a.company,TO_CHAR(a.tgl_berhenti,'YYYY'),TO_CHAR(a.tgl_berhenti, 'MM')) $prm_where GROUP BY a.n_i_k, a.no_ahli_waris, b.kd_periode, b.nominal_transfer, a.company, a.objid, a.tgl_meninggal, a.tgl_berhenti_sekolah, a.tgl_menikah, a.tgl_bekerja, a.dewasa, a.tgl_menikah_lagi ORDER BY a.n_i_k
        )";

        $sql2 = oci_parse($conn, $query2);
        oci_execute($sql2);

        $count2 = array();
        while ($dt = oci_fetch_assoc($sql2)) {
            $count2[] = $dt;
        }
    }/*
    elseif ($jns_api == 'nilai') {
        $query2 = "SELECT SUM(b.nominal_transfer) JMLH_NILAI_LBH_BYR FROM HAPUS_PENERIMA_MP a, PENERIMAAN_MP b, REKAP_MP c, REKENING_PENERIMA_MP d WHERE a.company = b.company AND a.n_i_k = b.n_i_k AND a.no_ahli_waris = b.no_ahli_waris AND b.company = d.company AND b.n_i_k = d.n_i_k AND b.no_ahli_waris = d.no_ahli_waris AND b.kd_periode = c.kd_periode AND a.state = 'Disetujui' AND c.tipe_proses = '1' AND c.state = 'SPPB Generated' AND b.kd_periode > PERIODE_API.Get_Kd_Periode(a.company,TO_CHAR(a.tgl_berhenti, 'YYYY'),TO_CHAR(a.tgl_berhenti, 'MM')) $prm_where";

        $sql2 = oci_parse($conn, $query2);
        oci_execute($sql2);
        $data2 = oci_fetch_array($sql2, OCI_ASSOC+OCI_RETURN_NULLS);
        $count2 = (int)$data2['JMLH_NILAI_LBH_BYR'];
    }*/

    return $count2;
}

function jmlhPmpLbhByrAll($periode_thn, $parameter, $jns_api) {
    $conn = getConnection();

    if ($jns_api == 'jns_sbb') {
        $jns_api_array = array("MENINGGAL", "ANAK", "MENIKAH_LAGI");
    } elseif ($jns_api == 'jns_penerima') {
        $jns_api_array = array("PESERTA", "ISTRI", "SUAMI", "ANAK", "WALI");
    } elseif ($jns_api == 'jns_bank') {
        $bank_query = "SELECT REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(NAMA_BANK,'PT. ',''),'PT.',''),', TBK',''),',TBK',''),' TBK',''),', N.A.',''),' N.A.',''),', LTD',''),'.LTD',''),' CO',''),'.UU',' UU'),'/','_'),'.',''),'(',''),')',''),'''',''),' ','_'),'__','_') NAMA_BANK FROM (SELECT a.kd_bank, a.kd_koordinator, PAYMENT_INSTITUTE_API.Get_Description(a.company,a.kd_bank) NAMA_BANK FROM DTL_BANK_KOORDINATOR a GROUP BY a.kd_bank, a.kd_koordinator, PAYMENT_INSTITUTE_API.Get_Description(a.company,a.kd_bank) ORDER BY PAYMENT_INSTITUTE_API.Get_Description(a.company,a.kd_bank))";

        $bank_sql = oci_parse($conn, $bank_query);
        oci_execute($bank_sql);
        
        $bank_count = array();
        while ($bank_dt = oci_fetch_assoc($bank_sql)) {
            $bank_count[] = $bank_dt;
        }

        $bank_row = oci_num_rows($bank_sql);
        $jns_api_array = $bank_count;
    }

    $where_query = "";
    $count2 = 0;
    $count1 = array();

    if ($parameter == 1) { //transaksi bulan
        $bln_array = array("TRANSAKSI_JANUARI", "TRANSAKSI_FEBRUARI", "TRANSAKSI_MARET", "TRANSAKSI_APRIL", "TRANSAKSI_MEI", "TRANSAKSI_JUNI", "TRANSAKSI_JULI", "TRANSAKSI_AGUSTUS", "TRANSAKSI_SEPTEMBER", "TRANSAKSI_OKTOBER", "TRANSAKSI_NOVEMBER", "TRANSAKSI_DESEMBER");

        //$total_pmp = 0;
        //$total_nilai = 0;
        $periode_bln = 0;
        $count2 = array();
        $max_count = 0;

        for ($i = 0; $i < 12; $i++) {
            if ($i < 10) { //bulan dlm angka
                $periode_bln = $periode_thn."0".$i+1;
            } else {
                $periode_bln = $periode_thn.$i+1;
            }

            if ($jns_api == 'jns_sbb') {
                $max_count = 3;
            } elseif ($jns_api == 'jns_penerima') {
                $max_count = 5;
            } elseif ($jns_api == 'jns_bank') {
                $max_count = $bank_row;
            }

            for ($j = 0; $j < $max_count; $j++) {
                if ($jns_api == 'jns_sbb') {
                    if ($j == 0) { //meninggal
                        $where_query = " AND a.tgl_meninggal IS NOT NULL";
                    } elseif ($j == 1) { //anak
                        $where_query = " AND (a.tgl_berhenti_sekolah IS NOT NULL OR a.tgl_menikah IS NOT NULL OR a.tgl_bekerja IS NOT NULL OR a.dewasa = 'TRUE')";
                    } else { //menikah lagi
                        $where_query = " AND a.tgl_menikah_lagi IS NOT NULL";
                    }
                } elseif ($jns_api == 'jns_penerima') {
                    if ($j == 0) { //peserta
                        $where_query = " AND UPPER(CALON_PENERIMA_MP_API.Get_Jenis_Ahli_Waris(a.company,a.n_i_k,a.no_ahli_waris)) = 'PESERTA'";
                    } elseif ($j == 1) { //istri
                        $where_query = " AND UPPER(CALON_PENERIMA_MP_API.Get_Jenis_Ahli_Waris(a.company,a.n_i_k,a.no_ahli_waris)) = 'ISTRI'";
                    } elseif ($j == 2) { //suami
                        $where_query = " AND UPPER(CALON_PENERIMA_MP_API.Get_Jenis_Ahli_Waris(a.company,a.n_i_k,a.no_ahli_waris)) = 'SUAMI'";
                    } elseif ($j == 3) { //anak
                        $where_query = " AND UPPER(CALON_PENERIMA_MP_API.Get_Jenis_Ahli_Waris(a.company,a.n_i_k,a.no_ahli_waris)) = 'ANAK'";
                    } else { //wali
                        $where_query = " AND UPPER(CALON_PENERIMA_MP_API.Get_Jenis_Ahli_Waris(a.company,a.n_i_k,a.no_ahli_waris)) = 'WALI'";
                    }
                } elseif ($jns_api == 'jns_bank') {
                    $where_query = " AND REKENING_PENERIMA_MP_API.Get_Kd_Bank_By_Param(a.company,a.n_i_k,a.no_ahli_waris,PERIODE_API.Get_Tanggal_Awal_Periode(a.company,a.kd_periode)) = (SELECT kd_bank FROM (SELECT (ROWNUM-1) no, kd_bank FROM (SELECT a.kd_bank, a.kd_koordinator, PAYMENT_INSTITUTE_API.Get_Description(a.company,a.kd_bank) nama_bank FROM DTL_BANK_KOORDINATOR a GROUP BY a.kd_bank, a.kd_koordinator, PAYMENT_INSTITUTE_API.Get_Description(a.company,a.kd_bank) ORDER BY PAYMENT_INSTITUTE_API.Get_Description(a.company,a.kd_bank))) WHERE no = '".$j."')";
                }

                $query2 = "SELECT SUM(JMLH_PMP_LEBIH_BAYAR) JMLH_PMP_LEBIH_BAYAR, SUM(JMLH_NILAI_LEBIH_BAYAR) JMLH_NILAI_LEBIH_BAYAR FROM (
                -- // -- // -- // -- // -- // --
                SELECT COUNT(*) JMLH_PMP_LEBIH_BAYAR, 0 JMLH_NILAI_LEBIH_BAYAR FROM (SELECT a.n_i_k, a.no_ahli_waris, b.kd_periode FROM HAPUS_PENERIMA_MP a, PENERIMAAN_MP b WHERE a.company = b.company AND a.n_i_k = b.n_i_k AND a.no_ahli_waris = b.no_ahli_waris AND a.kd_periode = b.kd_periode AND b.kd_periode > PERIODE_API.Get_Kd_Periode(a.company,TO_CHAR(a.tgl_berhenti,'YYYY'),TO_CHAR(a.tgl_berhenti,'MM')) AND a.state NOT IN ('BatalHapus','Dibatalkan') AND b.kd_periode = ('".$periode_bln."') $where_query GROUP BY a.n_i_k, a.no_ahli_waris, b.kd_periode ORDER BY a.n_i_k)
                -- // -- // -- // -- // -- // --
                UNION ALL
                -- // -- // -- // -- // -- // --
                SELECT 0 JMLH_PMP_LEBIH_BAYAR, SUM(nominal_transfer) JMLH_NILAI_LEBIH_BAYAR FROM (SELECT a.n_i_k, a.no_ahli_waris, b.kd_periode, b.nominal_transfer FROM HAPUS_PENERIMA_MP a, PENERIMAAN_MP b WHERE a.company = b.company AND a.n_i_k = b.n_i_k AND a.no_ahli_waris = b.no_ahli_waris AND a.kd_periode = b.kd_periode AND b.kd_periode > PERIODE_API.Get_Kd_Periode(a.company,TO_CHAR(a.tgl_berhenti,'YYYY'),TO_CHAR(a.tgl_berhenti,'MM')) AND a.state NOT IN ('BatalHapus','Dibatalkan') AND b.kd_periode = ('".$periode_bln."') $where_query GROUP BY a.n_i_k, a.no_ahli_waris, b.kd_periode, b.nominal_transfer ORDER BY a.n_i_k)
                )";
                
                $sql2 = oci_parse($conn, $query2);
                oci_execute($sql2);
                
                $count = array();
                while ($dt = oci_fetch_assoc($sql2)) {
                    $count[] = $dt;
                }
        
                if ($jns_api == 'jns_bank') {
                    $count1[$jns_api_array[$j]['NAMA_BANK']] = $count;
                } else {
                    $count1[$jns_api_array[$j]] = $count;
                }
            }

            $count2[$bln_array[$i]] = $count1;
        }
    }
    elseif ($parameter == 2) { //transaksi triwulan
        $triwulan_array = array("TRANSAKSI_TRIWULAN_I", "TRANSAKSI_TRIWULAN_II", "TRANSAKSI_TRIWULAN_III", "TRANSAKSI_TRIWULAN_IV");

        //$total_pmp = 0;
        //$total_nilai = 0;
        $periode_triwulan = "";
        $count2 = array();

        for ($i = 0; $i < 4; $i++) {
            if ($i == 0) { //triwulan i
                $periode_triwulan = "('".$periode_thn."01', '".$periode_thn."02', '".$periode_thn."03')";
            } elseif ($i == 1) { //triwulan ii
                $periode_triwulan = "('".$periode_thn."04', '".$periode_thn."05', '".$periode_thn."06')";
            } elseif ($i == 2) { //triwulan iii
                $periode_triwulan = "('".$periode_thn."07', '".$periode_thn."08', '".$periode_thn."09')";
            } elseif ($i == 3) { //triwulan iv
                $periode_triwulan = "('".$periode_thn."10', '".$periode_thn."11', '".$periode_thn."12')";
            }

            if ($jns_api == 'jns_sbb') {
                $max_count = 3;
            } elseif ($jns_api == 'jns_penerima') {
                $max_count = 5;
            } elseif ($jns_api == 'jns_bank') {
                $max_count = $bank_row;
            }

            for ($j = 0; $j < $max_count; $j++) {
                if ($jns_api == 'jns_sbb') {
                    if ($j == 0) { //meninggal
                        $where_query = " AND a.tgl_meninggal IS NOT NULL";
                    } elseif ($j == 1) { //anak
                        $where_query = " AND (a.tgl_berhenti_sekolah IS NOT NULL OR a.tgl_menikah IS NOT NULL OR a.tgl_bekerja IS NOT NULL OR a.dewasa = 'TRUE')";
                    } else { //menikah lagi
                        $where_query = " AND a.tgl_menikah_lagi IS NOT NULL";
                    }
                } elseif ($jns_api == 'jns_penerima') {
                    if ($j == 0) { //peserta
                        $where_query = " AND UPPER(CALON_PENERIMA_MP_API.Get_Jenis_Ahli_Waris(a.company,a.n_i_k,a.no_ahli_waris)) = 'PESERTA'";
                    } elseif ($j == 1) { //istri
                        $where_query = " AND UPPER(CALON_PENERIMA_MP_API.Get_Jenis_Ahli_Waris(a.company,a.n_i_k,a.no_ahli_waris)) = 'ISTRI'";
                    } elseif ($j == 2) { //suami
                        $where_query = " AND UPPER(CALON_PENERIMA_MP_API.Get_Jenis_Ahli_Waris(a.company,a.n_i_k,a.no_ahli_waris)) = 'SUAMI'";
                    } elseif ($j == 3) { //anak
                        $where_query = " AND UPPER(CALON_PENERIMA_MP_API.Get_Jenis_Ahli_Waris(a.company,a.n_i_k,a.no_ahli_waris)) = 'ANAK'";
                    } else { //wali
                        $where_query = " AND UPPER(CALON_PENERIMA_MP_API.Get_Jenis_Ahli_Waris(a.company,a.n_i_k,a.no_ahli_waris)) = 'WALI'";
                    }
                } elseif ($jns_api == 'jns_bank') {
                    $where_query = " AND REKENING_PENERIMA_MP_API.Get_Kd_Bank_By_Param(a.company,a.n_i_k,a.no_ahli_waris,PERIODE_API.Get_Tanggal_Awal_Periode(a.company,a.kd_periode)) = (SELECT kd_bank FROM (SELECT (ROWNUM-1) no, kd_bank FROM (SELECT a.kd_bank, a.kd_koordinator, PAYMENT_INSTITUTE_API.Get_Description(a.company,a.kd_bank) nama_bank FROM DTL_BANK_KOORDINATOR a GROUP BY a.kd_bank, a.kd_koordinator, PAYMENT_INSTITUTE_API.Get_Description(a.company,a.kd_bank) ORDER BY PAYMENT_INSTITUTE_API.Get_Description(a.company,a.kd_bank))) WHERE no = '".$j."')";
                }

                $query2 = "SELECT SUM(JMLH_PMP_LEBIH_BAYAR) JMLH_PMP_LEBIH_BAYAR, SUM(JMLH_NILAI_LEBIH_BAYAR) JMLH_NILAI_LEBIH_BAYAR FROM (
                -- // -- // -- // -- // -- // --
                SELECT COUNT(*) JMLH_PMP_LEBIH_BAYAR, 0 JMLH_NILAI_LEBIH_BAYAR FROM (SELECT a.n_i_k, a.no_ahli_waris, b.kd_periode FROM HAPUS_PENERIMA_MP a, PENERIMAAN_MP b WHERE a.company = b.company AND a.n_i_k = b.n_i_k AND a.no_ahli_waris = b.no_ahli_waris AND a.kd_periode = b.kd_periode AND b.kd_periode > PERIODE_API.Get_Kd_Periode(a.company,TO_CHAR(a.tgl_berhenti,'YYYY'),TO_CHAR(a.tgl_berhenti,'MM')) AND a.state NOT IN ('BatalHapus','Dibatalkan') AND b.kd_periode IN $periode_triwulan $where_query GROUP BY a.n_i_k, a.no_ahli_waris, b.kd_periode ORDER BY a.n_i_k)
                -- // -- // -- // -- // -- // --
                UNION ALL
                -- // -- // -- // -- // -- // --
                SELECT 0 JMLH_PMP_LEBIH_BAYAR, SUM(nominal_transfer) JMLH_NILAI_LEBIH_BAYAR FROM (SELECT a.n_i_k, a.no_ahli_waris, b.kd_periode, b.nominal_transfer FROM HAPUS_PENERIMA_MP a, PENERIMAAN_MP b WHERE a.company = b.company AND a.n_i_k = b.n_i_k AND a.no_ahli_waris = b.no_ahli_waris AND a.kd_periode = b.kd_periode AND b.kd_periode > PERIODE_API.Get_Kd_Periode(a.company,TO_CHAR(a.tgl_berhenti,'YYYY'),TO_CHAR(a.tgl_berhenti,'MM')) AND a.state NOT IN ('BatalHapus','Dibatalkan') AND b.kd_periode IN $periode_triwulan $where_query GROUP BY a.n_i_k, a.no_ahli_waris, b.kd_periode, b.nominal_transfer ORDER BY a.n_i_k)
                )";
                
                $sql2 = oci_parse($conn, $query2);
                oci_execute($sql2);
                
                $count = array();
                while ($dt = oci_fetch_assoc($sql2)) {
                    $count[] = $dt;
                }
        
                if ($jns_api == 'jns_bank') {
                    $count1[$jns_api_array[$j]['NAMA_BANK']] = $count;
                } else {
                    $count1[$jns_api_array[$j]] = $count;
                }
            }

            $count2[$triwulan_array[$i]] = $count1;
        }
    }
    elseif ($parameter == 3) { //transaksi semester
        $smt_array = array("TRANSAKSI_SEMESTER_I", "TRANSAKSI_SEMESTER_II");

        //$total_pmp = 0;
        //$total_nilai = 0;
        $periode_smt = "";
        $count2 = array();

        for ($i = 0; $i < 2; $i++) {
            if ($i == 0) { //semester i
                $periode_smt = "('".$periode_thn."01', '".$periode_thn."02', '".$periode_thn."03', '".$periode_thn."04', '".$periode_thn."05', '".$periode_thn."06')";
            } elseif ($i == 1) { //semester ii
                $periode_smt = "('".$periode_thn."07', '".$periode_thn."08', '".$periode_thn."09', '".$periode_thn."10', '".$periode_thn."11', '".$periode_thn."12')";
            }

            if ($jns_api == 'jns_sbb') {
                $max_count = 3;
            } elseif ($jns_api == 'jns_penerima') {
                $max_count = 5;
            } elseif ($jns_api == 'jns_bank') {
                $max_count = $bank_row;
            }

            for ($j = 0; $j < $max_count; $j++) {
                if ($jns_api == 'jns_sbb') {
                    if ($j == 0) { //meninggal
                        $where_query = " AND a.tgl_meninggal IS NOT NULL";
                    } elseif ($j == 1) { //anak
                        $where_query = " AND (a.tgl_berhenti_sekolah IS NOT NULL OR a.tgl_menikah IS NOT NULL OR a.tgl_bekerja IS NOT NULL OR a.dewasa = 'TRUE')";
                    } else { //menikah lagi
                        $where_query = " AND a.tgl_menikah_lagi IS NOT NULL";
                    }
                } elseif ($jns_api == 'jns_penerima') {
                    if ($j == 0) { //peserta
                        $where_query = " AND UPPER(CALON_PENERIMA_MP_API.Get_Jenis_Ahli_Waris(a.company,a.n_i_k,a.no_ahli_waris)) = 'PESERTA'";
                    } elseif ($j == 1) { //istri
                        $where_query = " AND UPPER(CALON_PENERIMA_MP_API.Get_Jenis_Ahli_Waris(a.company,a.n_i_k,a.no_ahli_waris)) = 'ISTRI'";
                    } elseif ($j == 2) { //suami
                        $where_query = " AND UPPER(CALON_PENERIMA_MP_API.Get_Jenis_Ahli_Waris(a.company,a.n_i_k,a.no_ahli_waris)) = 'SUAMI'";
                    } elseif ($j == 3) { //anak
                        $where_query = " AND UPPER(CALON_PENERIMA_MP_API.Get_Jenis_Ahli_Waris(a.company,a.n_i_k,a.no_ahli_waris)) = 'ANAK'";
                    } else { //wali
                        $where_query = " AND UPPER(CALON_PENERIMA_MP_API.Get_Jenis_Ahli_Waris(a.company,a.n_i_k,a.no_ahli_waris)) = 'WALI'";
                    }
                } elseif ($jns_api == 'jns_bank') {
                    $where_query = " AND REKENING_PENERIMA_MP_API.Get_Kd_Bank_By_Param(a.company,a.n_i_k,a.no_ahli_waris,PERIODE_API.Get_Tanggal_Awal_Periode(a.company,a.kd_periode)) = (SELECT kd_bank FROM (SELECT (ROWNUM-1) no, kd_bank FROM (SELECT a.kd_bank, a.kd_koordinator, PAYMENT_INSTITUTE_API.Get_Description(a.company,a.kd_bank) nama_bank FROM DTL_BANK_KOORDINATOR a GROUP BY a.kd_bank, a.kd_koordinator, PAYMENT_INSTITUTE_API.Get_Description(a.company,a.kd_bank) ORDER BY PAYMENT_INSTITUTE_API.Get_Description(a.company,a.kd_bank))) WHERE no = '".$j."')";
                }

                $query2 = "SELECT SUM(JMLH_PMP_LEBIH_BAYAR) JMLH_PMP_LEBIH_BAYAR, SUM(JMLH_NILAI_LEBIH_BAYAR) JMLH_NILAI_LEBIH_BAYAR FROM (
                -- // -- // -- // -- // -- // --
                SELECT COUNT(*) JMLH_PMP_LEBIH_BAYAR, 0 JMLH_NILAI_LEBIH_BAYAR FROM (SELECT a.n_i_k, a.no_ahli_waris, b.kd_periode FROM HAPUS_PENERIMA_MP a, PENERIMAAN_MP b WHERE a.company = b.company AND a.n_i_k = b.n_i_k AND a.no_ahli_waris = b.no_ahli_waris AND a.kd_periode = b.kd_periode AND b.kd_periode > PERIODE_API.Get_Kd_Periode(a.company,TO_CHAR(a.tgl_berhenti,'YYYY'),TO_CHAR(a.tgl_berhenti,'MM')) AND a.state NOT IN ('BatalHapus','Dibatalkan') AND b.kd_periode IN $periode_smt $where_query GROUP BY a.n_i_k, a.no_ahli_waris, b.kd_periode ORDER BY a.n_i_k)
                -- // -- // -- // -- // -- // --
                UNION ALL
                -- // -- // -- // -- // -- // --
                SELECT 0 JMLH_PMP_LEBIH_BAYAR, SUM(nominal_transfer) JMLH_NILAI_LEBIH_BAYAR FROM (SELECT a.n_i_k, a.no_ahli_waris, b.kd_periode, b.nominal_transfer FROM HAPUS_PENERIMA_MP a, PENERIMAAN_MP b WHERE a.company = b.company AND a.n_i_k = b.n_i_k AND a.no_ahli_waris = b.no_ahli_waris AND a.kd_periode = b.kd_periode AND b.kd_periode > PERIODE_API.Get_Kd_Periode(a.company,TO_CHAR(a.tgl_berhenti,'YYYY'),TO_CHAR(a.tgl_berhenti,'MM')) AND a.state NOT IN ('BatalHapus','Dibatalkan') AND b.kd_periode IN $periode_smt $where_query GROUP BY a.n_i_k, a.no_ahli_waris, b.kd_periode, b.nominal_transfer ORDER BY a.n_i_k)
                )";
                
                $sql2 = oci_parse($conn, $query2);
                oci_execute($sql2);
                
                $count = array();
                while ($dt = oci_fetch_assoc($sql2)) {
                    $count[] = $dt;
                }
        
                if ($jns_api == 'jns_bank') {
                    $count1[$jns_api_array[$j]['NAMA_BANK']] = $count;
                } else {
                    $count1[$jns_api_array[$j]] = $count;
                }
            }
            
            $count2[$smt_array[$i]] = $count1;
        }
    }
    elseif ($parameter == 4) { //transaksi tahun
        //$total_pmp = 0;
        //$total_nilai = 0;
        $periode_smt = "";
        $count2 = array();

        if ($jns_api == 'jns_sbb') {
            $max_count = 3;
        } elseif ($jns_api == 'jns_penerima') {
            $max_count = 5;
        } elseif ($jns_api == 'jns_bank') {
            $max_count = $bank_row;
        }

        for ($j = 0; $j < $max_count; $j++) {
            if ($jns_api == 'jns_sbb') {
                if ($j == 0) { //meninggal
                    $where_query = " AND a.tgl_meninggal IS NOT NULL";
                } elseif ($j == 1) { //anak
                    $where_query = " AND (a.tgl_berhenti_sekolah IS NOT NULL OR a.tgl_menikah IS NOT NULL OR a.tgl_bekerja IS NOT NULL OR a.dewasa = 'TRUE')";
                } else { //menikah lagi
                    $where_query = " AND a.tgl_menikah_lagi IS NOT NULL";
                }
            } elseif ($jns_api == 'jns_penerima') {
                if ($j == 0) { //peserta
                    $where_query = " AND UPPER(CALON_PENERIMA_MP_API.Get_Jenis_Ahli_Waris(a.company,a.n_i_k,a.no_ahli_waris)) = 'PESERTA'";
                } elseif ($j == 1) { //istri
                    $where_query = " AND UPPER(CALON_PENERIMA_MP_API.Get_Jenis_Ahli_Waris(a.company,a.n_i_k,a.no_ahli_waris)) = 'ISTRI'";
                } elseif ($j == 2) { //suami
                    $where_query = " AND UPPER(CALON_PENERIMA_MP_API.Get_Jenis_Ahli_Waris(a.company,a.n_i_k,a.no_ahli_waris)) = 'SUAMI'";
                } elseif ($j == 3) { //anak
                    $where_query = " AND UPPER(CALON_PENERIMA_MP_API.Get_Jenis_Ahli_Waris(a.company,a.n_i_k,a.no_ahli_waris)) = 'ANAK'";
                } else { //wali
                    $where_query = " AND UPPER(CALON_PENERIMA_MP_API.Get_Jenis_Ahli_Waris(a.company,a.n_i_k,a.no_ahli_waris)) = 'WALI'";
                }
            }

            $query2 = "SELECT SUM(JMLH_PMP_LEBIH_BAYAR) JMLH_PMP_LEBIH_BAYAR, SUM(JMLH_NILAI_LEBIH_BAYAR) JMLH_NILAI_LEBIH_BAYAR FROM (
            -- // -- // -- // -- // -- // --
            SELECT COUNT(*) JMLH_PMP_LEBIH_BAYAR, 0 JMLH_NILAI_LEBIH_BAYAR FROM (SELECT a.n_i_k, a.no_ahli_waris, b.kd_periode FROM HAPUS_PENERIMA_MP a, PENERIMAAN_MP b WHERE a.company = b.company AND a.n_i_k = b.n_i_k AND a.no_ahli_waris = b.no_ahli_waris AND a.kd_periode = b.kd_periode AND b.kd_periode > PERIODE_API.Get_Kd_Periode(a.company,TO_CHAR(a.tgl_berhenti,'YYYY'),TO_CHAR(a.tgl_berhenti,'MM')) AND a.state NOT IN ('BatalHapus','Dibatalkan') AND b.kd_periode LIKE '".$periode_thn."%' $where_query GROUP BY a.n_i_k, a.no_ahli_waris, b.kd_periode ORDER BY a.n_i_k)
            -- // -- // -- // -- // -- // --
            UNION ALL
            -- // -- // -- // -- // -- // --
            SELECT 0 JMLH_PMP_LEBIH_BAYAR, SUM(nominal_transfer) JMLH_NILAI_LEBIH_BAYAR FROM (SELECT a.n_i_k, a.no_ahli_waris, b.kd_periode, b.nominal_transfer FROM HAPUS_PENERIMA_MP a, PENERIMAAN_MP b WHERE a.company = b.company AND a.n_i_k = b.n_i_k AND a.no_ahli_waris = b.no_ahli_waris AND a.kd_periode = b.kd_periode AND b.kd_periode > PERIODE_API.Get_Kd_Periode(a.company,TO_CHAR(a.tgl_berhenti,'YYYY'),TO_CHAR(a.tgl_berhenti,'MM')) AND a.state NOT IN ('BatalHapus','Dibatalkan') AND b.kd_periode LIKE '".$periode_thn."%' $where_query GROUP BY a.n_i_k, a.no_ahli_waris, b.kd_periode, b.nominal_transfer ORDER BY a.n_i_k)
            )";
            
            $sql2 = oci_parse($conn, $query2);
            oci_execute($sql2);
            
            $count = array();
            while ($dt = oci_fetch_assoc($sql2)) {
                $count[] = $dt;
            }
    
            if ($jns_api == 'jns_bank') {
                $count1[$jns_api_array[$j]['NAMA_BANK']] = $count;
            } else {
                $count1[$jns_api_array[$j]] = $count;
            }
        }
        
        $count2['TRANSAKSI_TAHUN'] = $count1;
    }
    elseif ($parameter == 5) { //disetujui bulan
        $bln_array = array("DISETUJUI_JANUARI", "DISETUJUI_FEBRUARI", "DISETUJUI_MARET", "DISETUJUI_APRIL", "DISETUJUI_MEI", "DISETUJUI_JUNI", "DISETUJUI_JULI", "DISETUJUI_AGUSTUS", "DISETUJUI_SEPTEMBER", "DISETUJUI_OKTOBER", "DISETUJUI_NOVEMBER", "DISETUJUI_DESEMBER");

        //$total_pmp = 0;
        //$total_nilai = 0;
        $periode_bln = 0;
        $count2 = array();

        for ($i = 0; $i < 12; $i++) {
            if ($i < 10) { //bulan dlm angka
                $periode_bln = $periode_thn."0".$i+1;
            } else {
                $periode_bln = $periode_thn.$i+1;
            }

            if ($jns_api == 'jns_sbb') {
                $max_count = 3;
            } elseif ($jns_api == 'jns_penerima') {
                $max_count = 5;
            } elseif ($jns_api == 'jns_bank') {
                $max_count = $bank_row;
            }

            for ($j = 0; $j < $max_count; $j++) {
                if ($jns_api == 'jns_sbb') {
                    if ($j == 0) { //meninggal
                        $where_query = " AND a.tgl_meninggal IS NOT NULL";
                    } elseif ($j == 1) { //anak
                        $where_query = " AND (a.tgl_berhenti_sekolah IS NOT NULL OR a.tgl_menikah IS NOT NULL OR a.tgl_bekerja IS NOT NULL OR a.dewasa = 'TRUE')";
                    } else { //menikah lagi
                        $where_query = " AND a.tgl_menikah_lagi IS NOT NULL";
                    }
                } elseif ($jns_api == 'jns_penerima') {
                    if ($j == 0) { //peserta
                        $where_query = " AND UPPER(CALON_PENERIMA_MP_API.Get_Jenis_Ahli_Waris(a.company,a.n_i_k,a.no_ahli_waris)) = 'PESERTA'";
                    } elseif ($j == 1) { //istri
                        $where_query = " AND UPPER(CALON_PENERIMA_MP_API.Get_Jenis_Ahli_Waris(a.company,a.n_i_k,a.no_ahli_waris)) = 'ISTRI'";
                    } elseif ($j == 2) { //suami
                        $where_query = " AND UPPER(CALON_PENERIMA_MP_API.Get_Jenis_Ahli_Waris(a.company,a.n_i_k,a.no_ahli_waris)) = 'SUAMI'";
                    } elseif ($j == 3) { //anak
                        $where_query = " AND UPPER(CALON_PENERIMA_MP_API.Get_Jenis_Ahli_Waris(a.company,a.n_i_k,a.no_ahli_waris)) = 'ANAK'";
                    } else { //wali
                        $where_query = " AND UPPER(CALON_PENERIMA_MP_API.Get_Jenis_Ahli_Waris(a.company,a.n_i_k,a.no_ahli_waris)) = 'WALI'";
                    }
                } elseif ($jns_api == 'jns_bank') {
                    $where_query = " AND REKENING_PENERIMA_MP_API.Get_Kd_Bank_By_Param(a.company,a.n_i_k,a.no_ahli_waris,PERIODE_API.Get_Tanggal_Awal_Periode(a.company,a.kd_periode)) = (SELECT kd_bank FROM (SELECT (ROWNUM-1) no, kd_bank FROM (SELECT a.kd_bank, a.kd_koordinator, PAYMENT_INSTITUTE_API.Get_Description(a.company,a.kd_bank) nama_bank FROM DTL_BANK_KOORDINATOR a GROUP BY a.kd_bank, a.kd_koordinator, PAYMENT_INSTITUTE_API.Get_Description(a.company,a.kd_bank) ORDER BY PAYMENT_INSTITUTE_API.Get_Description(a.company,a.kd_bank))) WHERE no = '".$j."')";
                }

                $query2 = "SELECT SUM(JMLH_PMP_LEBIH_BAYAR) JMLH_PMP_LEBIH_BAYAR, SUM(JMLH_NILAI_LEBIH_BAYAR) JMLH_NILAI_LEBIH_BAYAR FROM (
                -- // -- // -- // -- // -- // --
                SELECT COUNT(*) JMLH_PMP_LEBIH_BAYAR, 0 JMLH_NILAI_LEBIH_BAYAR FROM (SELECT a.n_i_k, a.no_ahli_waris, b.kd_periode FROM HAPUS_PENERIMA_MP a, PENERIMAAN_MP b WHERE a.company = b.company AND a.n_i_k = b.n_i_k AND a.no_ahli_waris = b.no_ahli_waris AND a.kd_periode = b.kd_periode AND b.kd_periode > PERIODE_API.Get_Kd_Periode(a.company,TO_CHAR(a.tgl_berhenti,'YYYY'),TO_CHAR(a.tgl_berhenti,'MM')) AND INITCAP(a.state) = 'Disetujui' AND PERIODE_API.Get_Kd_Periode(a.company,TO_CHAR(DPT_AUDITTRAIL_API.Get_Tgl('HapusPenerimaMp',a.objid,a.state),'YYYY'),TO_CHAR(DPT_AUDITTRAIL_API.Get_Tgl('HapusPenerimaMp',a.objid,a.state),'MM')) = ('".$periode_bln."') $where_query GROUP BY a.n_i_k, a.no_ahli_waris, b.kd_periode ORDER BY a.n_i_k)
                -- // -- // -- // -- // -- // --
                UNION ALL
                -- // -- // -- // -- // -- // --
                SELECT 0 JMLH_PMP_LEBIH_BAYAR, SUM(nominal_transfer) JMLH_NILAI_LEBIH_BAYAR FROM (SELECT a.n_i_k, a.no_ahli_waris, b.kd_periode, b.nominal_transfer FROM HAPUS_PENERIMA_MP a, PENERIMAAN_MP b WHERE a.company = b.company AND a.n_i_k = b.n_i_k AND a.no_ahli_waris = b.no_ahli_waris AND a.kd_periode = b.kd_periode AND b.kd_periode > PERIODE_API.Get_Kd_Periode(a.company,TO_CHAR(a.tgl_berhenti,'YYYY'),TO_CHAR(a.tgl_berhenti,'MM')) AND INITCAP(a.state) = 'Disetujui' AND PERIODE_API.Get_Kd_Periode(a.company,TO_CHAR(DPT_AUDITTRAIL_API.Get_Tgl('HapusPenerimaMp',a.objid,a.state),'YYYY'),TO_CHAR(DPT_AUDITTRAIL_API.Get_Tgl('HapusPenerimaMp',a.objid,a.state),'MM')) = ('".$periode_bln."') $where_query GROUP BY a.n_i_k, a.no_ahli_waris, b.kd_periode, b.nominal_transfer ORDER BY a.n_i_k)
                )";
                
                $sql2 = oci_parse($conn, $query2);
                oci_execute($sql2);
                
                $count = array();
                while ($dt = oci_fetch_assoc($sql2)) {
                    $count[] = $dt;
                }
        
                if ($jns_api == 'jns_bank') {
                    $count1[$jns_api_array[$j]['NAMA_BANK']] = $count;
                } else {
                    $count1[$jns_api_array[$j]] = $count;
                }
            }

            $count2[$bln_array[$i]] = $count1;
        }
    }
    elseif ($parameter == 6) { //disetujui triwulan
        $triwulan_array = array("DISETUJUI_TRIWULAN_I", "DISETUJUI_TRIWULAN_II", "DISETUJUI_TRIWULAN_III", "DISETUJUI_TRIWULAN_IV");

        //$total_pmp = 0;
        //$total_nilai = 0;
        $periode_triwulan = "";
        $count2 = array();

        for ($i = 0; $i < 4; $i++) {
            if ($i == 0) { //triwulan i
                $periode_triwulan = "('".$periode_thn."01', '".$periode_thn."02', '".$periode_thn."03')";
            } elseif ($i == 1) { //triwulan ii
                $periode_triwulan = "('".$periode_thn."04', '".$periode_thn."05', '".$periode_thn."06')";
            } elseif ($i == 2) { //triwulan iii
                $periode_triwulan = "('".$periode_thn."07', '".$periode_thn."08', '".$periode_thn."09')";
            } elseif ($i == 3) { //triwulan iv
                $periode_triwulan = "('".$periode_thn."10', '".$periode_thn."11', '".$periode_thn."12')";
            }

            if ($jns_api == 'jns_sbb') {
                $max_count = 3;
            } elseif ($jns_api == 'jns_penerima') {
                $max_count = 5;
            } elseif ($jns_api == 'jns_bank') {
                $max_count = $bank_row;
            }

            for ($j = 0; $j < $max_count; $j++) {
                if ($jns_api == 'jns_sbb') {
                    if ($j == 0) { //meninggal
                        $where_query = " AND a.tgl_meninggal IS NOT NULL";
                    } elseif ($j == 1) { //anak
                        $where_query = " AND (a.tgl_berhenti_sekolah IS NOT NULL OR a.tgl_menikah IS NOT NULL OR a.tgl_bekerja IS NOT NULL OR a.dewasa = 'TRUE')";
                    } else { //menikah lagi
                        $where_query = " AND a.tgl_menikah_lagi IS NOT NULL";
                    }
                } elseif ($jns_api == 'jns_penerima') {
                    if ($j == 0) { //peserta
                        $where_query = " AND UPPER(CALON_PENERIMA_MP_API.Get_Jenis_Ahli_Waris(a.company,a.n_i_k,a.no_ahli_waris)) = 'PESERTA'";
                    } elseif ($j == 1) { //istri
                        $where_query = " AND UPPER(CALON_PENERIMA_MP_API.Get_Jenis_Ahli_Waris(a.company,a.n_i_k,a.no_ahli_waris)) = 'ISTRI'";
                    } elseif ($j == 2) { //suami
                        $where_query = " AND UPPER(CALON_PENERIMA_MP_API.Get_Jenis_Ahli_Waris(a.company,a.n_i_k,a.no_ahli_waris)) = 'SUAMI'";
                    } elseif ($j == 3) { //anak
                        $where_query = " AND UPPER(CALON_PENERIMA_MP_API.Get_Jenis_Ahli_Waris(a.company,a.n_i_k,a.no_ahli_waris)) = 'ANAK'";
                    } else { //wali
                        $where_query = " AND UPPER(CALON_PENERIMA_MP_API.Get_Jenis_Ahli_Waris(a.company,a.n_i_k,a.no_ahli_waris)) = 'WALI'";
                    }
                } elseif ($jns_api == 'jns_bank') {
                    $where_query = " AND REKENING_PENERIMA_MP_API.Get_Kd_Bank_By_Param(a.company,a.n_i_k,a.no_ahli_waris,PERIODE_API.Get_Tanggal_Awal_Periode(a.company,a.kd_periode)) = (SELECT kd_bank FROM (SELECT (ROWNUM-1) no, kd_bank FROM (SELECT a.kd_bank, a.kd_koordinator, PAYMENT_INSTITUTE_API.Get_Description(a.company,a.kd_bank) nama_bank FROM DTL_BANK_KOORDINATOR a GROUP BY a.kd_bank, a.kd_koordinator, PAYMENT_INSTITUTE_API.Get_Description(a.company,a.kd_bank) ORDER BY PAYMENT_INSTITUTE_API.Get_Description(a.company,a.kd_bank))) WHERE no = '".$j."')";
                }

                $query2 = "SELECT SUM(JMLH_PMP_LEBIH_BAYAR) JMLH_PMP_LEBIH_BAYAR, SUM(JMLH_NILAI_LEBIH_BAYAR) JMLH_NILAI_LEBIH_BAYAR FROM (
                -- // -- // -- // -- // -- // --
                SELECT COUNT(*) JMLH_PMP_LEBIH_BAYAR, 0 JMLH_NILAI_LEBIH_BAYAR FROM (SELECT a.n_i_k, a.no_ahli_waris, b.kd_periode FROM HAPUS_PENERIMA_MP a, PENERIMAAN_MP b WHERE a.company = b.company AND a.n_i_k = b.n_i_k AND a.no_ahli_waris = b.no_ahli_waris AND a.kd_periode = b.kd_periode AND b.kd_periode > PERIODE_API.Get_Kd_Periode(a.company,TO_CHAR(a.tgl_berhenti,'YYYY'),TO_CHAR(a.tgl_berhenti,'MM')) AND INITCAP(a.state) = 'Disetujui' AND PERIODE_API.Get_Kd_Periode(a.company,TO_CHAR(DPT_AUDITTRAIL_API.Get_Tgl('HapusPenerimaMp',a.objid,a.state),'YYYY'),TO_CHAR(DPT_AUDITTRAIL_API.Get_Tgl('HapusPenerimaMp',a.objid,a.state),'MM')) IN $periode_triwulan $where_query GROUP BY a.n_i_k, a.no_ahli_waris, b.kd_periode ORDER BY a.n_i_k)
                -- // -- // -- // -- // -- // --
                UNION ALL
                -- // -- // -- // -- // -- // --
                SELECT 0 JMLH_PMP_LEBIH_BAYAR, SUM(nominal_transfer) JMLH_NILAI_LEBIH_BAYAR FROM (SELECT a.n_i_k, a.no_ahli_waris, b.kd_periode, b.nominal_transfer FROM HAPUS_PENERIMA_MP a, PENERIMAAN_MP b WHERE a.company = b.company AND a.n_i_k = b.n_i_k AND a.no_ahli_waris = b.no_ahli_waris AND a.kd_periode = b.kd_periode AND b.kd_periode > PERIODE_API.Get_Kd_Periode(a.company,TO_CHAR(a.tgl_berhenti,'YYYY'),TO_CHAR(a.tgl_berhenti,'MM')) AND INITCAP(a.state) = 'Disetujui' AND PERIODE_API.Get_Kd_Periode(a.company,TO_CHAR(DPT_AUDITTRAIL_API.Get_Tgl('HapusPenerimaMp',a.objid,a.state),'YYYY'),TO_CHAR(DPT_AUDITTRAIL_API.Get_Tgl('HapusPenerimaMp',a.objid,a.state),'MM')) IN $periode_triwulan $where_query GROUP BY a.n_i_k, a.no_ahli_waris, b.kd_periode, b.nominal_transfer ORDER BY a.n_i_k)
                )";
                    
                $sql2 = oci_parse($conn, $query2);
                oci_execute($sql2);
                    
                $count = array();
                while ($dt = oci_fetch_assoc($sql2)) {
                    $count[] = $dt;
                }
            
                if ($jns_api == 'jns_bank') {
                    $count1[$jns_api_array[$j]['NAMA_BANK']] = $count;
                } else {
                    $count1[$jns_api_array[$j]] = $count;
                }
            }

            $count2[$triwulan_array[$i]] = $count1;
        }
    }
    elseif ($parameter == 7) { //disetujui semseter
        $smt_array = array("DISETUJUI_SEMESTER_I", "DISETUJUI_SEMESTER_II");

        //$total_pmp = 0;
        //$total_nilai = 0;
        $periode_smt = "";
        $count2 = array();

        for ($i = 0; $i < 2; $i++) {
            if ($i == 0) { //semester i
                $periode_smt = "('".$periode_thn."01', '".$periode_thn."02', '".$periode_thn."03', '".$periode_thn."04', '".$periode_thn."05', '".$periode_thn."06')";
            } elseif ($i == 1) { //semester ii
                $periode_smt = "('".$periode_thn."07', '".$periode_thn."08', '".$periode_thn."09', '".$periode_thn."10', '".$periode_thn."11', '".$periode_thn."12')";
            }

            if ($jns_api == 'jns_sbb') {
                $max_count = 3;
            } elseif ($jns_api == 'jns_penerima') {
                $max_count = 5;
            }

            for ($j = 0; $j < $max_count; $j++) {
                if ($jns_api == 'jns_sbb') {
                    if ($j == 0) { //meninggal
                        $where_query = " AND a.tgl_meninggal IS NOT NULL";
                    } elseif ($j == 1) { //anak
                        $where_query = " AND (a.tgl_berhenti_sekolah IS NOT NULL OR a.tgl_menikah IS NOT NULL OR a.tgl_bekerja IS NOT NULL OR a.dewasa = 'TRUE')";
                    } else { //menikah lagi
                        $where_query = " AND a.tgl_menikah_lagi IS NOT NULL";
                    }
                } elseif ($jns_api == 'jns_penerima') {
                    if ($j == 0) { //peserta
                        $where_query = " AND UPPER(CALON_PENERIMA_MP_API.Get_Jenis_Ahli_Waris(a.company,a.n_i_k,a.no_ahli_waris)) = 'PESERTA'";
                    } elseif ($j == 1) { //istri
                        $where_query = " AND UPPER(CALON_PENERIMA_MP_API.Get_Jenis_Ahli_Waris(a.company,a.n_i_k,a.no_ahli_waris)) = 'ISTRI'";
                    } elseif ($j == 2) { //suami
                        $where_query = " AND UPPER(CALON_PENERIMA_MP_API.Get_Jenis_Ahli_Waris(a.company,a.n_i_k,a.no_ahli_waris)) = 'SUAMI'";
                    } elseif ($j == 3) { //anak
                        $where_query = " AND UPPER(CALON_PENERIMA_MP_API.Get_Jenis_Ahli_Waris(a.company,a.n_i_k,a.no_ahli_waris)) = 'ANAK'";
                    } else { //wali
                        $where_query = " AND UPPER(CALON_PENERIMA_MP_API.Get_Jenis_Ahli_Waris(a.company,a.n_i_k,a.no_ahli_waris)) = 'WALI'";
                    }
                } elseif ($jns_api == 'jns_bank') {
                    $where_query = " AND REKENING_PENERIMA_MP_API.Get_Kd_Bank_By_Param(a.company,a.n_i_k,a.no_ahli_waris,PERIODE_API.Get_Tanggal_Awal_Periode(a.company,a.kd_periode)) = (SELECT kd_bank FROM (SELECT (ROWNUM-1) no, kd_bank FROM (SELECT a.kd_bank, a.kd_koordinator, PAYMENT_INSTITUTE_API.Get_Description(a.company,a.kd_bank) nama_bank FROM DTL_BANK_KOORDINATOR a GROUP BY a.kd_bank, a.kd_koordinator, PAYMENT_INSTITUTE_API.Get_Description(a.company,a.kd_bank) ORDER BY PAYMENT_INSTITUTE_API.Get_Description(a.company,a.kd_bank))) WHERE no = '".$j."')";
                }

                $query2 = "SELECT SUM(JMLH_PMP_LEBIH_BAYAR) JMLH_PMP_LEBIH_BAYAR, SUM(JMLH_NILAI_LEBIH_BAYAR) JMLH_NILAI_LEBIH_BAYAR FROM (
                -- // -- // -- // -- // -- // --
                SELECT COUNT(*) JMLH_PMP_LEBIH_BAYAR, 0 JMLH_NILAI_LEBIH_BAYAR FROM (SELECT a.n_i_k, a.no_ahli_waris, b.kd_periode FROM HAPUS_PENERIMA_MP a, PENERIMAAN_MP b WHERE a.company = b.company AND a.n_i_k = b.n_i_k AND a.no_ahli_waris = b.no_ahli_waris AND a.kd_periode = b.kd_periode AND b.kd_periode > PERIODE_API.Get_Kd_Periode(a.company,TO_CHAR(a.tgl_berhenti,'YYYY'),TO_CHAR(a.tgl_berhenti,'MM')) AND INITCAP(a.state) = 'Disetujui' AND PERIODE_API.Get_Kd_Periode(a.company,TO_CHAR(DPT_AUDITTRAIL_API.Get_Tgl('HapusPenerimaMp',a.objid,a.state),'YYYY'),TO_CHAR(DPT_AUDITTRAIL_API.Get_Tgl('HapusPenerimaMp',a.objid,a.state),'MM')) IN $periode_smt $where_query GROUP BY a.n_i_k, a.no_ahli_waris, b.kd_periode ORDER BY a.n_i_k)
                -- // -- // -- // -- // -- // --
                UNION ALL
                -- // -- // -- // -- // -- // --
                SELECT 0 JMLH_PMP_LEBIH_BAYAR, SUM(nominal_transfer) JMLH_NILAI_LEBIH_BAYAR FROM (SELECT a.n_i_k, a.no_ahli_waris, b.kd_periode, b.nominal_transfer FROM HAPUS_PENERIMA_MP a, PENERIMAAN_MP b WHERE a.company = b.company AND a.n_i_k = b.n_i_k AND a.no_ahli_waris = b.no_ahli_waris AND a.kd_periode = b.kd_periode AND b.kd_periode > PERIODE_API.Get_Kd_Periode(a.company,TO_CHAR(a.tgl_berhenti,'YYYY'),TO_CHAR(a.tgl_berhenti,'MM')) AND INITCAP(a.state) = 'Disetujui' AND PERIODE_API.Get_Kd_Periode(a.company,TO_CHAR(DPT_AUDITTRAIL_API.Get_Tgl('HapusPenerimaMp',a.objid,a.state),'YYYY'),TO_CHAR(DPT_AUDITTRAIL_API.Get_Tgl('HapusPenerimaMp',a.objid,a.state),'MM')) IN $periode_smt $where_query GROUP BY a.n_i_k, a.no_ahli_waris, b.kd_periode, b.nominal_transfer ORDER BY a.n_i_k)
                )";
                    
                $sql2 = oci_parse($conn, $query2);
                oci_execute($sql2);
                    
                $count = array();
                while ($dt = oci_fetch_assoc($sql2)) {
                    $count[] = $dt;
                }
            
                if ($jns_api == 'jns_bank') {
                    $count1[$jns_api_array[$j]['NAMA_BANK']] = $count;
                } else {
                    $count1[$jns_api_array[$j]] = $count;
                }
            }

            $count2[$smt_array[$i]] = $count1;
        }
    }
    elseif ($parameter == 8) { //disetujui tahun
        //$total_pmp = 0;
        //$total_nilai = 0;
        $periode_smt = "";
        $count2 = array();

        if ($jns_api == 'jns_sbb') {
            $max_count = 3;
        } elseif ($jns_api == 'jns_penerima') {
            $max_count = 5;
        } elseif ($jns_api == 'jns_bank') {
            $max_count = $bank_row;
        }

        for ($j = 0; $j < $max_count; $j++) {
            if ($jns_api == 'jns_sbb') {
                if ($j == 0) { //meninggal
                    $where_query = " AND a.tgl_meninggal IS NOT NULL";
                } elseif ($j == 1) { //anak
                    $where_query = " AND (a.tgl_berhenti_sekolah IS NOT NULL OR a.tgl_menikah IS NOT NULL OR a.tgl_bekerja IS NOT NULL OR a.dewasa = 'TRUE')";
                } else { //menikah lagi
                    $where_query = " AND a.tgl_menikah_lagi IS NOT NULL";
                }
            } elseif ($jns_api == 'jns_penerima') {
                if ($j == 0) { //peserta
                    $where_query = " AND UPPER(CALON_PENERIMA_MP_API.Get_Jenis_Ahli_Waris(a.company,a.n_i_k,a.no_ahli_waris)) = 'PESERTA'";
                } elseif ($j == 1) { //istri
                    $where_query = " AND UPPER(CALON_PENERIMA_MP_API.Get_Jenis_Ahli_Waris(a.company,a.n_i_k,a.no_ahli_waris)) = 'ISTRI'";
                } elseif ($j == 2) { //suami
                    $where_query = " AND UPPER(CALON_PENERIMA_MP_API.Get_Jenis_Ahli_Waris(a.company,a.n_i_k,a.no_ahli_waris)) = 'SUAMI'";
                } elseif ($j == 3) { //anak
                    $where_query = " AND UPPER(CALON_PENERIMA_MP_API.Get_Jenis_Ahli_Waris(a.company,a.n_i_k,a.no_ahli_waris)) = 'ANAK'";
                } else { //wali
                    $where_query = " AND UPPER(CALON_PENERIMA_MP_API.Get_Jenis_Ahli_Waris(a.company,a.n_i_k,a.no_ahli_waris)) = 'WALI'";
                }
            }

            $query2 = "SELECT SUM(JMLH_PMP_LEBIH_BAYAR) JMLH_PMP_LEBIH_BAYAR, SUM(JMLH_NILAI_LEBIH_BAYAR) JMLH_NILAI_LEBIH_BAYAR FROM (
            -- // -- // -- // -- // -- // --
            SELECT COUNT(*) JMLH_PMP_LEBIH_BAYAR, 0 JMLH_NILAI_LEBIH_BAYAR FROM (SELECT a.n_i_k, a.no_ahli_waris, b.kd_periode FROM HAPUS_PENERIMA_MP a, PENERIMAAN_MP b WHERE a.company = b.company AND a.n_i_k = b.n_i_k AND a.no_ahli_waris = b.no_ahli_waris AND a.kd_periode = b.kd_periode AND b.kd_periode > PERIODE_API.Get_Kd_Periode(a.company,TO_CHAR(a.tgl_berhenti,'YYYY'),TO_CHAR(a.tgl_berhenti,'MM')) AND INITCAP(a.state) = 'Disetujui' AND PERIODE_API.Get_Kd_Periode(a.company,TO_CHAR(DPT_AUDITTRAIL_API.Get_Tgl('HapusPenerimaMp',a.objid,a.state),'YYYY'),TO_CHAR(DPT_AUDITTRAIL_API.Get_Tgl('HapusPenerimaMp',a.objid,a.state),'MM')) LIKE '".$periode_thn."%' $where_query GROUP BY a.n_i_k, a.no_ahli_waris, b.kd_periode ORDER BY a.n_i_k)
            -- // -- // -- // -- // -- // --
            UNION ALL
            -- // -- // -- // -- // -- // --
            SELECT 0 JMLH_PMP_LEBIH_BAYAR, SUM(nominal_transfer) JMLH_NILAI_LEBIH_BAYAR FROM (SELECT a.n_i_k, a.no_ahli_waris, b.kd_periode, b.nominal_transfer FROM HAPUS_PENERIMA_MP a, PENERIMAAN_MP b WHERE a.company = b.company AND a.n_i_k = b.n_i_k AND a.no_ahli_waris = b.no_ahli_waris AND a.kd_periode = b.kd_periode AND b.kd_periode > PERIODE_API.Get_Kd_Periode(a.company,TO_CHAR(a.tgl_berhenti,'YYYY'),TO_CHAR(a.tgl_berhenti,'MM')) AND INITCAP(a.state) = 'Disetujui' AND PERIODE_API.Get_Kd_Periode(a.company,TO_CHAR(DPT_AUDITTRAIL_API.Get_Tgl('HapusPenerimaMp',a.objid,a.state),'YYYY'),TO_CHAR(DPT_AUDITTRAIL_API.Get_Tgl('HapusPenerimaMp',a.objid,a.state),'MM')) LIKE '".$periode_thn."%' $where_query GROUP BY a.n_i_k, a.no_ahli_waris, b.kd_periode, b.nominal_transfer ORDER BY a.n_i_k)
            )";
            
            $sql2 = oci_parse($conn, $query2);
            oci_execute($sql2);
            
            $count = array();
            while ($dt = oci_fetch_assoc($sql2)) {
                $count[] = $dt;
            }
    
            if ($jns_api == 'jns_bank') {
                $count1[$jns_api_array[$j]['NAMA_BANK']] = $count;
            } else {
                $count1[$jns_api_array[$j]] = $count;
            }
        }
        
        $count2['DISETUJUI_TAHUN'] = $count1;
    }

    return $count2;
}

// ----- Login and Get Log Functions ----- //

function getIfsUser($username) {
    $conn = getConnection();
    $query_ifs_user = "SELECT a.ifs_user FROM CTM_USER_LOGIN_API_TAB a WHERE a.username = '".$username."'";
    
    $sql_ifs_user = oci_parse($conn, $query_ifs_user);
    oci_execute($sql_ifs_user);
    $data_ifs_user = oci_fetch_array($sql_ifs_user, OCI_ASSOC+OCI_RETURN_NULLS);
    
    $ifs_user = $data_ifs_user['IFS_USER'];

    return $ifs_user;
}

function checkAccess($ifs_user, $api_id) {
    $conn = getConnection();
    $query_check_acccess = "SELECT 1 CHECK_ACCESS FROM CTM_USER_API_MAPPING a WHERE a.ifs_user = '".$ifs_user."' AND a.api_id = '".$api_id."'";
    
    $sql_check_acccess = oci_parse($conn, $query_check_acccess);
    oci_execute($sql_check_acccess);
    $data_check_acccess = oci_fetch_array($sql_check_acccess, OCI_ASSOC+OCI_RETURN_NULLS);
    $count_check_acccess = (int)$data_check_acccess['CHECK_ACCESS'];
    
    if ($count_check_acccess == 1) {
        $access = "true";
    } else {
        $access = "false";
    }

    return $access;
}

function insertUserLog($username, $api_id, $id_address) {
    $conn = getConnection();
    $query_get_log_id = "SELECT (NVL(LOG_ID,0)+1) LOG_ID FROM (SELECT LOG_ID FROM CTM_USER_LOG_API_TAB WHERE ROWNUM = 1 ORDER BY LOG_ID DESC)";
    
    $sql_get_log_id = oci_parse($conn, $query_get_log_id);
    oci_execute($sql_get_log_id);
    $data_get_log_id = oci_fetch_array($sql_get_log_id, OCI_ASSOC+OCI_RETURN_NULLS);
    $count_get_log_id = (int)$data_get_log_id['LOG_ID'];
    
    if ($count_get_log_id == '') {
        $log_id = 1;
    } else {
        $log_id = $count_get_log_id;
    }

    $query_insert_log = "INSERT INTO CTM_USER_LOG_API_TAB (LOG_ID, USERNAME, API_ID, LOG_START, LOG_FINISH, STATUS, IP_ADDRESS) VALUES (".$log_id.", '".$username."', '".$api_id."', TO_DATE(TO_CHAR(SYSDATE, 'DD/MM/YYYY HH24:MI:SS'), 'DD/MM/YYYY HH24:MI:SS'), '', 'Load!', '".$id_address."')";
    
    $sql_insert_log = oci_parse($conn, $query_insert_log);
    
    if (oci_execute($sql_insert_log)) {
        $e = "Success";
    } else {
        $e = oci_error();
    }

    return $log_id;
}

function updateUserLog($log_id, $status) {
    $conn = getConnection();
    $query_update_log = "UPDATE CTM_USER_LOG_API_TAB SET LOG_FINISH = TO_DATE(TO_CHAR(SYSDATE, 'DD/MM/YYYY HH24:MI:SS'), 'DD/MM/YYYY HH24:MI:SS'), STATUS = '".$status."' WHERE LOG_ID = ".$log_id;
    
    $sql_update_log = oci_parse($conn, $query_update_log);
    
    if (oci_execute($sql_update_log)) {
        $e = "Success";
    } else {
        $e = oci_error();
    }

    return $e;
}
?>