<?php

use Slim\App;
//use Slim\Http\Request;
//use Slim\Http\Response;
use Slim\Helper\Set;

use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Message\ResponseInterface;

use Firebase\JWT\JWT;
use Tuupola\Base62;

require '../vendor/autoload.php';

//include "connection.php";
include "selectlist.php";
include "input_type_reader.php";

$dotenv = Dotenv\Dotenv::createImmutable(__DIR__);
$dotenv->load();

return function (App $app) {
    $container = $app->getContainer();
    //$app = new App();
    
    //
    $app->post("/token", function (ServerRequestInterface $request, ResponseInterface $response, array $args) use ($container){
        $login_data = $request->getParsedBody();

        $conn = getConnection();

        $error_msg = "";
        $error_msg = string_type($login_data['USERNAME'], $error_msg, 'Username', 'NotNull');
        $error_msg = string_type($login_data['PASSWORD'], $error_msg, 'Password', 'NotNull');

        if ($error_msg == '') {
            /*
                $query2 = "SELECT USERNAME, PASSWORD FROM CTM_EXAMPLE_LOGIN_TAB WHERE username = '".$login_data['USERNAME']."' AND password = '".$login_data['PASSWORD']."'";
                
                $sql = oci_parse($conn, $query2);
                oci_execute($sql);
                
                $rows = array();
                while ($dt = oci_fetch_assoc($sql)) {
                    $rows[] = $dt;
                }
            */

            $data = array(
                'username' => $login_data['USERNAME'],
                'password' => $login_data['PASSWORD']
            );

            $url = 'https://apifactory.telkom.co.id:8243/hcm/auth/v1/token';
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch,CURLOPT_RETURNTRANSFER,true);
            curl_setopt($ch,CURLOPT_HEADER, false);
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                'Content-Type: application/x-www-form-urlencoded'));
            curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($data));
            
            // Fetch and return content, save it.
            $raw_data = curl_exec($ch);
            curl_close($ch);

            // If the API is JSON, use json_decode.
            $data1 = json_decode($raw_data);
            $data2 = $data1->data->auth;

            //if (oci_num_rows($sql) > 0) {
            if ($data2 != false) {
                $now = new DateTime();
                $future = new DateTime("+43200 minutes");
                $ifs_user = getIfsUser($login_data['USERNAME']);
                //
                $jti = (new Base62)->encode(random_bytes(16));
                $payload = [
                    "user" => $login_data['USERNAME'],
                    "ifs_user" => $ifs_user,
                    "iat" => $now->getTimeStamp(),
                    "exp" => $future->getTimeStamp(),
                    "jti" => $jti
                ];
                $secret = getenv('SECRET_KEY');
                $token = JWT::encode($payload, $secret, "HS256");
                $data3["user"] = $login_data['USERNAME'];
                $data3["ifs_user"] = $ifs_user;
                $data3["token"] = $token;
                $data3["expires"] = $future->getTimeStamp();
                $data3["message"] = "Login Successful!";

                return $response->withStatus(201)
                    ->withHeader("Content-Type", "application/json")
                    ->write(json_encode($data3, JSON_UNESCAPED_SLASHES | JSON_PRETTY_PRINT));
            } else {
                return $response->withJson(["success" => "false", "message" => "Invalid Username or Password!"], 200);
            }
        } elseif ($error_msg != '') {
            return $response->withJson(["success" => "false", "message" => $error_msg], 200);
        }
    });

    /*$app->group('/api', function (\Slim\App $app){});*/
    //

    $app->get('/[{name}]', function (ServerRequestInterface $request, ResponseInterface $response, array $args) use ($container) {
        // Sample log message
        $container->get('logger')->info("Slim-Skeleton '/' route");

        // Render index view
        return $container->get('renderer')->render($response, 'index.phtml', $args);
    });

    //----------------------------------------------------------------------------------//
    //---------------------------------// [INF_KPST] //---------------------------------//
    //----------------------------------------------------------------------------------//

    $app->get("/example/", function (ServerRequestInterface $request, ResponseInterface $response, array $args){
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, "http://localhost:8081/list_pmp/");
        curl_setopt($ch, CURLOPT_HEADER, 0);            // No header in the result 
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); // Return, do not echo result   

        // Fetch and return content, save it.
        $raw_data = curl_exec($ch);
        curl_close($ch);

        // If the API is JSON, use json_decode.
        $data = json_decode($raw_data);
        //var_dump($data);
        
        return $response->withJson(["data" => $data, "error" => null, "status" => "success"], 200);
    }); //[INF_KPST_01]

    $app->get("/jmlh_peserta/", function (ServerRequestInterface $request, ResponseInterface $response, array $args){
        $periode = $request->getHeaderLine('PERIODE');
        $api_id = "INF_KPST_01";
        
        $ipAddress = $request->getAttribute('ip_address');
        $decoded = $request->getAttribute("decoded_jwt");
        $username = $decoded['user'];
        $ifs_user = $decoded['ifs_user'];

        $log_id = insertUserLog($username, $api_id, $ipAddress);
        
        $conn = getConnection();

        $access = checkAccess($ifs_user, $api_id);
        
        $error_msg = "";
        $error_msg = string_type($periode, $error_msg, 'Periode', 'NotNull');

        if ($access == 'false') {
            $error_msg = "User ".$ifs_user." Does Not Have Access to This Action!";
        }

        if ($error_msg == '') {
            $query1 = "SELECT SUM(jumlah_pst) JMLH_PST_AKTIF FROM DTL_REKAP_IUR_UNT_PENYETOR_TAB a WHERE a.kd_periode = '".$periode."'";
            $query2 = "SELECT COUNT(DISTINCT(a.n_i_k)) JMLH_PENSIUN FROM PENERIMAAN_MP a WHERE a.kd_periode = '".$periode."' AND a.kd_jenis_proses = '1'";
            $query3 = "SELECT COUNT(a.n_i_k) JMLH_PENERIMA_MP FROM PENERIMAAN_MP a WHERE a.kd_periode = '".$periode."' AND a.kd_jenis_proses = '1'";
            $query4 = "SELECT COUNT(a.n_i_k) JMLH_PST_MANTAN FROM PENERIMA_MP2 a WHERE PERIODE_API.Get_Tanggal_Awal_Periode(a.company,'".$periode."') BETWEEN a.valid_from AND a.valid_to AND a.kd_jenis_pensiun = '99'";
        
            $sql1 = oci_parse($conn, $query1);
            $sql2 = oci_parse($conn, $query2);
            $sql3 = oci_parse($conn, $query3);
            $sql4 = oci_parse($conn, $query4);

            oci_execute($sql1);
            oci_execute($sql2);
            oci_execute($sql3);
            oci_execute($sql4);

            $data1 = oci_fetch_array($sql1, OCI_ASSOC+OCI_RETURN_NULLS);
            $data2 = oci_fetch_array($sql2, OCI_ASSOC+OCI_RETURN_NULLS);
            $data3 = oci_fetch_array($sql3, OCI_ASSOC+OCI_RETURN_NULLS);
            $data4 = oci_fetch_array($sql4, OCI_ASSOC+OCI_RETURN_NULLS);

            $count1 = (int)$data1['JMLH_PST_AKTIF'];
            $count2 = (int)$data2['JMLH_PENSIUN'];
            $count3 = (int)$data3['JMLH_PENERIMA_MP'];
            $count4 = (int)$data4['JMLH_PST_MANTAN'];
            $count5 = $count1 + $count2 + $count4; //jmlh_peserta by NIK
            $count6 = $count1 + $count3 + $count4; //jmlh_peserta by PENERIMA MP

            $data = array("JMLH_PST_AKTIF"=>$count1,"JMLH_PENSIUN"=>$count2,"JMLH_PENERIMA_MP"=>$count3,"JMLH_PST_MANTAN"=>$count4,"JMLH_PESERTA_BY_NIK"=>$count5,"JMLH_PESERTA_BY_PMP"=>$count6);
        }

        if ($error_msg != '') {
            $status = "Failed! ".$error_msg;
        } else {
            $status = "Success!";
        }

        updateUserLog($log_id, $status);
        
        if (!$conn) {
            $e = oci_error();
            return $response->withJson(["data" => null, "error" => $e, "status" => "failed"], 200);
        } else {
            if ($error_msg != '') {
                return $response->withJson(["data" => null, "error" => $error_msg, "status" => "failed"], 200);
            } else {
                return $response->withJson(["data" => $data, "error" => null, "status" => "success"], 200);
            }
        }
    }); //[INF_KPST_01]

    $app->get("/jmlh_pmp_all/", function (ServerRequestInterface $request, ResponseInterface $response){
        $periode = $request->getHeaderLine('PERIODE');
        $parameter = $request->getHeaderLine('PARAMETER');
        $api_id = "INF_KPST_02";
        
        $ipAddress = $request->getAttribute('ip_address');
        $decoded = $request->getAttribute("decoded_jwt");
        $username = $decoded['user'];
        $ifs_user = $decoded['ifs_user'];

        $log_id = insertUserLog($username, $api_id, $ipAddress);

        $conn = getConnection();

        $access = checkAccess($ifs_user, $api_id);
        
        $error_msg = "";
        $error_msg = string_type($periode, $error_msg, 'Periode', 'NotNull');
        $error_msg = parameter_type($parameter, $error_msg, 'Parameter', 1, 10, 'NotNull');

        if ($access == 'false') {
            $error_msg = "User ".$ifs_user." Does Not Have Access to This Action!";
        }

        if ($error_msg == '') {
            if ($parameter == 1) {
                // PMP_JENIS_AW //
                $jaw = array("PESERTA", "SUAMI", "ISTRI", "ANAK", "WALI", "NULL");
                $prm_type = "JENIS_AW";
                $param_name = "PMP_JENIS_AW_";
                $data['PMP_JENIS_AW'] = selectJmlhPmpAll($jaw, $prm_type, $param_name, $periode);
            } elseif ($parameter == 2) {
                // PMP_KD_JNS_PENSIUN //
                $kd_jns_pensiun = array("11", "12", "13", "21", "22", "23", "31", "32", "33", "41", "42", "43", "52", "53", "62", "63", "71", "14", "24", "34", "44", "99", "54", "48", "64", "45", "46", "47", "NULL");
                $prm_type = "KD_JNS_PENSIUN";
                $param_name = "PMP_KD_JNS_PENSIUN_";
                $data['PMP_KD_JNS_PENSIUN'] = selectJmlhPmpAll($kd_jns_pensiun, $prm_type, $param_name, $periode);
            } elseif ($parameter == 3) {
                // PMP_KLMPK_DAPEN //
                $dapen = array("DAPEN I", "DAPEN II", "NULL");
                $prm_type = "DAPEN";
                $param_name = "PMP_KLMPK_";
                $data['PMP_KLMPK_DAPEN'] = selectJmlhPmpAll($dapen, $prm_type, $param_name, $periode);
            } elseif ($parameter == 4) {
                // PMP_KD_KLMPK //
                $kd_klmpk = array("PRA", "PASCA", "NULL");
                $prm_type = "KD_KLMPK";
                $param_name = "PMP_KD_KLMPK_";
                $data['PMP_KD_KLMPK'] = selectJmlhPmpAll($kd_klmpk, $prm_type, $param_name, $periode);
            } elseif ($parameter == 5) {
                // JMLH_PMP_BY_NIK //
                //$nik_array = array();
                $nik_query = "SELECT COUNT(DISTINCT(a.n_i_k)) JMLH_PMP_BY_NIK FROM PENERIMAAN_MP a WHERE a.kd_periode = '".$periode."' AND a.kd_jenis_proses = '1'";
                $nik_sql = oci_parse($conn, $nik_query);
                oci_execute($nik_sql);
                $nik_data = oci_fetch_array($nik_sql, OCI_ASSOC+OCI_RETURN_NULLS);
                $nik_count = (int)$nik_data['JMLH_PMP_BY_NIK'];
                //$nik_array['JMLH_PMP_BY_NIK'] = $nik_count;
                $data['JMLH_PMP_BY_NIK'] = $nik_count;
            } elseif ($parameter == 6) {
                // PMP_JENIS_KELAMIN //
                $jk = array("PRIA", "WANITA", "NULL");
                $prm_type = "JK";
                $param_name = "PMP_JK_";
                $data['PMP_JENIS_KELAMIN'] = selectJmlhPmpAll($jk, $prm_type, $param_name, $periode);
            } elseif ($parameter == 7) {
                // PMP_AGAMA //
                $agama = array("ISLAM", "KATOLIK", "BUDHA", "KRISTEN", "HINDU", "NULL");
                $prm_type = "AGAMA";
                $param_name = "PMP_AGAMA_";
                $data['PMP_AGAMA'] = selectJmlhPmpAll($agama, $prm_type, $param_name, $periode);
            } elseif ($parameter == 8) {
                // PMP_GOL_DRH //
                $goldar = array("O", "A", "B", "AB", "OMIN", "AMIN", "BMIN", "ABMIN", "OPLUS", "APLUS", "BPLUS", "ABPLUS", "NULL");
                $prm_type = "GOLDAR";
                $param_name = "PMP_GOL_DRH_";
                $data['PMP_GOL_DRH'] = selectJmlhPmpAll($goldar, $prm_type, $param_name, $periode);
            } elseif ($parameter == 9) {
                // PMP_KD_BAND //
                $goldar = array("I", "II", "III", "IV", "V", "VI", "VII", "NULL");
                $prm_type = "KD_BAND";
                $param_name = "PMP_KD_BAND_";
                $data['PMP_KD_BAND'] = selectJmlhPmpAll($goldar, $prm_type, $param_name, $periode);
            } elseif ($parameter == 10) {
                // PMP_PSNGN //
                $psngn = array("PMP", "MPS", "AKTIF", "HAPUS", "NULL");
                $param_name = "PMP_PSNGN_";
                $data['PMP_PSNGN'] = selectPsngnPmpAll($psngn, $param_name, $periode);
            }
        }
        
        if ($error_msg != '') {
            $status = "Failed! ".$error_msg;
        } else {
            $status = "Success!";
        }

        updateUserLog($log_id, $status);
        
        if (!$conn) {
            $e = oci_error();
            return $response->withJson(["data" => null, "error" => $e, "status" => "failed"], 200);
        } else {
            if ($error_msg != '') {
                return $response->withJson(["data" => null, "error" => $error_msg, "status" => "failed"], 200);
            } else {
                return $response->withJson(["data" => $data, "error" => null, "status" => "success"], 200);
            }
        }
    }); //[INF_KPST_02]

    $app->get("/jmlh_pmp/", function (ServerRequestInterface $request, ResponseInterface $response){
        $periode = $request->getHeaderLine('PERIODE');
        //$jmlh_aw = $request->getHeaderLine('JMLH_AW');
        $jmlh_aw = "";
        $param_jenis_aw = $request->getHeaderLine('PARAM_JENIS_AW');
        $kd_jenis_pensiun = $request->getHeaderLine('KD_JENIS_PENSIUN');
        $param_dapen = $request->getHeaderLine('PARAM_DAPEN');
        $param_kd_kelompok = $request->getHeaderLine('PARAM_KD_KELOMPOK');
        $param_jenis_kelamin = $request->getHeaderLine('PARAM_JENIS_KELAMIN');
        $param_agama = $request->getHeaderLine('PARAM_AGAMA');
        $param_gol_darah = $request->getHeaderLine('PARAM_GOL_DARAH');
        $n_i_k = $request->getHeaderLine('N_I_K');
        $range_tgl_kerja = $request->getHeaderLine('RANGE_TGL_KERJA');
        $range_tgl_lahir = $request->getHeaderLine('RANGE_TGL_LAHIR');
        $range_tgl_capeg = $request->getHeaderLine('RANGE_TGL_CAPEG');
        $range_tgl_berhenti = $request->getHeaderLine('RANGE_TGL_BERHENTI');
        $range_tgl_pensiun = $request->getHeaderLine('RANGE_TGL_PENSIUN');
        $range_tgl_mulai_pmp = $request->getHeaderLine('RANGE_TGL_MULAI_PMP');
        $param_kd_band = $request->getHeaderLine('PARAM_KD_BAND');
        $param_pasangan_mp = $request->getHeaderLine('PARAM_PASANGAN_PMP');
        $api_id = "INF_KPST_03";
        
        $ipAddress = $request->getAttribute('ip_address');
        $decoded = $request->getAttribute("decoded_jwt");
        $username = $decoded['user'];
        $ifs_user = $decoded['ifs_user'];

        $log_id = insertUserLog($username, $api_id, $ipAddress);

        $jns_api = "jmlh";

        $conn = getConnection();

        $access = checkAccess($ifs_user, $api_id);
        
        $error_msg = "";
        $error_msg = string_type($periode, $error_msg, 'Periode', 'NotNull');
        $error_msg = parameter_type($param_jenis_aw, $error_msg, 'Parameter Jenis AW', 1, 6, 'Nullable');
        $error_msg = string_type($kd_jenis_pensiun, $error_msg, 'Kd Jenis Pensiun', 'Nullable');
        $error_msg = parameter_type($param_dapen, $error_msg, 'Parameter Dapen', 1, 3, 'Nullable');
        $error_msg = parameter_type($param_kd_kelompok, $error_msg, 'Parameter Kd Kelompok', 1, 3, 'Nullable');
        $error_msg = parameter_type($param_jenis_kelamin, $error_msg, 'Parameter Jenis Kelamin', 1, 3, 'Nullable');
        $error_msg = parameter_type($param_agama, $error_msg, 'Parameter Agama', 1, 6, 'Nullable');
        $error_msg = parameter_type($param_gol_darah, $error_msg, 'Parameter Golongan Darah', 1, 13, 'Nullable');
        $error_msg = string_type($n_i_k, $error_msg, 'NIK', 'Nullable');
        $error_msg = string_type($range_tgl_kerja, $error_msg, 'Range Tanggal Kerja', 'Nullable');
        $error_msg = string_type($range_tgl_lahir, $error_msg, 'Range Tanggal Lahir', 'Nullable');
        $error_msg = string_type($range_tgl_capeg, $error_msg, 'Range Tanggal Capeg', 'Nullable');
        $error_msg = string_type($range_tgl_berhenti, $error_msg, 'Range Tanggal Berhenti', 'Nullable');
        $error_msg = string_type($range_tgl_pensiun, $error_msg, 'Range Tanggal Pensiun', 'Nullable');
        $error_msg = string_type($range_tgl_mulai_pmp, $error_msg, 'Range Tanggal Mulai PMP', 'Nullable');
        $error_msg = parameter_type($param_kd_band, $error_msg, 'Parameter Kd Band', 1, 8, 'Nullable');
        $error_msg = parameter_type($param_pasangan_mp, $error_msg, 'Parameter Pasangan MP', 1, 4, 'Nullable');

        if ($access == 'false') {
            $error_msg = "User ".$ifs_user." Does Not Have Access to This Action!";
        }

        if ($error_msg == '') {
            $count = jmlhPmp($periode, $jmlh_aw, $param_jenis_aw, $kd_jenis_pensiun, $param_dapen, $param_kd_kelompok, $param_jenis_kelamin, $param_agama, $param_gol_darah, $n_i_k, $range_tgl_kerja, $range_tgl_lahir, $range_tgl_capeg, $range_tgl_berhenti, $range_tgl_pensiun, $range_tgl_mulai_pmp, $param_kd_band, $param_pasangan_mp, $jns_api);

            $data['JMLH_PMP'] = $count;
        }
        
        if ($error_msg != '') {
            $status = "Failed! ".$error_msg;
        } else {
            $status = "Success!";
        }

        updateUserLog($log_id, $status);
        
        if (!$conn) {
            $e = oci_error();
            return $response->withJson(["data" => null, "error" => $e, "status" => "failed"], 200);
        } else {
            if ($error_msg != '') {
                return $response->withJson(["data" => null, "error" => $error_msg, "status" => "failed"], 200);
            } else {
                return $response->withJson(["data" => $data, "error" => null, "status" => "success"], 200);
            }
        }
    }); //[INF_KPST_03]

    $app->get("/list_pmp/", function (ServerRequestInterface $request, ResponseInterface $response){
        $periode = $request->getHeaderLine('PERIODE');
        $jmlh_aw = "";
        $param_jenis_aw = $request->getHeaderLine('PARAM_JENIS_AW');
        $kd_jenis_pensiun = $request->getHeaderLine('KD_JENIS_PENSIUN');
        $param_dapen = $request->getHeaderLine('PARAM_DAPEN');
        $param_kd_kelompok = $request->getHeaderLine('PARAM_KD_KELOMPOK');
        $param_jenis_kelamin = $request->getHeaderLine('PARAM_JENIS_KELAMIN');
        $param_agama = $request->getHeaderLine('PARAM_AGAMA');
        $param_gol_darah = $request->getHeaderLine('PARAM_GOL_DARAH');
        $n_i_k = $request->getHeaderLine('N_I_K');
        $range_tgl_kerja = $request->getHeaderLine('RANGE_TGL_KERJA');
        $range_tgl_lahir = $request->getHeaderLine('RANGE_TGL_LAHIR');
        $range_tgl_capeg = $request->getHeaderLine('RANGE_TGL_CAPEG');
        $range_tgl_berhenti = $request->getHeaderLine('RANGE_TGL_BERHENTI');
        $range_tgl_pensiun = $request->getHeaderLine('RANGE_TGL_PENSIUN');
        $range_tgl_mulai_pmp = $request->getHeaderLine('RANGE_TGL_MULAI_PMP');
        $param_kd_band = $request->getHeaderLine('PARAM_KD_BAND');
        $param_pasangan_mp = $request->getHeaderLine('PARAM_PASANGAN_PMP');
        $api_id = "INF_KPST_04";
        
        $ipAddress = $request->getAttribute('ip_address');
        $decoded = $request->getAttribute("decoded_jwt");
        $username = $decoded['user'];
        $ifs_user = $decoded['ifs_user'];

        $log_id = insertUserLog($username, $api_id, $ipAddress);

        $jns_api = "list";

        $conn = getConnection();

        $access = checkAccess($ifs_user, $api_id);

        $error_msg = "";
        $error_msg = string_type($periode, $error_msg, 'Periode', 'NotNull');
        $error_msg = parameter_type($param_jenis_aw, $error_msg, 'Parameter Jenis AW', 1, 6, 'Nullable');
        $error_msg = string_type($kd_jenis_pensiun, $error_msg, 'Kd Jenis Pensiun', 'Nullable');
        $error_msg = parameter_type($param_dapen, $error_msg, 'Parameter Dapen', 1, 3, 'Nullable');
        $error_msg = parameter_type($param_kd_kelompok, $error_msg, 'Parameter Kd Kelompok', 1, 3, 'Nullable');
        $error_msg = parameter_type($param_jenis_kelamin, $error_msg, 'Parameter Jenis Kelamin', 1, 3, 'Nullable');
        $error_msg = parameter_type($param_agama, $error_msg, 'Parameter Agama', 1, 6, 'Nullable');
        $error_msg = parameter_type($param_gol_darah, $error_msg, 'Parameter Golongan Darah', 1, 13, 'Nullable');
        $error_msg = string_type($n_i_k, $error_msg, 'NIK', 'Nullable');
        $error_msg = string_type($range_tgl_kerja, $error_msg, 'Range Tanggal Kerja', 'Nullable');
        $error_msg = string_type($range_tgl_lahir, $error_msg, 'Range Tanggal Lahir', 'Nullable');
        $error_msg = string_type($range_tgl_capeg, $error_msg, 'Range Tanggal Capeg', 'Nullable');
        $error_msg = string_type($range_tgl_berhenti, $error_msg, 'Range Tanggal Berhenti', 'Nullable');
        $error_msg = string_type($range_tgl_pensiun, $error_msg, 'Range Tanggal Pensiun', 'Nullable');
        $error_msg = string_type($range_tgl_mulai_pmp, $error_msg, 'Range Tanggal Mulai PMP', 'Nullable');
        $error_msg = parameter_type($param_kd_band, $error_msg, 'Parameter Kd Band', 1, 8, 'Nullable');
        $error_msg = parameter_type($param_pasangan_mp, $error_msg, 'Parameter Pasangan MP', 1, 4, 'Nullable');

        if ($access == 'false') {
            $error_msg = "User ".$ifs_user." Does Not Have Access to This Action!";
        }

        if ($error_msg == '') {
            $data = jmlhPmp($periode, $jmlh_aw, $param_jenis_aw, $kd_jenis_pensiun, $param_dapen, $param_kd_kelompok, $param_jenis_kelamin, $param_agama, $param_gol_darah, $n_i_k, $range_tgl_kerja, $range_tgl_lahir, $range_tgl_capeg, $range_tgl_berhenti, $range_tgl_pensiun, $range_tgl_mulai_pmp, $param_kd_band, $param_pasangan_mp, $jns_api);
        }
        
        if ($error_msg != '') {
            $status = "Failed! ".$error_msg;
        } else {
            $status = "Success!";
        }

        updateUserLog($log_id, $status);
        
        if (!$conn) {
            $e = oci_error();
            return $response->withJson(["data" => null, "error" => $e, "status" => "failed"], 200);
        } else {
            if ($periode == '' && $range_tgl_kerja == '' && $range_tgl_lahir == '' && $range_tgl_capeg == '' && $range_tgl_berhenti == '' && $range_tgl_pensiun == '' && $range_tgl_mulai_pmp == '') {
                return $response->withJson(["data" => null, "error" => "'Periode/Range Tgl Kerja/Range Tgl Lahir/Range Tgl Capeg/Range Tgl Berhenti/Range Tgl Pensiun/Range Tgl Mulai PMP' Cannot be Empty!", "status" => "failed"], 200);
            } elseif ($error_msg != '') {
                return $response->withJson(["data" => null, "error" => $error_msg, "status" => "failed"], 200);
            } else {
                return $response->withJson(["data" => $data, "error" => null, "status" => "success"], 200);
            }
        }
    }); //[INF_KPST_04]

    $app->get("/detail_pmp/", function (ServerRequestInterface $request, ResponseInterface $response, array $args) {
        $periode = $request->getHeaderLine('PERIODE');
        $nik = $request->getHeaderLine('NIK');
        $naw = $request->getHeaderLine('NO_AHLI_WARIS');
        $api_id = "INF_KPST_05";
        
        $ipAddress = $request->getAttribute('ip_address');
        $decoded = $request->getAttribute("decoded_jwt");
        $username = $decoded['user'];
        $ifs_user = $decoded['ifs_user'];

        $log_id = insertUserLog($username, $api_id, $ipAddress);

        $conn = getConnection();

        $access = checkAccess($ifs_user, $api_id);

        $error_msg = "";
        $error_msg = string_type($periode, $error_msg, 'Periode', 'NotNull');
        $error_msg = string_type($nik, $error_msg, 'NIK', 'NotNull');
        $error_msg = int_type($naw, $error_msg, 'No Ahli Waris', 'NotNull');

        if ($access == 'false') {
            $error_msg = "User ".$ifs_user." Does Not Have Access to This Action!";
        }

        if ($error_msg == '') {
            $query1 = "SELECT a.n_i_k, CALON_PENERIMA_MP_API.Get_Nama_Ahli_Waris(a.company, a.n_i_k, a.no_ahli_waris) nama, CALON_PENERIMA_MP_API.Get_Agama(a.company, a.n_i_k, a.no_ahli_waris) agama, CALON_PENERIMA_MP_API.Get_Gol_Darah(a.company, a.n_i_k, a.no_ahli_waris) gol_darah, CALON_PENERIMA_MP_API.Get_Jenis_Kelamin2(a.company, a.n_i_k, a.no_ahli_waris) jenis_kelamin, a.no_ahli_waris, PESERTA_API.Get_Kd_Kelompok(a.company, a.n_i_k) kd_kelompok, KELOMPOK_PESERTA_API.Get_Nama_Kelompok(a.company, PESERTA_API.Get_Kd_Kelompok(a.company, a.n_i_k)) nama_kelompok, KELOMPOK_PESERTA_API.Get_Kelompok_Dapen(a.company, PESERTA_API.Get_Kd_Kelompok(a.company, a.n_i_k)) kelompok_dapen, MITRA_KERJA_API.Get_Nama_Mitra_Kerja(a.company, ALAMAT_PENERIMA_MP_API.Get_P2TEL_default(a.company, a.n_i_k, a.no_ahli_waris, sysdate)) nama_cabang, (a.no_ahli_waris || '' || a.n_i_k) id, (a.n_i_k || '_' || a.no_ahli_waris) nik_naw, CALON_PENERIMA_MP_API.Get_No_Ktp(a.company, a.n_i_k, a.no_ahli_waris) ktp, CALON_PENERIMA_MP_API.Get_No_Npwp(a.company, a.n_i_k, a.no_ahli_waris) npwp, (SELECT b.picture_id FROM PENERIMA_MP2 b WHERE b.company = a.company AND b.n_i_k = a.n_i_k AND b.no_ahli_waris = a.no_ahli_waris) photo, CALON_PENERIMA_MP_API.Get_Tgl_Lahir(a.company, a.n_i_k, a.no_ahli_waris) tgl_lahir, CALON_PENERIMA_MP_API.Get_Tgl_Meninggal(a.company, a.n_i_k, a.no_ahli_waris) tgl_meninggal, PENERIMA_MP_API.Get_Tanggal_Pensiun(a.company,a.n_i_k) tgl_pensiun, CALON_PENERIMA_MP_API.Get_Jenis_Ahli_Waris(a.company, a.n_i_k, a.no_ahli_waris) tipe_aw, ALAMAT_PENERIMA_MP_API.Get_P2TEL_default(a.company, a.n_i_k, a.no_ahli_waris, sysdate) cab_p2tel, (a.no_ahli_waris || '.' || a.n_i_k || '.' || ALAMAT_PENERIMA_MP_API.Get_No_Urut_Default(a.company, a.n_i_k, a.no_ahli_waris)) id_alamat, ALAMAT_PENERIMA_MP_API.Get_No_Urut_Default(a.company, a.n_i_k, a.no_ahli_waris) id_default, ALAMAT_PENERIMA_MP_API.Get_Jalan(a.company, a.no_ahli_waris, a.n_i_k, ALAMAT_PENERIMA_MP_API.Get_No_Urut_Default(a.company, a.n_i_k, a.no_ahli_waris)) jalan, ALAMAT_PENERIMA_MP_API.Get_Komplek(a.company, a.no_ahli_waris, a.n_i_k, ALAMAT_PENERIMA_MP_API.Get_No_Urut_Default(a.company, a.n_i_k, a.no_ahli_waris)) komplek, ALAMAT_PENERIMA_MP_API.Get_Blok(a.company, a.no_ahli_waris, a.n_i_k, ALAMAT_PENERIMA_MP_API.Get_No_Urut_Default(a.company, a.n_i_k, a.no_ahli_waris)) blok, ALAMAT_PENERIMA_MP_API.Get_No_Rumah(a.company, a.no_ahli_waris, a.n_i_k, ALAMAT_PENERIMA_MP_API.Get_No_Urut_Default(a.company, a.n_i_k, a.no_ahli_waris)) no, ALAMAT_PENERIMA_MP_API.Get_Rt(a.company, a.no_ahli_waris, a.n_i_k, ALAMAT_PENERIMA_MP_API.Get_No_Urut_Default(a.company, a.n_i_k, a.no_ahli_waris)) rt, ALAMAT_PENERIMA_MP_API.Get_Rw(a.company, a.no_ahli_waris, a.n_i_k, ALAMAT_PENERIMA_MP_API.Get_No_Urut_Default(a.company, a.n_i_k, a.no_ahli_waris)) rw, ALAMAT_PENERIMA_MP_API.Get_Kelurahan_Id(a.company, a.no_ahli_waris, a.n_i_k, ALAMAT_PENERIMA_MP_API.Get_No_Urut_Default(a.company, a.n_i_k, a.no_ahli_waris)) kd_kelurahan, ALAMAT_PENERIMA_MP_API.Get_Kecamatan_Id(a.company, a.no_ahli_waris, a.n_i_k, ALAMAT_PENERIMA_MP_API.Get_No_Urut_Default(a.company, a.n_i_k, a.no_ahli_waris)) kd_kecamatan, ALAMAT_PENERIMA_MP_API.Get_Kota_Id(a.company, a.no_ahli_waris, a.n_i_k, ALAMAT_PENERIMA_MP_API.Get_No_Urut_Default(a.company, a.n_i_k, a.no_ahli_waris)) kd_kota, ALAMAT_PENERIMA_MP_API.Get_Provinsi_Id(a.company, a.no_ahli_waris, a.n_i_k, ALAMAT_PENERIMA_MP_API.Get_No_Urut_Default(a.company, a.n_i_k, a.no_ahli_waris)) kd_provinsi, ALAMAT_PENERIMA_MP_API.Get_Kode_Pos(a.company, a.no_ahli_waris, a.n_i_k, ALAMAT_PENERIMA_MP_API.Get_No_Urut_Default(a.company, a.n_i_k, a.no_ahli_waris)) kd_pos, KELURAHAN_API.Get_Nama_By_Id(ALAMAT_PENERIMA_MP_API.Get_Kelurahan_Id(a.company, a.no_ahli_waris, a.n_i_k, ALAMAT_PENERIMA_MP_API.Get_No_Urut_Default(a.company, a.n_i_k, a.no_ahli_waris))) nama_kelurahan, KECAMATAN_API.Get_Nama_By_Id(ALAMAT_PENERIMA_MP_API.Get_Kecamatan_Id(a.company, a.no_ahli_waris, a.n_i_k, ALAMAT_PENERIMA_MP_API.Get_No_Urut_Default(a.company, a.n_i_k, a.no_ahli_waris))) nama_kecamatan, KOTA_API.Get_Nama_By_Id(ALAMAT_PENERIMA_MP_API.Get_Kota_Id(a.company, a.no_ahli_waris, a.n_i_k, ALAMAT_PENERIMA_MP_API.Get_No_Urut_Default(a.company, a.n_i_k, a.no_ahli_waris))) nama_kota, PROVINSI_API.Get_Nama_By_Id(ALAMAT_PENERIMA_MP_API.Get_Provinsi_Id(a.company, a.no_ahli_waris, a.n_i_k, ALAMAT_PENERIMA_MP_API.Get_No_Urut_Default(a.company, a.n_i_k, a.no_ahli_waris))) nama_provinsi FROM PENERIMAAN_MP a WHERE a.kd_periode = '".$periode."' AND a.n_i_k = '".$nik."' AND a.no_ahli_waris = '".$naw."' AND a.kd_jenis_proses = '1'";

            $kontak_query = "SELECT (a.no_ahli_waris || '.' || a.n_i_k || '.' || a.no_urut_alamat || '.' || a.no_urut_kontak) id_kontak, JENIS_KONTAK_API.Get_Keterangan(a.kd_kontak) tipe_kontak, a.kd_kontak ref_tipe_kontak, a.data_kontak FROM KONTAK_PENERIMA_MP_TAB a WHERE a.n_i_k = '".$nik."' AND a.no_ahli_waris = '".$naw."'";
            
            $sql1 = oci_parse($conn, $query1);
            $kontak_sql = oci_parse($conn, $kontak_query);
            
            oci_execute($sql1);
            oci_execute($kontak_sql);
            
            $rows = array();
            while ($dt = oci_fetch_assoc($sql1)) {
                $rows[] = $dt;
            }

            $kntk_rows = array();
            while ($kntk = oci_fetch_assoc($kontak_sql)) {
                $kntk_rows[] = $kntk;
            }

            $data['DATA_DIRI'] = $rows;
            $data['KONTAK'] = $kntk_rows;
        }
        
        if ($error_msg != '') {
            $status = "Failed! ".$error_msg;
        } else {
            $status = "Success!";
        }

        updateUserLog($log_id, $status);
        
        if (!$conn) {
            $e = oci_error();
            return $response->withJson(["data" => null, "error" => $e, "status" => "failed"], 200);
        } else {
            if ($error_msg != '') {
                return $response->withJson(["data" => null, "error" => $error_msg, "status" => "failed"], 200);
            } else {
                return $response->withJson(["data" => $data, "error" => null, "status" => "success"], 200);
            }
        }
    }); //[INF_KPST_05]

    $app->get("/jmlh_pst_aktif/", function (ServerRequestInterface $request, ResponseInterface $response){
        $conn = getConnection();
        $jns_api = "Jumlah";
        $pst_type = "Aktif";
        
        $periode = $request->getHeaderLine('PERIODE');
        $param_kd_kelompok = $request->getHeaderLine('PARAM_KD_KELOMPOK');
        $dapen = "";
        $param_jenis_kelamin = $request->getHeaderLine('PARAM_JENIS_KELAMIN');
        $param_agama = $request->getHeaderLine('PARAM_AGAMA');
        $param_gol_darah = $request->getHeaderLine('PARAM_GOL_DARAH');
        /*
            $kd_klmpk = $request->getHeaderLine('KD_KELOMPOK');
            $jk = $request->getHeaderLine('JENIS_KELAMIN');
            $agama = $request->getHeaderLine('AGAMA');
            $goldar = $request->getHeaderLine('GOL_DARAH');
        */
        $api_id = "INF_KPST_06";
        
        $ipAddress = $request->getAttribute('ip_address');
        $decoded = $request->getAttribute("decoded_jwt");
        $username = $decoded['user'];
        $ifs_user = $decoded['ifs_user'];

        $log_id = insertUserLog($username, $api_id, $ipAddress);

        $access = checkAccess($ifs_user, $api_id);

        $error_msg = "";
        $error_msg = string_type($periode, $error_msg, 'Periode', 'NotNull');
        $error_msg = parameter_type($param_kd_kelompok, $error_msg, 'Parameter Kd Kelompok', 1, 3, 'Nullable');
        $error_msg = parameter_type($param_jenis_kelamin, $error_msg, 'Parameter Jenis Kelamin', 1, 3, 'Nullable');
        $error_msg = parameter_type($param_agama, $error_msg, 'Parameter Agama', 1, 6, 'Nullable');
        $error_msg = parameter_type($param_gol_darah, $error_msg, 'Parameter Golongan Darah', 1, 13, 'Nullable');
        /*
            $error_msg = string_type($kd_klmpk, $error_msg, 'Kd Kelompok', 'Nullable');
            $error_msg = string_type($jk, $error_msg, 'Jenis Kelamin', 'Nullable');
            $error_msg = int_type($agama, $error_msg, 'Agama', 'Nullable');
            $error_msg = int_type($goldar, $error_msg, 'Golongan Darah', 'Nullable');
        */

        if ($access == 'false') {
            $error_msg = "User ".$ifs_user." Does Not Have Access to This Action!";
        }

        if ($error_msg == '') {
            $data['JMLH_PST_AKTIF'] = jmlhPst($periode, $jns_api, $pst_type, $param_kd_kelompok, $dapen, $param_jenis_kelamin, $param_agama, $param_gol_darah);
        }
    
        if ($error_msg != '') {
            $status = "Failed! ".$error_msg;
        } else {
            $status = "Success!";
        }

        updateUserLog($log_id, $status);
        
        if (!$conn) {
            $e = oci_error();
            return $response->withJson(["data" => null, "error" => $e, "status" => "failed"], 200);
        } else {
            if ($error_msg != '') {
                return $response->withJson(["data" => null, "error" => $error_msg, "status" => "failed"], 200);
            } else {
                return $response->withJson(["data" => $data, "error" => null, "status" => "success"], 200);
            }
        }
    }); //[INF_KPST_06]

    $app->get("/jmlh_pst_aktif_all/", function (ServerRequestInterface $request, ResponseInterface $response){
        $periode = $request->getHeaderLine('PERIODE');
        $parameter = $request->getHeaderLine('PARAMETER');
        $api_id = "INF_KPST_07";
        
        $ipAddress = $request->getAttribute('ip_address');
        $decoded = $request->getAttribute("decoded_jwt");
        $username = $decoded['user'];
        $ifs_user = $decoded['ifs_user'];

        $log_id = insertUserLog($username, $api_id, $ipAddress);

        $conn = getConnection();
        $pst_type = "Aktif";

        $access = checkAccess($ifs_user, $api_id);

        $error_msg = "";
        $error_msg = string_type($periode, $error_msg, 'Periode', 'NotNull');
        $error_msg = parameter_type($parameter, $error_msg, 'Parameter', 1, 4, 'NotNull');

        if ($access == 'false') {
            $error_msg = "User ".$ifs_user." Does Not Have Access to This Action!";
        }

        if ($error_msg == '') {
            /*
                if ($parameter == '1') {
                    // JMLH_PST_AKTIF //
                    $query1 = "SELECT COUNT(a.n_i_k) jmlh_pst_aktif FROM PESERTA_AKTIF a WHERE a.status_peserta = 'Aktif'";
                    $sql1 = oci_parse($conn, $query1);
                    oci_execute($sql1);
                    $data1 = oci_fetch_array($sql1, OCI_ASSOC+OCI_RETURN_NULLS);
                    $count1 = (int)$data1['JMLH_PST_AKTIF'];
                    $data['JMLH_PST_AKTIF'] = $count1;
                } else
            */
            if ($parameter == '1') {
                // PST_AKTIF_KD_KLMPK //
                $kd_klmpk = array("PRA", "PASCA", "NULL");
                $prm_type = "KD_KLMPK";
                $pst = "KELOMPOK_PESERTA_API.Get_Nama_Kelompok(a.company, a.kd_kelompok)";
                $data['PST_AKTIF_KD_KLMPK'] = selectJmlhPstAll($kd_klmpk, $prm_type, $pst, $pst_type, $periode);
            } elseif ($parameter == '2') {
                // PST_AKTIF_JNS_KLMN //
                $jk = array("Pria", "Wanita", "NULL");
                $prm_type = "JK";
                $pst = "CALON_PENERIMA_MP_API.Get_Jenis_Kelamin2(a.COMPANY, a.N_I_K, 1)";
                $data['PST_AKTIF_JNS_KLMN'] = selectJmlhPstAll($jk, $prm_type, $pst, $pst_type, $periode);
            } elseif ($parameter == '3') {
                // PST_AKTIF_AGAMA //
                $agama = array("Islam", "Katolik", "Budha", "Kristen", "Hindu", "NULL");
                $prm_type = "AGAMA";
                $pst = "CALON_PENERIMA_MP_API.Get_Agama(a.COMPANY, a.N_I_K, 1)";
                $data['PST_AKTIF_AGAMA'] = selectJmlhPstAll($agama, $prm_type, $pst, $pst_type, $periode);
            } elseif ($parameter == '4') {
                // PST_AKTIF_GOLDAR //
                $goldar = array("O", "A", "B", "AB", "OMIN", "AMIN", "BMIN", "ABMIN", "OPLUS", "APLUS", "BPLUS", "ABPLUS", "NULL");
                $prm_type = "GOLDAR";
                $pst = "CALON_PENERIMA_MP_API.Get_Gol_Darah(a.COMPANY, a.N_I_K, 1)";
                $data['PST_AKTIF_GOLDAR'] = selectJmlhPstAll($goldar, $prm_type, $pst, $pst_type, $periode);
            }
        }
        
        if ($error_msg != '') {
            $status = "Failed! ".$error_msg;
        } else {
            $status = "Success!";
        }

        updateUserLog($log_id, $status);
        
        if (!$conn) {
            $e = oci_error();
            return $response->withJson(["data" => null, "error" => $e, "status" => "failed"], 200);
        } else {
            if ($error_msg != '') {
                return $response->withJson(["data" => null, "error" => $error_msg, "status" => "failed"], 200);
            } else {
                return $response->withJson(["data" => $data, "error" => null, "status" => "success"], 200);
            }
        }
    }); //[INF_KPST_07]

    $app->get("/list_pst_aktif/", function (ServerRequestInterface $request, ResponseInterface $response, array $args) {
        $conn = getConnection();
        $jns_api = "List";
        $pst_type = "Aktif";

        $periode = $request->getHeaderLine('PERIODE');
        $param_kd_kelompok = $request->getHeaderLine('PARAM_KD_KELOMPOK');
        $dapen = "";
        $param_jenis_kelamin = $request->getHeaderLine('PARAM_JENIS_KELAMIN');
        $param_agama = $request->getHeaderLine('PARAM_AGAMA');
        $param_gol_darah = $request->getHeaderLine('PARAM_GOL_DARAH');
        /*
            $kd_klmpk = $request->getHeaderLine('KD_KELOMPOK');
            $jk = $request->getHeaderLine('JENIS_KELAMIN');
            $agama = $request->getHeaderLine('AGAMA');
            $goldar = $request->getHeaderLine('GOL_DARAH');
        */
        $api_id = "INF_KPST_08";
        
        $ipAddress = $request->getAttribute('ip_address');
        $decoded = $request->getAttribute("decoded_jwt");
        $username = $decoded['user'];
        $ifs_user = $decoded['ifs_user'];

        $log_id = insertUserLog($username, $api_id, $ipAddress);

        $access = checkAccess($ifs_user, $api_id);

        $error_msg = "";
        $error_msg = string_type($periode, $error_msg, 'Periode', 'NotNull');
        $error_msg = parameter_type($param_kd_kelompok, $error_msg, 'Parameter Kd Kelompok', 1, 3, 'Nullable');
        $error_msg = parameter_type($param_jenis_kelamin, $error_msg, 'Parameter Jenis Kelamin', 1, 3, 'Nullable');
        $error_msg = parameter_type($param_agama, $error_msg, 'Parameter Agama', 1, 6, 'Nullable');
        $error_msg = parameter_type($param_gol_darah, $error_msg, 'Parameter Golongan Darah', 1, 13, 'Nullable');
        /*
            $error_msg = string_type($kd_klmpk, $error_msg, 'Kd Kelompok', 'Nullable');
            $error_msg = string_type($jk, $error_msg, 'Jenis Kelamin', 'Nullable');
            $error_msg = int_type($agama, $error_msg, 'Agama', 'Nullable');
            $error_msg = int_type($goldar, $error_msg, 'Golongan Darah', 'Nullable');
        */

        if ($access == 'false') {
            $error_msg = "User ".$ifs_user." Does Not Have Access to This Action!";
        }

        if ($error_msg == '') {
            $data = jmlhPst($periode, $jns_api, $pst_type, $param_kd_kelompok, $dapen, $param_jenis_kelamin, $param_agama, $param_gol_darah);
        }

        if ($error_msg != '') {
            $status = "Failed! ".$error_msg;
        } else {
            $status = "Success!";
        }

        updateUserLog($log_id, $status);
        
        if (!$conn) {
            $e = oci_error();
            return $response->withJson(["data" => null, "error" => $e, "status" => "failed"], 200);
        } else {
            if ($error_msg != '') {
                return $response->withJson(["data" => null, "error" => $error_msg, "status" => "failed"], 200);
            } else {
                return $response->withJson(["data" => $data, "error" => null, "status" => "success"], 200);
            }
        }
    }); //[INF_KPST_08]

    $app->get("/detail_pst_aktif/", function (ServerRequestInterface $request, ResponseInterface $response, array $args) {
        $nik = $request->getHeaderLine('NIK');
        $api_id = "INF_KPST_09";
        
        $ipAddress = $request->getAttribute('ip_address');
        $decoded = $request->getAttribute("decoded_jwt");
        $username = $decoded['user'];
        $ifs_user = $decoded['ifs_user'];

        $log_id = insertUserLog($username, $api_id, $ipAddress);

        $conn = getConnection();

        $access = checkAccess($ifs_user, $api_id);

        $error_msg = "";
        $error_msg = string_type($nik, $error_msg, 'NIK', 'NotNull');

        if ($access == 'false') {
            $error_msg = "User ".$ifs_user." Does Not Have Access to This Action!";
        }

        if ($error_msg == '') {
            $query1 = "SELECT a.n_i_k, a.nama_pegawai nama, CALON_PENERIMA_MP_API.Get_Jenis_Kelamin2(a.COMPANY, a.N_I_K, 1) jenis_kelamin, CALON_PENERIMA_MP_API.Get_Agama(a.COMPANY, a.N_I_K, 1) agama, CALON_PENERIMA_MP_API.Get_Gol_Darah(a.COMPANY, a.N_I_K, 1) gol_darah, a.tgl_lahir, a.tgl_kerja, a.tgl_capeg, a.tgl_pegprus, a.tgl_berhenti, a.kd_band, a.kd_kelompok, KELOMPOK_PESERTA_API.Get_Nama_Kelompok(a.company, a.kd_kelompok) nama_kelompok, a.gadas_pensiun, a.curr_unit_kerja, UNIT_KERJA_API.Get_Nama_Unit_Kerja(a.company,a.curr_unit_kerja) nama_unit_kerja, CASE WHEN CALON_PENERIMA_MP_API.Get_Tgl_Nikah_Lagi(a.company,a.n_i_k,1) IS NOT NULL THEN 'Menikah Lagi' WHEN CALON_PENERIMA_MP_API.Get_Tgl_Cerai(a.company,a.n_i_k,1) IS NOT NULL THEN 'Bercerai' WHEN CALON_PENERIMA_MP_API.Get_Tgl_Nikah(a.company,a.n_i_k,1) IS NOT NULL THEN 'Menikah' ELSE 'Lajang' END status_pernikahan, a.n_i_k_pasangan, PESERTA_API.Get_Nama_Pegawai(a.company,a.n_i_k_pasangan) nama_pasangan FROM PESERTA_AKTIF a WHERE a.status_peserta = 'Aktif' AND a.n_i_k = '".$nik."'";
            
            $sql1 = oci_parse($conn, $query1);
            oci_execute($sql1);
            //$data1 = oci_fetch_array($sql1, OCI_ASSOC+OCI_RETURN_NULLS);

            $rows = array();
            while ($dt = oci_fetch_assoc($sql1)) {
                $rows[] = $dt;
            }
        }

        if ($error_msg != '') {
            $status = "Failed! ".$error_msg;
        } else {
            $status = "Success!";
        }

        updateUserLog($log_id, $status);
        
        if (!$conn) {
            $e = oci_error();
            return $response->withJson(["data" => null, "error" => $e, "status" => "failed"], 200);
        } else {
            if ($error_msg != '') {
                return $response->withJson(["data" => null, "error" => $error_msg, "status" => "failed"], 200);
            } else {
                return $response->withJson(["data" => $rows, "error" => null, "status" => "success"], 200);
            }
        }
    }); //[INF_KPST_09]

    $app->get("/jmlh_pst_mantan/", function (ServerRequestInterface $request, ResponseInterface $response){
        $conn = getConnection();
        $jns_api = "Jumlah";
        $pst_type = "Mantan";

        $periode = $request->getHeaderLine('PERIODE');
        $param_kd_kelompok = $request->getHeaderLine('PARAM_KD_KELOMPOK');
        $param_dapen = $request->getHeaderLine('PARAM_DAPEN');
        $param_jenis_kelamin = $request->getHeaderLine('PARAM_JENIS_KELAMIN');
        $param_agama = $request->getHeaderLine('PARAM_AGAMA');
        $param_gol_darah = $request->getHeaderLine('PARAM_GOL_DARAH');
        /*
            $kd_klmpk = $request->getHeaderLine('KD_KELOMPOK');
            $dapen = $request->getHeaderLine('DAPEN');
            $jk = $request->getHeaderLine('JENIS_KELAMIN');
            $agama = $request->getHeaderLine('AGAMA');
            $goldar = $request->getHeaderLine('GOL_DARAH');
        */
        $api_id = "INF_KPST_10";
        
        $ipAddress = $request->getAttribute('ip_address');
        $decoded = $request->getAttribute("decoded_jwt");
        $username = $decoded['user'];
        $ifs_user = $decoded['ifs_user'];

        $log_id = insertUserLog($username, $api_id, $ipAddress);

        $access = checkAccess($ifs_user, $api_id);

        $error_msg = "";
        $error_msg = string_type($periode, $error_msg, 'Periode', 'NotNull');
        $error_msg = parameter_type($param_kd_kelompok, $error_msg, 'Parameter Kd Kelompok', 1, 3, 'Nullable');
        $error_msg = parameter_type($param_dapen, $error_msg, 'Parameter Dapen', 1, 3, 'Nullable');
        $error_msg = parameter_type($param_jenis_kelamin, $error_msg, 'Parameter Jenis Kelamin', 1, 3, 'Nullable');
        $error_msg = parameter_type($param_agama, $error_msg, 'Parameter Agama', 1, 6, 'Nullable');
        $error_msg = parameter_type($param_gol_darah, $error_msg, 'Parameter Golongan Darah', 1, 13, 'Nullable');
        /*
            $error_msg = string_type($kd_klmpk, $error_msg, 'Kd Kelompok', 'Nullable');
            $error_msg = string_type($dapen, $error_msg, 'Dapen', 'Nullable');
            $error_msg = string_type($jk, $error_msg, 'Jenis Kelamin', 'Nullable');
            $error_msg = int_type($agama, $error_msg, 'Agama', 'Nullable');
            $error_msg = int_type($goldar, $error_msg, 'Golongan Darah', 'Nullable');
        */

        if ($access == 'false') {
            $error_msg = "User ".$ifs_user." Does Not Have Access to This Action!";
        }

        if ($error_msg == '') {
            $data['JMLH_PST_MANTAN'] = jmlhPst($periode, $jns_api, $pst_type, $param_kd_kelompok, $param_dapen, $param_jenis_kelamin, $param_agama, $param_gol_darah);
        }
    
        if ($error_msg != '') {
            $status = "Failed! ".$error_msg;
        } else {
            $status = "Success!";
        }

        updateUserLog($log_id, $status);
        
        if (!$conn) {
            $e = oci_error();
            return $response->withJson(["data" => null, "error" => $e, "status" => "failed"], 200);
        } else {
            if ($error_msg != '') {
                return $response->withJson(["data" => null, "error" => $error_msg, "status" => "failed"], 200);
            } else {
                return $response->withJson(["data" => $data, "error" => null, "status" => "success"], 200);
            }
        }
    }); //[INF_KPST_10]

    $app->get("/jmlh_pst_mantan_all/", function (ServerRequestInterface $request, ResponseInterface $response){
        $periode = $request->getHeaderLine('PERIODE');
        $parameter = $request->getHeaderLine('PARAMETER');
        $api_id = "INF_KPST_11";
        
        $ipAddress = $request->getAttribute('ip_address');
        $decoded = $request->getAttribute("decoded_jwt");
        $username = $decoded['user'];
        $ifs_user = $decoded['ifs_user'];

        $log_id = insertUserLog($username, $api_id, $ipAddress);

        $conn = getConnection();
        $pst_type = "Mantan";

        $access = checkAccess($ifs_user, $api_id);

        $error_msg = "";
        $error_msg = string_type($periode, $error_msg, 'Periode', 'NotNull');
        $error_msg = parameter_type($parameter, $error_msg, 'Parameter', 1, 5, 'NotNull');

        if ($access == 'false') {
            $error_msg = "User ".$ifs_user." Does Not Have Access to This Action!";
        }

        if ($error_msg == '') {
            /*
                if ($parameter == '1') {
                    // JMLH_PST_MANTAN //
                    $query1 = "SELECT COUNT(a.n_i_k) JMLH_PST_MANTAN FROM PENERIMA_MP2 a WHERE PERIODE_API.Get_Tanggal_Awal_Periode(a.company,'".$periode."') BETWEEN a.valid_from AND a.valid_to AND a.kd_jenis_pensiun = '99'";
                    $sql1 = oci_parse($conn, $query1);
                    oci_execute($sql1);
                    $data1 = oci_fetch_array($sql1, OCI_ASSOC+OCI_RETURN_NULLS);
                    $count1 = (int)$data1['JMLH_PST_MANTAN'];
                    $data['JMLH_PST_MANTAN'] = $count1;
                } else
            */
            if ($parameter == '1') {
                // PST_MANTAN_KD_KLMPK //
                $kd_klmpk = array("PRA", "PASCA", "NULL");
                $prm_type = "KD_KLMPK";
                $pst = "KELOMPOK_PESERTA_API.Get_Nama_Kelompok(a.company, PESERTA_API.Get_Kd_Kelompok(a.company, a.n_i_k))";
                $data['PST_MANTAN_KD_KLMPK'] = selectJmlhPstAll($kd_klmpk, $prm_type, $pst, $pst_type, $periode);
            } elseif ($parameter == '2') {
                // PST_MANTAN_DAPEN //
                $dapen = array("DAPEN I", "DAPEN II", "NULL");
                $prm_type = "DAPEN";
                $pst = "KELOMPOK_PESERTA_API.Get_Kelompok_Dapen(a.company, PESERTA_API.Get_Kd_Kelompok(a.company, a.n_i_k))";
                $data['PST_MANTAN_DAPEN'] = selectJmlhPstAll($dapen, $prm_type, $pst, $pst_type, $periode);
                //$data['PST_MANTAN_DAPEN'] = getMantanDapen();
            } elseif ($parameter == '3') {
                // PST_MANTAN_JNS_KLMN //
                $jk = array("Pria", "Wanita", "NULL");
                $prm_type = "JK";
                $pst = "CALON_PENERIMA_MP_API.Get_Jenis_Kelamin2(a.COMPANY, a.N_I_K, 1)";
                $data['PST_MANTAN_JNS_KLMN'] = selectJmlhPstAll($jk, $prm_type, $pst, $pst_type, $periode);
            } elseif ($parameter == '4') {
                // PST_MANTAN_AGAMA //
                $agama = array("Islam", "Katolik", "Budha", "Kristen", "Hindu", "NULL");
                $prm_type = "AGAMA";
                $pst = "CALON_PENERIMA_MP_API.Get_Agama(a.COMPANY, a.N_I_K, 1)";
                $data['PST_MANTAN_AGAMA'] = selectJmlhPstAll($agama, $prm_type, $pst, $pst_type, $periode);
            } elseif ($parameter == '5') {
                // PST_MANTAN_GOLDAR //
                $goldar = array("O", "O-", "AB", "A", "B", "NULL");
                $prm_type = "GOLDAR";
                $pst = "CALON_PENERIMA_MP_API.Get_Gol_Darah(a.COMPANY, a.N_I_K, 1)";
                $data['PST_MANTAN_GOLDAR'] = selectJmlhPstAll($goldar, $prm_type, $pst, $pst_type, $periode);
            }
        }
    
        if ($error_msg != '') {
            $status = "Failed! ".$error_msg;
        } else {
            $status = "Success!";
        }

        updateUserLog($log_id, $status);
        
        if (!$conn) {
            $e = oci_error();
            return $response->withJson(["data" => null, "error" => $e, "status" => "failed"], 200);
        } else {
            if ($error_msg != '') {
                return $response->withJson(["data" => null, "error" => $error_msg, "status" => "failed"], 200);
            } else {
                return $response->withJson(["data" => $data, "error" => null, "status" => "success"], 200);
            }
        }
    }); //[INF_KPST_11]

    $app->get("/list_pst_mantan/", function (ServerRequestInterface $request, ResponseInterface $response){
        $conn = getConnection();
        $jns_api = "List";
        $pst_type = "Mantan";

        $periode = $request->getHeaderLine('PERIODE');
        $param_kd_kelompok = $request->getHeaderLine('PARAM_KD_KELOMPOK');
        $param_dapen = $request->getHeaderLine('PARAM_DAPEN');
        $param_jenis_kelamin = $request->getHeaderLine('PARAM_JENIS_KELAMIN');
        $param_agama = $request->getHeaderLine('PARAM_AGAMA');
        $param_gol_darah = $request->getHeaderLine('PARAM_GOL_DARAH');
        /*
            $kd_klmpk = $request->getHeaderLine('KD_KELOMPOK');
            $dapen = $request->getHeaderLine('DAPEN');
            $jk = $request->getHeaderLine('JENIS_KELAMIN');
            $agama = $request->getHeaderLine('AGAMA');
            $goldar = $request->getHeaderLine('GOL_DARAH');
        */
        $api_id = "INF_KPST_12";
        
        $ipAddress = $request->getAttribute('ip_address');
        $decoded = $request->getAttribute("decoded_jwt");
        $username = $decoded['user'];
        $ifs_user = $decoded['ifs_user'];

        $log_id = insertUserLog($username, $api_id, $ipAddress);

        $access = checkAccess($ifs_user, $api_id);

        $error_msg = "";
        $error_msg = string_type($periode, $error_msg, 'Periode', 'NotNull');
        $error_msg = parameter_type($param_kd_kelompok, $error_msg, 'Parameter Kd Kelompok', 1, 3, 'Nullable');
        $error_msg = parameter_type($param_dapen, $error_msg, 'Parameter Dapen', 1, 3, 'Nullable');
        $error_msg = parameter_type($param_jenis_kelamin, $error_msg, 'Parameter Jenis Kelamin', 1, 3, 'Nullable');
        $error_msg = parameter_type($param_agama, $error_msg, 'Parameter Agama', 1, 6, 'Nullable');
        $error_msg = parameter_type($param_gol_darah, $error_msg, 'Parameter Golongan Darah', 1, 13, 'Nullable');
        /*
            $error_msg = string_type($kd_klmpk, $error_msg, 'Kd Kelompok', 'Nullable');
            $error_msg = string_type($dapen, $error_msg, 'Dapen', 'Nullable');
            $error_msg = string_type($jk, $error_msg, 'Jenis Kelamin', 'Nullable');
            $error_msg = int_type($agama, $error_msg, 'Agama', 'Nullable');
            $error_msg = int_type($goldar, $error_msg, 'Golongan Darah', 'Nullable');
        */

        if ($access == 'false') {
            $error_msg = "User ".$ifs_user." Does Not Have Access to This Action!";
        }

        if ($error_msg == '') {
            $data = jmlhPst($periode, $jns_api, $pst_type, $param_kd_kelompok, $param_dapen, $param_jenis_kelamin, $param_agama, $param_gol_darah);
        }

        if ($error_msg != '') {
            $status = "Failed! ".$error_msg;
        } else {
            $status = "Success!";
        }

        updateUserLog($log_id, $status);
        
        if (!$conn) {
            $e = oci_error();
            return $response->withJson(["data" => null, "error" => $e, "status" => "failed"], 200);
        } else {
            if ($error_msg != '') {
                return $response->withJson(["data" => null, "error" => $error_msg, "status" => "failed"], 200);
            } else {
                return $response->withJson(["data" => $data, "error" => null, "status" => "success"], 200);
            }
        }
    }); //[INF_KPST_12]

    $app->get("/detail_pst_mantan/", function (ServerRequestInterface $request, ResponseInterface $response, array $args) {
        $nik = $request->getHeaderLine('NIK');
        $api_id = "INF_KPST_13";
        
        $ipAddress = $request->getAttribute('ip_address');
        $decoded = $request->getAttribute("decoded_jwt");
        $username = $decoded['user'];
        $ifs_user = $decoded['ifs_user'];

        $log_id = insertUserLog($username, $api_id, $ipAddress);

        $conn = getConnection();

        $access = checkAccess($ifs_user, $api_id);

        $error_msg = "";
        $error_msg = string_type($nik, $error_msg, 'NIK', 'NotNull');

        if ($access == 'false') {
            $error_msg = "User ".$ifs_user." Does Not Have Access to This Action!";
        }

        if ($error_msg == '') {
            $query1 = "SELECT a.n_i_k, 1 no_ahli_waris, PESERTA_API.Get_Nama_Pegawai(a.company, a.n_i_k) nama, CALON_PENERIMA_MP_API.Get_Jenis_Kelamin2(a.COMPANY, a.N_I_K, 1) jenis_kelamin, CALON_PENERIMA_MP_API.Get_Agama(a.COMPANY, a.N_I_K, 1) agama, CALON_PENERIMA_MP_API.Get_Gol_Darah(a.COMPANY, a.N_I_K, 1) gol_darah, PESERTA_API.Get_Tgl_Lahir(a.company, a.n_i_k) tgl_lahir, PENERIMA_MP_API.GET_TANGGAL_PENSIUN(a.company, a.n_i_k) tgl_pensiun, PESERTA_API.Get_Tgl_Kerja(a.company,a.n_i_k) tgl_kerja, PESERTA_API.Get_Tgl_Capeg(a.company,a.n_i_k) tgl_capeg, PESERTA_API.Get_Tgl_Pegprus(a.company,a.n_i_k) tgl_pegprus, PESERTA_API.Get_Tgl_Berhenti(a.company,a.n_i_k) tgl_berhenti, PESERTA_API.Get_Kd_Band(a.company,a.n_i_k) kd_band, PESERTA_API.Get_Kd_Kelompok(a.company,a.n_i_k) kd_kelompok, KELOMPOK_PESERTA_API.Get_Nama_Kelompok(a.company,PESERTA_API.Get_Kd_Kelompok(a.company,a.n_i_k)) nama_kelompok, KELOMPOK_PESERTA_API.Get_Kelompok_Dapen(a.company,PESERTA_API.Get_Kd_Kelompok(a.company,a.n_i_k)) kelompok_dapen, PESERTA_API.Get_Gadas_Pensiun(a.company,a.n_i_k) gadas_pensiun, PESERTA_API.Get_Kd_Unit_Kerja(a.company,a.n_i_k) kd_unit_kerja, UNIT_KERJA_API.Get_Nama_Unit_Kerja(a.company,PESERTA_API.Get_Kd_Unit_Kerja(a.company,a.n_i_k)) nama_unit_kerja, CASE WHEN CALON_PENERIMA_MP_API.Get_Tgl_Nikah_Lagi(a.company,a.n_i_k, 1) IS NOT NULL THEN 'Menikah Lagi' WHEN CALON_PENERIMA_MP_API.Get_Tgl_Cerai(a.company,a.n_i_k,1) IS NOT NULL THEN 'Bercerai' WHEN CALON_PENERIMA_MP_API.Get_Tgl_Nikah(a.company,a.n_i_k,1) IS NOT NULL THEN 'Menikah' ELSE 'Lajang' END status_pernikahan, PESERTA_API.Get_N_I_K_Pasangan(a.company,a.n_i_k) n_i_k_pasangan, PESERTA_API.Get_Nama_Pegawai(a.company,PESERTA_API.Get_N_I_K_Pasangan(a.company,a.n_i_k)) nama_pasangan FROM PENERIMA_MP2 a WHERE a.kd_jenis_pensiun = '99' AND a.n_i_k = '".$nik."' AND a.no_ahli_waris = '1'";
            
            $sql1 = oci_parse($conn, $query1);
            oci_execute($sql1);
            //$data1 = oci_fetch_array($sql1, OCI_ASSOC+OCI_RETURN_NULLS);
            
            $rows = array();
            while ($dt = oci_fetch_assoc($sql1)) {
                $rows[] = $dt;
            }
        }

        if ($error_msg != '') {
            $status = "Failed! ".$error_msg;
        } else {
            $status = "Success!";
        }

        updateUserLog($log_id, $status);
        
        if (!$conn) {
            $e = oci_error();
            return $response->withJson(["data" => null, "error" => $e, "status" => "failed"], 200);
        } else {
            if ($error_msg != '') {
                return $response->withJson(["data" => null, "error" => $error_msg, "status" => "failed"], 200);
            } else {
                return $response->withJson(["data" => $rows, "error" => null, "status" => "success"], 200);
            }
        }
    }); //[INF_KPST_13]

    //-------------------------------------------------------------------------------------//
    //------------------------------------// [INF_MP] //-----------------------------------//
    //-------------------------------------------------------------------------------------//

    $app->get("/nilai_rp_summ_dapen/", function (ServerRequestInterface $request, ResponseInterface $response, array $args){
        //$periode = $args['periode'];

        $periode = $request->getHeaderLine('PERIODE');
        $api_id = "INF_MP_01";
        
        $ipAddress = $request->getAttribute('ip_address');
        $decoded = $request->getAttribute("decoded_jwt");
        $username = $decoded['user'];
        $ifs_user = $decoded['ifs_user'];

        $log_id = insertUserLog($username, $api_id, $ipAddress);

        $conn = getConnection();

        $access = checkAccess($ifs_user, $api_id);

        $error_msg = "";
        $error_msg = string_type($periode, $error_msg, 'Periode', 'NotNull');

        if ($access == 'false') {
            $error_msg = "User ".$ifs_user." Does Not Have Access to This Action!";
        }

        if ($error_msg == '') {
            $query1 = "SELECT '".$periode."' PERIODE, zpokbesar, dapen_i, dapen_ii, total_of_mpb FROM (SELECT NO_URUT, zpokbesar, SUM(NVL(dapen_i,0)) dapen_i, SUM(NVL(dapen_ii,0)) dapen_ii, (SUM(NVL(dapen_i,0)) + SUM(NVL(dapen_ii,0))) total_of_mpb FROM (SELECT 6 NO_URUT, '> 10 juta' zpokbesar, SUM(a.mpb) dapen_i, 0 dapen_ii FROM PENERIMAAN_MP a WHERE a.kd_periode = '".$periode."' AND KELOMPOK_PESERTA_API.Get_Kelompok_Dapen(a.company, PESERTA_API.Get_Kd_Kelompok(a.company, a.n_i_k)) = 'DAPEN I' AND a.mpb > 10000000 GROUP BY '> 10 juta', 6 UNION ALL SELECT 6 NO_URUT, '> 10 juta' zpokbesar, 0 dapen_i, SUM(a.mpb) dapen_ii FROM PENERIMAAN_MP a WHERE a.kd_periode = '".$periode."' AND KELOMPOK_PESERTA_API.Get_Kelompok_Dapen(a.company, PESERTA_API.Get_Kd_Kelompok(a.company, a.n_i_k)) = 'DAPEN II' AND a.mpb > 10000000 GROUP BY '> 10 juta', 6) GROUP BY zpokbesar, NO_URUT UNION ALL /* >10jt || >5jt - 10jt */ SELECT NO_URUT, zpokbesar, SUM(NVL(dapen_i,0)) dapen_i, SUM(NVL(dapen_ii,0)) dapen_ii, (SUM(NVL(dapen_i,0)) + SUM(NVL(dapen_ii,0))) total_of_mpb FROM (SELECT 5 NO_URUT, '>5 juta - 10 juta' zpokbesar, SUM(a.mpb) dapen_i, 0 dapen_ii FROM PENERIMAAN_MP a WHERE a.kd_periode = '".$periode."' AND KELOMPOK_PESERTA_API.Get_Kelompok_Dapen(a.company, PESERTA_API.Get_Kd_Kelompok(a.company, a.n_i_k)) = 'DAPEN I' AND a.mpb > 5000000 AND a.mpb <= 10000000 GROUP BY '>5 juta - 10 juta', 5 UNION ALL SELECT 5 NO_URUT, '>5 juta - 10 juta' zpokbesar, 0 dapen_i, SUM(a.mpb) dapen_ii FROM PENERIMAAN_MP a WHERE a.kd_periode = '".$periode."' AND KELOMPOK_PESERTA_API.Get_Kelompok_Dapen(a.company, PESERTA_API.Get_Kd_Kelompok(a.company, a.n_i_k)) = 'DAPEN II' AND a.mpb > 5000000 AND a.mpb <= 10000000 GROUP BY '>5 juta - 10 juta', 5) GROUP BY zpokbesar, NO_URUT UNION ALL /* >5jt - 10jt || >1.5jt - 5jt */ SELECT NO_URUT, zpokbesar, SUM(NVL(dapen_i,0)) dapen_i, SUM(NVL(dapen_ii,0)) dapen_ii, (SUM(NVL(dapen_i,0)) + SUM(NVL(dapen_ii,0))) total_of_mpb FROM (SELECT 4 NO_URUT, '>1.5 juta - 5 juta' zpokbesar, SUM(a.mpb) dapen_i, 0 dapen_ii FROM PENERIMAAN_MP a WHERE a.kd_periode = '".$periode."' AND KELOMPOK_PESERTA_API.Get_Kelompok_Dapen(a.company, PESERTA_API.Get_Kd_Kelompok(a.company, a.n_i_k)) = 'DAPEN I' AND a.mpb > 1500000 AND a.mpb <= 5000000 GROUP BY '>1.5 juta - 5 juta', 4 UNION ALL SELECT 4 NO_URUT, '>1.5 juta - 5 juta' zpokbesar, 0 dapen_i, SUM(a.mpb) dapen_ii FROM PENERIMAAN_MP a WHERE a.kd_periode = '".$periode."' AND KELOMPOK_PESERTA_API.Get_Kelompok_Dapen(a.company, PESERTA_API.Get_Kd_Kelompok(a.company, a.n_i_k)) = 'DAPEN II' AND a.mpb > 1500000 AND a.mpb <= 5000000 GROUP BY '>1.5 juta - 5 juta', 4) GROUP BY zpokbesar, NO_URUT UNION ALL /* >1.5jt - 5jt || >1jt - 1.5jt */ SELECT NO_URUT, zpokbesar, SUM(NVL(dapen_i,0)) dapen_i, SUM(NVL(dapen_ii,0)) dapen_ii, (SUM(NVL(dapen_i,0)) + SUM(NVL(dapen_ii,0))) total_of_mpb FROM (SELECT 3 NO_URUT, '>1 juta - 1.5 juta' zpokbesar, SUM(a.mpb) dapen_i, 0 dapen_ii FROM PENERIMAAN_MP a WHERE a.kd_periode = '".$periode."' AND KELOMPOK_PESERTA_API.Get_Kelompok_Dapen(a.company, PESERTA_API.Get_Kd_Kelompok(a.company, a.n_i_k)) = 'DAPEN I' AND a.mpb > 1000000 AND a.mpb <= 1500000 GROUP BY '>1 juta - 1.5 juta', 3 UNION ALL SELECT 3 NO_URUT, '>1 juta - 1.5 juta' zpokbesar, 0 dapen_i, SUM(a.mpb) dapen_ii FROM PENERIMAAN_MP a WHERE a.kd_periode = '".$periode."' AND KELOMPOK_PESERTA_API.Get_Kelompok_Dapen(a.company, PESERTA_API.Get_Kd_Kelompok(a.company, a.n_i_k)) = 'DAPEN II' AND a.mpb > 1000000 AND a.mpb <= 1500000 GROUP BY '>1 juta - 1.5 juta', 3) GROUP BY zpokbesar, NO_URUT UNION ALL /* >1jt - 1.5jt || >600rb - 1jt */ SELECT NO_URUT, zpokbesar, SUM(NVL(dapen_i,0)) dapen_i, SUM(NVL(dapen_ii,0)) dapen_ii, (SUM(NVL(dapen_i,0)) + SUM(NVL(dapen_ii,0))) total_of_mpb FROM (SELECT 2 NO_URUT, '>600 rb - 1 juta' zpokbesar, SUM(a.mpb) dapen_i, 0 dapen_ii FROM PENERIMAAN_MP a WHERE a.kd_periode = '".$periode."' AND KELOMPOK_PESERTA_API.Get_Kelompok_Dapen(a.company, PESERTA_API.Get_Kd_Kelompok(a.company, a.n_i_k)) = 'DAPEN I' AND a.mpb > 600000 AND a.mpb <= 1000000 GROUP BY '>600 rb - 1 juta', 2 UNION ALL SELECT 2 NO_URUT, '>600 rb - 1 juta' zpokbesar, 0 dapen_i, SUM(a.mpb) dapen_ii FROM PENERIMAAN_MP a WHERE a.kd_periode = '".$periode."' AND KELOMPOK_PESERTA_API.Get_Kelompok_Dapen(a.company, PESERTA_API.Get_Kd_Kelompok(a.company, a.n_i_k)) = 'DAPEN II' AND a.mpb > 600000 AND a.mpb <= 1000000 GROUP BY '>600 rb - 1 juta', 2) GROUP BY zpokbesar, NO_URUT UNION ALL /* >600rb - 1jt || 450rb - 600rb */ SELECT NO_URUT, zpokbesar, SUM(NVL(dapen_i,0)) dapen_i, SUM(NVL(dapen_ii,0)) dapen_ii, (SUM(NVL(dapen_i,0)) + SUM(NVL(dapen_ii,0))) total_of_mpb FROM (SELECT 1 NO_URUT, '450 rb - 600 rb' zpokbesar, SUM(a.mpb) dapen_i, 0 dapen_ii FROM PENERIMAAN_MP a WHERE a.kd_periode = '".$periode."' AND KELOMPOK_PESERTA_API.Get_Kelompok_Dapen(a.company, PESERTA_API.Get_Kd_Kelompok(a.company, a.n_i_k)) = 'DAPEN I' AND a.mpb <= 600000 GROUP BY '450 rb - 600 rb', 1 UNION ALL SELECT 1 NO_URUT, '450 rb - 600 rb' zpokbesar, 0 dapen_i, SUM(a.mpb) dapen_ii FROM PENERIMAAN_MP a WHERE a.kd_periode = '".$periode."' AND KELOMPOK_PESERTA_API.Get_Kelompok_Dapen(a.company, PESERTA_API.Get_Kd_Kelompok(a.company, a.n_i_k)) = 'DAPEN II' AND a.mpb <= 600000 GROUP BY '450 rb - 600 rb', 1) GROUP BY zpokbesar, NO_URUT) ORDER BY NO_URUT";
            
            $sql1 = oci_parse($conn, $query1);

            oci_execute($sql1);

            $data = array();
            while ($dt = oci_fetch_assoc($sql1)) {
                $data[] = $dt;
            }
        }
        
        if ($error_msg != '') {
            $status = "Failed! ".$error_msg;
        } else {
            $status = "Success!";
        }

        updateUserLog($log_id, $status);
        
        if (!$conn) {
            $e = oci_error();
            return $response->withJson(["data" => null, "error" => $e, "status" => "failed"], 200);
        } else {
            if ($error_msg != '') {
                return $response->withJson(["data" => null, "error" => $error_msg, "status" => "failed"], 200);
            } else {
                return $response->withJson(["data" => $data, "error" => null, "status" => "success"], 200);
            }
        }
    }); //[INF_MP_01]

    $app->get("/jmlh_pmp_summ_dapen/", function (ServerRequestInterface $request, ResponseInterface $response, array $args){
        //$periode = $args['periode'];

        $periode = $request->getHeaderLine('PERIODE');
        $api_id = "INF_MP_02";
        
        $ipAddress = $request->getAttribute('ip_address');
        $decoded = $request->getAttribute("decoded_jwt");
        $username = $decoded['user'];
        $ifs_user = $decoded['ifs_user'];

        $log_id = insertUserLog($username, $api_id, $ipAddress);

        $conn = getConnection();

        $access = checkAccess($ifs_user, $api_id);

        $error_msg = "";
        $error_msg = string_type($periode, $error_msg, 'Periode', 'NotNull');

        if ($access == 'false') {
            $error_msg = "User ".$ifs_user." Does Not Have Access to This Action!";
        }

        if ($error_msg == '') {
            $query1 = "SELECT '".$periode."' PERIODE, zpokbesar, dapen_i, dapen_ii, total_of_nik FROM (SELECT NO_URUT, zpokbesar, SUM(dapen_i) dapen_i, SUM(dapen_ii) dapen_ii, (SUM(dapen_i) + SUM(dapen_ii)) total_of_nik FROM (SELECT 6 NO_URUT, '> 10 juta' zpokbesar, COUNT(KELOMPOK_PESERTA_API.Get_Kelompok_Dapen(a.company, PESERTA_API.Get_Kd_Kelompok(a.company, a.n_i_k))) dapen_i, 0 dapen_ii FROM PENERIMAAN_MP a WHERE a.kd_periode = '".$periode."' AND KELOMPOK_PESERTA_API.Get_Kelompok_Dapen(a.company, PESERTA_API.Get_Kd_Kelompok(a.company, a.n_i_k)) = 'DAPEN I' AND a.mpb > 10000000 GROUP BY '> 10 juta', 6 UNION ALL SELECT 6 NO_URUT, '> 10 juta' zpokbesar, 0 dapen_i, COUNT(KELOMPOK_PESERTA_API.Get_Kelompok_Dapen(a.company, PESERTA_API.Get_Kd_Kelompok(a.company, a.n_i_k))) dapen_ii FROM PENERIMAAN_MP a WHERE a.kd_periode = '".$periode."' AND KELOMPOK_PESERTA_API.Get_Kelompok_Dapen(a.company, PESERTA_API.Get_Kd_Kelompok(a.company, a.n_i_k)) = 'DAPEN II' AND a.mpb > 10000000 GROUP BY '> 10 juta', 6) GROUP BY zpokbesar, NO_URUT UNION ALL /* >10jt || >5jt - 10jt */ SELECT NO_URUT, zpokbesar, SUM(dapen_i) dapen_i, SUM(dapen_ii) dapen_ii, (SUM(dapen_i) + SUM(dapen_ii)) total_of_nik FROM (SELECT 5 NO_URUT, '>5 juta - 10 juta' zpokbesar, COUNT(KELOMPOK_PESERTA_API.Get_Kelompok_Dapen(a.company, PESERTA_API.Get_Kd_Kelompok(a.company, a.n_i_k))) dapen_i, 0 dapen_ii FROM PENERIMAAN_MP a WHERE a.kd_periode = '".$periode."' AND KELOMPOK_PESERTA_API.Get_Kelompok_Dapen(a.company, PESERTA_API.Get_Kd_Kelompok(a.company, a.n_i_k)) = 'DAPEN I' AND a.mpb > 5000000 AND a.mpb <= 10000000 GROUP BY '>5 juta - 10 juta', 5 UNION ALL SELECT 5 NO_URUT, '>5 juta - 10 juta' zpokbesar, 0 dapen_i, COUNT(KELOMPOK_PESERTA_API.Get_Kelompok_Dapen(a.company, PESERTA_API.Get_Kd_Kelompok(a.company, a.n_i_k))) dapen_ii FROM PENERIMAAN_MP a WHERE a.kd_periode = '".$periode."' AND KELOMPOK_PESERTA_API.Get_Kelompok_Dapen(a.company, PESERTA_API.Get_Kd_Kelompok(a.company, a.n_i_k)) = 'DAPEN II' AND a.mpb > 5000000 AND a.mpb <= 10000000 GROUP BY '>5 juta - 10 juta', 5) GROUP BY zpokbesar, NO_URUT UNION ALL /* >5jt - 10jt || >1.5jt - 5jt */ SELECT NO_URUT, zpokbesar, SUM(dapen_i) dapen_i, SUM(dapen_ii) dapen_ii, (SUM(dapen_i) + SUM(dapen_ii)) total_of_nik FROM (SELECT 4 NO_URUT, '>1.5 juta - 5 juta' zpokbesar, COUNT(KELOMPOK_PESERTA_API.Get_Kelompok_Dapen(a.company, PESERTA_API.Get_Kd_Kelompok(a.company, a.n_i_k))) dapen_i, 0 dapen_ii FROM PENERIMAAN_MP a WHERE a.kd_periode = '".$periode."' AND KELOMPOK_PESERTA_API.Get_Kelompok_Dapen(a.company, PESERTA_API.Get_Kd_Kelompok(a.company, a.n_i_k)) = 'DAPEN I' AND a.mpb > 1500000 AND a.mpb <= 5000000 GROUP BY '>1.5 juta - 5 juta', 4 UNION ALL SELECT 4 NO_URUT, '>1.5 juta - 5 juta' zpokbesar, 0 dapen_i, COUNT(KELOMPOK_PESERTA_API.Get_Kelompok_Dapen(a.company, PESERTA_API.Get_Kd_Kelompok(a.company, a.n_i_k))) dapen_ii FROM PENERIMAAN_MP a WHERE a.kd_periode = '".$periode."' AND KELOMPOK_PESERTA_API.Get_Kelompok_Dapen(a.company, PESERTA_API.Get_Kd_Kelompok(a.company, a.n_i_k)) = 'DAPEN II' AND a.mpb > 1500000 AND a.mpb <= 5000000 GROUP BY '>1.5 juta - 5 juta', 4) GROUP BY zpokbesar, NO_URUT UNION ALL /* >1.5jt - 5jt || >1jt - 1.5jt */ SELECT NO_URUT, zpokbesar, SUM(dapen_i) dapen_i, SUM(dapen_ii) dapen_ii, (SUM(dapen_i) + SUM(dapen_ii)) total_of_nik FROM (SELECT 3 NO_URUT, '>1 juta - 1.5 juta' zpokbesar, COUNT(KELOMPOK_PESERTA_API.Get_Kelompok_Dapen(a.company, PESERTA_API.Get_Kd_Kelompok(a.company, a.n_i_k))) dapen_i, 0 dapen_ii FROM PENERIMAAN_MP a WHERE a.kd_periode = '".$periode."' AND KELOMPOK_PESERTA_API.Get_Kelompok_Dapen(a.company, PESERTA_API.Get_Kd_Kelompok(a.company, a.n_i_k)) = 'DAPEN I' AND a.mpb > 1000000 AND a.mpb <= 1500000 GROUP BY '>1 juta - 1.5 juta', 3 UNION ALL SELECT 3 NO_URUT, '>1 juta - 1.5 juta' zpokbesar, 0 dapen_i, COUNT(KELOMPOK_PESERTA_API.Get_Kelompok_Dapen(a.company, PESERTA_API.Get_Kd_Kelompok(a.company, a.n_i_k))) dapen_ii FROM PENERIMAAN_MP a WHERE a.kd_periode = '".$periode."' AND KELOMPOK_PESERTA_API.Get_Kelompok_Dapen(a.company, PESERTA_API.Get_Kd_Kelompok(a.company, a.n_i_k)) = 'DAPEN II' AND a.mpb > 1000000 AND a.mpb <= 1500000 GROUP BY '>1 juta - 1.5 juta', 3) GROUP BY zpokbesar, NO_URUT UNION ALL /* >1jt - 1.5jt || >600rb - 1jt */ SELECT NO_URUT, zpokbesar, SUM(dapen_i) dapen_i, SUM(dapen_ii) dapen_ii, (SUM(dapen_i) + SUM(dapen_ii)) total_of_nik FROM (SELECT 2 NO_URUT, '>600 rb - 1 juta' zpokbesar, COUNT(KELOMPOK_PESERTA_API.Get_Kelompok_Dapen(a.company, PESERTA_API.Get_Kd_Kelompok(a.company, a.n_i_k))) dapen_i, 0 dapen_ii FROM PENERIMAAN_MP a WHERE a.kd_periode = '".$periode."' AND KELOMPOK_PESERTA_API.Get_Kelompok_Dapen(a.company, PESERTA_API.Get_Kd_Kelompok(a.company, a.n_i_k)) = 'DAPEN I' AND a.mpb > 600000 AND a.mpb <= 1000000 GROUP BY '>600 rb - 1 juta', 2 UNION ALL SELECT 2 NO_URUT, '>600 rb - 1 juta' zpokbesar, 0 dapen_i, COUNT(KELOMPOK_PESERTA_API.Get_Kelompok_Dapen(a.company, PESERTA_API.Get_Kd_Kelompok(a.company, a.n_i_k))) dapen_ii FROM PENERIMAAN_MP a WHERE a.kd_periode = '".$periode."' AND KELOMPOK_PESERTA_API.Get_Kelompok_Dapen(a.company, PESERTA_API.Get_Kd_Kelompok(a.company, a.n_i_k)) = 'DAPEN II' AND a.mpb > 600000 AND a.mpb <= 1000000 GROUP BY '>600 rb - 1 juta', 2) GROUP BY zpokbesar, NO_URUT UNION ALL /* >600rb - 1jt || 450rb - 600rb */ SELECT NO_URUT, zpokbesar, SUM(dapen_i) dapen_i, SUM(dapen_ii) dapen_ii, (SUM(dapen_i) + SUM(dapen_ii)) total_of_nik FROM (SELECT 1 NO_URUT, '450 rb - 600 rb' zpokbesar, COUNT(KELOMPOK_PESERTA_API.Get_Kelompok_Dapen(a.company, PESERTA_API.Get_Kd_Kelompok(a.company, a.n_i_k))) dapen_i, 0 dapen_ii FROM PENERIMAAN_MP a WHERE a.kd_periode = '".$periode."' AND KELOMPOK_PESERTA_API.Get_Kelompok_Dapen(a.company, PESERTA_API.Get_Kd_Kelompok(a.company, a.n_i_k)) = 'DAPEN I' AND a.mpb <= 600000 GROUP BY '450 rb - 600 rb', 1 UNION ALL SELECT 1 NO_URUT, '450 rb - 600 rb' zpokbesar, 0 dapen_i, COUNT(KELOMPOK_PESERTA_API.Get_Kelompok_Dapen(a.company, PESERTA_API.Get_Kd_Kelompok(a.company, a.n_i_k))) dapen_ii FROM PENERIMAAN_MP a WHERE a.kd_periode = '".$periode."' AND KELOMPOK_PESERTA_API.Get_Kelompok_Dapen(a.company, PESERTA_API.Get_Kd_Kelompok(a.company, a.n_i_k)) = 'DAPEN II' AND a.mpb <= 600000 GROUP BY '450 rb - 600 rb', 1) GROUP BY zpokbesar, NO_URUT) ORDER BY NO_URUT";
            
            $sql1 = oci_parse($conn, $query1);

            oci_execute($sql1);

            $data = array();
            while ($dt = oci_fetch_assoc($sql1)) {
                $data[] = $dt;
            }
        }
        
        if ($error_msg != '') {
            $status = "Failed! ".$error_msg;
        } else {
            $status = "Success!";
        }

        updateUserLog($log_id, $status);
        
        if (!$conn) {
            $e = oci_error();
            return $response->withJson(["data" => null, "error" => $e, "status" => "failed"], 200);
        } else {
            if ($error_msg != '') {
                return $response->withJson(["data" => null, "error" => $error_msg, "status" => "failed"], 200);
            } else {
                return $response->withJson(["data" => $data, "error" => null, "status" => "success"], 200);
            }
        }
    }); //[INF_MP_02]

    $app->get("/jmlh_pmp_jaw/", function (ServerRequestInterface $request, ResponseInterface $response, array $args){
        //$periode = $args['periode'];

        $periode = $request->getHeaderLine('PERIODE');
        $api_id = "INF_MP_03";
        
        $ipAddress = $request->getAttribute('ip_address');
        $decoded = $request->getAttribute("decoded_jwt");
        $username = $decoded['user'];
        $ifs_user = $decoded['ifs_user'];

        $log_id = insertUserLog($username, $api_id, $ipAddress);

        $conn = getConnection();

        $access = checkAccess($ifs_user, $api_id);

        $error_msg = "";
        $error_msg = string_type($periode, $error_msg, 'Periode', 'NotNull');

        if ($access == 'false') {
            $error_msg = "User ".$ifs_user." Does Not Have Access to This Action!";
        }

        if ($error_msg == '') {
            $query1 = "SELECT '".$periode."' KD_PERIODE, zpokbesar, total_of_nik, anak, istri, peserta, suami, wali FROM (SELECT NO_URUT, zpokbesar, (SUM(peserta) + SUM(suami) + SUM(istri) + SUM(anak) + SUM(wali)) total_of_nik, SUM(anak) anak, SUM(istri) istri, SUM(peserta) peserta, SUM(suami) suami, SUM(wali) wali FROM (SELECT 6 NO_URUT, '> 10 juta' zpokbesar, COUNT(CALON_PENERIMA_MP_API.Get_Jenis_Ahli_Waris(a.company, a.n_i_k, a.no_ahli_waris)) peserta, 0 suami, 0 istri, 0 anak, 0 wali FROM  PENERIMAAN_MP a WHERE a.kd_periode = '".$periode."' AND CALON_PENERIMA_MP_API.Get_Jenis_Ahli_Waris(a.company, a.n_i_k, a.no_ahli_waris) = 'Peserta' AND a.mpb > 10000000 GROUP BY '> 10 juta', 6 UNION ALL SELECT 6 NO_URUT, '> 10 juta' zpokbesar, 0 peserta, COUNT(CALON_PENERIMA_MP_API.Get_Jenis_Ahli_Waris(a.company, a.n_i_k, a.no_ahli_waris)) suami, 0 istri, 0 anak, 0 wali FROM  PENERIMAAN_MP a WHERE a.kd_periode = '".$periode."' AND CALON_PENERIMA_MP_API.Get_Jenis_Ahli_Waris(a.company, a.n_i_k, a.no_ahli_waris) = 'Suami' AND a.mpb > 10000000 GROUP BY '> 10 juta', 6 UNION ALL SELECT 6 NO_URUT, '> 10 juta' zpokbesar, 0 peserta, 0 suami, COUNT(CALON_PENERIMA_MP_API.Get_Jenis_Ahli_Waris(a.company, a.n_i_k, a.no_ahli_waris)) istri, 0 anak, 0 wali FROM  PENERIMAAN_MP a WHERE a.kd_periode = '".$periode."' AND CALON_PENERIMA_MP_API.Get_Jenis_Ahli_Waris(a.company, a.n_i_k, a.no_ahli_waris) = 'Istri' AND a.mpb > 10000000 GROUP BY '> 10 juta', 6 UNION ALL SELECT 6 NO_URUT, '> 10 juta' zpokbesar, 0 peserta, 0 suami, 0 istri, COUNT(CALON_PENERIMA_MP_API.Get_Jenis_Ahli_Waris(a.company, a.n_i_k, a.no_ahli_waris)) anak, 0 wali FROM  PENERIMAAN_MP a WHERE a.kd_periode = '".$periode."' AND CALON_PENERIMA_MP_API.Get_Jenis_Ahli_Waris(a.company, a.n_i_k, a.no_ahli_waris) = 'Anak' AND a.mpb > 10000000 GROUP BY '> 10 juta', 6 UNION ALL SELECT 6 NO_URUT, '> 10 juta' zpokbesar, 0 peserta, 0 suami, 0 istri, 0 anak, COUNT(CALON_PENERIMA_MP_API.Get_Jenis_Ahli_Waris(a.company, a.n_i_k, a.no_ahli_waris)) wali FROM  PENERIMAAN_MP a WHERE a.kd_periode = '".$periode."' AND CALON_PENERIMA_MP_API.Get_Jenis_Ahli_Waris(a.company, a.n_i_k, a.no_ahli_waris) = 'Wali' AND a.mpb > 10000000 GROUP BY '> 10 juta', 6) GROUP BY zpokbesar, NO_URUT UNION ALL /* >10jt || >5jt - 10jt */ SELECT NO_URUT, zpokbesar, (SUM(peserta) + SUM(suami) + SUM(istri) + SUM(anak) + SUM(wali)) total_of_nik, SUM(anak) anak, SUM(istri) istri, SUM(peserta) peserta, SUM(suami) suami, SUM(wali) wali FROM (SELECT 5 NO_URUT, '>5 juta - 10 juta' zpokbesar, COUNT(CALON_PENERIMA_MP_API.Get_Jenis_Ahli_Waris(a.company, a.n_i_k, a.no_ahli_waris)) peserta, 0 suami, 0 istri, 0 anak, 0 wali FROM  PENERIMAAN_MP a WHERE a.kd_periode = '".$periode."' AND CALON_PENERIMA_MP_API.Get_Jenis_Ahli_Waris(a.company, a.n_i_k, a.no_ahli_waris) = 'Peserta' AND a.mpb > 5000000 AND a.mpb <= 10000000 GROUP BY '>5 juta - 10 juta', 5 UNION ALL SELECT 5 NO_URUT, '>5 juta - 10 juta' zpokbesar, 0 peserta, COUNT(CALON_PENERIMA_MP_API.Get_Jenis_Ahli_Waris(a.company, a.n_i_k, a.no_ahli_waris)) suami, 0 istri, 0 anak, 0 wali FROM  PENERIMAAN_MP a WHERE a.kd_periode = '".$periode."' AND CALON_PENERIMA_MP_API.Get_Jenis_Ahli_Waris(a.company, a.n_i_k, a.no_ahli_waris) = 'Suami' AND a.mpb > 5000000 AND a.mpb <= 10000000 GROUP BY '>5 juta - 10 juta', 5 UNION ALL SELECT 5 NO_URUT, '>5 juta - 10 juta' zpokbesar, 0 peserta, 0 suami, COUNT(CALON_PENERIMA_MP_API.Get_Jenis_Ahli_Waris(a.company, a.n_i_k, a.no_ahli_waris)) istri, 0 anak, 0 wali FROM  PENERIMAAN_MP a WHERE a.kd_periode = '".$periode."' AND CALON_PENERIMA_MP_API.Get_Jenis_Ahli_Waris(a.company, a.n_i_k, a.no_ahli_waris) = 'Istri' AND a.mpb > 5000000 AND a.mpb <= 10000000 GROUP BY '>5 juta - 10 juta', 5 UNION ALL SELECT 5 NO_URUT, '>5 juta - 10 juta' zpokbesar, 0 peserta, 0 suami, 0 istri, COUNT(CALON_PENERIMA_MP_API.Get_Jenis_Ahli_Waris(a.company, a.n_i_k, a.no_ahli_waris)) anak, 0 wali FROM  PENERIMAAN_MP a WHERE a.kd_periode = '".$periode."' AND CALON_PENERIMA_MP_API.Get_Jenis_Ahli_Waris(a.company, a.n_i_k, a.no_ahli_waris) = 'Anak' AND a.mpb > 5000000 AND a.mpb <= 10000000 GROUP BY '>5 juta - 10 juta', 5 UNION ALL SELECT 5 NO_URUT, '>5 juta - 10 juta' zpokbesar, 0 peserta, 0 suami, 0 istri, 0 anak, COUNT(CALON_PENERIMA_MP_API.Get_Jenis_Ahli_Waris(a.company, a.n_i_k, a.no_ahli_waris)) wali FROM  PENERIMAAN_MP a WHERE a.kd_periode = '".$periode."' AND CALON_PENERIMA_MP_API.Get_Jenis_Ahli_Waris(a.company, a.n_i_k, a.no_ahli_waris) = 'Wali' AND a.mpb > 5000000 AND a.mpb <= 10000000 GROUP BY '>5 juta - 10 juta', 5) GROUP BY zpokbesar, NO_URUT UNION ALL /* >5jt - 10jt || >1.5jt - 5jt */ SELECT NO_URUT, zpokbesar, (SUM(peserta) + SUM(suami) + SUM(istri) + SUM(anak) + SUM(wali)) total_of_nik, SUM(anak) anak, SUM(istri) istri, SUM(peserta) peserta, SUM(suami) suami, SUM(wali) wali FROM (SELECT 4 NO_URUT, '>1.5 juta - 5 juta' zpokbesar, COUNT(CALON_PENERIMA_MP_API.Get_Jenis_Ahli_Waris(a.company, a.n_i_k, a.no_ahli_waris)) peserta, 0 suami, 0 istri, 0 anak, 0 wali FROM  PENERIMAAN_MP a WHERE a.kd_periode = '".$periode."' AND CALON_PENERIMA_MP_API.Get_Jenis_Ahli_Waris(a.company, a.n_i_k, a.no_ahli_waris) = 'Peserta' AND a.mpb > 1500000 AND a.mpb <= 5000000 GROUP BY '>1.5 juta - 5 juta', 4 UNION ALL SELECT 4 NO_URUT, '>1.5 juta - 5 juta' zpokbesar, 0 peserta, COUNT(CALON_PENERIMA_MP_API.Get_Jenis_Ahli_Waris(a.company, a.n_i_k, a.no_ahli_waris)) suami, 0 istri, 0 anak, 0 wali FROM  PENERIMAAN_MP a WHERE a.kd_periode = '".$periode."' AND CALON_PENERIMA_MP_API.Get_Jenis_Ahli_Waris(a.company, a.n_i_k, a.no_ahli_waris) = 'Suami' AND a.mpb > 1500000 AND a.mpb <= 5000000 GROUP BY '>1.5 juta - 5 juta', 4 UNION ALL SELECT 4 NO_URUT, '>1.5 juta - 5 juta' zpokbesar, 0 peserta, 0 suami, COUNT(CALON_PENERIMA_MP_API.Get_Jenis_Ahli_Waris(a.company, a.n_i_k, a.no_ahli_waris)) istri, 0 anak, 0 wali FROM  PENERIMAAN_MP a WHERE a.kd_periode = '".$periode."' AND CALON_PENERIMA_MP_API.Get_Jenis_Ahli_Waris(a.company, a.n_i_k, a.no_ahli_waris) = 'Istri' AND a.mpb > 1500000 AND a.mpb <= 5000000 GROUP BY '>1.5 juta - 5 juta', 4 UNION ALL SELECT 4 NO_URUT, '>1.5 juta - 5 juta' zpokbesar, 0 peserta, 0 suami, 0 istri, COUNT(CALON_PENERIMA_MP_API.Get_Jenis_Ahli_Waris(a.company, a.n_i_k, a.no_ahli_waris)) anak, 0 wali FROM  PENERIMAAN_MP a WHERE a.kd_periode = '".$periode."' AND CALON_PENERIMA_MP_API.Get_Jenis_Ahli_Waris(a.company, a.n_i_k, a.no_ahli_waris) = 'Anak' AND a.mpb > 1500000 AND a.mpb <= 5000000 GROUP BY '>1.5 juta - 5 juta', 4 UNION ALL SELECT 4 NO_URUT, '>1.5 juta - 5 juta' zpokbesar, 0 peserta, 0 suami, 0 istri, 0 anak, COUNT(CALON_PENERIMA_MP_API.Get_Jenis_Ahli_Waris(a.company, a.n_i_k, a.no_ahli_waris)) wali FROM  PENERIMAAN_MP a WHERE a.kd_periode = '".$periode."' AND CALON_PENERIMA_MP_API.Get_Jenis_Ahli_Waris(a.company, a.n_i_k, a.no_ahli_waris) = 'Wali' AND a.mpb > 1500000 AND a.mpb <= 5000000 GROUP BY '>1.5 juta - 5 juta', 4) GROUP BY zpokbesar, NO_URUT UNION ALL /* >1.5jt - 5jt || >1jt - 1.5jt */ SELECT NO_URUT, zpokbesar, (SUM(peserta) + SUM(suami) + SUM(istri) + SUM(anak) + SUM(wali)) total_of_nik, SUM(anak) anak, SUM(istri) istri, SUM(peserta) peserta, SUM(suami) suami, SUM(wali) wali FROM (SELECT 3 NO_URUT, '>1 juta - 1.5 juta' zpokbesar, COUNT(CALON_PENERIMA_MP_API.Get_Jenis_Ahli_Waris(a.company, a.n_i_k, a.no_ahli_waris)) peserta, 0 suami, 0 istri, 0 anak, 0 wali FROM  PENERIMAAN_MP a WHERE a.kd_periode = '".$periode."' AND CALON_PENERIMA_MP_API.Get_Jenis_Ahli_Waris(a.company, a.n_i_k, a.no_ahli_waris) = 'Peserta' AND a.mpb > 1000000 AND a.mpb <= 1500000 GROUP BY '>1 juta - 1.5 juta', 3 UNION ALL SELECT 3 NO_URUT, '>1 juta - 1.5 juta' zpokbesar, 0 peserta, COUNT(CALON_PENERIMA_MP_API.Get_Jenis_Ahli_Waris(a.company, a.n_i_k, a.no_ahli_waris)) suami, 0 istri, 0 anak, 0 wali FROM  PENERIMAAN_MP a WHERE a.kd_periode = '".$periode."' AND CALON_PENERIMA_MP_API.Get_Jenis_Ahli_Waris(a.company, a.n_i_k, a.no_ahli_waris) = 'Suami' AND a.mpb > 1000000 AND a.mpb <= 1500000 GROUP BY '>1 juta - 1.5 juta', 3 UNION ALL SELECT 3 NO_URUT, '>1 juta - 1.5 juta' zpokbesar, 0 peserta, 0 suami, COUNT(CALON_PENERIMA_MP_API.Get_Jenis_Ahli_Waris(a.company, a.n_i_k, a.no_ahli_waris)) istri, 0 anak, 0 wali FROM  PENERIMAAN_MP a WHERE a.kd_periode = '".$periode."' AND CALON_PENERIMA_MP_API.Get_Jenis_Ahli_Waris(a.company, a.n_i_k, a.no_ahli_waris) = 'Istri' AND a.mpb > 1000000 AND a.mpb <= 1500000 GROUP BY '>1 juta - 1.5 juta', 3 UNION ALL SELECT 3 NO_URUT, '>1 juta - 1.5 juta' zpokbesar, 0 peserta, 0 suami, 0 istri, COUNT(CALON_PENERIMA_MP_API.Get_Jenis_Ahli_Waris(a.company, a.n_i_k, a.no_ahli_waris)) anak, 0 wali FROM  PENERIMAAN_MP a WHERE a.kd_periode = '".$periode."' AND CALON_PENERIMA_MP_API.Get_Jenis_Ahli_Waris(a.company, a.n_i_k, a.no_ahli_waris) = 'Anak' AND a.mpb > 1000000 AND a.mpb <= 1500000 GROUP BY '>1 juta - 1.5 juta', 3 UNION ALL SELECT 3 NO_URUT, '>1 juta - 1.5 juta' zpokbesar, 0 peserta, 0 suami, 0 istri, 0 anak, COUNT(CALON_PENERIMA_MP_API.Get_Jenis_Ahli_Waris(a.company, a.n_i_k, a.no_ahli_waris)) wali FROM  PENERIMAAN_MP a WHERE a.kd_periode = '".$periode."' AND CALON_PENERIMA_MP_API.Get_Jenis_Ahli_Waris(a.company, a.n_i_k, a.no_ahli_waris) = 'Wali' AND a.mpb > 1000000 AND a.mpb <= 1500000 GROUP BY '>1 juta - 1.5 juta', 3) GROUP BY zpokbesar, NO_URUT UNION ALL /* >1jt - 1.5jt || >600rb - 1jt */ SELECT NO_URUT, zpokbesar, (SUM(peserta) + SUM(suami) + SUM(istri) + SUM(anak) + SUM(wali)) total_of_nik, SUM(anak) anak, SUM(istri) istri, SUM(peserta) peserta, SUM(suami) suami, SUM(wali) wali FROM (SELECT 2 NO_URUT, '>600 rb - 1 juta' zpokbesar, COUNT(CALON_PENERIMA_MP_API.Get_Jenis_Ahli_Waris(a.company, a.n_i_k, a.no_ahli_waris)) peserta, 0 suami, 0 istri, 0 anak, 0 wali FROM  PENERIMAAN_MP a WHERE a.kd_periode = '".$periode."' AND CALON_PENERIMA_MP_API.Get_Jenis_Ahli_Waris(a.company, a.n_i_k, a.no_ahli_waris) = 'Peserta' AND a.mpb > 600000 AND a.mpb <= 1000000 GROUP BY '>600 rb - 1 juta', 2 UNION ALL SELECT 2 NO_URUT, '>600 rb - 1 juta' zpokbesar, 0 peserta, COUNT(CALON_PENERIMA_MP_API.Get_Jenis_Ahli_Waris(a.company, a.n_i_k, a.no_ahli_waris)) suami, 0 istri, 0 anak, 0 wali FROM  PENERIMAAN_MP a WHERE a.kd_periode = '".$periode."' AND CALON_PENERIMA_MP_API.Get_Jenis_Ahli_Waris(a.company, a.n_i_k, a.no_ahli_waris) = 'Suami' AND a.mpb > 600000 AND a.mpb <= 1000000 GROUP BY '>600 rb - 1 juta', 2 UNION ALL SELECT 2 NO_URUT, '>600 rb - 1 juta' zpokbesar, 0 peserta, 0 suami, COUNT(CALON_PENERIMA_MP_API.Get_Jenis_Ahli_Waris(a.company, a.n_i_k, a.no_ahli_waris)) istri, 0 anak, 0 wali FROM  PENERIMAAN_MP a WHERE a.kd_periode = '".$periode."' AND CALON_PENERIMA_MP_API.Get_Jenis_Ahli_Waris(a.company, a.n_i_k, a.no_ahli_waris) = 'Istri' AND a.mpb > 600000 AND a.mpb <= 1000000 GROUP BY '>600 rb - 1 juta', 2 UNION ALL SELECT 2 NO_URUT, '>600 rb - 1 juta' zpokbesar, 0 peserta, 0 suami, 0 istri, COUNT(CALON_PENERIMA_MP_API.Get_Jenis_Ahli_Waris(a.company, a.n_i_k, a.no_ahli_waris)) anak, 0 wali FROM  PENERIMAAN_MP a WHERE a.kd_periode = '".$periode."' AND CALON_PENERIMA_MP_API.Get_Jenis_Ahli_Waris(a.company, a.n_i_k, a.no_ahli_waris) = 'Anak' AND a.mpb > 600000 AND a.mpb <= 1000000 GROUP BY '>600 rb - 1 juta', 2 UNION ALL SELECT 2 NO_URUT, '>600 rb - 1 juta' zpokbesar, 0 peserta, 0 suami, 0 istri, 0 anak, COUNT(CALON_PENERIMA_MP_API.Get_Jenis_Ahli_Waris(a.company, a.n_i_k, a.no_ahli_waris)) wali FROM  PENERIMAAN_MP a WHERE a.kd_periode = '".$periode."' AND CALON_PENERIMA_MP_API.Get_Jenis_Ahli_Waris(a.company, a.n_i_k, a.no_ahli_waris) = 'Wali' AND a.mpb > 600000 AND a.mpb <= 1000000 GROUP BY '>600 rb - 1 juta', 2) GROUP BY zpokbesar, NO_URUT UNION ALL /* >600rb - 1jt || 450rb - 600rb */ SELECT NO_URUT, zpokbesar, (SUM(peserta) + SUM(suami) + SUM(istri) + SUM(anak) + SUM(wali)) total_of_nik, SUM(anak) anak, SUM(istri) istri, SUM(peserta) peserta, SUM(suami) suami, SUM(wali) wali FROM (SELECT 1 NO_URUT, '450 rb - 600 rb' zpokbesar, COUNT(CALON_PENERIMA_MP_API.Get_Jenis_Ahli_Waris(a.company, a.n_i_k, a.no_ahli_waris)) peserta, 0 suami, 0 istri, 0 anak, 0 wali FROM  PENERIMAAN_MP a WHERE a.kd_periode = '".$periode."' AND CALON_PENERIMA_MP_API.Get_Jenis_Ahli_Waris(a.company, a.n_i_k, a.no_ahli_waris) = 'Peserta' AND a.mpb <= 600000 GROUP BY '450 rb - 600 rb', 1 UNION ALL SELECT 1 NO_URUT, '450 rb - 600 rb' zpokbesar, 0 peserta, COUNT(CALON_PENERIMA_MP_API.Get_Jenis_Ahli_Waris(a.company, a.n_i_k, a.no_ahli_waris)) suami, 0 istri, 0 anak, 0 wali FROM  PENERIMAAN_MP a WHERE a.kd_periode = '".$periode."' AND CALON_PENERIMA_MP_API.Get_Jenis_Ahli_Waris(a.company, a.n_i_k, a.no_ahli_waris) = 'Suami' AND a.mpb <= 600000 GROUP BY '450 rb - 600 rb', 1 UNION ALL SELECT 1 NO_URUT, '450 rb - 600 rb' zpokbesar, 0 peserta, 0 suami, COUNT(CALON_PENERIMA_MP_API.Get_Jenis_Ahli_Waris(a.company, a.n_i_k, a.no_ahli_waris)) istri, 0 anak, 0 wali FROM  PENERIMAAN_MP a WHERE a.kd_periode = '".$periode."' AND CALON_PENERIMA_MP_API.Get_Jenis_Ahli_Waris(a.company, a.n_i_k, a.no_ahli_waris) = 'Istri' AND a.mpb <= 600000 GROUP BY '450 rb - 600 rb', 1 UNION ALL SELECT 1 NO_URUT, '450 rb - 600 rb' zpokbesar, 0 peserta, 0 suami, 0 istri, COUNT(CALON_PENERIMA_MP_API.Get_Jenis_Ahli_Waris(a.company, a.n_i_k, a.no_ahli_waris)) anak, 0 wali FROM  PENERIMAAN_MP a WHERE a.kd_periode = '".$periode."' AND CALON_PENERIMA_MP_API.Get_Jenis_Ahli_Waris(a.company, a.n_i_k, a.no_ahli_waris) = 'Anak' AND a.mpb <= 600000 GROUP BY '450 rb - 600 rb', 1 UNION ALL SELECT 1 NO_URUT, '450 rb - 600 rb' zpokbesar, 0 peserta, 0 suami, 0 istri, 0 anak, COUNT(CALON_PENERIMA_MP_API.Get_Jenis_Ahli_Waris(a.company, a.n_i_k, a.no_ahli_waris)) wali FROM  PENERIMAAN_MP a WHERE a.kd_periode = '".$periode."' AND CALON_PENERIMA_MP_API.Get_Jenis_Ahli_Waris(a.company, a.n_i_k, a.no_ahli_waris) = 'Wali' AND a.mpb <= 600000 GROUP BY '450 rb - 600 rb', 1) GROUP BY zpokbesar, NO_URUT) ORDER BY NO_URUT";

            $sql1 = oci_parse($conn, $query1);

            oci_execute($sql1);

            $data = array();
            while ($dt = oci_fetch_assoc($sql1)) {
                $data[] = $dt;
            }
        }
        
        if ($error_msg != '') {
            $status = "Failed! ".$error_msg;
        } else {
            $status = "Success!";
        }

        updateUserLog($log_id, $status);
        
        if (!$conn) {
            $e = oci_error();
            return $response->withJson(["data" => null, "error" => $e, "status" => "failed"], 200);
        } else {
            if ($error_msg != '') {
                return $response->withJson(["data" => null, "error" => $error_msg, "status" => "failed"], 200);
            } else {
                return $response->withJson(["data" => $data, "error" => null, "status" => "success"], 200);
            }
        }
    }); //[INF_MP_03]

    $app->get("/jmlh_pmp_dapen_jaw/", function (ServerRequestInterface $request, ResponseInterface $response, array $args){
        //$periode = $args['periode'];

        $periode = $request->getHeaderLine('PERIODE');
        $api_id = "INF_MP_04";
        
        $ipAddress = $request->getAttribute('ip_address');
        $decoded = $request->getAttribute("decoded_jwt");
        $username = $decoded['user'];
        $ifs_user = $decoded['ifs_user'];

        $log_id = insertUserLog($username, $api_id, $ipAddress);

        $conn = getConnection();

        $access = checkAccess($ifs_user, $api_id);

        $error_msg = "";
        $error_msg = string_type($periode, $error_msg, 'Periode', 'NotNull');

        if ($access == 'false') {
            $error_msg = "User ".$ifs_user." Does Not Have Access to This Action!";
        }

        if ($error_msg == '') {
            $query1 = "SELECT '".$periode."' PERIODE, kelompok_dapen, total_of_nik, anak, istri, peserta, suami, wali FROM (SELECT kelompok_dapen, (SUM(peserta) + SUM(suami) + SUM(istri) + SUM(anak) + SUM(wali)) total_of_nik, SUM(anak) anak, SUM(istri) istri, SUM(peserta) peserta, SUM(suami) suami, SUM(wali) wali FROM (SELECT KELOMPOK_PESERTA_API.Get_Kelompok_Dapen(a.company, PESERTA_API.Get_Kd_Kelompok(a.company, a.n_i_k)) kelompok_dapen, COUNT(CALON_PENERIMA_MP_API.Get_Jenis_Ahli_Waris(a.company, a.n_i_k, a.no_ahli_waris)) peserta, 0 suami, 0 istri, 0 anak, 0 wali FROM PENERIMAAN_MP a WHERE a.kd_periode = '".$periode."' AND CALON_PENERIMA_MP_API.Get_Jenis_Ahli_Waris(a.company, a.n_i_k, a.no_ahli_waris) = 'Peserta' AND KELOMPOK_PESERTA_API.Get_Kelompok_Dapen(a.company, PESERTA_API.Get_Kd_Kelompok(a.company, a.n_i_k)) = 'DAPEN I' GROUP BY KELOMPOK_PESERTA_API.Get_Kelompok_Dapen(a.company, PESERTA_API.Get_Kd_Kelompok(a.company, a.n_i_k)) UNION ALL SELECT KELOMPOK_PESERTA_API.Get_Kelompok_Dapen(a.company, PESERTA_API.Get_Kd_Kelompok(a.company, a.n_i_k)) kelompok_dapen, 0 peserta, COUNT(CALON_PENERIMA_MP_API.Get_Jenis_Ahli_Waris(a.company, a.n_i_k, a.no_ahli_waris)) suami, 0 istri, 0 anak, 0 wali FROM PENERIMAAN_MP a WHERE a.kd_periode = '".$periode."' AND CALON_PENERIMA_MP_API.Get_Jenis_Ahli_Waris(a.company, a.n_i_k, a.no_ahli_waris) = 'Suami' AND KELOMPOK_PESERTA_API.Get_Kelompok_Dapen(a.company, PESERTA_API.Get_Kd_Kelompok(a.company, a.n_i_k)) = 'DAPEN I' GROUP BY KELOMPOK_PESERTA_API.Get_Kelompok_Dapen(a.company, PESERTA_API.Get_Kd_Kelompok(a.company, a.n_i_k)) UNION ALL SELECT KELOMPOK_PESERTA_API.Get_Kelompok_Dapen(a.company, PESERTA_API.Get_Kd_Kelompok(a.company, a.n_i_k)) kelompok_dapen, 0 peserta, 0 suami, COUNT(CALON_PENERIMA_MP_API.Get_Jenis_Ahli_Waris(a.company, a.n_i_k, a.no_ahli_waris)) istri, 0 anak, 0 wali FROM PENERIMAAN_MP a WHERE a.kd_periode = '".$periode."' AND CALON_PENERIMA_MP_API.Get_Jenis_Ahli_Waris(a.company, a.n_i_k, a.no_ahli_waris) = 'Istri' AND KELOMPOK_PESERTA_API.Get_Kelompok_Dapen(a.company, PESERTA_API.Get_Kd_Kelompok(a.company, a.n_i_k)) = 'DAPEN I' GROUP BY KELOMPOK_PESERTA_API.Get_Kelompok_Dapen(a.company, PESERTA_API.Get_Kd_Kelompok(a.company, a.n_i_k)) UNION ALL SELECT KELOMPOK_PESERTA_API.Get_Kelompok_Dapen(a.company, PESERTA_API.Get_Kd_Kelompok(a.company, a.n_i_k)) kelompok_dapen, 0 peserta, 0 suami, 0 istri, COUNT(CALON_PENERIMA_MP_API.Get_Jenis_Ahli_Waris(a.company, a.n_i_k, a.no_ahli_waris)) anak, 0 wali FROM PENERIMAAN_MP a WHERE a.kd_periode = '".$periode."' AND CALON_PENERIMA_MP_API.Get_Jenis_Ahli_Waris(a.company, a.n_i_k, a.no_ahli_waris) = 'Anak' AND KELOMPOK_PESERTA_API.Get_Kelompok_Dapen(a.company, PESERTA_API.Get_Kd_Kelompok(a.company, a.n_i_k)) = 'DAPEN I' GROUP BY KELOMPOK_PESERTA_API.Get_Kelompok_Dapen(a.company, PESERTA_API.Get_Kd_Kelompok(a.company, a.n_i_k)) UNION ALL SELECT KELOMPOK_PESERTA_API.Get_Kelompok_Dapen(a.company, PESERTA_API.Get_Kd_Kelompok(a.company, a.n_i_k)) kelompok_dapen, 0 peserta, 0 suami, 0 istri, 0 anak, COUNT(CALON_PENERIMA_MP_API.Get_Jenis_Ahli_Waris(a.company, a.n_i_k, a.no_ahli_waris)) wali FROM PENERIMAAN_MP a WHERE a.kd_periode = '".$periode."' AND CALON_PENERIMA_MP_API.Get_Jenis_Ahli_Waris(a.company, a.n_i_k, a.no_ahli_waris) = 'Wali' AND KELOMPOK_PESERTA_API.Get_Kelompok_Dapen(a.company, PESERTA_API.Get_Kd_Kelompok(a.company, a.n_i_k)) = 'DAPEN I' GROUP BY KELOMPOK_PESERTA_API.Get_Kelompok_Dapen(a.company, PESERTA_API.Get_Kd_Kelompok(a.company, a.n_i_k))) GROUP BY kelompok_dapen UNION ALL /* DAPEN I || DAPEN II */ SELECT kelompok_dapen, (SUM(peserta) + SUM(suami) + SUM(istri) + SUM(anak) + SUM(wali)) total_of_nik, SUM(anak) anak, SUM(istri) istri, SUM(peserta) peserta, SUM(suami) suami, SUM(wali) wali FROM (SELECT KELOMPOK_PESERTA_API.Get_Kelompok_Dapen(a.company, PESERTA_API.Get_Kd_Kelompok(a.company, a.n_i_k)) kelompok_dapen, COUNT(CALON_PENERIMA_MP_API.Get_Jenis_Ahli_Waris(a.company, a.n_i_k, a.no_ahli_waris)) peserta, 0 suami, 0 istri, 0 anak, 0 wali FROM PENERIMAAN_MP a WHERE a.kd_periode = '".$periode."' AND CALON_PENERIMA_MP_API.Get_Jenis_Ahli_Waris(a.company, a.n_i_k, a.no_ahli_waris) = 'Peserta' AND KELOMPOK_PESERTA_API.Get_Kelompok_Dapen(a.company, PESERTA_API.Get_Kd_Kelompok(a.company, a.n_i_k)) = 'DAPEN II' GROUP BY KELOMPOK_PESERTA_API.Get_Kelompok_Dapen(a.company, PESERTA_API.Get_Kd_Kelompok(a.company, a.n_i_k)) UNION ALL SELECT KELOMPOK_PESERTA_API.Get_Kelompok_Dapen(a.company, PESERTA_API.Get_Kd_Kelompok(a.company, a.n_i_k)) kelompok_dapen, 0 peserta, COUNT(CALON_PENERIMA_MP_API.Get_Jenis_Ahli_Waris(a.company, a.n_i_k, a.no_ahli_waris)) suami, 0 istri, 0 anak, 0 wali FROM PENERIMAAN_MP a WHERE a.kd_periode = '".$periode."' AND CALON_PENERIMA_MP_API.Get_Jenis_Ahli_Waris(a.company, a.n_i_k, a.no_ahli_waris) = 'Suami' AND KELOMPOK_PESERTA_API.Get_Kelompok_Dapen(a.company, PESERTA_API.Get_Kd_Kelompok(a.company, a.n_i_k)) = 'DAPEN II' GROUP BY KELOMPOK_PESERTA_API.Get_Kelompok_Dapen(a.company, PESERTA_API.Get_Kd_Kelompok(a.company, a.n_i_k)) UNION ALL SELECT KELOMPOK_PESERTA_API.Get_Kelompok_Dapen(a.company, PESERTA_API.Get_Kd_Kelompok(a.company, a.n_i_k)) kelompok_dapen, 0 peserta, 0 suami, COUNT(CALON_PENERIMA_MP_API.Get_Jenis_Ahli_Waris(a.company, a.n_i_k, a.no_ahli_waris)) istri, 0 anak, 0 wali FROM PENERIMAAN_MP a WHERE a.kd_periode = '".$periode."' AND CALON_PENERIMA_MP_API.Get_Jenis_Ahli_Waris(a.company, a.n_i_k, a.no_ahli_waris) = 'Istri' AND KELOMPOK_PESERTA_API.Get_Kelompok_Dapen(a.company, PESERTA_API.Get_Kd_Kelompok(a.company, a.n_i_k)) = 'DAPEN II' GROUP BY KELOMPOK_PESERTA_API.Get_Kelompok_Dapen(a.company, PESERTA_API.Get_Kd_Kelompok(a.company, a.n_i_k)) UNION ALL SELECT KELOMPOK_PESERTA_API.Get_Kelompok_Dapen(a.company, PESERTA_API.Get_Kd_Kelompok(a.company, a.n_i_k)) kelompok_dapen, 0 peserta, 0 suami, 0 istri, COUNT(CALON_PENERIMA_MP_API.Get_Jenis_Ahli_Waris(a.company, a.n_i_k, a.no_ahli_waris)) anak, 0 wali FROM PENERIMAAN_MP a WHERE a.kd_periode = '".$periode."' AND CALON_PENERIMA_MP_API.Get_Jenis_Ahli_Waris(a.company, a.n_i_k, a.no_ahli_waris) = 'Anak' AND KELOMPOK_PESERTA_API.Get_Kelompok_Dapen(a.company, PESERTA_API.Get_Kd_Kelompok(a.company, a.n_i_k)) = 'DAPEN II' GROUP BY KELOMPOK_PESERTA_API.Get_Kelompok_Dapen(a.company, PESERTA_API.Get_Kd_Kelompok(a.company, a.n_i_k)) UNION ALL SELECT KELOMPOK_PESERTA_API.Get_Kelompok_Dapen(a.company, PESERTA_API.Get_Kd_Kelompok(a.company, a.n_i_k)) kelompok_dapen, 0 peserta, 0 suami, 0 istri, 0 anak, COUNT(CALON_PENERIMA_MP_API.Get_Jenis_Ahli_Waris(a.company, a.n_i_k, a.no_ahli_waris)) wali FROM PENERIMAAN_MP a WHERE a.kd_periode = '".$periode."' AND CALON_PENERIMA_MP_API.Get_Jenis_Ahli_Waris(a.company, a.n_i_k, a.no_ahli_waris) = 'Wali' AND KELOMPOK_PESERTA_API.Get_Kelompok_Dapen(a.company, PESERTA_API.Get_Kd_Kelompok(a.company, a.n_i_k)) = 'DAPEN II' GROUP BY KELOMPOK_PESERTA_API.Get_Kelompok_Dapen(a.company, PESERTA_API.Get_Kd_Kelompok(a.company, a.n_i_k))) GROUP BY kelompok_dapen) ";

            $sql1 = oci_parse($conn, $query1);

            oci_execute($sql1);

            $data = array();
            while ($dt = oci_fetch_assoc($sql1)) {
                $data[] = $dt;
            }
        }
        
        if ($error_msg != '') {
            $status = "Failed! ".$error_msg;
        } else {
            $status = "Success!";
        }

        updateUserLog($log_id, $status);
        
        if (!$conn) {
            $e = oci_error();
            return $response->withJson(["data" => null, "error" => $e, "status" => "failed"], 200);
        } else {
            if ($error_msg != '') {
                return $response->withJson(["data" => null, "error" => $error_msg, "status" => "failed"], 200);
            } else {
                return $response->withJson(["data" => $data, "error" => null, "status" => "success"], 200);
            }
        }
    }); //[INF_MP_04]

    //-------------------------------------------------------------------------------------//
    //---------------------------------// [INF_BAYAR_MP] //--------------------------------//
    //-------------------------------------------------------------------------------------//

    $app->get("/jmlh_bayar_mp/", function (ServerRequestInterface $request, ResponseInterface $response, array $args){
        $periode = $request->getHeaderLine('PERIODE');
        $bank = $request->getHeaderLine('KD_BANK');
        $p2tel = $request->getHeaderLine('P2TEL');
        $kategori = $request->getHeaderLine('KATEGORI');
        $api_id = "INF_BAYAR_MP_01";
        
        $ipAddress = $request->getAttribute('ip_address');
        $decoded = $request->getAttribute("decoded_jwt");
        $username = $decoded['user'];
        $ifs_user = $decoded['ifs_user'];

        $log_id = insertUserLog($username, $api_id, $ipAddress);

        $conn = getConnection();
        $jns_api = "jmlh";

        $access = checkAccess($ifs_user, $api_id);

        $error_msg = "";
        $error_msg = string_type($periode, $error_msg, 'Periode', 'NotNull');
        $error_msg = string_type($bank, $error_msg, 'Kd Bank', 'Nullable');
        $error_msg = string_type($p2tel, $error_msg, 'P2Tel', 'Nullable');
        $error_msg = parameter_type($kategori, $error_msg, 'Kategori', 1, 3, 'Nullable');

        if ($access == 'false') {
            $error_msg = "User ".$ifs_user." Does Not Have Access to This Action!";
        }

        if ($error_msg == '') {
            $data = jmlhPmpByrMp($periode, $bank, $p2tel, $kategori, $jns_api);
        }

        if ($error_msg != '') {
            $status = "Failed! ".$error_msg;
        } else {
            $status = "Success!";
        }

        updateUserLog($log_id, $status);
        
        if (!$conn) {
            $e = oci_error();
            return $response->withJson(["data" => null, "error" => $e, "status" => "failed"], 200);
        } else {
            if ($error_msg != '') {
                return $response->withJson(["data" => null, "error" => $error_msg, "status" => "failed"], 200);
            } else {
                return $response->withJson(["data" => $data, "error" => null, "status" => "success"], 200);
            }
        }
    }); //[INF_BAYAR_MP_01]

    $app->get("/jmlh_bayar_mp_all/", function (ServerRequestInterface $request, ResponseInterface $response, array $args){
        $periode = $request->getHeaderLine('PERIODE');
        $parameter = $request->getHeaderLine('PARAMETER');
        $api_id = "INF_BAYAR_MP_02";
        
        $ipAddress = $request->getAttribute('ip_address');
        $decoded = $request->getAttribute("decoded_jwt");
        $username = $decoded['user'];
        $ifs_user = $decoded['ifs_user'];

        $log_id = insertUserLog($username, $api_id, $ipAddress);

        $conn = getConnection();

        $access = checkAccess($ifs_user, $api_id);

        $error_msg = "";
        $error_msg = string_type($periode, $error_msg, 'Periode', 'NotNull');
        $error_msg = parameter_type($parameter, $error_msg, 'Parameter', 1, 3, 'NotNull');

        if ($access == 'false') {
            $error_msg = "User ".$ifs_user." Does Not Have Access to This Action!";
        }

        if ($error_msg == '') {
            $data = jmlhPmpByrMpAll($periode, $parameter);
        }

        if ($error_msg != '') {
            $status = "Failed! ".$error_msg;
        } else {
            $status = "Success!";
        }

        updateUserLog($log_id, $status);
        
        if (!$conn) {
            $e = oci_error();
            return $response->withJson(["data" => null, "error" => $e, "status" => "failed"], 200);
        } else {
            if ($error_msg != '') {
                return $response->withJson(["data" => null, "error" => $error_msg, "status" => "failed"], 200);
            } else {
                return $response->withJson(["data" => $data, "error" => null, "status" => "success"], 200);
            }
        }
    }); //[INF_BAYAR_MP_02]

    $app->get("/list_penerima_mp/", function (ServerRequestInterface $request, ResponseInterface $response, array $args){
        $periode = $request->getHeaderLine('PERIODE');
        $bank = $request->getHeaderLine('KD_BANK');
        $p2tel = $request->getHeaderLine('P2TEL');
        $kategori = $request->getHeaderLine('KATEGORI');
        $api_id = "INF_BAYAR_MP_03";
        
        $ipAddress = $request->getAttribute('ip_address');
        $decoded = $request->getAttribute("decoded_jwt");
        $username = $decoded['user'];
        $ifs_user = $decoded['ifs_user'];

        $log_id = insertUserLog($username, $api_id, $ipAddress);

        $conn = getConnection();
        $jns_api = "list";

        $access = checkAccess($ifs_user, $api_id);

        $error_msg = "";
        $error_msg = string_type($periode, $error_msg, 'Periode', 'NotNull');
        $error_msg = string_type($bank, $error_msg, 'Kd Bank', 'Nullable');
        $error_msg = string_type($p2tel, $error_msg, 'P2Tel', 'Nullable');
        $error_msg = parameter_type($kategori, $error_msg, 'Kategori', 1, 3, 'Nullable');

        if ($access == 'false') {
            $error_msg = "User ".$ifs_user." Does Not Have Access to This Action!";
        }

        if ($error_msg == '') {
            $data = jmlhPmpByrMp($periode, $bank, $p2tel, $kategori, $jns_api);
        }

        if ($error_msg != '') {
            $status = "Failed! ".$error_msg;
        } else {
            $status = "Success!";
        }

        updateUserLog($log_id, $status);
        
        if (!$conn) {
            $e = oci_error();
            return $response->withJson(["data" => null, "error" => $e, "status" => "failed"], 200);
        } else {
            if ($error_msg != '') {
                return $response->withJson(["data" => null, "error" => $error_msg, "status" => "failed"], 200);
            } else {
                return $response->withJson(["data" => $data, "error" => null, "status" => "success"], 200);
            }
        }
    }); //[INF_BAYAR_MP_03]

    $app->get("/jmlh_bayar_mt/", function (ServerRequestInterface $request, ResponseInterface $response, array $args){
        $conn = getConnection();

        $jns_api = "jmlh";

        $periode = $request->getHeaderLine('PERIODE');
        $param_bank = $request->getHeaderLine('PARAM_BANK');
        $p2tel = $request->getHeaderLine('P2TEL');
        $api_id = "INF_BAYAR_MP_04";
        
        $ipAddress = $request->getAttribute('ip_address');
        $decoded = $request->getAttribute("decoded_jwt");
        $username = $decoded['user'];
        $ifs_user = $decoded['ifs_user'];

        $log_id = insertUserLog($username, $api_id, $ipAddress);

        $access = checkAccess($ifs_user, $api_id);

        $error_msg = "";
        $error_msg = string_type($periode, $error_msg, 'Periode', 'NotNull');
        $error_msg = parameter_type($param_bank, $error_msg, 'Parameter Bank', 1, 6, 'Nullable');
        $error_msg = string_type($p2tel, $error_msg, 'P2Tel', 'Nullable');

        if ($access == 'false') {
            $error_msg = "User ".$ifs_user." Does Not Have Access to This Action!";
        }

        if ($error_msg == '') {
            $data = jmlhPmpByrMt($jns_api, $periode, $param_bank, $p2tel);
        }

        if ($error_msg != '') {
            $status = "Failed! ".$error_msg;
        } else {
            $status = "Success!";
        }

        updateUserLog($log_id, $status);
        
        if (!$conn) {
            $e = oci_error();
            return $response->withJson(["data" => null, "error" => $e, "status" => "failed"], 200);
        } else {
            if ($error_msg != '') {
                return $response->withJson(["data" => null, "error" => $error_msg, "status" => "failed"], 200);
            } else {
                return $response->withJson(["data" => $data, "error" => null, "status" => "success"], 200);
            }
        }
    }); //[INF_BAYAR_MP_04]

    $app->get("/jmlh_bayar_mt_all/", function (ServerRequestInterface $request, ResponseInterface $response, array $args){
        $conn = getConnection();

        $periode = $request->getHeaderLine('PERIODE');
        $parameter = $request->getHeaderLine('PARAMETER');
        $api_id = "INF_BAYAR_MP_05";
        
        $ipAddress = $request->getAttribute('ip_address');
        $decoded = $request->getAttribute("decoded_jwt");
        $username = $decoded['user'];
        $ifs_user = $decoded['ifs_user'];

        $log_id = insertUserLog($username, $api_id, $ipAddress);

        $access = checkAccess($ifs_user, $api_id);

        $error_msg = "";
        $error_msg = string_type($periode, $error_msg, 'Periode', 'NotNull');
        $error_msg = parameter_type($parameter, $error_msg, 'Parameter', 1, 2, 'NotNull');

        if ($access == 'false') {
            $error_msg = "User ".$ifs_user." Does Not Have Access to This Action!";
        }

        if ($error_msg == '') {
            $data = jmlhPmpByrMtAll($periode, $parameter);
        }

        if ($error_msg != '') {
            $status = "Failed! ".$error_msg;
        } else {
            $status = "Success!";
        }

        updateUserLog($log_id, $status);
        
        if (!$conn) {
            $e = oci_error();
            return $response->withJson(["data" => null, "error" => $e, "status" => "failed"], 200);
        } else {
            if ($error_msg != '') {
                return $response->withJson(["data" => null, "error" => $error_msg, "status" => "failed"], 200);
            } else {
                return $response->withJson(["data" => $data, "error" => null, "status" => "success"], 200);
            }
        }
    }); //[INF_BAYAR_MP_05]

    $app->get("/list_penerima_mt/", function (ServerRequestInterface $request, ResponseInterface $response, array $args){
        $conn = getConnection();
        
        $jns_api = "list";

        $periode = $request->getHeaderLine('PERIODE');
        $param_bank = $request->getHeaderLine('PARAM_BANK');
        $p2tel = $request->getHeaderLine('P2TEL');
        $api_id = "INF_BAYAR_MP_06";
        
        $ipAddress = $request->getAttribute('ip_address');
        $decoded = $request->getAttribute("decoded_jwt");
        $username = $decoded['user'];
        $ifs_user = $decoded['ifs_user'];

        $log_id = insertUserLog($username, $api_id, $ipAddress);

        $access = checkAccess($ifs_user, $api_id);

        $error_msg = "";
        $error_msg = string_type($periode, $error_msg, 'Periode', 'NotNull');
        $error_msg = parameter_type($param_bank, $error_msg, 'Parameter Bank', 1, 6, 'Nullable');
        $error_msg = string_type($p2tel, $error_msg, 'P2Tel', 'Nullable');

        if ($access == 'false') {
            $error_msg = "User ".$ifs_user." Does Not Have Access to This Action!";
        }

        if ($error_msg == '') {
            $data = jmlhPmpByrMt($jns_api, $periode, $param_bank, $p2tel);
        }

        if ($error_msg != '') {
            $status = "Failed! ".$error_msg;
        } else {
            $status = "Success!";
        }

        updateUserLog($log_id, $status);
        
        if (!$conn) {
            $e = oci_error();
            return $response->withJson(["data" => null, "error" => $e, "status" => "failed"], 200);
        } else {
            if ($error_msg != '') {
                return $response->withJson(["data" => null, "error" => $error_msg, "status" => "failed"], 200);
            } else {
                return $response->withJson(["data" => $data, "error" => null, "status" => "success"], 200);
            }
        }
    }); //[INF_BAYAR_MP_06]
    
    $app->get("/jmlh_saldo_retur_mp/", function (ServerRequestInterface $request, ResponseInterface $response, array $args){
        $conn = getConnection();

        $periode = $request->getHeaderLine('PERIODE');
        $nik = $request->getHeaderLine('NIK');
        $nominal_retur = $request->getHeaderLine('NOMINAL_RETUR');
        //$periode_terima = $request->getHeaderLine('PERIODE_TERIMA');
        $periode_terima = "";
        $periode_dibayar = $request->getHeaderLine('PERIODE_DIBAYAR');
        $api_id = "INF_BAYAR_MP_07";
        
        $ipAddress = $request->getAttribute('ip_address');
        $decoded = $request->getAttribute("decoded_jwt");
        $username = $decoded['user'];
        $ifs_user = $decoded['ifs_user'];

        $log_id = insertUserLog($username, $api_id, $ipAddress);

        $access = checkAccess($ifs_user, $api_id);

        $error_msg = "";
        $error_msg = string_type($periode, $error_msg, 'Periode', 'NotNull');
        $error_msg = string_type($nik, $error_msg, 'NIK', 'Nullable');
        $error_msg = int_type($nominal_retur, $error_msg, 'Nominal Retur', 'Nullable');
        $error_msg = string_type($periode_terima, $error_msg, 'Periode Terima', 'Nullable');
        $error_msg = string_type($periode_dibayar, $error_msg, 'Periode Dibayar', 'Nullable');

        if ($access == 'false') {
            $error_msg = "User ".$ifs_user." Does Not Have Access to This Action!";
        }

        if ($error_msg == '') {
            $data['JMLH_SALDO_RETUR_MP'] = jmlhSaldoReturMp($periode, $nik, $nominal_retur, $periode_terima, $periode_dibayar);
        }

        if ($error_msg != '') {
            $status = "Failed! ".$error_msg;
        } else {
            $status = "Success!";
        }

        updateUserLog($log_id, $status);
        
        if (!$conn) {
            $e = oci_error();
            return $response->withJson(["data" => null, "error" => $e, "status" => "failed"], 200);
        } else {
            if ($error_msg != '') {
                return $response->withJson(["data" => null, "error" => $error_msg, "status" => "failed"], 200);
            } else {
                return $response->withJson(["data" => $data, "error" => null, "status" => "success"], 200);
            }
        }
    }); //[INF_BAYAR_MP_07]

    //----------------------------------------------------------------------------------------//
    //------------------------------------// [INF_IURAN] //-----------------------------------//
    //----------------------------------------------------------------------------------------//

    $app->get("/dftr_rekap_iuran/", function (ServerRequestInterface $request, ResponseInterface $response, array $args){
        $periode = $request->getHeaderLine('PERIODE');
        $api_id = "INF_IURAN_01";
        
        $ipAddress = $request->getAttribute('ip_address');
        $decoded = $request->getAttribute("decoded_jwt");
        $username = $decoded['user'];
        $ifs_user = $decoded['ifs_user'];

        $log_id = insertUserLog($username, $api_id, $ipAddress);

        $conn = getConnection();

        $access = checkAccess($ifs_user, $api_id);

        $error_msg = "";
        $error_msg = string_type($periode, $error_msg, 'Periode', 'NotNull');

        if ($access == 'false') {
            $error_msg = "User ".$ifs_user." Does Not Have Access to This Action!";
        }

        //$query1 = "SELECT a.kd_unit_penyetor, UNIT_KERJA_API.Get_Nama_Unit_Kerja(a.company,a.kd_unit_penyetor) nama_unit_penyetor, '-- not yet --' saldo_titipan_bln_sblmnya, '-- not yet --' saldo_piutang_bln_sblmnya, '-- not yet --' nilai_iuran_bln_sblmnya, a.rekap_ipk nilai_ipk_bln_berjalan, a.rekap_ipst nilai_iuran_bln_berjalan, '-- not yet --' nilai_iuran_bln_sblmnya, '-- not yet --' nilai_iuran_setor_sampai_bln_berjalan, '-- not yet --' penerimaan, '-- not yet --' titipan, '-- not yet --' piutang FROM DTL_REKAP_IUR_UNT_PENYETOR_TAB a, REKAP_IURAN_NORMAL_TAB b WHERE a.kd_periode = b.kd_periode AND b.kd_periode = $periode";

        if ($error_msg == '') {
            $query1 = "SELECT b.kd_periode, b.kd_unit_penyetor, UNIT_KERJA_API.Get_Nama_Unit_Kerja(a.COMPANY,b.KD_UNIT_PENYETOR) nama_unit_penyetor, CTM_API_UTIL_API.Get_Iuran_Sld_Titipan (to_number(a.kd_periode)-1, b.kd_unit_penyetor) sld_ttpn_bln_sblm, CTM_API_UTIL_API.Get_Iuran_Sld_Piutang (to_number(a.kd_periode)-1, b.kd_unit_penyetor) sld_ptg_bln_sblm, DTL_REKAP_IUR_UNT_PENYETOR_API.Get_Rekap_Ipk(a.company, to_char(to_number(a.kd_periode)-1),b.kd_unit_penyetor) nilai_ipk_bln_sblm, b.rekap_ipk nilai_ipk_bln_bjln, DTL_REKAP_IUR_UNT_PENYETOR_API.Get_Rekap_Ipst(a.company, to_char(to_number(a.kd_periode)-1),b.kd_unit_penyetor) nilai_iuran_bln_sblm, b.rekap_ipst nilai_iuran_bln_bjln, CTM_API_UTIL_API.Get_Nilai_Setor_Sd_Bln_Bjln (a.kd_periode, b.kd_unit_penyetor) nilai_setor_sd_bln_bjln, b.jumlah_pst jmlh_pst_bln_bjln, CTM_API_UTIL_API.Get_Penerimaan_Iuran (to_number(a.kd_periode), b.kd_unit_penyetor) penerimaan, CTM_API_UTIL_API.Get_Titipan_Iuran (to_number(a.kd_periode), b.kd_unit_penyetor) titipan, CTM_API_UTIL_API.Get_Piutang_Iuran (to_number(a.kd_periode), b.kd_unit_penyetor) piutang, CTM_API_UTIL_API.Get_Tgl_Penerimaan_Iuran (to_number(a.kd_periode), b.kd_unit_penyetor) tgl_penerimaan FROM REKAP_IURAN_NORMAL a, DTL_REKAP_IUR_UNT_PENYETOR b WHERE a.kd_periode = b.kd_periode AND a.kd_periode = '".$periode."' AND a.objstate  ='Disetujui'";
            
            $sql1 = oci_parse($conn, $query1);

            oci_execute($sql1);

            $rows = array();
            while ($dt = oci_fetch_assoc($sql1)) {
                $rows[] = $dt;
            }
        }

        if ($error_msg != '') {
            $status = "Failed! ".$error_msg;
        } else {
            $status = "Success!";
        }

        updateUserLog($log_id, $status);
        
        if (!$conn) {
            $e = oci_error();
            return $response->withJson(["data" => null, "error" => $e, "status" => "failed"], 200);
        } else {
            if ($error_msg != '') {
                return $response->withJson(["data" => null, "error" => $error_msg, "status" => "failed"], 200);
            } else {
                return $response->withJson(["data" => $rows, "error" => null, "status" => "success"], 200);
            }
        }
    }); //[INF_IURAN_01]

    $app->get("/data_kartu_pengawasan_iuran/", function (ServerRequestInterface $request, ResponseInterface $response, array $args){
        $periode_thn = $request->getHeaderLine('PERIODE_TAHUN');
        $kode_up = $request->getHeaderLine('KODE_UP'); //JVC-PTPN
        $api_id = "INF_IURAN_02";
        
        $ipAddress = $request->getAttribute('ip_address');
        $decoded = $request->getAttribute("decoded_jwt");
        $username = $decoded['user'];
        $ifs_user = $decoded['ifs_user'];

        $log_id = insertUserLog($username, $api_id, $ipAddress);

        $conn = getConnection();

        $access = checkAccess($ifs_user, $api_id);

        $error_msg = "";
        $error_msg = string_type($periode_thn, $error_msg, 'Periode Tahun', 'NotNull');
        $error_msg = string_type($kode_up, $error_msg, 'Kode UP', 'NotNull');

        if ($access == 'false') {
            $error_msg = "User ".$ifs_user." Does Not Have Access to This Action!";
        }

        if ($error_msg == '') {
            $query1 = "SELECT UNIT_KERJA_API.Get_Nama_Unit_Kerja(a.COMPANY, b.KD_UNIT_PENYETOR) nama_unit_penyetor, SUBSTR(a.kd_periode, 5, 2) periode_bln, b.jumlah_pst jmlh_peserta, b.rekap_ipst jmlh_iuran_peserta_dihitung, CTM_API_UTIL_API.Get_Penerimaan_Iuran(TO_NUMBER(a.kd_periode),b.kd_unit_penyetor) penerimaan, CTM_API_UTIL_API.Get_Tgl_Penerimaan_Iuran(TO_NUMBER(a.kd_periode),b.kd_unit_penyetor) tgl_penerimaan, b.rekap_ipst - CTM_API_UTIL_API.Get_Penerimaan_Iuran(TO_NUMBER(a.kd_periode),b.kd_unit_penyetor) selisih, CASE WHEN CTM_API_UTIL_API.Get_Tgl_Penerimaan_Iuran(TO_NUMBER(a.kd_periode),b.kd_unit_penyetor) <= (TRUNC(PERIODE_API.Get_Tanggal_Awal(a.company,PERIODE_API.Get_Next_Period(a.company,a.kd_periode)), 'MONTH') + 14) THEN 0 ELSE (CTM_API_UTIL_API.Get_Tgl_Penerimaan_Iuran(TO_NUMBER(a.kd_periode),b.kd_unit_penyetor) - (TRUNC(PERIODE_API.Get_Tanggal_Awal(a.company,PERIODE_API.Get_Next_Period(a.company,a.kd_periode)), 'MONTH') + 14)) END hari_lambat, DENDA_IURAN_API.Get_Denda(a.company,a.kd_periode) bunga, ((CASE WHEN CTM_API_UTIL_API.Get_Tgl_Penerimaan_Iuran(TO_NUMBER(a.kd_periode/**/),b.kd_unit_penyetor) <= (TRUNC(CTM_API_UTIL_API.Get_Tgl_Penerimaan_Iuran(TO_NUMBER(a.kd_periode),b.kd_unit_penyetor), 'MONTH')+14) THEN 0 ELSE (CTM_API_UTIL_API.Get_Tgl_Penerimaan_Iuran(TO_NUMBER(a.kd_periode/**/),b.kd_unit_penyetor)-(TRUNC(CTM_API_UTIL_API.Get_Tgl_Penerimaan_Iuran(TO_NUMBER(a.kd_periode),b.kd_unit_penyetor),'MONTH')+14))END*DENDA_IURAN_API.Get_Denda(a.company,a.kd_periode)*(b.rekap_ipst))/365) denda FROM REKAP_IURAN_NORMAL a, DTL_REKAP_IUR_UNT_PENYETOR b WHERE a.kd_periode = b.kd_periode AND SUBSTR(a.kd_periode,1,4) = '".$periode_thn."' AND b.kd_unit_penyetor = '".$kode_up."' AND a.objstate  = 'Disetujui' ORDER BY a.kd_periode";
            
            $sql1 = oci_parse($conn, $query1);

            oci_execute($sql1);

            $rows = array();
            while ($dt = oci_fetch_assoc($sql1)) {
                $rows[] = $dt;
            }
        }

        if ($error_msg != '') {
            $status = "Failed! ".$error_msg;
        } else {
            $status = "Success!";
        }

        updateUserLog($log_id, $status);
        
        if (!$conn) {
            $e = oci_error();
            return $response->withJson(["data" => null, "error" => $e, "status" => "failed"], 200);
        } else {
            if ($error_msg != '') {
                return $response->withJson(["data" => null, "error" => $error_msg, "status" => "failed"], 200);
            } else {
                return $response->withJson(["data" => $rows, "error" => null, "status" => "success"], 200);
            }
        }
    }); //[INF_IURAN_02]

    $app->get("/data_kartu_piutang_per_up/", function (ServerRequestInterface $request, ResponseInterface $response, array $args){
        $periode = $request->getHeaderLine('PERIODE');
        $api_id = "INF_IURAN_03";
        
        $ipAddress = $request->getAttribute('ip_address');
        $decoded = $request->getAttribute("decoded_jwt");
        $username = $decoded['user'];
        $ifs_user = $decoded['ifs_user'];

        $log_id = insertUserLog($username, $api_id, $ipAddress);

        $conn = getConnection();

        $access = checkAccess($ifs_user, $api_id);

        $error_msg = "";
        $error_msg = string_type($periode, $error_msg, 'Periode', 'NotNull');

        if ($access == 'false') {
            $error_msg = "User ".$ifs_user." Does Not Have Access to This Action!";
        }

        if ($error_msg == '') {
            $query1 = "SELECT a.kd_periode, b.kd_unit_penyetor, UNIT_KERJA_API.Get_Nama_Unit_Kerja(a.COMPANY,b.KD_UNIT_PENYETOR) nama_unit_penyetor, (NVL(CTM_API_UTIL_API.Get_Sld_Ptg_Less_3(a.kd_periode,b.kd_unit_penyetor),0) + NVL(CTM_API_UTIL_API.Get_Sld_Ptg_More_3(a.kd_periode,b.kd_unit_penyetor),0)) total_piutang, NVL(CTM_API_UTIL_API.Get_Sld_Ptg_Less_3(a.kd_periode,b.kd_unit_penyetor),0) piutang_krg_3_bln, NVL(CTM_API_UTIL_API.Get_Sld_Ptg_More_3(a.kd_periode,b.kd_unit_penyetor),0) piutang_lbh_3_bln, NVL(CTM_API_UTIL_API.Get_Jml_Bln_More_3(a.kd_periode,b.kd_unit_penyetor),0) jml_bln_piutang_lbh_3_bln FROM REKAP_IURAN_NORMAL a, DTL_REKAP_IUR_UNT_PENYETOR b WHERE a.kd_periode = '".$periode."' AND a.objstate = 'Disetujui'";
            
            $sql1 = oci_parse($conn, $query1);

            oci_execute($sql1);

            $rows = array();
            while ($dt = oci_fetch_assoc($sql1)) {
                $rows[] = $dt;
            }
        }

        if ($error_msg != '') {
            $status = "Failed! ".$error_msg;
        } else {
            $status = "Success!";
        }

        updateUserLog($log_id, $status);
        
        if (!$conn) {
            $e = oci_error();
            return $response->withJson(["data" => null, "error" => $e, "status" => "failed"], 200);
        } else {
            if ($error_msg != '') {
                return $response->withJson(["data" => null, "error" => $error_msg, "status" => "failed"], 200);
            } else {
                return $response->withJson(["data" => $rows, "error" => null, "status" => "success"], 200);
            }
        }
    }); //[INF_IURAN_03]

    //-------------------------------------------------------------------------------------//
    //---------------------------------// [INF_TANGGUH] //---------------------------------//
    //-------------------------------------------------------------------------------------//

    $app->get("/jmlh_pmp_tangguh/", function (ServerRequestInterface $request, ResponseInterface $response){
        $conn = getConnection();
        $jns_api = "Jumlah";
        
        $periode = $request->getHeaderLine('PERIODE');
        $api_id = "INF_TANGGUH_01";
        
        $ipAddress = $request->getAttribute('ip_address');
        $decoded = $request->getAttribute("decoded_jwt");
        $username = $decoded['user'];
        $ifs_user = $decoded['ifs_user'];

        $log_id = insertUserLog($username, $api_id, $ipAddress);

        $access = checkAccess($ifs_user, $api_id);

        $error_msg = "";
        $error_msg = string_type($periode, $error_msg, 'Periode', 'NotNull');

        if ($access == 'false') {
            $error_msg = "User ".$ifs_user." Does Not Have Access to This Action!";
        }
        /*
            $usia_tangguh = $request->getHeaderLine('USIA_TANGGUHAN');
            $saldo_awal = $request->getHeaderLine('SALDO_AWAL');
            $penambahan = $request->getHeaderLine('PENAMBAHAN');
            $sls_bln_brjln = $request->getHeaderLine('SELESAI_BLN_BERJALAN');
            $sls_periode_sblm = $request->getHeaderLine('SELESAI_PERIODE_SBLM');
            $total_penyelesaian = $request->getHeaderLine('TOTAL_PENYELESAIAN');
            $saldo_akhir = $request->getHeaderLine('SALDO_AKHIR');
        */ //parameter

        if ($error_msg == '') {
            $data['JMLH_PMP_TANGGUH'] = pmpTangguh($jns_api, $periode, "", "");
        }

        if ($error_msg != '') {
            $status = "Failed! ".$error_msg;
        } else {
            $status = "Success!";
        }

        updateUserLog($log_id, $status);
        
        if (!$conn) {
            $e = oci_error();
            return $response->withJson(["data" => null, "error" => $e, "status" => "failed"], 200);
        } else {
            if ($error_msg != '') {
                return $response->withJson(["data" => null, "error" => $error_msg, "status" => "failed"], 200);
            } else {
                return $response->withJson(["data" => $data, "error" => null, "status" => "success"], 200);
            }
        }
    }); //[INF_TANGGUH_01]

    $app->get("/list_pmp_tangguh/", function (ServerRequestInterface $request, ResponseInterface $response){
        $conn = getConnection();
        $jns_api = "List";
        
        $periode = $request->getHeaderLine('PERIODE');
        //$param_usia_tangguh = $request->getHeaderLine('PARAM_USIA_TANGGUHAN');
        $param_usia_tangguh = "";
        $param_tipe_tangguh = $request->getHeaderLine('PARAM_TIPE_TANGGUH');
        $api_id = "INF_TANGGUH_02";
        
        $ipAddress = $request->getAttribute('ip_address');
        $decoded = $request->getAttribute("decoded_jwt");
        $username = $decoded['user'];
        $ifs_user = $decoded['ifs_user'];

        $log_id = insertUserLog($username, $api_id, $ipAddress);

        $access = checkAccess($ifs_user, $api_id);

        $error_msg = "";
        $error_msg = string_type($periode, $error_msg, 'Periode', 'NotNull');
        //$error_msg = parameter_type($param_usia_tangguh, $error_msg, 'Parameter Usia Tangguh', 1, 10, 'NotNull');
        $error_msg = parameter_type($param_tipe_tangguh, $error_msg, 'Parameter Tipe Tangguh', 1, 6, 'NotNull');

        if ($access == 'false') {
            $error_msg = "User ".$ifs_user." Does Not Have Access to This Action!";
        }

        if ($error_msg == '') {
            $data = pmpTangguh($jns_api, $periode, $param_usia_tangguh, $param_tipe_tangguh);
        }

        if ($error_msg != '') {
            $status = "Failed! ".$error_msg;
        } else {
            $status = "Success!";
        }

        updateUserLog($log_id, $status);
        
        if (!$conn) {
            $e = oci_error();
            return $response->withJson(["data" => null, "error" => $e, "status" => "failed"], 200);
        } else {
            if ($error_msg != '') {
                return $response->withJson(["data" => null, "error" => $error_msg, "status" => "failed"], 200);
            } else {
                return $response->withJson(["data" => $data, "error" => null, "status" => "success"], 200);
            }
        }
    }); //[INF_TANGGUH_02]

    $app->get("/detail_pmp_tangguh/", function (ServerRequestInterface $request, ResponseInterface $response){
        $conn = getConnection();
        
        $nik = $request->getHeaderLine('NIK');
        $naw = $request->getHeaderLine('NO_AHLI_WARIS');
        $api_id = "INF_TANGGUH_03";
        
        $ipAddress = $request->getAttribute('ip_address');
        $decoded = $request->getAttribute("decoded_jwt");
        $username = $decoded['user'];
        $ifs_user = $decoded['ifs_user'];

        $log_id = insertUserLog($username, $api_id, $ipAddress);

        $access = checkAccess($ifs_user, $api_id);

        $error_msg = "";
        $error_msg = string_type($nik, $error_msg, 'NIK', 'NotNull');
        $error_msg = int_type($naw, $error_msg, 'No Ahli Waris', 'NotNull');

        if ($access == 'false') {
            $error_msg = "User ".$ifs_user." Does Not Have Access to This Action!";
        }

        if ($error_msg == '') {
            $query1 = "SELECT DISTINCT(a.n_i_k) n_i_k, a.no_ahli_waris, CALON_PENERIMA_MP_API.Get_Nama_Ahli_Waris(a.company, a.n_i_k, a.no_ahli_waris) nama, CALON_PENERIMA_MP_API.Get_Jenis_Kelamin2(a.company, a.n_i_k, a.no_ahli_waris) jenis_kelamin, CALON_PENERIMA_MP_API.Get_Agama(a.company, a.n_i_k, a.no_ahli_waris) agama, CALON_PENERIMA_MP_API.Get_Gol_Darah(a.company, a.n_i_k, a.no_ahli_waris) gol_darah, PENERIMA_MP_API.Get_Tanggal_Pensiun(a.company,a.n_i_k) tgl_pensiun, a.tgl_kerja, PESERTA_API.Get_Tgl_Capeg(a.company, a.n_i_k) tgl_capeg, PESERTA_API.Get_Tgl_Pegprus(a.company, a.n_i_k) tgl_pegprus, PESERTA_API.Get_Tgl_Berhenti(a.company, a.n_i_k) tgl_berhenti, PESERTA_API.Get_Kd_Band(a.company, a.n_i_k) kd_band, PESERTA_API.Get_Kd_Kelompok(a.company, a.n_i_k) kd_kelompok, KELOMPOK_PESERTA_API.Get_Nama_Kelompok(a.company, PESERTA_API.Get_Kd_Kelompok(a.company, a.n_i_k)) keterangan_kelompok, PESERTA_API.Get_Gadas_Pensiun(a.company, a.n_i_k) gadas_penisun, RIWAYAT_UNIT_KERJA_API.Get_Unit_Kerja_Akhir(a.company, a.n_i_k) kd_unit_kerja, UNIT_KERJA_API.Get_Nama_Unit_Kerja(a.company, RIWAYAT_UNIT_KERJA_API.Get_Unit_Kerja_Akhir(a.company, a.n_i_k)) nama_unit_kerja, CASE a.tgl_menikah WHEN NULL THEN 'Lajang' ELSE 'Menikah' END status_pernikahan, PESERTA_API.Get_N_I_K_Pasangan(a.company, a.n_i_k) nik_pasangan, PESERTA_API.Get_Nama_Pegawai(a.company, PESERTA_API.Get_N_I_K_Pasangan(a.company, a.n_i_k)) nama_pasangan, REKENING_PENERIMA_MP_API.Get_Kd_Bank_By_Param(a.company, a.n_i_k, a.no_ahli_waris, SYSDATE) kd_bank, PAYMENT_INSTITUTE_API.Get_Description(a.company, REKENING_PENERIMA_MP_API.Get_Kd_Bank_By_Param(a.company, a.n_i_k, a.no_ahli_waris, SYSDATE)) nama_bank, REKENING_PENERIMA_MP_API.Get_Kd_Cab_By_Param(a.company, a.n_i_k, a.no_ahli_waris, SYSDATE) kd_cabang, PAYMENT_INSTITUTE_OFFICE_API.Get_Description(a.company, REKENING_PENERIMA_MP_API.Get_Kd_Bank_By_Param(a.company, a.n_i_k, a.no_ahli_waris, SYSDATE), REKENING_PENERIMA_MP_API.Get_Kd_Cab_By_Param(a.company, a.n_i_k, a.no_ahli_waris, SYSDATE)) nama_cabang, REKENING_PENERIMA_MP_API.Get_No_Rek_By_Param(a.company, a.n_i_k, a.no_ahli_waris, SYSDATE) no_rekening_bank, REKENING_PENERIMA_MP_API.Get_Nama_Rek_By_Param(a.company, a.n_i_k, a.no_ahli_waris, SYSDATE) nama_pemilik_rekening/*,  no_sppb /**/ FROM DAFTAR_PST_TANGGUH a, PENERIMAAN_MP b WHERE a.company = b.company AND a.n_i_k = b.n_i_k AND a.no_ahli_waris = b.no_ahli_waris AND (a.state = 'Disetujui' OR a.state = 'Diproses') AND a.n_i_k = '".$nik."' AND a.no_ahli_waris = '".$naw."'";

            $conn = getConnection();
            
            $sql1 = oci_parse($conn, $query1);
            oci_execute($sql1);
            //$data1 = oci_fetch_array($sql1, OCI_ASSOC+OCI_RETURN_NULLS);
            
            $data = array();
            while ($dt = oci_fetch_assoc($sql1)) {
                $data[] = $dt;
            }
        }

        if ($error_msg != '') {
            $status = "Failed! ".$error_msg;
        } else {
            $status = "Success!";
        }

        updateUserLog($log_id, $status);
        
        if (!$conn) {
            $e = oci_error();
            return $response->withJson(["data" => null, "error" => $e, "status" => "failed"], 200);
        } else {
            if ($error_msg != '') {
                return $response->withJson(["data" => null, "error" => $error_msg, "status" => "failed"], 200);
            } else {
                return $response->withJson(["data" => $data, "error" => null, "status" => "success"], 200);
            }
        }
    }); //[INF_TANGGUH_03]

    $app->get("/jmlh_nilai_tangguh/", function (ServerRequestInterface $request, ResponseInterface $response){
        $conn = getConnection();
        
        $periode = $request->getHeaderLine('PERIODE');
        $api_id = "INF_TANGGUH_04";
        
        $ipAddress = $request->getAttribute('ip_address');
        $decoded = $request->getAttribute("decoded_jwt");
        $username = $decoded['user'];
        $ifs_user = $decoded['ifs_user'];

        $log_id = insertUserLog($username, $api_id, $ipAddress);

        $access = checkAccess($ifs_user, $api_id);

        $error_msg = "";
        $error_msg = string_type($periode, $error_msg, 'Periode', 'NotNull');

        if ($access == 'false') {
            $error_msg = "User ".$ifs_user." Does Not Have Access to This Action!";
        }

        if ($error_msg == '') {
            $query = "SELECT '".$periode."' periode, SUM(saldo_awal) nilai_saldo_awal, SUM(penambahan) nilai_penambahan, SUM(selesai_periode_sblm) nilai_selesai_periode_sblm, SUM(selesai_bln_berjalan) nilai_selesai_bln_berjalan, (SUM(selesai_periode_sblm)+SUM(selesai_bln_berjalan)) nilai_total_penyelesaian, (SUM(saldo_awal)+SUM(penambahan)-SUM(selesai_periode_sblm)-SUM(selesai_bln_berjalan)) nilai_saldo_akhir FROM (
            /* SALDO-AWAL */
            SELECT SUM(nominal_transfer) saldo_awal, 0 penambahan, 0 selesai_periode_sblm, 0 selesai_bln_berjalan FROM (SELECT a.n_i_k, a.no_ahli_waris, b.nominal_transfer FROM DAFTAR_PST_TANGGUH a, PENERIMAAN_MP b WHERE a.company = b.company AND a.n_i_k = b.n_i_k AND a.no_ahli_waris = b.no_ahli_waris AND a.periode_tangguh < '".$periode."' AND (a.periode_selesai IS NULL OR a.periode_selesai >= '".$periode."') AND b.kd_periode < '".$periode."' AND b.tangguh = 'TRUE' AND (a.state = 'Disetujui' OR a.state = 'Diproses') GROUP BY a.n_i_k, a.no_ahli_waris, b.nominal_transfer)
            /**/
            UNION ALL
            /* PENAMBAHAN */
            SELECT 0 saldo_awal, SUM(nominal_transfer) penambahan, 0 selesai_periode_sblm, 0 selesai_bln_berjalan FROM (SELECT a.n_i_k, a.no_ahli_waris, b.nominal_transfer FROM DAFTAR_PST_TANGGUH a, PENERIMAAN_MP b WHERE a.company = b.company AND a.n_i_k = b.n_i_k AND a.no_ahli_waris = b.no_ahli_waris AND (a.state = 'Disetujui' OR a.state = 'Diproses') /*AND a.periode_tangguh = */ AND b.kd_periode = '".$periode."' AND b.tangguh = 'TRUE' AND (PENERIMAAN_MP_API.Get_Tangguh(a.company,a.n_i_k,a.no_ahli_waris,PERIODE_API.Get_Prev_Period(b.kd_periode),b.kd_jenis_proses) <> 'TRUE' OR PENERIMAAN_MP_API.Get_Tangguh(a.company,a.n_i_k,a.no_ahli_waris,PERIODE_API.Get_Prev_Period(b.kd_periode),b.kd_jenis_proses) IS NULL) GROUP BY a.n_i_k, a.no_ahli_waris, b.nominal_transfer HAVING MIN(b.kd_periode) = '".$periode."')
            /**/
            UNION ALL
            /* SELESAI_PERIODE_SBLM */
            SELECT 0 saldo_awal, 0 penambahan, SUM(nominal_transfer) selesai_periode_sblm, 0 selesai_bln_berjalan FROM (SELECT a.n_i_k, a.no_ahli_waris, b.nominal_transfer FROM DAFTAR_PST_TANGGUH a, PENERIMAAN_MP b WHERE a.company = b.company AND a.n_i_k = b.n_i_k AND a.no_ahli_waris = b.no_ahli_waris /*AND a.periode_tangguh < */ AND a.periode_selesai = '".$periode."' AND b.kd_periode < '".$periode."' AND b.tangguh = 'TRUE' AND (a.state = 'Disetujui' OR a.state = 'Diproses') AND (PENERIMAAN_MP_API.Get_Tangguh(a.company,a.n_i_k,a.no_ahli_waris,PERIODE_API.Get_Prev_Period(b.kd_periode),b.kd_jenis_proses) = 'TRUE' OR PENERIMAAN_MP_API.Get_Tangguh(a.company,a.n_i_k,a.no_ahli_waris,PERIODE_API.Get_Prev_Period(b.kd_periode),b.kd_jenis_proses) IS NOT NULL) AND a.dibayarkan > 0 GROUP BY a.n_i_k, a.no_ahli_waris, b.nominal_transfer)
            /**/
            UNION ALL
            /* SELESAI_BLN_BERJALAN */
            SELECT 0 saldo_awal, 0 penambahan, 0 selesai_periode_sblm, SUM(nominal_transfer) selesai_bln_berjalan FROM (SELECT a.n_i_k, a.no_ahli_waris, b.nominal_transfer FROM DAFTAR_PST_TANGGUH a, PENERIMAAN_MP b WHERE a.company = b.company AND a.n_i_k = b.n_i_k AND a.no_ahli_waris = b.no_ahli_waris AND (a.state = 'Disetujui' OR a.state = 'Diproses') /*AND a.periode_tangguh = */ AND b.kd_periode = '".$periode."' AND b.tangguh = 'TRUE' AND a.periode_selesai = '".$periode."' AND (PENERIMAAN_MP_API.Get_Tangguh(a.company,a.n_i_k,a.no_ahli_waris,PERIODE_API.Get_Prev_Period(b.kd_periode),b.kd_jenis_proses) <> 'TRUE' OR PENERIMAAN_MP_API.Get_Tangguh(a.company,a.n_i_k,a.no_ahli_waris,PERIODE_API.Get_Prev_Period(b.kd_periode),b.kd_jenis_proses) IS NULL) AND a.dibayarkan > 0 GROUP BY a.n_i_k, a.no_ahli_waris, b.nominal_transfer HAVING MIN(b.kd_periode) = '".$periode."'))
            /**/";
        
            $sql = oci_parse($conn, $query);
            oci_execute($sql);
            
            $data = array();
            while ($dt = oci_fetch_assoc($sql)) {
                $data[] = $dt;
            }
        }

        if ($error_msg != '') {
            $status = "Failed! ".$error_msg;
        } else {
            $status = "Success!";
        }

        updateUserLog($log_id, $status);
        
        if (!$conn) {
            $e = oci_error();
            return $response->withJson(["data" => null, "error" => $e, "status" => "failed"], 200);
        } else {
            if ($error_msg != '') {
                return $response->withJson(["data" => null, "error" => $error_msg, "status" => "failed"], 200);
            } else {
                return $response->withJson(["data" => $data, "error" => null, "status" => "success"], 200);
            }
        }
    }); //[INF_TANGGUH_04]

    //--------------------------------------------------------------------------------------------//
    //------------------------------------// [INF_LEBIH_BYR] //-----------------------------------//
    //--------------------------------------------------------------------------------------------//

    $app->get("/jmlh_pmp_lbh_byr/", function (ServerRequestInterface $request, ResponseInterface $response, array $args) {
        $periode = $request->getHeaderLine('PERIODE');
        $periode_setuju = $request->getHeaderLine('PERIODE_DISETUJUI');
        $jenis_sbb_lbh_byr = $request->getHeaderLine('PENYEBAB_LEBIH_BAYAR');
        $cab_p2tel = $request->getHeaderLine('CAB_P2TEL');
        $bank_koor = $request->getHeaderLine('BANK_KOOR');
        $nama_bank = $request->getHeaderLine('NAMA_BANK');
        $api_id = "INF_LEBIH_BYR_01";
        
        $ipAddress = $request->getAttribute('ip_address');
        $decoded = $request->getAttribute("decoded_jwt");
        $username = $decoded['user'];
        $ifs_user = $decoded['ifs_user'];

        $log_id = insertUserLog($username, $api_id, $ipAddress);

        $jns_api = "jmlh_nilai";

        $conn = getConnection();

        $access = checkAccess($ifs_user, $api_id);

        $error_msg = "";
        $error_msg = string_type($periode, $error_msg, 'Periode', 'Nullable');
        $error_msg = string_type($periode_setuju, $error_msg, 'Periode Setuju', 'Nullable');
        $error_msg = parameter_type($jenis_sbb_lbh_byr, $error_msg, 'Jenis Sebab Lebih Bayar', 1, 3, 'Nullable');
        $error_msg = string_type($cab_p2tel, $error_msg, 'Cabang P2Tel', 'Nullable');
        $error_msg = parameter_type($bank_koor, $error_msg, 'Bank Koordinator', 1, 6, 'Nullable');
        $error_msg = string_type($nama_bank, $error_msg, 'Nama Bank', 'Nullable');

        if ($access == 'false') {
            $error_msg = "User ".$ifs_user." Does Not Have Access to This Action!";
        }

        if ($error_msg == '') {
            $data = jmlhPmpLbhByr($periode, $periode_setuju, $jenis_sbb_lbh_byr, $cab_p2tel, $bank_koor, $nama_bank, $jns_api);
        
            //$data['JMLH_PMP_LBH_BYR'] = $count;
        }

        if ($error_msg != '') {
            $status = "Failed! ".$error_msg;
        } else {
            $status = "Success!";
        }

        updateUserLog($log_id, $status);
        
        if (!$conn) {
            $e = oci_error();
            return $response->withJson(["data" => null, "error" => $e, "status" => "failed"], 200);
        } else {
            if ($periode == '' && $periode_setuju == '') {
                return $response->withJson(["data" => null, "error" => "Field [Periode] or [Periode Setuju] Cannot be Empty!", "status" => "failed"], 200);
            } elseif ($periode != '' && $periode_setuju != '') {
                return $response->withJson(["data" => null, "error" => "Fields [Periode] and [Periode Setuju] Cannot be Filled at the Same Time!", "status" => "failed"], 200);
            } elseif ($error_msg != '') {
                return $response->withJson(["data" => null, "error" => $error_msg, "status" => "failed"], 200);
            } else {
                return $response->withJson(["data" => $data, "error" => null, "status" => "success"], 200);
            }
        }
    }); //[INF_LEBIH_BYR_01]

    $app->get("/jmlh_pmp_lbh_byr_sbb_all/", function (ServerRequestInterface $request, ResponseInterface $response, array $args) {
        $periode_thn = $request->getHeaderLine('PERIODE_TAHUN');
        $parameter = $request->getHeaderLine('PARAMETER');
        $api_id = "INF_LEBIH_BYR_02";
        
        $ipAddress = $request->getAttribute('ip_address');
        $decoded = $request->getAttribute("decoded_jwt");
        $username = $decoded['user'];
        $ifs_user = $decoded['ifs_user'];

        $log_id = insertUserLog($username, $api_id, $ipAddress);

        $jns_api = "jns_sbb";

        $conn = getConnection();

        $access = checkAccess($ifs_user, $api_id);

        $error_msg = "";
        $error_msg = string_type($periode_thn, $error_msg, 'Periode Tahun', 'NotNull');
        $error_msg = parameter_type($parameter, $error_msg, 'Parameter', 1, 8, 'NotNull');

        if ($access == 'false') {
            $error_msg = "User ".$ifs_user." Does Not Have Access to This Action!";
        }

        if ($error_msg == '') {
            $data = jmlhPmpLbhByrAll($periode_thn, $parameter, $jns_api);
        }

        if ($error_msg != '') {
            $status = "Failed! ".$error_msg;
        } else {
            $status = "Success!";
        }

        updateUserLog($log_id, $status);
        
        if (!$conn) {
            $e = oci_error();
            return $response->withJson(["data" => null, "error" => $e, "status" => "failed"], 200);
        } else {
            if ($error_msg != '') {
                return $response->withJson(["data" => null, "error" => $error_msg, "status" => "failed"], 200);
            } else {
                return $response->withJson(["data" => $data, "error" => null, "status" => "success"], 200);
            }
        }
    }); //[INF_LEBIH_BYR_02]

    $app->get("/list_pmp_lbh_byr/", function (ServerRequestInterface $request, ResponseInterface $response, array $args) {
        $periode = $request->getHeaderLine('PERIODE');
        $periode_setuju = $request->getHeaderLine('PERIODE_DISETUJUI');
        $jenis_sbb_lbh_byr = $request->getHeaderLine('PENYEBAB_LEBIH_BAYAR');
        $cab_p2tel = $request->getHeaderLine('CAB_P2TEL');
        $bank_koor = $request->getHeaderLine('BANK_KOOR');
        $nama_bank = $request->getHeaderLine('NAMA_BANK');
        $api_id = "INF_LEBIH_BYR_03";
        
        $ipAddress = $request->getAttribute('ip_address');
        $decoded = $request->getAttribute("decoded_jwt");
        $username = $decoded['user'];
        $ifs_user = $decoded['ifs_user'];

        $log_id = insertUserLog($username, $api_id, $ipAddress);

        $jns_api = "list";

        $conn = getConnection();

        $access = checkAccess($ifs_user, $api_id);

        $error_msg = "";
        $error_msg = string_type($periode, $error_msg, 'Periode', 'Nullable');
        $error_msg = string_type($periode_setuju, $error_msg, 'Periode Setuju', 'Nullable');
        $error_msg = parameter_type($jenis_sbb_lbh_byr, $error_msg, 'Jenis Sebab Lebih Bayar', 1, 3, 'Nullable');
        $error_msg = string_type($cab_p2tel, $error_msg, 'Cabang P2Tel', 'Nullable');
        $error_msg = parameter_type($bank_koor, $error_msg, 'Bank Koordinator', 1, 6, 'Nullable');
        $error_msg = string_type($nama_bank, $error_msg, 'Nama Bank', 'Nullable');

        if ($access == 'false') {
            $error_msg = "User ".$ifs_user." Does Not Have Access to This Action!";
        }

        if ($error_msg == '') {
            $data = jmlhPmpLbhByr($periode, $periode_setuju, $jenis_sbb_lbh_byr, $cab_p2tel, $bank_koor, $nama_bank, $jns_api);

            //$data['JMLH_PMP_LBH_BYR'] = $count;
        }

        if ($error_msg != '') {
            $status = "Failed! ".$error_msg;
        } else {
            $status = "Success!";
        }

        updateUserLog($log_id, $status);
        
        if (!$conn) {
            $e = oci_error();
            return $response->withJson(["data" => null, "error" => $e, "status" => "failed"], 200);
        } else {
            if ($periode == '' && $periode_setuju == '') {
                return $response->withJson(["data" => null, "error" => "'Periode'/'Periode Setuju' Cannot be Empty!", "status" => "failed"], 200);
            } elseif ($error_msg != '') {
                return $response->withJson(["data" => null, "error" => $error_msg, "status" => "failed"], 200);
            } else {
                return $response->withJson(["data" => $data, "error" => null, "status" => "success"], 200);
            }
        }
    }); //[INF_LEBIH_BYR_03]

    $app->get("/detail_pmp_lbh_byr/", function (ServerRequestInterface $request, ResponseInterface $response, array $args) {
        //$periode = $request->getHeaderLine('PERIODE');
        $nik = $request->getHeaderLine('NIK');
        $no_ahli_waris = $request->getHeaderLine('NO_AHLI_WARIS');
        $api_id = "INF_LEBIH_BYR_04";
        
        $ipAddress = $request->getAttribute('ip_address');
        $decoded = $request->getAttribute("decoded_jwt");
        $username = $decoded['user'];
        $ifs_user = $decoded['ifs_user'];

        $log_id = insertUserLog($username, $api_id, $ipAddress);

        $conn = getConnection();

        $access = checkAccess($ifs_user, $api_id);

        $error_msg = "";
        $error_msg = string_type($nik, $error_msg, 'NIK', 'NotNull');
        $error_msg = int_type($no_ahli_waris, $error_msg, 'No Ahli Waris', 'NotNull');

        if ($access == 'false') {
            $error_msg = "User ".$ifs_user." Does Not Have Access to This Action!";
        }

        if ($error_msg == '') {
            $query1 = "SELECT n_i_k, PESERTA_API.Get_Nama_Pegawai(company,n_i_k) nama_pegawai, PESERTA_API.Get_Kd_Band(company,n_i_k) kd_band, PESERTA_API.Get_Kd_Kelompok(company,n_i_k) kd_kelompok, PESERTA_API.Get_N_I_K_Pasangan(company,n_i_k) nik_pasangan, PESERTA_API.Get_Nama_Pegawai(company,PESERTA_API.Get_N_I_K_Pasangan(company,n_i_k)) nama_pasangan, CALON_PENERIMA_MP_API.Get_Nama_Ahli_Waris(company,n_i_k,no_ahli_waris) nama_penerima, CALON_PENERIMA_MP_API.Get_Jenis_Penerima(company,n_i_k,no_ahli_waris) jenis_penerima, CALON_PENERIMA_MP_API.Get_Jenis_Kelamin(company,n_i_k,no_ahli_waris) jenis_kelamin, CASE WHEN CALON_PENERIMA_MP_API.Get_Tgl_Nikah(company,n_i_k,no_ahli_waris) IS NULL THEN 'Belum Menikah' ELSE 'Menikah' END status_pernikahan, ALAMAT_PENERIMA_MP_API.Get_Jalan(company,no_ahli_waris,n_i_k,ALAMAT_PENERIMA_MP_API.Get_No_Urut_Default(company,n_i_k,no_ahli_waris)) jalan, ALAMAT_PENERIMA_MP_API.Get_Blok(company,no_ahli_waris,n_i_k,ALAMAT_PENERIMA_MP_API.Get_No_Urut_Default(company,n_i_k,no_ahli_waris)) blok, ALAMAT_PENERIMA_MP_API.Get_No_Rumah(company,no_ahli_waris,n_i_k,ALAMAT_PENERIMA_MP_API.Get_No_Urut_Default(company,n_i_k,no_ahli_waris)) no_rumah, ALAMAT_PENERIMA_MP_API.Get_Komplek(company,no_ahli_waris,n_i_k,ALAMAT_PENERIMA_MP_API.Get_No_Urut_Default(company,n_i_k,no_ahli_waris)) komplek, ALAMAT_PENERIMA_MP_API.Get_Rt(company,no_ahli_waris,n_i_k,ALAMAT_PENERIMA_MP_API.Get_No_Urut_Default(company,n_i_k,no_ahli_waris)) rt, ALAMAT_PENERIMA_MP_API.Get_Rw(company,no_ahli_waris,n_i_k,ALAMAT_PENERIMA_MP_API.Get_No_Urut_Default(company,n_i_k,no_ahli_waris)) rw, KELURAHAN_API.Get_Nama_By_Id(ALAMAT_PENERIMA_MP_API.Get_Kelurahan_Id(company,no_ahli_waris,n_i_k,ALAMAT_PENERIMA_MP_API.Get_No_Urut_Default(company,n_i_k,no_ahli_waris))) kelurahan, KECAMATAN_API.Get_Nama_By_Id(ALAMAT_PENERIMA_MP_API.Get_Kecamatan_Id(company,no_ahli_waris,n_i_k,ALAMAT_PENERIMA_MP_API.Get_No_Urut_Default(company,n_i_k,no_ahli_waris))) kecamatan, KOTA_API.Get_Nama_By_Id(ALAMAT_PENERIMA_MP_API.Get_Kota_Id(company,no_ahli_waris,n_i_k,ALAMAT_PENERIMA_MP_API.Get_No_Urut_Default(company,n_i_k,no_ahli_waris))) kota, PROVINSI_API.Get_Nama_By_Id(ALAMAT_PENERIMA_MP_API.Get_Provinsi_Id(company,no_ahli_waris,n_i_k,ALAMAT_PENERIMA_MP_API.Get_No_Urut_Default(company,n_i_k,no_ahli_waris))) provinsi, ALAMAT_PENERIMA_MP_API.Get_Kode_Pos(company,no_ahli_waris,n_i_k,ALAMAT_PENERIMA_MP_API.Get_No_Urut_Default(company,n_i_k,no_ahli_waris)) kode_pos, KELURAHAN_API.Get_P2tel_Id2(ALAMAT_PENERIMA_MP_API.Get_Kelurahan_Id(company,no_ahli_waris,n_i_k,ALAMAT_PENERIMA_MP_API.Get_No_Urut_Default(company,n_i_k,no_ahli_waris))) cabang_p2tel, valid_from tgl_transaksi, DPT_AUDITTRAIL_API.Get_Tgl('HapusPenerimaMp',objid,'Disetujui') tgl_disetujui, tgl_meninggal, tgl_berhenti_sekolah tgl_berhenti_sekolah_pmp_anak, tgl_menikah tgl_menikah_pmp_anak, tgl_bekerja tgl_bekerja_pmp_anak, dewasa dewasa_pmp_anak, tgl_menikah_lagi tgl_nikah_lagi, tgl_berhenti FROM (
            SELECT a.n_i_k, a.no_ahli_waris, b.nominal_transfer, a.company, a.objid, a.tgl_meninggal, a.tgl_berhenti_sekolah, a.tgl_menikah, a.tgl_bekerja, a.dewasa, a.tgl_menikah_lagi, a.tgl_berhenti, a.valid_from FROM HAPUS_PENERIMA_MP a, PENERIMAAN_MP b WHERE a.company = b.company AND a.n_i_k = b.n_i_k AND a.no_ahli_waris = b.no_ahli_waris AND a.kd_periode = b.kd_periode AND b.kd_periode > PERIODE_API.Get_Kd_Periode(a.company,TO_CHAR(a.tgl_berhenti, 'YYYY'),TO_CHAR(a.tgl_berhenti, 'MM')) AND a.n_i_k = '".$nik."' AND a.no_ahli_waris = '".$no_ahli_waris."' GROUP BY a.n_i_k, a.no_ahli_waris, b.kd_periode, b.nominal_transfer, a.company, a.objid, a.tgl_meninggal, a.tgl_berhenti_sekolah, a.tgl_menikah, a.tgl_bekerja, a.dewasa, a.tgl_menikah_lagi, a.tgl_berhenti, a.valid_from ORDER BY a.n_i_k
            )";

            $kontak_query = "SELECT a.no_urut_kontak, JENIS_KONTAK_API.Get_Keterangan(a.kd_kontak) tipe_kontak, a.kd_kontak ref_tipe_kontak, a.data_kontak FROM KONTAK_PENERIMA_MP_TAB a WHERE a.n_i_k = '".$nik."' AND a.no_ahli_waris = '".$no_ahli_waris."'";

            $detail_query = "SELECT b.kd_periode, b.kd_jenis_proses, b.kd_jenis_manfaat, b.mpb, b.nominal, b.nominal_transfer jmlh_dibayarkan, CASE WHEN c.kd_bank = '002' THEN 'BRI' WHEN c.kd_bank = '008' THEN 'MANDIRI' WHEN c.kd_bank = '009' THEN 'BNI' WHEN c.kd_bank = '213' THEN 'BTPN' WHEN c.kd_bank NOT IN ('002','008','009','213','998','999') THEN 'BHS' WHEN c.kd_bank IN ('998','999') THEN 'TUNAI' END bank_koor, c.kd_bank, PAYMENT_INSTITUTE_API.Get_Description(c.company,c.institute_id) nama_bank, c.kd_cab, PAYMENT_INSTITUTE_OFFICE_API.Get_Description(c.company,c.institute_id,c.office_code) cabang_bank, c.no_rekening no_rekening_bank, c.nama_pemegang_rek nama_pemilik_rekening FROM HAPUS_PENERIMA_MP a, PENERIMAAN_MP b, REKENING_PENERIMA_MP c WHERE a.company = b.company AND a.n_i_k = b.n_i_k AND a.no_ahli_waris = b.no_ahli_waris AND b.company = c.company AND b.n_i_k = c.n_i_k AND b.no_ahli_waris = c.no_ahli_waris AND (PERIODE_API.Get_Tanggal_Awal(b.company,b.kd_periode) > a.tgl_meninggal OR PERIODE_API.Get_Tanggal_Awal(b.company,b.kd_periode) > a.tgl_berhenti_sekolah OR PERIODE_API.Get_Tanggal_Awal(b.company,b.kd_periode) > a.tgl_menikah OR PERIODE_API.Get_Tanggal_Awal(b.company,b.kd_periode) > a.tgl_bekerja OR PERIODE_API.Get_Tanggal_Awal(b.company,b.kd_periode) > a.tgl_menikah_lagi) AND PERIODE_API.Get_Tanggal_Awal(b.company,b.kd_periode) BETWEEN c.valid_from AND c.valid_to AND a.n_i_k = '".$nik."' AND a.no_ahli_waris = '".$no_ahli_waris."'";

            $sql1 = oci_parse($conn, $query1);
            $kontak_sql = oci_parse($conn, $kontak_query);
            $detail_sql = oci_parse($conn, $detail_query);

            oci_execute($sql1);
            oci_execute($kontak_sql);
            oci_execute($detail_sql);

            $rows = array();
            while ($dt = oci_fetch_assoc($sql1)) {
                $rows[] = $dt;
            }

            $kntk_rows = array();
            while ($kntk = oci_fetch_assoc($kontak_sql)) {
                $kntk_rows[] = $kntk;
            }

            $detail_rows = array();
            while ($detail = oci_fetch_assoc($detail_sql)) {
                $detail_rows[] = $detail;
            }

            $data['DATA_DIRI'] = $rows;
            $data['KONTAK'] = $kntk_rows;
            $data['DETAIL_PMP'] = $detail_rows;
        }

        if ($error_msg != '') {
            $status = "Failed! ".$error_msg;
        } else {
            $status = "Success!";
        }

        updateUserLog($log_id, $status);
        
        if (!$conn) {
            $e = oci_error();
            return $response->withJson(["data" => null, "error" => $e, "status" => "failed"], 200);
        } else {
            if ($error_msg != '') {
                return $response->withJson(["data" => null, "error" => $error_msg, "status" => "failed"], 200);
            } else {
                return $response->withJson(["data" => $data, "error" => null, "status" => "success"], 200);
            }
        }
    }); //[INF_LEBIH_BYR_04]

    $app->get("/jmlh_pmp_lbh_byr_jns_all/", function (ServerRequestInterface $request, ResponseInterface $response, array $args) {
        $periode_thn = $request->getHeaderLine('PERIODE_TAHUN');
        $parameter = $request->getHeaderLine('PARAMETER');
        $api_id = "INF_LEBIH_BYR_05";
        
        $ipAddress = $request->getAttribute('ip_address');
        $decoded = $request->getAttribute("decoded_jwt");
        $username = $decoded['user'];
        $ifs_user = $decoded['ifs_user'];

        $log_id = insertUserLog($username, $api_id, $ipAddress);

        $jns_api = "jns_penerima";

        $conn = getConnection();

        $access = checkAccess($ifs_user, $api_id);

        $error_msg = "";
        $error_msg = string_type($periode_thn, $error_msg, 'Periode Tahun', 'NotNull');
        $error_msg = parameter_type($parameter, $error_msg, 'Parameter', 1, 8, 'NotNull');

        if ($access == 'false') {
            $error_msg = "User ".$ifs_user." Does Not Have Access to This Action!";
        }

        if ($error_msg == '') {
            $data = jmlhPmpLbhByrAll($periode_thn, $parameter, $jns_api);
        }

        if ($error_msg != '') {
            $status = "Failed! ".$error_msg;
        } else {
            $status = "Success!";
        }

        updateUserLog($log_id, $status);
        
        if (!$conn) {
            $e = oci_error();
            return $response->withJson(["data" => null, "error" => $e, "status" => "failed"], 200);
        } else {
            if ($error_msg != '') {
                return $response->withJson(["data" => null, "error" => $error_msg, "status" => "failed"], 200);
            } else {
                return $response->withJson(["data" => $data, "error" => null, "status" => "success"], 200);
            }
        }
    }); //[INF_LEBIH_BYR_05]

    $app->get("/jmlh_pmp_lbh_byr_bank_all/", function (ServerRequestInterface $request, ResponseInterface $response, array $args) {
        $periode_thn = $request->getHeaderLine('PERIODE_TAHUN');
        $parameter = $request->getHeaderLine('PARAMETER');
        $api_id = "INF_LEBIH_BYR_06";
        
        $ipAddress = $request->getAttribute('ip_address');
        $decoded = $request->getAttribute("decoded_jwt");
        $username = $decoded['user'];
        $ifs_user = $decoded['ifs_user'];

        $log_id = insertUserLog($username, $api_id, $ipAddress);

        $jns_api = "jns_bank";

        $conn = getConnection();

        $access = checkAccess($ifs_user, $api_id);

        $error_msg = "";
        $error_msg = string_type($periode_thn, $error_msg, 'Periode Tahun', 'NotNull');
        $error_msg = parameter_type($parameter, $error_msg, 'Parameter', 1, 8, 'NotNull');

        if ($access == 'false') {
            $error_msg = "User ".$ifs_user." Does Not Have Access to This Action!";
        }

        if ($error_msg == '') {
            $data = jmlhPmpLbhByrAll($periode_thn, $parameter, $jns_api);
        }

        if ($error_msg != '') {
            $status = "Failed! ".$error_msg;
        } else {
            $status = "Success!";
        }

        updateUserLog($log_id, $status);
        
        if (!$conn) {
            $e = oci_error();
            return $response->withJson(["data" => null, "error" => $e, "status" => "failed"], 200);
        } else {
            if ($error_msg != '') {
                return $response->withJson(["data" => null, "error" => $error_msg, "status" => "failed"], 200);
            } else {
                return $response->withJson(["data" => $data, "error" => null, "status" => "success"], 200);
            }
        }
    }); //[INF_LEBIH_BYR_06]

    $app->get("/rekap_lbh_byr_per_periode/", function (ServerRequestInterface $request, ResponseInterface $response, array $args) {
        $range_tgl_transaksi = $request->getHeaderLine('RANGE_TGL_TRANSAKSI');
        $range_tgl_setuju = $request->getHeaderLine('RANGE_TGL_DISETUJUI');
        $api_id = "INF_LEBIH_BYR_07";
        
        $ipAddress = $request->getAttribute('ip_address');
        $decoded = $request->getAttribute("decoded_jwt");
        $username = $decoded['user'];
        $ifs_user = $decoded['ifs_user'];

        $log_id = insertUserLog($username, $api_id, $ipAddress);

        $conn = getConnection();

        $access = checkAccess($ifs_user, $api_id);

        $error_msg = "";
        $error_msg = string_type($range_tgl_transaksi, $error_msg, 'Range Tanggal Transaksi', 'Nullable');
        $error_msg = string_type($range_tgl_setuju, $error_msg, 'Range Tanggal Setuju', 'Nullable');

        if ($access == 'false') {
            $error_msg = "User ".$ifs_user." Does Not Have Access to This Action!";
        }

        if ($error_msg == '') {
            $where_query = "";
            
            if ($range_tgl_transaksi != '') {
                $tgl_transaksi = explode("..", $range_tgl_transaksi);
                $where_query = $where_query." AND a.state NOT IN ('BatalHapus','Dibatalkan') AND (a.valid_from BETWEEN TO_DATE('".$tgl_transaksi[0]."','MM/DD/YYYY') AND TO_DATE('".$tgl_transaksi[1]."','MM/DD/YYYY'))";
            }
            if ($range_tgl_setuju != '') {
                $tgl_setuju = explode("..", $range_tgl_setuju);
                $where_query = $where_query." AND INITCAP(a.state) = 'Disetujui' AND (DPT_AUDITTRAIL_API.Get_Tgl('HapusPenerimaMp',a.objid,'Disetujui') BETWEEN TO_DATE('".$tgl_setuju[0]."','MM/DD/YYYY') AND TO_DATE('".$tgl_setuju[1]."','MM/DD/YYYY'))";
            }

            $query1 = "SELECT n_i_k, PESERTA_API.Get_Nama_Pegawai(company, n_i_k) nama_pegawai, CALON_PENERIMA_MP_API.Get_Nama_Ahli_Waris(company,n_i_k,no_ahli_waris) nama_penerima, CALON_PENERIMA_MP_API.Get_Jenis_Penerima(company,n_i_k,no_ahli_waris) jenis_penerima, tgl_meninggal, tgl_berhenti_sekolah tgl_berhenti_sekolah_pmp_anak, tgl_menikah tgl_menikah_pmp_anak, tgl_bekerja tgl_bekerja_pmp_anak, dewasa dewasa_pmp_anak, tgl_menikah_lagi tgl_nikah_lagi, tgl_berhenti, BANK_KOORDINATOR_API.Get_Keterangan(company,REKENING_PENERIMA_MP_API.Get_Kd_Bank_By_Param(company,n_i_k,no_ahli_waris,PERIODE_API.Get_Tanggal_Awal_Periode(company,kd_periode))) BANK_KOOR, PAYMENT_INSTITUTE_API.Get_Description(company,REKENING_PENERIMA_MP_API.Get_Kd_Bank_By_Param(company,n_i_k,no_ahli_waris,PERIODE_API.Get_Tanggal_Awal_Periode(company,kd_periode))) NAMA_BANK, PAYMENT_INSTITUTE_OFFICE_API.Get_Description(company,REKENING_PENERIMA_MP_API.Get_Kd_Bank_By_Param(company,n_i_k,no_ahli_waris,PERIODE_API.Get_Tanggal_Awal_Periode(company,kd_periode)),REKENING_PENERIMA_MP_API.Get_Kd_Cab_By_Param(company,n_i_k,no_ahli_waris,PERIODE_API.Get_Tanggal_Awal_Periode(company,kd_periode))) CABANG_BANK, REKENING_PENERIMA_MP_API.Get_No_Rek_By_Param(company,n_i_k,no_ahli_waris,PERIODE_API.Get_Tanggal_Awal_Periode(company,kd_periode)) NO_REKENING, REKENING_PENERIMA_MP_API.Get_Nama_Rek_By_Param(company,n_i_k,no_ahli_waris,PERIODE_API.Get_Tanggal_Awal_Periode(company,kd_periode)) NAMA_REKENING, nominal_transfer JMLH_KELEBIHAN_BAYAR FROM (
            SELECT a.n_i_k, a.no_ahli_waris, b.kd_periode, b.nominal_transfer, a.company, a.objid, a.tgl_meninggal, a.tgl_berhenti_sekolah, a.tgl_menikah, a.tgl_bekerja, a.dewasa, a.tgl_menikah_lagi, a.tgl_berhenti, a.valid_from FROM HAPUS_PENERIMA_MP a, PENERIMAAN_MP b WHERE a.company = b.company AND a.n_i_k = b.n_i_k AND a.no_ahli_waris = b.no_ahli_waris AND a.kd_periode = b.kd_periode AND b.kd_periode > PERIODE_API.Get_Kd_Periode(a.company,TO_CHAR(a.tgl_berhenti, 'YYYY'),TO_CHAR(a.tgl_berhenti, 'MM')) $where_query GROUP BY a.n_i_k, a.no_ahli_waris, b.kd_periode, b.nominal_transfer, a.company, a.objid, a.tgl_meninggal, a.tgl_berhenti_sekolah, a.tgl_menikah, a.tgl_bekerja, a.dewasa, a.tgl_menikah_lagi, a.tgl_berhenti, a.valid_from ORDER BY a.n_i_k
            )";
            
            $sql1 = oci_parse($conn, $query1);
            oci_execute($sql1);
            
            $data = array();
            while ($dt = oci_fetch_assoc($sql1)) {
                $data[] = $dt;
            }
        }

        if ($error_msg != '') {
            $status = "Failed! ".$error_msg;
        } else {
            $status = "Success!";
        }

        updateUserLog($log_id, $status);
        
        if (!$conn) {
            $e = oci_error();
            return $response->withJson(["data" => null, "error" => $e, "status" => "failed"], 200);
        } else {
            if ($error_msg != '') {
                return $response->withJson(["data" => null, "error" => $error_msg, "status" => "failed"], 200);
            } else {
                return $response->withJson(["data" => $data, "error" => null, "status" => "success"], 200);
            }
        }
    }); //[INF_LEBIH_BYR_07]

    $app->get("/detail_lbh_byr_per_bank/", function (ServerRequestInterface $request, ResponseInterface $response, array $args) {
        $range_tgl_transaksi = $request->getHeaderLine('RANGE_TGL_TRANSAKSI');
        $range_tgl_setuju = $request->getHeaderLine('RANGE_TGL_DISETUJUI');
        $bank_koor = $request->getHeaderLine('BANK_KOOR');
        $api_id = "INF_LEBIH_BYR_08";
        
        $ipAddress = $request->getAttribute('ip_address');
        $decoded = $request->getAttribute("decoded_jwt");
        $username = $decoded['user'];
        $ifs_user = $decoded['ifs_user'];

        $log_id = insertUserLog($username, $api_id, $ipAddress);

        $conn = getConnection();

        $access = checkAccess($ifs_user, $api_id);

        $error_msg = "";
        $error_msg = string_type($range_tgl_transaksi, $error_msg, 'Range Tanggal Transaksi', 'Nullable');
        $error_msg = string_type($range_tgl_setuju, $error_msg, 'Range Tanggal Setuju', 'Nullable');
        $error_msg = parameter_type($bank_koor, $error_msg, 'Bank Koordinator', 1, 6, 'Nullable');

        if ($access == 'false') {
            $error_msg = "User ".$ifs_user." Does Not Have Access to This Action!";
        }

        if ($error_msg == '') {
            $where_query = "";
            
            if ($range_tgl_transaksi != '') {
                $tgl_transaksi = explode("..", $range_tgl_transaksi);
                $where_query = $where_query." AND a.state NOT IN ('BatalHapus','Dibatalkan') AND (a.valid_from BETWEEN TO_DATE('".$tgl_transaksi[0]."','MM/DD/YYYY') AND TO_DATE('".$tgl_transaksi[1]."','MM/DD/YYYY'))";
            }
            if ($range_tgl_setuju) {
                $tgl_setuju = explode("..", $range_tgl_setuju);
                $where_query = $where_query." AND INITCAP(a.state) = 'Disetujui' AND (DPT_AUDITTRAIL_API.Get_Tgl('HapusPenerimaMp',a.objid,'Disetujui') BETWEEN TO_DATE('".$tgl_setuju[0]."','MM/DD/YYYY') AND TO_DATE('".$tgl_setuju[1]."','MM/DD/YYYY'))";
            }
            if ($bank_koor != '') {
                if ($bank_koor == '1') { //BRI
                    $where_query = $where_query." AND REKENING_PENERIMA_MP_API.Get_Kd_Bank_By_Param(a.company,a.n_i_k,a.no_ahli_waris,PERIODE_API.Get_Tanggal_Awal_Periode(a.company,a.kd_periode)) = '002'";
                }
                elseif ($bank_koor == '2') { //MANDIRI
                    $where_query = $where_query." AND REKENING_PENERIMA_MP_API.Get_Kd_Bank_By_Param(a.company,a.n_i_k,a.no_ahli_waris,PERIODE_API.Get_Tanggal_Awal_Periode(a.company,a.kd_periode)) = '008'";
                }
                elseif ($bank_koor == '3') { //BNI
                    $where_query = $where_query." AND REKENING_PENERIMA_MP_API.Get_Kd_Bank_By_Param(a.company,a.n_i_k,a.no_ahli_waris,PERIODE_API.Get_Tanggal_Awal_Periode(a.company,a.kd_periode)) = '009'";
                }
                elseif ($bank_koor == '4') { //BTPN
                    $where_query = $where_query." AND REKENING_PENERIMA_MP_API.Get_Kd_Bank_By_Param(a.company,a.n_i_k,a.no_ahli_waris,PERIODE_API.Get_Tanggal_Awal_Periode(a.company,a.kd_periode)) = '213'";
                }
                elseif ($bank_koor == '5') { // BHS
                    $where_query = $where_query." AND REKENING_PENERIMA_MP_API.Get_Kd_Bank_By_Param(a.company,a.n_i_k,a.no_ahli_waris,PERIODE_API.Get_Tanggal_Awal_Periode(a.company,a.kd_periode)) NOT IN ('002','008','009','213','998','999')";
                }
                elseif ($bank_koor == '6') { // TUNAI
                    $where_query = $where_query." AND REKENING_PENERIMA_MP_API.Get_Kd_Bank_By_Param(a.company,a.n_i_k,a.no_ahli_waris,PERIODE_API.Get_Tanggal_Awal_Periode(a.company,a.kd_periode)) IN ('998','999')";
                }
            }

            $query1 = "SELECT n_i_k, PESERTA_API.Get_Nama_Pegawai(company, n_i_k) nama_pegawai, CALON_PENERIMA_MP_API.Get_Nama_Ahli_Waris(company,n_i_k,no_ahli_waris) nama_penerima, CALON_PENERIMA_MP_API.Get_Jenis_Penerima(company,n_i_k,no_ahli_waris) jenis_penerima, tgl_meninggal, tgl_berhenti_sekolah tgl_berhenti_sekolah_pmp_anak, tgl_menikah tgl_menikah_pmp_anak, tgl_bekerja tgl_bekerja_pmp_anak, dewasa dewasa_pmp_anak, tgl_menikah_lagi tgl_nikah_lagi, tgl_berhenti, BANK_KOORDINATOR_API.Get_Keterangan(company,REKENING_PENERIMA_MP_API.Get_Kd_Bank_By_Param(company,n_i_k,no_ahli_waris,PERIODE_API.Get_Tanggal_Awal_Periode(company,kd_periode))) BANK_KOOR, PAYMENT_INSTITUTE_API.Get_Description(company,REKENING_PENERIMA_MP_API.Get_Kd_Bank_By_Param(company,n_i_k,no_ahli_waris,PERIODE_API.Get_Tanggal_Awal_Periode(company,kd_periode))) NAMA_BANK, PAYMENT_INSTITUTE_OFFICE_API.Get_Description(company,REKENING_PENERIMA_MP_API.Get_Kd_Bank_By_Param(company,n_i_k,no_ahli_waris,PERIODE_API.Get_Tanggal_Awal_Periode(company,kd_periode)),REKENING_PENERIMA_MP_API.Get_Kd_Cab_By_Param(company,n_i_k,no_ahli_waris,PERIODE_API.Get_Tanggal_Awal_Periode(company,kd_periode))) CABANG_BANK, REKENING_PENERIMA_MP_API.Get_No_Rek_By_Param(company,n_i_k,no_ahli_waris,PERIODE_API.Get_Tanggal_Awal_Periode(company,kd_periode)) NO_REKENING, REKENING_PENERIMA_MP_API.Get_Nama_Rek_By_Param(company,n_i_k,no_ahli_waris,PERIODE_API.Get_Tanggal_Awal_Periode(company,kd_periode)) NAMA_REKENING, nominal_transfer JMLH_KELEBIHAN_BAYAR FROM (
            SELECT a.n_i_k, a.no_ahli_waris, b.kd_periode, b.nominal_transfer, a.company, a.objid, a.tgl_meninggal, a.tgl_berhenti_sekolah, a.tgl_menikah, a.tgl_bekerja, a.dewasa, a.tgl_menikah_lagi, a.tgl_berhenti, a.valid_from FROM HAPUS_PENERIMA_MP a, PENERIMAAN_MP b WHERE a.company = b.company AND a.n_i_k = b.n_i_k AND a.no_ahli_waris = b.no_ahli_waris AND a.kd_periode = b.kd_periode AND b.kd_periode > PERIODE_API.Get_Kd_Periode(a.company,TO_CHAR(a.tgl_berhenti, 'YYYY'),TO_CHAR(a.tgl_berhenti, 'MM')) $where_query GROUP BY a.n_i_k, a.no_ahli_waris, b.kd_periode, b.nominal_transfer, a.company, a.objid, a.tgl_meninggal, a.tgl_berhenti_sekolah, a.tgl_menikah, a.tgl_bekerja, a.dewasa, a.tgl_menikah_lagi, a.tgl_berhenti, a.valid_from ORDER BY a.n_i_k
            )";
            
            $sql1 = oci_parse($conn, $query1);
            oci_execute($sql1);
            
            $data = array();
            while ($dt = oci_fetch_assoc($sql1)) {
                $data[] = $dt;
            }
        }

        if ($error_msg != '') {
            $status = "Failed! ".$error_msg;
        } else {
            $status = "Success!";
        }

        updateUserLog($log_id, $status);
        
        if (!$conn) {
            $e = oci_error();
            return $response->withJson(["data" => null, "error" => $e, "status" => "failed"], 200);
        } else {
            if ($error_msg != '') {
                return $response->withJson(["data" => null, "error" => $error_msg, "status" => "failed"], 200);
            } else {
                return $response->withJson(["data" => $data, "error" => null, "status" => "success"], 200);
            }
        }
    }); //[INF_LEBIH_BYR_08]

    $app->get("/detail_lbh_byr_per_periode/", function (ServerRequestInterface $request, ResponseInterface $response, array $args) {
        $periode = $request->getHeaderLine('PERIODE');
        $range_tgl_transaksi = $request->getHeaderLine('RANGE_TGL_TRANSAKSI');
        $range_tgl_setuju = $request->getHeaderLine('RANGE_TGL_DISETUJUI');
        $api_id = "INF_LEBIH_BYR_09";
        
        $ipAddress = $request->getAttribute('ip_address');
        $decoded = $request->getAttribute("decoded_jwt");
        $username = $decoded['user'];
        $ifs_user = $decoded['ifs_user'];

        $log_id = insertUserLog($username, $api_id, $ipAddress);

        $conn = getConnection();

        $access = checkAccess($ifs_user, $api_id);

        $error_msg = "";
        $error_msg = string_type($periode, $error_msg, 'Periode', 'Nullable');
        $error_msg = string_type($range_tgl_transaksi, $error_msg, 'Range Tanggal Transaksi', 'Nullable');
        $error_msg = string_type($range_tgl_setuju, $error_msg, 'Range Tanggal Setuju', 'Nullable');

        if ($access == 'false') {
            $error_msg = "User ".$ifs_user." Does Not Have Access to This Action!";
        }

        if ($error_msg == '') {
            $where_query = "";
            $nik_array = array();
            
            if ($periode != '') {
                $where_query = $where_query." AND a.state NOT IN ('BatalHapus','Dibatalkan') AND b.kd_periode = '".$periode."'";
            }
            if ($range_tgl_transaksi != '') {
                $tgl_transaksi = explode("..", $range_tgl_transaksi);
                $where_query = $where_query." AND a.state NOT IN ('BatalHapus','Dibatalkan') AND (a.valid_from BETWEEN TO_DATE('".$tgl_transaksi[0]."','MM/DD/YYYY') AND TO_DATE('".$tgl_transaksi[1]."','MM/DD/YYYY'))";
            }
            if ($range_tgl_setuju) {
                $tgl_setuju = explode("..", $range_tgl_setuju);
                $where_query = $where_query." AND INITCAP(a.state) = 'Disetujui' AND (DPT_AUDITTRAIL_API.Get_Tgl('HapusPenerimaMp',a.objid,'Disetujui') BETWEEN TO_DATE('".$tgl_setuju[0]."','MM/DD/YYYY') AND TO_DATE('".$tgl_setuju[1]."','MM/DD/YYYY'))";
            }

            $nik_query = "SELECT n_i_k, no_ahli_waris FROM (
            SELECT a.n_i_k, a.no_ahli_waris, b.nominal_transfer, a.company, a.objid, a.tgl_meninggal, a.tgl_berhenti_sekolah, a.tgl_menikah, a.tgl_bekerja, a.dewasa, a.tgl_menikah_lagi, a.tgl_berhenti, a.valid_from FROM HAPUS_PENERIMA_MP a, PENERIMAAN_MP b WHERE a.company = b.company AND a.n_i_k = b.n_i_k AND a.no_ahli_waris = b.no_ahli_waris AND a.kd_periode = b.kd_periode AND b.kd_periode > PERIODE_API.Get_Kd_Periode(a.company,TO_CHAR(a.tgl_berhenti, 'YYYY'),TO_CHAR(a.tgl_berhenti, 'MM')) $where_query GROUP BY a.n_i_k, a.no_ahli_waris, b.kd_periode, b.nominal_transfer, a.company, a.objid, a.tgl_meninggal, a.tgl_berhenti_sekolah, a.tgl_menikah, a.tgl_bekerja, a.dewasa, a.tgl_menikah_lagi, a.tgl_berhenti, a.valid_from ORDER BY a.n_i_k
            )";

            $nik_sql = oci_parse($conn, $nik_query);

            oci_execute($nik_sql);

            $nik_rows = array();
            while ($nik_dt = oci_fetch_assoc($nik_sql)) {
                $nik_rows[] = $nik_dt;
            }

            for ($i = 0; $i < oci_num_rows($nik_sql); $i++) {
                $query1 = "SELECT n_i_k, PESERTA_API.Get_Nama_Pegawai(company,n_i_k) nama_pegawai, PESERTA_API.Get_Kd_Band(company,n_i_k) kd_band, PESERTA_API.Get_Kd_Kelompok(company,n_i_k) kd_kelompok, PESERTA_API.Get_N_I_K_Pasangan(company,n_i_k) nik_pasangan, PESERTA_API.Get_Nama_Pegawai(company,PESERTA_API.Get_N_I_K_Pasangan(company,n_i_k)) nama_pasangan, CALON_PENERIMA_MP_API.Get_Nama_Ahli_Waris(company,n_i_k,no_ahli_waris) nama_penerima, CALON_PENERIMA_MP_API.Get_Jenis_Penerima(company,n_i_k,no_ahli_waris) jenis_penerima, CALON_PENERIMA_MP_API.Get_Jenis_Kelamin(company,n_i_k,no_ahli_waris) jenis_kelamin, CASE WHEN CALON_PENERIMA_MP_API.Get_Tgl_Nikah(company,n_i_k,no_ahli_waris) IS NULL THEN 'Belum Menikah' ELSE 'Menikah' END status_pernikahan, ALAMAT_PENERIMA_MP_API.Get_Jalan(company,no_ahli_waris,n_i_k,ALAMAT_PENERIMA_MP_API.Get_No_Urut_Default(company,n_i_k,no_ahli_waris)) jalan, ALAMAT_PENERIMA_MP_API.Get_Blok(company,no_ahli_waris,n_i_k,ALAMAT_PENERIMA_MP_API.Get_No_Urut_Default(company,n_i_k,no_ahli_waris)) blok, ALAMAT_PENERIMA_MP_API.Get_No_Rumah(company,no_ahli_waris,n_i_k,ALAMAT_PENERIMA_MP_API.Get_No_Urut_Default(company,n_i_k,no_ahli_waris)) no_rumah, ALAMAT_PENERIMA_MP_API.Get_Komplek(company,no_ahli_waris,n_i_k,ALAMAT_PENERIMA_MP_API.Get_No_Urut_Default(company,n_i_k,no_ahli_waris)) komplek, ALAMAT_PENERIMA_MP_API.Get_Rt(company,no_ahli_waris,n_i_k,ALAMAT_PENERIMA_MP_API.Get_No_Urut_Default(company,n_i_k,no_ahli_waris)) rt, ALAMAT_PENERIMA_MP_API.Get_Rw(company,no_ahli_waris,n_i_k,ALAMAT_PENERIMA_MP_API.Get_No_Urut_Default(company,n_i_k,no_ahli_waris)) rw, KELURAHAN_API.Get_Nama_By_Id(ALAMAT_PENERIMA_MP_API.Get_Kelurahan_Id(company,no_ahli_waris,n_i_k,ALAMAT_PENERIMA_MP_API.Get_No_Urut_Default(company,n_i_k,no_ahli_waris))) kelurahan, KECAMATAN_API.Get_Nama_By_Id(ALAMAT_PENERIMA_MP_API.Get_Kecamatan_Id(company,no_ahli_waris,n_i_k,ALAMAT_PENERIMA_MP_API.Get_No_Urut_Default(company,n_i_k,no_ahli_waris))) kecamatan, KOTA_API.Get_Nama_By_Id(ALAMAT_PENERIMA_MP_API.Get_Kota_Id(company,no_ahli_waris,n_i_k,ALAMAT_PENERIMA_MP_API.Get_No_Urut_Default(company,n_i_k,no_ahli_waris))) kota, PROVINSI_API.Get_Nama_By_Id(ALAMAT_PENERIMA_MP_API.Get_Provinsi_Id(company,no_ahli_waris,n_i_k,ALAMAT_PENERIMA_MP_API.Get_No_Urut_Default(company,n_i_k,no_ahli_waris))) provinsi, ALAMAT_PENERIMA_MP_API.Get_Kode_Pos(company,no_ahli_waris,n_i_k,ALAMAT_PENERIMA_MP_API.Get_No_Urut_Default(company,n_i_k,no_ahli_waris)) kode_pos, KELURAHAN_API.Get_P2tel_Id2(ALAMAT_PENERIMA_MP_API.Get_Kelurahan_Id(company,no_ahli_waris,n_i_k,ALAMAT_PENERIMA_MP_API.Get_No_Urut_Default(company,n_i_k,no_ahli_waris))) cabang_p2tel, valid_from tgl_transaksi, DPT_AUDITTRAIL_API.Get_Tgl('HapusPenerimaMp',objid,'Disetujui') tgl_disetujui, tgl_meninggal, tgl_berhenti_sekolah tgl_berhenti_sekolah_pmp_anak, tgl_menikah tgl_menikah_pmp_anak, tgl_bekerja tgl_bekerja_pmp_anak, dewasa dewasa_pmp_anak, tgl_menikah_lagi tgl_nikah_lagi, tgl_berhenti FROM (
                SELECT a.n_i_k, a.no_ahli_waris, b.nominal_transfer, a.company, a.objid, a.tgl_meninggal, a.tgl_berhenti_sekolah, a.tgl_menikah, a.tgl_bekerja, a.dewasa, a.tgl_menikah_lagi, a.tgl_berhenti, a.valid_from FROM HAPUS_PENERIMA_MP a, PENERIMAAN_MP b WHERE a.company = b.company AND a.n_i_k = b.n_i_k AND a.no_ahli_waris = b.no_ahli_waris AND a.kd_periode = b.kd_periode AND b.kd_periode > PERIODE_API.Get_Kd_Periode(a.company,TO_CHAR(a.tgl_berhenti, 'YYYY'),TO_CHAR(a.tgl_berhenti, 'MM')) AND a.n_i_k = '".$nik_rows[$i]['N_I_K']."' AND a.no_ahli_waris = '".$nik_rows[$i]['NO_AHLI_WARIS']."' GROUP BY a.n_i_k, a.no_ahli_waris, b.kd_periode, b.nominal_transfer, a.company, a.objid, a.tgl_meninggal, a.tgl_berhenti_sekolah, a.tgl_menikah, a.tgl_bekerja, a.dewasa, a.tgl_menikah_lagi, a.tgl_berhenti, a.valid_from ORDER BY a.n_i_k
                )";

                $kontak_query = "SELECT a.no_urut_kontak, JENIS_KONTAK_API.Get_Keterangan(a.kd_kontak) tipe_kontak, a.kd_kontak ref_tipe_kontak, a.data_kontak FROM KONTAK_PENERIMA_MP_TAB a WHERE a.n_i_k = '".$nik_rows[$i]['N_I_K']."' AND a.no_ahli_waris = '".$nik_rows[$i]['NO_AHLI_WARIS']."'";

                $detail_query = "SELECT b.kd_periode, b.kd_jenis_proses, b.kd_jenis_manfaat, b.mpb, b.nominal, b.nominal_transfer jmlh_dibayarkan, CASE WHEN c.kd_bank = '002' THEN 'BRI' WHEN c.kd_bank = '008' THEN 'MANDIRI' WHEN c.kd_bank = '009' THEN 'BNI' WHEN c.kd_bank = '213' THEN 'BTPN' WHEN c.kd_bank NOT IN ('002','008','009','213','998','999') THEN 'BHS' WHEN c.kd_bank IN ('998','999') THEN 'TUNAI' END bank_koor, c.kd_bank, PAYMENT_INSTITUTE_API.Get_Description(c.company,c.institute_id) nama_bank, c.kd_cab, PAYMENT_INSTITUTE_OFFICE_API.Get_Description(c.company,c.institute_id,c.office_code) cabang_bank, c.no_rekening no_rekening_bank, c.nama_pemegang_rek nama_pemilik_rekening FROM HAPUS_PENERIMA_MP a, PENERIMAAN_MP b, REKENING_PENERIMA_MP c WHERE a.company = b.company AND a.n_i_k = b.n_i_k AND a.no_ahli_waris = b.no_ahli_waris AND b.company = c.company AND b.n_i_k = c.n_i_k AND b.no_ahli_waris = c.no_ahli_waris AND (PERIODE_API.Get_Tanggal_Awal(b.company,b.kd_periode) > a.tgl_meninggal OR PERIODE_API.Get_Tanggal_Awal(b.company,b.kd_periode) > a.tgl_berhenti_sekolah OR PERIODE_API.Get_Tanggal_Awal(b.company,b.kd_periode) > a.tgl_menikah OR PERIODE_API.Get_Tanggal_Awal(b.company,b.kd_periode) > a.tgl_bekerja OR PERIODE_API.Get_Tanggal_Awal(b.company,b.kd_periode) > a.tgl_menikah_lagi) AND PERIODE_API.Get_Tanggal_Awal(b.company,b.kd_periode) BETWEEN c.valid_from AND c.valid_to AND a.n_i_k = '".$nik_rows[$i]['N_I_K']."' AND a.no_ahli_waris = '".$nik_rows[$i]['NO_AHLI_WARIS']."'";

                $conn = getConnection();

                $sql1 = oci_parse($conn, $query1);
                $kontak_sql = oci_parse($conn, $kontak_query);
                $detail_sql = oci_parse($conn, $detail_query);

                oci_execute($sql1);
                oci_execute($kontak_sql);
                oci_execute($detail_sql);

                $rows = array();
                while ($dt = oci_fetch_assoc($sql1)) {
                    $rows[] = $dt;
                }

                $kntk_rows = array();
                while ($kntk = oci_fetch_assoc($kontak_sql)) {
                    $kntk_rows[] = $kntk;
                }

                $detail_rows = array();
                while ($detail = oci_fetch_assoc($detail_sql)) {
                    $detail_rows[] = $detail;
                }

                $data['DATA_DIRI'] = $rows;
                $data['KONTAK'] = $kntk_rows;
                $data['DETAIL_PMP'] = $detail_rows;

                $nik_array[$i+1] = $data;
            }
        }

        if ($error_msg != '') {
            $status = "Failed! ".$error_msg;
        } else {
            $status = "Success!";
        }

        updateUserLog($log_id, $status);
        
        if (!$conn) {
            $e = oci_error();
            return $response->withJson(["data" => null, "error" => $e, "status" => "failed"], 200);
        } else {
            if ($error_msg != '') {
                return $response->withJson(["data" => null, "error" => $error_msg, "status" => "failed"], 200);
            } else {
                return $response->withJson(["data" => $data, "error" => null, "status" => "success"], 200);
            }
        }
    }); //[INF_LEBIH_BYR_09]

    //--------------------------------------------------------------------------------------------//
    //------------------------------------// [INF_POTONG_PJK] //-----------------------------------//
    //--------------------------------------------------------------------------------------------//

    $app->get("/detail_data_rekon_mutasi_pjk/", function (ServerRequestInterface $request, ResponseInterface $response, array $args) {
        $tahun = $request->getHeaderLine('TAHUN');
        $api_id = "INF_POTONG_PJK_01";
        
        $ipAddress = $request->getAttribute('ip_address');
        $decoded = $request->getAttribute("decoded_jwt");
        $username = $decoded['user'];
        $ifs_user = $decoded['ifs_user'];

        $log_id = insertUserLog($username, $api_id, $ipAddress);

        $conn = getConnection();

        $access = checkAccess($ifs_user, $api_id);

        $error_msg = "";
        $error_msg = string_type($tahun, $error_msg, 'Tahun', 'NotNull');

        if ($access == 'false') {
            $error_msg = "User ".$ifs_user." Does Not Have Access to This Action!";
        }

        if ($error_msg == '') {
            $query1 = "SELECT SUBSTR(a.nama, 1, 6) nik, b.no_ahli_waris, CALON_PENERIMA_MP_API.Get_Nama_Ahli_Waris(b.company,b.n_i_k,b.no_ahli_waris) nama, b.penghasilan_bruto jmlh_bruto, (b.pajak_mutasi + SUM(a.jumlah_pph)) jmlh_pph, b.pajak_akhir jmlh_pjk_seharusnya, (b.pajak_akhir - b.pajak_mutasi - SUM(a.jumlah_pph)) selisih FROM VIEW_SPT_AKUMULASI_PAJAK a, SPT_PENSIUN b WHERE a.company = b.company AND SUBSTR(a.nama, 1, 6) = b.n_i_k AND a.tahun_pajak = b.accounting_year AND a.tahun_pajak = '".$tahun."' AND a.masa_pajak < 12 AND a.nama LIKE SUBSTR(a.nama, 1, 6) || '%' GROUP BY SUBSTR(a.nama, 1, 6), b.no_ahli_waris, CALON_PENERIMA_MP_API.Get_Nama_Ahli_Waris(b.company,b.n_i_k,b.no_ahli_waris), b.penghasilan_bruto, b.pajak_akhir, b.pajak_mutasi ORDER BY SUBSTR(a.nama, 1, 6)";
            
            $sql1 = oci_parse($conn, $query1);
            oci_execute($sql1);
            
            $data = array();
            while ($dt = oci_fetch_assoc($sql1)) {
                $data[] = $dt;
            }
        }

        if ($error_msg != '') {
            $status = "Failed! ".$error_msg;
        } else {
            $status = "Success!";
        }

        updateUserLog($log_id, $status);
        
        if (!$conn) {
            $e = oci_error();
            return $response->withJson(["data" => null, "error" => $e, "status" => "failed"], 200);
        } else {
            if ($error_msg != '') {
                return $response->withJson(["data" => null, "error" => $error_msg, "status" => "failed"], 200);
            } else {
                return $response->withJson(["data" => $data, "error" => null, "status" => "success"], 200);
            }
        }
    }); //[INF_POTONG_PJK_01]

    //--------------------------------------------------------------------------------------------//
    //------------------------------------// [INF_CAB_P2TEL] //-----------------------------------//
    //--------------------------------------------------------------------------------------------//

    $app->get("/data_p2tel/", function (ServerRequestInterface $request, ResponseInterface $response, array $args){
        //$periode = $request->getHeaderLine('PERIODE');
        $kd_cabang = $request->getHeaderLine('KD_CABANG');
        $api_id = "INF_CAB_P2TEL_01";
        
        $ipAddress = $request->getAttribute('ip_address');
        $decoded = $request->getAttribute("decoded_jwt");
        $username = $decoded['user'];
        $ifs_user = $decoded['ifs_user'];

        $log_id = insertUserLog($username, $api_id, $ipAddress);

        $conn = getConnection();

        $access = checkAccess($ifs_user, $api_id);

        $error_msg = "";
        $error_msg = string_type($kd_cabang, $error_msg, 'Kd Cabang', 'Nullable');

        if ($access == 'false') {
            $error_msg = "User ".$ifs_user." Does Not Have Access to This Action!";
        }

        if ($error_msg == '') {
            $query_where = "";

            if ($kd_cabang != '') {
                $query_where = $query_where." AND a.kd_mitra_kerja = '".$kd_cabang."'";
            }

            $query1 = "SELECT a.kd_mitra_kerja KD_CABANG, a.nama_mitra_kerja NAMA_CABANG, a.alamat ALAMAT_SEKRETARIAT, a.no_fax, a.no_telp, a.email, a.kd_bank, PAYMENT_INSTITUTE_API.Get_Description(a.company,a.kd_bank) BANK, a.kd_cab, PAYMENT_INSTITUTE_OFFICE_API.Get_Description(a.company,a.kd_bank,a.kd_cab) CABANG_BANK, a.no_rek NO_REKENING, a.nama_rek NAMA_REKENING FROM MITRA_KERJA_TAB a WHERE a.kd_kel_mitra_kerja = 'P2TEL' $query_where ORDER BY a.kd_mitra_kerja";
            
            $sql1 = oci_parse($conn, $query1);

            oci_execute($sql1);

            $data = array();
            while ($dt = oci_fetch_assoc($sql1)) {
                $data[] = $dt;
            }
        }

        if ($error_msg != '') {
            $status = "Failed! ".$error_msg;
        } else {
            $status = "Success!";
        }

        updateUserLog($log_id, $status);
        
        if (!$conn) {
            $e = oci_error();
            return $response->withJson(["data" => null, "error" => $e, "status" => "failed"], 200);
        } else {
            if ($error_msg != '') {
                return $response->withJson(["data" => null, "error" => $error_msg, "status" => "failed"], 200);
            } else {
                return $response->withJson(["data" => $data, "error" => null, "status" => "success"], 200);
            }
        }
    }); //[INF_CAB_P2TEL_01]

    $app->get("/list_pengurus_p2tel/", function (ServerRequestInterface $request, ResponseInterface $response, array $args){
        $periode = $request->getHeaderLine('PERIODE');
        $kd_cabang = $request->getHeaderLine('KD_CABANG');
        $nik = $request->getHeaderLine('NIK');
        $api_id = "INF_CAB_P2TEL_02";
        
        $ipAddress = $request->getAttribute('ip_address');
        $decoded = $request->getAttribute("decoded_jwt");
        $username = $decoded['user'];
        $ifs_user = $decoded['ifs_user'];

        $log_id = insertUserLog($username, $api_id, $ipAddress);

        $conn = getConnection();

        $access = checkAccess($ifs_user, $api_id);

        $error_msg = "";
        $error_msg = string_type($periode, $error_msg, 'Periode', 'Nullable');
        $error_msg = string_type($kd_cabang, $error_msg, 'Kd Cabang', 'Nullable');
        $error_msg = string_type($nik, $error_msg, 'NIK', 'Nullable');

        if ($access == 'false') {
            $error_msg = "User ".$ifs_user." Does Not Have Access to This Action!";
        }

        if ($error_msg == '') {
            $query_where = "";

            if ($periode != '' && $kd_cabang != '' && $nik == '') {
                $query_where = $query_where." WHERE PERIODE_API.Get_Tanggal_Awal_Periode('DPT02', '".$periode."') BETWEEN a.valid_from AND a.valid_to AND a.kd_mitra_kerja = UPPER('".$kd_cabang."')";
            } elseif ($periode != '' && $kd_cabang != '' && $nik != '') {
                $query_where = $query_where." WHERE PERIODE_API.Get_Tanggal_Awal_Periode('DPT02', '".$periode."') BETWEEN a.valid_from AND a.valid_to AND a.kd_mitra_kerja = UPPER('".$kd_cabang."') AND a.n_i_k = '".$nik."'";
            } elseif ($periode == '' && $kd_cabang != '' && $nik != '') {
                $query_where = $query_where." WHERE a.kd_mitra_kerja = UPPER('".$kd_cabang."') AND a.n_i_k = '".$nik."'";
            } elseif ($periode == '' && $kd_cabang == '' && $nik != '') {
                $query_where = $query_where." WHERE a.n_i_k = '".$nik."'";
            } elseif ($periode != '' && $kd_cabang == '' && $nik != '') {
                $query_where = $query_where." WHERE PERIODE_API.Get_Tanggal_Awal_Periode('DPT02', '".$periode."') BETWEEN a.valid_from AND a.valid_to AND a.n_i_k = '".$nik."'";
            } elseif ($periode == '' && $kd_cabang != '' && $nik == '') {
                $query_where = $query_where." WHERE SYSDATE BETWEEN a.valid_from AND a.valid_to AND a.kd_mitra_kerja = UPPER('".$kd_cabang."')";
            }

            $query1 = "SELECT a.kd_mitra_kerja kd_cabang, MITRA_KERJA_API.Get_Nama_Mitra_Kerja(a.company, a.kd_mitra_kerja) nama_cabang, a.no_kontak, a.n_i_k, PESERTA_API.Get_Nama_Pegawai(a.company,a.n_i_k) nama_karyawan, a.nama_kontak nama_kontak, a.jabatan, a.alamat alamat_pengurus, a.no_telp, a.hp no_hp_pengurus, a.email, MITRA_KERJA_API.Get_Alamat(a.company, a.kd_mitra_kerja) alamat_cabang_p2tel, MITRA_KERJA_API.Get_No_Telp(a.company, a.kd_mitra_kerja) no_telp_cabang_p2tel, '' periode_masa_bakti /**/, a.keterangan, a.valid_from, a.valid_to FROM KONTAK_MITRA_KERJA a $query_where";
            
            $sql1 = oci_parse($conn, $query1);

            oci_execute($sql1);

            $data = array();
            while ($dt = oci_fetch_assoc($sql1)) {
                $data[] = $dt;
            }
        }
        
        if ($error_msg != '') {
            $status = "Failed! ".$error_msg;
        } else {
            $status = "Success!";
        }

        updateUserLog($log_id, $status);
        
        if (!$conn) {
            $e = oci_error();
            return $response->withJson(["data" => null, "error" => $e, "status" => "failed"], 200);
        } else {
            if ($periode == '' && $kd_cabang == '' && $nik == '') {
                return $response->withJson(["data" => null, "error" => "'Periode'/'KD Cabang'/'NIK' Cannot be Empty!", "status" => "failed"], 200);
            } elseif ($error_msg != '') {
                return $response->withJson(["data" => null, "error" => $error_msg, "status" => "failed"], 200);
            } else {
                return $response->withJson(["data" => $data, "error" => null, "status" => "success"], 200);
            }
        }
    }); //[INF_CAB_P2TEL_02]

    $app->get("/jmlh_pengurus_p2tel/", function (ServerRequestInterface $request, ResponseInterface $response, array $args){
        $periode = $request->getHeaderLine('PERIODE');
        $kd_cabang = $request->getHeaderLine('KD_CABANG');
        $api_id = "INF_CAB_P2TEL_03";
        
        $ipAddress = $request->getAttribute('ip_address');
        $decoded = $request->getAttribute("decoded_jwt");
        $username = $decoded['user'];
        $ifs_user = $decoded['ifs_user'];

        $log_id = insertUserLog($username, $api_id, $ipAddress);

        $conn = getConnection();

        $access = checkAccess($ifs_user, $api_id);

        $error_msg = "";
        $error_msg = string_type($periode, $error_msg, 'Periode', 'NotNull');
        $error_msg = string_type($kd_cabang, $error_msg, 'Kd Cabang', 'Nullable');

        if ($access == 'false') {
            $error_msg = "User ".$ifs_user." Does Not Have Access to This Action!";
        }

        if ($error_msg == '') {
            $data = jmlhPengurusP2tel($periode, $kd_cabang);
        }
        
        if ($error_msg != '') {
            $status = "Failed! ".$error_msg;
        } else {
            $status = "Success!";
        }

        updateUserLog($log_id, $status);
        
        if (!$conn) {
            $e = oci_error();
            return $response->withJson(["data" => null, "error" => $e, "status" => "failed"], 200);
        } else {
            if ($error_msg != '') {
                return $response->withJson(["data" => null, "error" => $error_msg, "status" => "failed"], 200);
            } else {
                return $response->withJson(["data" => $data, "error" => null, "status" => "success"], 200);
            }
        }
    }); //[INF_CAB_P2TEL_03]

    $app->get("/jmlh_anggota_pmp_p2tel/", function (ServerRequestInterface $request, ResponseInterface $response, array $args){
        $periode = $request->getHeaderLine('PERIODE');
        $cab_p2tel = $request->getHeaderLine('CAB_P2TEL');
        $api_id = "INF_CAB_P2TEL_04";
        
        $ipAddress = $request->getAttribute('ip_address');
        $decoded = $request->getAttribute("decoded_jwt");
        $username = $decoded['user'];
        $ifs_user = $decoded['ifs_user'];

        $log_id = insertUserLog($username, $api_id, $ipAddress);

        $conn = getConnection();

        $access = checkAccess($ifs_user, $api_id);

        $error_msg = "";
        $error_msg = string_type($periode, $error_msg, 'Periode', 'NotNull');
        $error_msg = string_type($cab_p2tel, $error_msg, 'Cabang P2Tel', 'Nullable');

        if ($access == 'false') {
            $error_msg = "User ".$ifs_user." Does Not Have Access to This Action!";
        }

        if ($error_msg == '') {
            // -- //
            /*$query = "SELECT COUNT(a.n_i_k) JMLH_ANGGOTA_PMP FROM PENERIMA_MP2 a WHERE (NVL(PERIODE_API.Get_Tanggal_Awal_Periode('DPT02','".$periode."'),SYSDATE) BETWEEN a.valid_from AND a.valid_to)";
            
            $sql = oci_parse($conn, $query);
            oci_execute($sql);
            $data = oci_fetch_array($sql, OCI_ASSOC+OCI_RETURN_NULLS);
            $count = (int)$data['JMLH_ANGGOTA_PMP'];

            $data2['JMLH_ANGGOTA_PMP'] = $count;*/

            $data = jmlhPmpP2tel($periode, $cab_p2tel);
        }
        
        if ($error_msg != '') {
            $status = "Failed! ".$error_msg;
        } else {
            $status = "Success!";
        }

        updateUserLog($log_id, $status);
        
        if (!$conn) {
            $e = oci_error();
            return $response->withJson(["data" => null, "error" => $e, "status" => "failed"], 200);
        } else {
            if ($error_msg != '') {
                return $response->withJson(["data" => null, "error" => $error_msg, "status" => "failed"], 200);
            } else {
                return $response->withJson(["data" => $data, "error" => null, "status" => "success"], 200);
            }
        }
    }); //[INF_CAB_P2TEL_04]

    $app->get("/jmlh_p2tel/", function (ServerRequestInterface $request, ResponseInterface $response, array $args){
        $periode = $request->getHeaderLine('PERIODE');
        $api_id = "INF_CAB_P2TEL_05";
        
        $ipAddress = $request->getAttribute('ip_address');
        $decoded = $request->getAttribute("decoded_jwt");
        $username = $decoded['user'];
        $ifs_user = $decoded['ifs_user'];

        $log_id = insertUserLog($username, $api_id, $ipAddress);

        $conn = getConnection();

        $access = checkAccess($ifs_user, $api_id);

        $error_msg = "";
        $error_msg = string_type($periode, $error_msg, 'Periode', 'Nullable');

        if ($access == 'false') {
            $error_msg = "User ".$ifs_user." Does Not Have Access to This Action!";
        }

        if ($error_msg == '') {
            $periode_query = "";

            if ($periode != '') {
                $periode_query = $periode_query." AND PERIODE_API.Get_Tanggal_Awal_Periode(a.company,'".$periode."') BETWEEN b.valid_from AND b.valid_to";
            }

            $query1 = "SELECT COUNT(DISTINCT(b.p2tel_id)) JMLH_CAB_P2TEL FROM MITRA_KERJA_TAB a, KELURAHAN_TAB b, KONTAK_MITRA_KERJA c WHERE a.kd_mitra_kerja = b.p2tel_id AND a.kd_mitra_kerja = c.kd_mitra_kerja AND b.p2tel_id = c.kd_mitra_kerja AND a.kd_kel_mitra_kerja = 'P2TEL' AND c.kd_mitra_kerja IS NOT NULL $periode_query";
            
            $sql1 = oci_parse($conn, $query1);

            oci_execute($sql1);

            $data = array();
            while ($dt = oci_fetch_assoc($sql1)) {
                $data[] = $dt;
            }
        }
        
        if ($error_msg != '') {
            $status = "Failed! ".$error_msg;
        } else {
            $status = "Success!";
        }

        updateUserLog($log_id, $status);
        
        if (!$conn) {
            $e = oci_error();
            return $response->withJson(["data" => null, "error" => $e, "status" => "failed"], 200);
        } else {
            if ($error_msg != '') {
                return $response->withJson(["data" => null, "error" => $error_msg, "status" => "failed"], 200);
            } else {
                return $response->withJson(["data" => $data, "error" => null, "status" => "success"], 200);
            }
        }
    }); //[INF_CAB_P2TEL_05]

    $app->get("/list_p2tel/", function (ServerRequestInterface $request, ResponseInterface $response, array $args){
        $periode = $request->getHeaderLine('PERIODE');
        $api_id = "INF_CAB_P2TEL_06";
        
        $ipAddress = $request->getAttribute('ip_address');
        $decoded = $request->getAttribute("decoded_jwt");
        $username = $decoded['user'];
        $ifs_user = $decoded['ifs_user'];

        $log_id = insertUserLog($username, $api_id, $ipAddress);

        $conn = getConnection();

        $access = checkAccess($ifs_user, $api_id);

        $error_msg = "";
        $error_msg = string_type($periode, $error_msg, 'Periode', 'Nullable');

        if ($access == 'false') {
            $error_msg = "User ".$ifs_user." Does Not Have Access to This Action!";
        }

        if ($error_msg == '') {
            $periode_query = "";

            if ($periode != '') {
                $periode_query = $periode_query." AND PERIODE_API.Get_Tanggal_Awal_Periode(a.company,'".$periode."') BETWEEN b.valid_from AND b.valid_to";
            }

            $query1 = "SELECT a.kd_mitra_kerja KD_CABANG, a.provinsi_id, a.nama_mitra_kerja NAMA_CABANG, a.alamat, a.no_telp, a.email FROM MITRA_KERJA_TAB a, KELURAHAN_TAB b, KONTAK_MITRA_KERJA c WHERE a.kd_mitra_kerja = b.p2tel_id AND a.kd_mitra_kerja = c.kd_mitra_kerja AND b.p2tel_id = c.kd_mitra_kerja AND a.kd_kel_mitra_kerja = 'P2TEL' AND c.kd_mitra_kerja IS NOT NULL $periode_query GROUP BY a.kd_mitra_kerja, a.provinsi_id, a.nama_mitra_kerja, a.alamat, a.no_telp, a.email";
            
            $sql1 = oci_parse($conn, $query1);

            oci_execute($sql1);

            $data = array();
            while ($dt = oci_fetch_assoc($sql1)) {
                $data[] = $dt;
            }
        }
        
        if ($error_msg != '') {
            $status = "Failed! ".$error_msg;
        } else {
            $status = "Success!";
        }

        updateUserLog($log_id, $status);
        
        if (!$conn) {
            $e = oci_error();
            return $response->withJson(["data" => null, "error" => $e, "status" => "failed"], 200);
        } else {
            if ($error_msg != '') {
                return $response->withJson(["data" => null, "error" => $error_msg, "status" => "failed"], 200);
            } else {
                return $response->withJson(["data" => $data, "error" => null, "status" => "success"], 200);
            }
        }
    }); //[INF_CAB_P2TEL_06]

    //--------------------------------------------------------------------------------------------//
    //------------------------------------// [INF_PMP_MD] //--------------------------------------//
    //--------------------------------------------------------------------------------------------//

    $app->get("/jmlh_pmp_meninggal/", function (ServerRequestInterface $request, ResponseInterface $response){
        $periode = $request->getHeaderLine('PERIODE');
        $periode_trnsksi = $request->getHeaderLine('PERIODE_TRANSAKSI');
        $periode_setuju = $request->getHeaderLine('PERIODE_SETUJU');
        $range_tgl_trans = $request->getHeaderLine('RANGE_TGL_TRANSAKSI');
        $range_tgl_setuju = $request->getHeaderLine('RANGE_TGL_SETUJU');
        $jenis_trnsksi = $request->getHeaderLine('JENIS_TRANSAKSI');
        $cab_p2tel = $request->getHeaderLine('CAB_P2TEL');
        $sk_non_sk = $request->getHeaderLine('SK_NON_SK');
        $pmp_non_pmp = $request->getHeaderLine('PMP_NON_PMP');
        $jenis_aw = $request->getHeaderLine('JENIS_AW');
        $api_id = "INF_PMP_MD_01";

        $param_count = 0;
        $param_count = check_param_multiple_filled($periode, $param_count);
        $param_count = check_param_multiple_filled($periode_trnsksi, $param_count);
        $param_count = check_param_multiple_filled($periode_setuju, $param_count);
        $param_count = check_param_multiple_filled($range_tgl_trans, $param_count);
        $param_count = check_param_multiple_filled($range_tgl_setuju, $param_count);
        
        $ipAddress = $request->getAttribute('ip_address');
        $decoded = $request->getAttribute("decoded_jwt");
        $username = $decoded['user'];
        $ifs_user = $decoded['ifs_user'];

        $log_id = insertUserLog($username, $api_id, $ipAddress);

        $jns_api = "jmlh";

        $conn = getConnection();

        $access = checkAccess($ifs_user, $api_id);

        $error_msg = "";
        $error_msg = string_type($periode, $error_msg, 'Periode', 'Nullable');
        $error_msg = string_type($periode_trnsksi, $error_msg, 'Periode Transaksi', 'Nullable');
        $error_msg = string_type($periode_setuju, $error_msg, 'Periode Setuju', 'Nullable');
        $error_msg = string_type($range_tgl_trans, $error_msg, 'Range Tanggal Transaksi', 'Nullable');
        $error_msg = string_type($range_tgl_setuju, $error_msg, 'Range Tanggal Setuju', 'Nullable');
        $error_msg = string_type($jenis_trnsksi, $error_msg, 'Jenis Transaksi', 'NotNull');
        $error_msg = string_type($cab_p2tel, $error_msg, 'Cabang P2Tel', 'Nullable');
        $error_msg = parameter_type($sk_non_sk, $error_msg, 'SK Non SK', 1, 2, 'Nullable');
        $error_msg = parameter_type($pmp_non_pmp, $error_msg, 'PMP Non PMP', 1, 2, 'Nullable');
        //$error_msg = string_type($jenis_aw, $error_msg, 'Jenis Ahli Waris', 'Nullable');
        $error_msg = parameter_type($jenis_aw, $error_msg, 'Jenis Ahli Waris', 1, 5, 'Nullable');

        if ($access == 'false') {
            $error_msg = "User ".$ifs_user." Does Not Have Access to This Action!";
        }

        if ($error_msg == '') {
            $count = jmlhPmpMeninggal($periode, $periode_trnsksi, $periode_setuju, $range_tgl_trans, $range_tgl_setuju, $jenis_trnsksi, $cab_p2tel, $sk_non_sk, $pmp_non_pmp, $jenis_aw, $jns_api);

            $data['JMLH_PMP_MENINGGAL'] = $count;
        }

        if ($error_msg != '') {
            $status = "Failed! ".$error_msg;
        } else {
            $status = "Success!";
        }

        updateUserLog($log_id, $status);
        
        if (!$conn) {
            $e = oci_error();
            return $response->withJson(["data" => null, "error" => $e, "status" => "failed"], 200);
        } else {
            if ($periode == '' && $periode_trnsksi == '' && $periode_setuju == '' && $range_tgl_trans == '' && $range_tgl_setuju == '') {
                return $response->withJson(["data" => null, "error" => "Field [Periode] or [Periode Transaksi] or [Periode Setuju] or [Range Tanggal Transaksi] or [Range Tanggal Setuju] Cannot be Empty!", "status" => "failed"], 200);
            } elseif ($param_count > 1) {
                return $response->withJson(["data" => null, "error" => "Fields [Periode], [Periode Transaksi], [Periode Setuju], [Range Tanggal Transaksi], and [Range Tanggal Setuju] Cannot be Filled at the Same Time!", "status" => "failed"], 200);
            } elseif ($error_msg != '') {
                return $response->withJson(["data" => null, "error" => $error_msg, "status" => "failed"], 200);
            } else {
                return $response->withJson(["data" => $data, "error" => null, "status" => "success"], 200);
            }
        }
    }); //[INF_PMP_MD_01]

    $app->get("/jmlh_pmp_meninggal_all/", function (ServerRequestInterface $request, ResponseInterface $response){
        $tahun = $request->getHeaderLine('TAHUN_PERIODE');
        $parameter = $request->getHeaderLine('PARAMETER');
        $api_id = "INF_PMP_MD_02";
        
        $ipAddress = $request->getAttribute('ip_address');
        $decoded = $request->getAttribute("decoded_jwt");
        $username = $decoded['user'];
        $ifs_user = $decoded['ifs_user'];

        $log_id = insertUserLog($username, $api_id, $ipAddress);

        $conn = getConnection();

        $access = checkAccess($ifs_user, $api_id);

        $error_msg = "";
        $error_msg = string_type($tahun, $error_msg, 'Tahun Periode', 'NotNull');
        $error_msg = parameter_type($parameter, $error_msg, 'Parameter', 1, 8, 'NotNull');

        if ($access == 'false') {
            $error_msg = "User ".$ifs_user." Does Not Have Access to This Action!";
        }

        if ($error_msg == '') {
            if ($parameter == '1') {
                $data['JMLH_PMP_MD_BULAN'] = jmlhPmpMeninggalAll($tahun, 'bulan');
            } elseif ($parameter == '2') {
                $data['JMLH_PMP_MD_TRIWULAN'] = jmlhPmpMeninggalAll($tahun, 'triwulan');
            } elseif ($parameter == '3') {
                $data['JMLH_PMP_MD_SEMESTER'] = jmlhPmpMeninggalAll($tahun, 'semester');
            } elseif ($parameter == '4') {
                $data['JMLH_PMP_MD_TAHUN'] = jmlhPmpMeninggalAll($tahun, 'tahun');
            } elseif ($parameter == '5') {
                $data['JMLH_PMP_MD_JNS_TRANSAKSI'] = jmlhPmpMeninggalAll($tahun, 'jns_trnsksi');
            } elseif ($parameter == '6') {
                $data['JMLH_PMP_MD_SK_NON_SK'] = jmlhPmpMeninggalAll($tahun, 'sk_non_sk');
            } elseif ($parameter == '7') {
                $data['JMLH_PMP_MD_PMP_NON_PMP'] = jmlhPmpMeninggalAll($tahun, 'pmp_non_pmp');
            } elseif ($parameter == '8') {
                $data['JMLH_PMP_MD_JNS_AW'] = jmlhPmpMeninggalAll($tahun, 'jns_aw');
            }
        }

        if ($error_msg != '') {
            $status = "Failed! ".$error_msg;
        } else {
            $status = "Success!";
        }

        updateUserLog($log_id, $status);
        
        if (!$conn) {
            $e = oci_error();
            return $response->withJson(["data" => null, "error" => $e, "status" => "failed"], 200);
        } else {
            if ($error_msg != '') {
                return $response->withJson(["data" => null, "error" => $error_msg, "status" => "failed"], 200);
            } else {
                return $response->withJson(["data" => $data, "error" => null, "status" => "success"], 200);
            }
        }
    }); //[INF_PMP_MD_02]

    $app->get("/list_pmp_meninggal/", function (ServerRequestInterface $request, ResponseInterface $response){
        $periode = $request->getHeaderLine('PERIODE');
        $periode_trnsksi = $request->getHeaderLine('PERIODE_TRANSAKSI');
        $periode_setuju = $request->getHeaderLine('PERIODE_SETUJU');
        $range_tgl_trans = $request->getHeaderLine('RANGE_TGL_TRANSAKSI');
        $range_tgl_setuju = $request->getHeaderLine('RANGE_TGL_SETUJU');
        $jenis_trnsksi = $request->getHeaderLine('JENIS_TRANSAKSI');
        $cab_p2tel = $request->getHeaderLine('CAB_P2TEL');
        $sk_non_sk = $request->getHeaderLine('SK_NON_SK');
        $pmp_non_pmp = $request->getHeaderLine('PMP_NON_PMP');
        $jenis_aw = $request->getHeaderLine('JENIS_AW');
        $api_id = "INF_PMP_MD_03";

        $param_count = 0;
        $param_count = check_param_multiple_filled($periode, $param_count);
        $param_count = check_param_multiple_filled($periode_trnsksi, $param_count);
        $param_count = check_param_multiple_filled($periode_setuju, $param_count);
        $param_count = check_param_multiple_filled($range_tgl_trans, $param_count);
        $param_count = check_param_multiple_filled($range_tgl_setuju, $param_count);
        
        $ipAddress = $request->getAttribute('ip_address');
        $decoded = $request->getAttribute("decoded_jwt");
        $username = $decoded['user'];
        $ifs_user = $decoded['ifs_user'];

        $log_id = insertUserLog($username, $api_id, $ipAddress);

        $jns_api = "list";

        $conn = getConnection();

        $access = checkAccess($ifs_user, $api_id);

        $error_msg = "";
        $error_msg = string_type($periode, $error_msg, 'Periode', 'Nullable');
        $error_msg = string_type($periode_trnsksi, $error_msg, 'Periode Transaksi', 'Nullable');
        $error_msg = string_type($periode_setuju, $error_msg, 'Periode Setuju', 'Nullable');
        $error_msg = string_type($range_tgl_trans, $error_msg, 'Range Tanggal Transaksi', 'Nullable');
        $error_msg = string_type($range_tgl_setuju, $error_msg, 'Range Tanggal Setuju', 'Nullable');
        $error_msg = string_type($jenis_trnsksi, $error_msg, 'Jenis Transaksi', 'NotNull');
        $error_msg = string_type($cab_p2tel, $error_msg, 'Cabang P2Tel', 'Nullable');
        $error_msg = parameter_type($sk_non_sk, $error_msg, 'SK Non SK', 1, 2, 'Nullable');
        $error_msg = parameter_type($pmp_non_pmp, $error_msg, 'PMP Non PMP', 1, 2, 'Nullable');
        //$error_msg = string_type($jenis_aw, $error_msg, 'Jenis Ahli Waris', 'Nullable');
        $error_msg = parameter_type($jenis_aw, $error_msg, 'Jenis Ahli Waris', 1, 5, 'Nullable');

        if ($access == 'false') {
            $error_msg = "User ".$ifs_user." Does Not Have Access to This Action!";
        }

        if ($error_msg == '') {
            $data = jmlhPmpMeninggal($periode, $periode_trnsksi, $periode_setuju, $range_tgl_trans, $range_tgl_setuju, $jenis_trnsksi, $cab_p2tel, $sk_non_sk, $pmp_non_pmp, $jenis_aw, $jns_api);
        }

        if ($error_msg != '') {
            $status = "Failed! ".$error_msg;
        } else {
            $status = "Success!";
        }

        updateUserLog($log_id, $status);
        
        if (!$conn) {
            $e = oci_error();
            return $response->withJson(["data" => null, "error" => $e, "status" => "failed"], 200);
        } else {
            if ($periode == '' && $periode_trnsksi == '' && $periode_setuju == '' && $range_tgl_trans == '' && $range_tgl_setuju == '') {
                return $response->withJson(["data" => null, "error" => "Field [Periode] or [Periode Transaksi] or [Periode Setuju] or [Range Tanggal Transaksi] or [Range Tanggal Setuju] Cannot be Empty!", "status" => "failed"], 200);
            } elseif ($param_count > 1) {
                return $response->withJson(["data" => null, "error" => "Fields [Periode], [Periode Transaksi], [Periode Setuju], [Range Tanggal Transaksi], and [Range Tanggal Setuju] Cannot be Filled at the Same Time!", "status" => "failed"], 200);
            } elseif ($error_msg != '') {
                return $response->withJson(["data" => null, "error" => $error_msg, "status" => "failed"], 200);
            } else {
                return $response->withJson(["data" => $data, "error" => null, "status" => "success"], 200);
            }
        }
    }); //[INF_PMP_MD_03]

    $app->get("/detail_pmp_meninggal/", function (ServerRequestInterface $request, ResponseInterface $response, array $args) {
        $nik = $request->getHeaderLine('NIK');
        $api_id = "INF_PMP_MD_04";
        
        $ipAddress = $request->getAttribute('ip_address');
        $decoded = $request->getAttribute("decoded_jwt");
        $username = $decoded['user'];
        $ifs_user = $decoded['ifs_user'];

        $log_id = insertUserLog($username, $api_id, $ipAddress);

        $conn = getConnection();

        $access = checkAccess($ifs_user, $api_id);

        $error_msg = "";
        $error_msg = string_type($nik, $error_msg, 'NIK', 'NotNull');

        if ($access == 'false') {
            $error_msg = "User ".$ifs_user." Does Not Have Access to This Action!";
        }

        if ($error_msg == '') {
            $hapus_query = "SELECT a.n_i_k, a.no_ahli_waris, CALON_PENERIMA_MP_API.Get_Nama_Ahli_Waris(a.company,a.n_i_k,a.no_ahli_waris) nama, PESERTA_API.Get_Pangkat(a.company,a.n_i_k) pangkat, PESERTA_API.Get_Kd_Kelompok(a.company,a.n_i_k) kd_kelompok, KELOMPOK_PESERTA_API.Get_Nama_Kelompok(a.company,PESERTA_API.Get_Kd_Kelompok(a.company,a.n_i_k)) nama_kelompok, a.jenis_ahli_waris jenis_aw, a.tgl_meninggal, 'Hapus MP' jenis_transaksi, TRUNC(a.last_update) tgl_transaksi, a.state, TRUNC(DPT_AUDITTRAIL_API.Get_Tgl('HapusPenerimaMp',a.objid,a.state)) tgl_status FROM HAPUS_PENERIMA_MP a WHERE a.n_i_k = '".$nik."' AND a.no_ahli_waris = '1'";
            $hapus_sql = oci_parse($conn, $hapus_query);
            oci_execute($hapus_sql);

            $hapus_rows = array();
            while ($hapus_dt = oci_fetch_assoc($hapus_sql)) {
                $hapus_rows[] = $hapus_dt;
            }

            $mutasi_pmp_query = "SELECT a.n_i_k, a.no_ahli_waris, CALON_PENERIMA_MP_API.Get_Nama_Ahli_Waris(a.company,a.n_i_k,a.no_ahli_waris) nama, PESERTA_API.Get_Pangkat(a.company,a.n_i_k) pangkat, PESERTA_API.Get_Kd_Kelompok(a.company,a.n_i_k) kd_kelompok, KELOMPOK_PESERTA_API.Get_Nama_Kelompok(a.company,PESERTA_API.Get_Kd_Kelompok(a.company,a.n_i_k)) nama_kelompok, a.jenis_ahli_waris jenis_aw, a.tgl_meninggal, 'Mutasi PMP' jenis_transaksi, TRUNC(a.last_update) tgl_transaksi, a.state, TRUNC(DPT_AUDITTRAIL_API.Get_Tgl('MutasiPenerimaMp',a.objid,a.state)) tgl_status FROM MUTASI_PENERIMA_MP a WHERE NVL(a.gabung_mp,'FALSE') != 'TRUE' AND a.n_i_k = '".$nik."' AND a.no_ahli_waris = '1'";
            $mutasi_pmp_sql = oci_parse($conn, $mutasi_pmp_query);
            oci_execute($mutasi_pmp_sql);

            $mutasi_pmp_rows = array();
            while ($mutasi_pmp_dt = oci_fetch_assoc($mutasi_pmp_sql)) {
                $mutasi_pmp_rows[] = $mutasi_pmp_dt;
            }

            $ubah_aw1_query = "SELECT a.n_i_k, CALON_PENERIMA_MP_API.Get_Nama_Ahli_Waris(a.company,a.n_i_k,a.no_ahli_waris) nama, PESERTA_API.Get_Pangkat(a.company,a.n_i_k) pangkat, PESERTA_API.Get_Kd_Kelompok(a.company,a.n_i_k) kd_kelompok, KELOMPOK_PESERTA_API.Get_Nama_Kelompok(a.company,PESERTA_API.Get_Kd_Kelompok(a.company,a.n_i_k)) nama_kelompok, a.jenis_ahli_waris jenis_aw, a.tgl_meninggal, 'Ubah AW' jenis_transaksi, TRUNC(a.last_update) tgl_transaksi, a.state, TRUNC(DPT_AUDITTRAIL_API.Get_Tgl('TrUbahAhliWaris',a.objid,a.state)) tgl_status FROM TR_UBAH_AHLI_WARIS a WHERE a.tgl_meninggal_lama IS NULL AND a.tgl_meninggal IS NOT NULL AND a.n_i_k = '".$nik."' AND a.no_ahli_waris = '1'";
            $ubah_aw1_sql = oci_parse($conn, $ubah_aw1_query);
            oci_execute($ubah_aw1_sql);

            $ubah_aw1_rows = array();
            while ($ubah_aw1_dt = oci_fetch_assoc($ubah_aw1_sql)) {
                $ubah_aw1_rows[] = $ubah_aw1_dt;
            }

            $rows1 = array();
            if (oci_num_rows($hapus_sql) > 0) {
                $rows1 = $hapus_rows;
            }
            if (oci_num_rows($mutasi_pmp_sql) > 0) {
                $rows1 = $mutasi_pmp_rows;
            }
            if (oci_num_rows($ubah_aw1_sql) > 0) {
                $rows1 = $ubah_aw1_rows;
            }

            /************************************************************************************************************/

            $hapus_aw_query = "SELECT a.n_i_k, CALON_PENERIMA_MP_API.Get_Nama_Ahli_Waris(a.company,a.n_i_k,a.no_ahli_waris) nama, a.jenis_ahli_waris jenis_aw, a.tgl_meninggal, 'Hapus MP' jenis_transaksi, TRUNC(a.last_update) tgl_transaksi, a.state, TRUNC(DPT_AUDITTRAIL_API.Get_Tgl('HapusPenerimaMp',a.objid,a.state)) tgl_status FROM HAPUS_PENERIMA_MP a WHERE a.n_i_k = '".$nik."' AND a.no_ahli_waris <> '1'";
            $hapus_aw_sql = oci_parse($conn, $hapus_aw_query);
            oci_execute($hapus_aw_sql);

            $hapus_aw_rows = array();
            while ($hapus_aw_dt = oci_fetch_assoc($hapus_aw_sql)) {
                $hapus_aw_rows[] = $hapus_aw_dt;
            }

            $mutasi_pmp_aw_query = "SELECT a.n_i_k, CALON_PENERIMA_MP_API.Get_Nama_Ahli_Waris(a.company,a.n_i_k,a.no_ahli_waris) nama, PESERTA_API.Get_Pangkat(a.company,a.n_i_k) pangkat, PESERTA_API.Get_Kd_Kelompok(a.company,a.n_i_k) kd_kelompok, KELOMPOK_PESERTA_API.Get_Nama_Kelompok(a.company,PESERTA_API.Get_Kd_Kelompok(a.company,a.n_i_k)) nama_kelompok, a.jenis_ahli_waris jenis_aw, a.tgl_meninggal, 'Mutasi PMP' jenis_transaksi, TRUNC(a.last_update) tgl_transaksi, a.state, TRUNC(DPT_AUDITTRAIL_API.Get_Tgl('MutasiPenerimaMp',a.objid,a.state)) tgl_status FROM MUTASI_PENERIMA_MP a WHERE NVL(a.gabung_mp,'FALSE') != 'TRUE' AND a.n_i_k = '".$nik."' AND a.no_ahli_waris <> '1'";
            $mutasi_pmp_aw_sql = oci_parse($conn, $mutasi_pmp_aw_query);
            oci_execute($mutasi_pmp_aw_sql);

            $mutasi_pmp_aw_rows = array();
            while ($mutasi_pmp_aw_dt = oci_fetch_assoc($mutasi_pmp_aw_sql)) {
                $mutasi_pmp_aw_rows[] = $mutasi_pmp_aw_dt;
            }

            $mutasi_aw_query = "SELECT a.n_i_k, CALON_PENERIMA_MP_API.Get_Nama_Ahli_Waris(a.company,a.n_i_k,a.no_ahli_waris) nama, a.jenis_ahli_waris jenis_aw, a.tgl_meninggal, 'Mutasi AW' jenis_transaksi, TRUNC(a.last_update) tgl_transaksi, a.state, TRUNC(DPT_AUDITTRAIL_API.Get_Tgl('MutasiPenerimaMp',a.objid,a.state)) tgl_status FROM MUTASI_PENERIMA_MP_AW a WHERE a.n_i_k = '".$nik."' AND a.no_ahli_waris <> '1'";
            $mutasi_aw_sql = oci_parse($conn, $mutasi_aw_query);
            oci_execute($mutasi_aw_sql);

            $mutasi_aw_rows = array();
            while ($mutasi_aw_dt = oci_fetch_assoc($mutasi_aw_sql)) {
                $mutasi_aw_rows[] = $mutasi_aw_dt;
            }

            $mutasi_gabung_query = "SELECT a.n_i_k, CALON_PENERIMA_MP_API.Get_Nama_Ahli_Waris(a.company,a.n_i_k,a.no_ahli_waris) nama, a.jenis_ahli_waris jenis_aw, a.tgl_meninggal, 'Mutasi Gabung' jenis_transaksi, TRUNC(a.last_update) tgl_transaksi, a.state, TRUNC(DPT_AUDITTRAIL_API.Get_Tgl('MutasiPenerimaMp',a.objid,a.state)) tgl_status FROM MUTASI_PENGGABUNGAN_MP a WHERE NVL(a.gabung_mp,'FALSE') = 'TRUE' AND a.n_i_k = '".$nik."' AND a.no_ahli_waris <> '1'";
            $mutasi_gabung_sql = oci_parse($conn, $mutasi_gabung_query);
            oci_execute($mutasi_gabung_sql);

            $mutasi_gabung_rows = array();
            while ($mutasi_gabung_dt = oci_fetch_assoc($mutasi_gabung_sql)) {
                $mutasi_gabung_rows[] = $mutasi_gabung_dt;
            }

            $ubah_aw_query = "SELECT a.n_i_k, CALON_PENERIMA_MP_API.Get_Nama_Ahli_Waris(a.company,a.n_i_k,a.no_ahli_waris) nama, a.jenis_ahli_waris jenis_aw, a.tgl_meninggal, 'Ubah AW' jenis_transaksi, TRUNC(a.last_update) tgl_transaksi, a.state, TRUNC(DPT_AUDITTRAIL_API.Get_Tgl('TrUbahAhliWaris',a.objid,a.state)) tgl_status FROM TR_UBAH_AHLI_WARIS a WHERE a.tgl_meninggal_lama IS NULL AND a.tgl_meninggal IS NOT NULL AND a.n_i_k = '".$nik."' AND a.no_ahli_waris <> '1'";
            $ubah_aw_sql = oci_parse($conn, $ubah_aw_query);
            oci_execute($ubah_aw_sql);

            $ubah_aw_rows = array();
            while ($ubah_aw_dt = oci_fetch_assoc($ubah_aw_sql)) {
                $ubah_aw_rows[] = $ubah_aw_dt;
            }

            $rows2 = array();
            if (oci_num_rows($hapus_aw_sql) > 0) {
                $rows2 = $hapus_aw_rows;
            }
            if (oci_num_rows($mutasi_pmp_aw_sql) > 0) {
                $rows2 = $mutasi_pmp_aw_rows;
            }
            if (oci_num_rows($mutasi_aw_sql) > 0) {
                $rows2 = $mutasi_aw_rows;
            }
            if (oci_num_rows($mutasi_gabung_sql) > 0) {
                $rows2 = $mutasi_gabung_rows;
            }
            if (oci_num_rows($ubah_aw_sql) > 0) {
                $rows2 = $ubah_aw_rows;
            }

            $data['DATA_PEGAWAI'] = $rows1;
            $data['AHLI_WARIS'] = $rows2;
        }

        if ($error_msg != '') {
            $status = "Failed! ".$error_msg;
        } else {
            $status = "Success!";
        }

        updateUserLog($log_id, $status);
        
        if (!$conn) {
            $e = oci_error();
            return $response->withJson(["data" => null, "error" => $e, "status" => "failed"], 200);
        } else {
            if ($error_msg != '') {
                return $response->withJson(["data" => null, "error" => $error_msg, "status" => "failed"], 200);
            } else {
                return $response->withJson(["data" => $data, "error" => null, "status" => "success"], 200);
            }
        }
    }); //[INF_PMP_MD_04]

    //--------------------------------------------------------------------------------------------//
    //------------------------------------// [INF_TASK] //-----------------------------------//
    //--------------------------------------------------------------------------------------------//

    $app->get("/info_task_per_user/", function (ServerRequestInterface $request, ResponseInterface $response){
        $conn = getConnection();
        
        $user = $request->getHeaderLine('USER'); //KPST06
        $api_id = "INF_TASK_01";
        
        $ipAddress = $request->getAttribute('ip_address');
        $decoded = $request->getAttribute("decoded_jwt");
        $username = $decoded['user'];
        $ifs_user = $decoded['ifs_user'];

        $log_id = insertUserLog($username, $api_id, $ipAddress);

        $access = checkAccess($ifs_user, $api_id);

        $error_msg = "";
        $error_msg = string_type($user, $error_msg, 'User', 'NotNull');

        if ($access == 'false') {
            $error_msg = "User ".$ifs_user." Does Not Have Access to This Action!";
        }

        if ($error_msg == '') {
            $rows = array();
            
            $query = "SELECT CTM_API_UTIL_API.Get_Task_Sppb('".$user."') TASK_SPPB FROM DUAL";
            
            $sql = oci_parse($conn, $query);
            oci_execute($sql);
            $data1 = oci_fetch_array($sql, OCI_ASSOC+OCI_RETURN_NULLS);
            $result = $data1['TASK_SPPB'];

            $task_sppb = explode(";", $result);

            if ($result == '') {
                $rows['TASK_SPPB'] = "";
            } else {
                for ($i = 0; $i < count($task_sppb); $i++) {
                    $query1 = "SELECT a.no_sppb NO_SPPB FROM sppb_header a WHERE a.no_sppb = '".$task_sppb[$i]."'";
        
                    $sql1 = oci_parse($conn, $query1);
                    oci_execute($sql1);
                    $data1 = oci_fetch_array($sql1, OCI_ASSOC+OCI_RETURN_NULLS);
                    $result1 = $data1['NO_SPPB'];
                    
                    $array[$i+1] = $result1;
                }
                $rows['TASK_SPPB'] = $array;
            }
        }

        if ($error_msg != '') {
            $status = "Failed! ".$error_msg;
        } else {
            $status = "Success!";
        }

        updateUserLog($log_id, $status);
        
        if (!$conn) {
            $e = oci_error();
            return $response->withJson(["data" => null, "error" => $e, "status" => "failed"], 200);
        } else {
            if ($error_msg != '') {
                return $response->withJson(["data" => null, "error" => $error_msg, "status" => "failed"], 200);
            } else {
                return $response->withJson(["data" => $rows, "error" => null, "status" => "success"], 200);
            }
        }
    }); //[INF_TASK_01]
};
?>