<?php

$app->get("/jmlh_pmp_all/", function (ServerRequestInterface $request, ResponseInterface $response){
    $conn = getConnection(); // ganti jadi kode
    
    $kd_jns_pensiun = array("NORMAL PESERTA", "NORMAL JANDA DUDA", "NORMAL ANAK", "DIPERCEPAT PESERTA", "DIPERCEPAT JANDA DUDA", "DIPERCEPAT ANAK", "CACAT PESERTA", "CACAT JANDA DUDA", "CACAT ANAK", "DITUNDA PESERTA", "DITUNDA JANDA DUDA", "DITUNDA ANAK", "MENINGGAL JANDA DUDA", "MENINGGAL ANAK", "TEWAS JANDA DUDA", "TEWAS ANAK", "DANA PESERTA LAJANG", "NORMAL ANAK DARI JANDA DUDA", "DIPERCEPAT ANAK DARI JANDA DUDA", "CACAT ANAK DARI JANDA DUDA", "DITUNDA ANAK DARI JANDA DUDA", "PESERTA MANTAN", "MENINGGAL ANAK DARI JANDA DUDA", "DITUNDA DANA PESERTA MANTAN MENINGGAL", "TEWAS ANAK DARI JANDA DUDA", "DITUNDA JANDA DUDA MANTAN MENINGGAL", "DITUNDA ANAK MANTAN MENINGGAL", "DITUNDA ANAK MANTAN MENINGGAL JANDA DUDA", "NULL");
    $prm_type = "KD_JNS_PENSIUN";
    $pst = "JENIS_PENSIUN_API.Get_Nama_Jenis_Pensiun(a.company,a.kd_jenis_pensiun)";
    $data['PMP_KD_JNS_PENSIUN'] = selectJmlhPmpAll($kd_jns_pensiun, $prm_type, $pst);

    if (!$conn) {
        $e = oci_error();
        return $response->withJson(["data" => null, "error" => $e, "status" => "success"], 200);
    } else {
        return $response->withJson(["data" => $data, "error" => null, "status" => "success"], 200);
    }
}); //[INF_KPST_02]

$app->get("/jmlh_pst_aktif_all/", function (Request $request, Response $response){
    $query1 = "SELECT COUNT(a.n_i_k) jmlh_pst_aktif FROM PESERTA_AKTIF a WHERE a.status_peserta = 'Aktif'";

    $query2 = "SELECT sum(jmlh_pra) jmlh_pra, sum(jmlh_pasca) jmlh_pasca, sum(jmlh_null) jmlh_kd_klmpk_null, (sum(jmlh_pra) + sum(jmlh_pasca) + sum(jmlh_null)) total_kd_klmpk FROM (SELECT COUNT(a.n_i_k) jmlh_pra, 0 jmlh_pasca, 0 jmlh_null FROM PESERTA_AKTIF a WHERE a.status_peserta = 'Aktif' AND (KELOMPOK_PESERTA_API.Get_Nama_Kelompok(a.company, a.kd_kelompok) LIKE '%PRA%') UNION ALL SELECT 0 jmlh_pra, COUNT(a.n_i_k) jmlh_pasca, 0 jmlh_null FROM PESERTA_AKTIF a WHERE a.status_peserta = 'Aktif' AND (KELOMPOK_PESERTA_API.Get_Nama_Kelompok(a.company, a.kd_kelompok) LIKE '%PASCA%') UNION ALL SELECT 0 jmlh_pra, 0 jmlh_pasca, COUNT(a.n_i_k) jmlh_null FROM PESERTA_AKTIF a WHERE a.status_peserta = 'Aktif' AND (a.kd_kelompok IS NULL))";

    $query3 = "SELECT sum(jmlh_pria) jmlh_pria, sum(jmlh_wanita) jmlh_wanita, sum(jmlh_null) jmlh_jk_null, (sum(jmlh_pria) + sum(jmlh_wanita) + sum(jmlh_null)) total_jk FROM (SELECT COUNT(a.n_i_k) jmlh_pria, 0 jmlh_wanita, 0 jmlh_null FROM PESERTA_AKTIF a, CALON_PENERIMA_MP b WHERE a.status_peserta = 'Aktif' AND a.company = b.company AND a.n_i_k = b.n_i_k AND (b.jenis_ahli_waris = 'Peserta' OR b.no_ahli_waris = 1) AND CALON_PENERIMA_MP_API.Get_Jenis_Kelamin2(b.COMPANY, b.N_I_K, b.NO_AHLI_WARIS) = 'Pria' UNION ALL SELECT 0 jmlh_pria, COUNT(a.n_i_k) jmlh_wanita, 0 jmlh_null FROM PESERTA_AKTIF a, CALON_PENERIMA_MP b WHERE a.status_peserta = 'Aktif' AND a.company = b.company AND a.n_i_k = b.n_i_k AND (b.jenis_ahli_waris = 'Peserta' OR b.no_ahli_waris = 1) AND CALON_PENERIMA_MP_API.Get_Jenis_Kelamin2(b.COMPANY, b.N_I_K, b.NO_AHLI_WARIS) = 'Wanita' UNION ALL SELECT 0 jmlh_pria, 0 jmlh_wanita, COUNT(a.n_i_k) jmlh_null FROM PESERTA_AKTIF a, CALON_PENERIMA_MP b WHERE a.status_peserta = 'Aktif' AND a.company = b.company AND a.n_i_k = b.n_i_k AND (b.jenis_ahli_waris = 'Peserta' OR b.no_ahli_waris = 1) AND CALON_PENERIMA_MP_API.Get_Jenis_Kelamin2(b.COMPANY, b.N_I_K, b.NO_AHLI_WARIS) IS NULL)";

    $query4 = "SELECT sum(jmlh_islam) jmlh_islam, sum(jmlh_katolik) jmlh_katolik, sum(jmlh_budha) jmlh_budha, sum(jmlh_kristen) jmlh_kristen, sum(jmlh_hindu) jmlh_hindu, sum(jmlh_null) jmlh_agama_null, (sum(jmlh_islam) + sum(jmlh_katolik) + sum(jmlh_budha) + sum(jmlh_kristen) + sum(jmlh_hindu) + sum(jmlh_null)) total_pst_agama FROM (SELECT COUNT(a.n_i_k) jmlh_islam, 0 jmlh_katolik, 0 jmlh_budha, 0 jmlh_kristen, 0 jmlh_hindu, 0 jmlh_null FROM PESERTA_AKTIF a, CALON_PENERIMA_MP b WHERE a.status_peserta = 'Aktif' AND a.company = b.company AND a.n_i_k = b.n_i_k AND (b.jenis_ahli_waris = 'Peserta' OR b.no_ahli_waris = 1) AND CALON_PENERIMA_MP_API.Get_Agama(b.COMPANY, b.N_I_K, b.NO_AHLI_WARIS) = 'Islam' UNION ALL SELECT 0 jmlh_islam, COUNT(a.n_i_k) jmlh_katolik, 0 jmlh_budha, 0 jmlh_kristen, 0 jmlh_hindu, 0 jmlh_null FROM PESERTA_AKTIF a, CALON_PENERIMA_MP b WHERE a.status_peserta = 'Aktif' AND a.company = b.company AND a.n_i_k = b.n_i_k AND (b.jenis_ahli_waris = 'Peserta' OR b.no_ahli_waris = 1) AND CALON_PENERIMA_MP_API.Get_Agama(b.COMPANY, b.N_I_K, b.NO_AHLI_WARIS) = 'Katolik' UNION ALL SELECT 0 jmlh_islam, 0 jmlh_katolik, COUNT(a.n_i_k) jmlh_budha, 0 jmlh_kristen, 0 jmlh_hindu, 0 jmlh_null FROM PESERTA_AKTIF a, CALON_PENERIMA_MP b WHERE a.status_peserta = 'Aktif' AND a.company = b.company AND a.n_i_k = b.n_i_k AND (b.jenis_ahli_waris = 'Peserta' OR b.no_ahli_waris = 1) AND CALON_PENERIMA_MP_API.Get_Agama(b.COMPANY, b.N_I_K, b.NO_AHLI_WARIS) = 'Budha' UNION ALL SELECT 0 jmlh_islam, 0 jmlh_katolik, 0 jmlh_budha, COUNT(a.n_i_k) jmlh_kristen, 0 jmlh_hindu, 0 jmlh_null FROM PESERTA_AKTIF a, CALON_PENERIMA_MP b WHERE a.status_peserta = 'Aktif' AND a.company = b.company AND a.n_i_k = b.n_i_k AND (b.jenis_ahli_waris = 'Peserta' OR b.no_ahli_waris = 1) AND CALON_PENERIMA_MP_API.Get_Agama(b.COMPANY, b.N_I_K, b.NO_AHLI_WARIS) = 'Kristen' UNION ALL SELECT 0 jmlh_islam, 0 jmlh_katolik, 0 jmlh_budha, 0 jmlh_kristen, COUNT(a.n_i_k) jmlh_hindu, 0 jmlh_null FROM PESERTA_AKTIF a, CALON_PENERIMA_MP b WHERE a.status_peserta = 'Aktif' AND a.company = b.company AND a.n_i_k = b.n_i_k AND (b.jenis_ahli_waris = 'Peserta' OR b.no_ahli_waris = 1) AND CALON_PENERIMA_MP_API.Get_Agama(b.COMPANY, b.N_I_K, b.NO_AHLI_WARIS) = 'Hindu' UNION ALL SELECT 0 jmlh_islam, 0 jmlh_katolik, 0 jmlh_budha, 0 jmlh_kristen, 0 jmlh_hindu, COUNT(a.n_i_k) jmlh_null FROM PESERTA_AKTIF a, CALON_PENERIMA_MP b WHERE a.status_peserta = 'Aktif' AND a.company = b.company AND a.n_i_k = b.n_i_k AND (b.jenis_ahli_waris = 'Peserta' OR b.no_ahli_waris = 1) AND CALON_PENERIMA_MP_API.Get_Agama(b.COMPANY, b.N_I_K, b.NO_AHLI_WARIS) IS NULL)";

    $query5 = "SELECT sum(jmlh_o) jmlh_o, sum(jmlh_o_min) jmlh_o_min, sum(jmlh_ab) jmlh_ab, sum(jmlh_a) jmlh_a, sum(jmlh_b) jmlh_b, sum(jmlh_null) jmlh_goldar_null, (sum(jmlh_o) + sum(jmlh_o_min) + sum(jmlh_ab) + sum(jmlh_a) + sum(jmlh_b) + sum(jmlh_null)) total_pst_goldar FROM (SELECT COUNT(a.n_i_k) jmlh_o, 0 jmlh_o_min, 0 jmlh_ab, 0  jmlh_a, 0 jmlh_b, 0 jmlh_null FROM PESERTA_AKTIF a, CALON_PENERIMA_MP b WHERE a.status_peserta = 'Aktif' AND a.company = b.company AND a.n_i_k = b.n_i_k AND (b.jenis_ahli_waris = 'Peserta' OR b.no_ahli_waris = 1) AND CALON_PENERIMA_MP_API.Get_Gol_Darah(b.COMPANY, b.N_I_K, b.NO_AHLI_WARIS) = 'O' UNION ALL SELECT 0 jmlh_o, COUNT(a.n_i_k) jmlh_o_min, 0 jmlh_ab, 0 jmlh_a, 0 jmlh_b, 0 jmlh_null FROM PESERTA_AKTIF a, CALON_PENERIMA_MP b WHERE a.status_peserta = 'Aktif' AND a.company = b.company AND a.n_i_k = b.n_i_k AND (b.jenis_ahli_waris = 'Peserta' OR b.no_ahli_waris = 1) AND CALON_PENERIMA_MP_API.Get_Gol_Darah(b.COMPANY, b.N_I_K, b.NO_AHLI_WARIS) = 'O-' UNION ALL SELECT 0 jmlh_o, 0 jmlh_o_min, COUNT(a.n_i_k) jmlh_ab, 0 jmlh_a, 0 jmlh_b, 0 jmlh_null FROM PESERTA_AKTIF a, CALON_PENERIMA_MP b WHERE a.status_peserta = 'Aktif' AND a.company = b.company AND a.n_i_k = b.n_i_k AND (b.jenis_ahli_waris = 'Peserta' OR b.no_ahli_waris = 1) AND CALON_PENERIMA_MP_API.Get_Gol_Darah(b.COMPANY, b.N_I_K, b.NO_AHLI_WARIS) = 'AB' UNION ALL SELECT 0 jmlh_o, 0 jmlh_o_min, 0 jmlh_ab, COUNT(a.n_i_k) jmlh_a, 0 jmlh_b, 0 jmlh_null FROM PESERTA_AKTIF a, CALON_PENERIMA_MP b WHERE a.status_peserta = 'Aktif' AND a.company = b.company AND a.n_i_k = b.n_i_k AND (b.jenis_ahli_waris = 'Peserta' OR b.no_ahli_waris = 1) AND CALON_PENERIMA_MP_API.Get_Gol_Darah(b.COMPANY, b.N_I_K, b.NO_AHLI_WARIS) = 'A' UNION ALL SELECT 0 jmlh_o, 0 jmlh_o_min, 0 jmlh_ab, 0 jmlh_a, COUNT(a.n_i_k) jmlh_b, 0 jmlh_null FROM PESERTA_AKTIF a, CALON_PENERIMA_MP b WHERE a.status_peserta = 'Aktif' AND a.company = b.company AND a.n_i_k = b.n_i_k AND (b.jenis_ahli_waris = 'Peserta' OR b.no_ahli_waris = 1) AND CALON_PENERIMA_MP_API.Get_Gol_Darah(b.COMPANY, b.N_I_K, b.NO_AHLI_WARIS) = 'B' UNION ALL SELECT 0 jmlh_o, 0 jmlh_o_min, 0 jmlh_ab, 0 jmlh_a, 0 jmlh_b, COUNT(a.n_i_k) jmlh_null FROM PESERTA_AKTIF a, CALON_PENERIMA_MP b WHERE a.status_peserta = 'Aktif' AND a.company = b.company AND a.n_i_k = b.n_i_k AND (b.jenis_ahli_waris = 'Peserta' OR b.no_ahli_waris = 1) AND CALON_PENERIMA_MP_API.Get_Gol_Darah(b.COMPANY, b.N_I_K, b.NO_AHLI_WARIS) IS NULL)";

    $conn = getConnection();
    $sql1 = oci_parse($conn, $query1);
    $sql2 = oci_parse($conn, $query2);
    $sql3 = oci_parse($conn, $query3);
    $sql4 = oci_parse($conn, $query4);
    $sql5 = oci_parse($conn, $query5);

    oci_execute($sql1);
    oci_execute($sql2);
    oci_execute($sql3);
    oci_execute($sql4);
    oci_execute($sql5);
    
    $data1 = oci_fetch_array($sql1, OCI_ASSOC+OCI_RETURN_NULLS);
    $data2 = oci_fetch_array($sql2, OCI_ASSOC+OCI_RETURN_NULLS);
    $data3 = oci_fetch_array($sql3, OCI_ASSOC+OCI_RETURN_NULLS);
    $data4 = oci_fetch_array($sql4, OCI_ASSOC+OCI_RETURN_NULLS);
    $data5 = oci_fetch_array($sql5, OCI_ASSOC+OCI_RETURN_NULLS);

    $count1 = (int)$data1['JMLH_PST_AKTIF'];

    $count2 = (int)$data2['JMLH_PRA'];
    $count3 = (int)$data2['JMLH_PASCA'];
    $count4 = (int)$data2['JMLH_KD_KLMPK_NULL'];
    $count5 = (int)$data2['TOTAL_KD_KLMPK'];

    $count6 = (int)$data3['JMLH_PRIA'];
    $count7 = (int)$data3['JMLH_WANITA'];
    $count8 = (int)$data3['JMLH_JK_NULL'];
    $count9 = (int)$data3['TOTAL_JK'];

    $count10 = (int)$data4['JMLH_ISLAM'];
    $count11 = (int)$data4['JMLH_KATOLIK'];
    $count12 = (int)$data4['JMLH_BUDHA'];
    $count13 = (int)$data4['JMLH_KRISTEN'];
    $count14 = (int)$data4['JMLH_HINDU'];
    $count15 = (int)$data4['JMLH_AGAMA_NULL'];
    $count16 = (int)$data4['TOTAL_PST_AGAMA'];

    $count17 = (int)$data5['JMLH_O'];
    $count18 = (int)$data5['JMLH_O_MIN'];
    $count19 = (int)$data5['JMLH_AB'];
    $count20 = (int)$data5['JMLH_A'];
    $count21 = (int)$data5['JMLH_B'];
    $count22 = (int)$data5['JMLH_GOLDAR_NULL'];
    $count23 = (int)$data5['TOTAL_PST_GOLDAR'];

    $array1 = array("JMLH_PST_AKTIF_KLMPK_PRA"=>$count2,"JMLH_PST_AKTIF_KLMPK_PASCA"=>$count3,"JMLH_PST_AKTIF_KD_KLMPK_NULL"=>$count4,"TOTAL_PST_AKTIF_KD_KLMPK"=>$count5);
    $array2 = array("JMLH_PST_AKTIF_PRIA"=>$count6,"JMLH_PST_AKTIF_WANITA"=>$count7,"JMLH_PST_AKTIF_JK_NULL"=>$count8,"TOTAL_JK"=>$count9);
    $array3 = array("JMLH_PST_AKTIF_ISLAM"=>$count10,"JMLH_PST_AKTIF_KATOLIK"=>$count11,"JMLH_PST_AKTIF_BUDHA"=>$count12,"JMLH_PST_AKTIF_KRISTEN"=>$count13,"JMLH_PST_AKTIF_HINDU"=>$count14,"JMLH_PST_AKTIF_AGAMA_NULL"=>$count15,"TOTAL_PST_AKTIF_AGAMA"=>$count16);
    $array4 = array("JMLH_PST_AKTIF_O"=>$count17,"JMLH_PST_AKTIF_O_MIN"=>$count18,"JMLH_PST_AKTIF_AB"=>$count19,"JMLH_PST_AKTIF_A"=>$count20,"JMLH_PST_AKTIF_B"=>$count21,"JMLH_PST_AKTIF_GOLDAR_NULL"=>$count22,"TOTAL_PST_AKTIF_GOLDAR"=>$count23);
    
    $data = array("JMLH_PST_AKTIF"=>$count1,"PST_AKTIF_KD_KLMPK"=>$array1,"PST_AKTIF_JNS_KLMN"=>$array2,"PST_AKTIF_AGAMA"=>$array3,"PST_AKTIF_GOLDAR"=>$array4);

    if (!$conn) {
        $e = oci_error();
        return $response->withJson(["data" => null, "error" => $e, "status" => "success"], 200);
    } else {
        return $response->withJson(["data" => $data, "error" => null, "status" => "success"], 200);
    }
});

$app->get("/jmlh_pst_mantan_all/", function (Request $request, Response $response){
    $query1 = "SELECT COUNT(a.n_i_k) jmlh_pst_mantan FROM PESERTA_AKTIF a WHERE a.status_peserta = 'Mantan'";

    $query2 = "SELECT sum(jmlh_pra) jmlh_pra, sum(jmlh_pasca) jmlh_pasca, sum(jmlh_null) jmlh_kd_klmpk_null, (sum(jmlh_pra) + sum(jmlh_pasca) + sum(jmlh_null)) total_kd_klmpk FROM (SELECT COUNT(a.n_i_k) jmlh_pra, 0 jmlh_pasca, 0 jmlh_null FROM PESERTA_AKTIF a WHERE a.status_peserta = 'Mantan' AND (KELOMPOK_PESERTA_API.Get_Nama_Kelompok(a.company, a.kd_kelompok) LIKE '%PRA%') UNION ALL SELECT 0 jmlh_pra, COUNT(a.n_i_k) jmlh_pasca, 0 jmlh_null FROM PESERTA_AKTIF a WHERE a.status_peserta = 'Mantan' AND (KELOMPOK_PESERTA_API.Get_Nama_Kelompok(a.company, a.kd_kelompok) LIKE '%PASCA%') UNION ALL SELECT 0 jmlh_pra, 0 jmlh_pasca, COUNT(a.n_i_k) jmlh_null FROM PESERTA_AKTIF a WHERE a.status_peserta = 'Mantan' AND (a.kd_kelompok IS NULL))";

    $query3 = "SELECT sum(jmlh_pria) jmlh_pria, sum(jmlh_wanita) jmlh_wanita, sum(jmlh_null) jmlh_jk_null, (sum(jmlh_pria) + sum(jmlh_wanita) + sum(jmlh_null)) total_jk FROM (SELECT COUNT(a.n_i_k) jmlh_pria, 0 jmlh_wanita, 0 jmlh_null FROM PESERTA_AKTIF a, CALON_PENERIMA_MP b WHERE a.status_peserta = 'Mantan' AND a.company = b.company AND a.n_i_k = b.n_i_k AND (b.jenis_ahli_waris = 'Peserta' OR b.no_ahli_waris = 1) AND CALON_PENERIMA_MP_API.Get_Jenis_Kelamin2(b.COMPANY, b.N_I_K, b.NO_AHLI_WARIS) = 'Pria' UNION ALL SELECT 0 jmlh_pria, COUNT(a.n_i_k) jmlh_wanita, 0 jmlh_null FROM PESERTA_AKTIF a, CALON_PENERIMA_MP b WHERE a.status_peserta = 'Mantan' AND a.company = b.company AND a.n_i_k = b.n_i_k AND (b.jenis_ahli_waris = 'Peserta' OR b.no_ahli_waris = 1) AND CALON_PENERIMA_MP_API.Get_Jenis_Kelamin2(b.COMPANY, b.N_I_K, b.NO_AHLI_WARIS) = 'Wanita' UNION ALL SELECT 0 jmlh_pria, 0 jmlh_wanita, COUNT(a.n_i_k) jmlh_null FROM PESERTA_AKTIF a, CALON_PENERIMA_MP b WHERE a.status_peserta = 'Mantan' AND a.company = b.company AND a.n_i_k = b.n_i_k AND (b.jenis_ahli_waris = 'Peserta' OR b.no_ahli_waris = 1) AND CALON_PENERIMA_MP_API.Get_Jenis_Kelamin2(b.COMPANY, b.N_I_K, b.NO_AHLI_WARIS) IS NULL)";

    $query4 = "SELECT sum(jmlh_islam) jmlh_islam, sum(jmlh_katolik) jmlh_katolik, sum(jmlh_budha) jmlh_budha, sum(jmlh_kristen) jmlh_kristen, sum(jmlh_hindu) jmlh_hindu, sum(jmlh_null) jmlh_agama_null, (sum(jmlh_islam) + sum(jmlh_katolik) + sum(jmlh_budha) + sum(jmlh_kristen) + sum(jmlh_hindu) + sum(jmlh_null)) total_pst_agama FROM (SELECT COUNT(a.n_i_k) jmlh_islam, 0 jmlh_katolik, 0 jmlh_budha, 0 jmlh_kristen, 0 jmlh_hindu, 0 jmlh_null FROM PESERTA_AKTIF a, CALON_PENERIMA_MP b WHERE a.status_peserta = 'Mantan' AND a.company = b.company AND a.n_i_k = b.n_i_k AND (b.jenis_ahli_waris = 'Peserta' OR b.no_ahli_waris = 1) AND CALON_PENERIMA_MP_API.Get_Agama(b.COMPANY, b.N_I_K, b.NO_AHLI_WARIS) = 'Islam' UNION ALL SELECT 0 jmlh_islam, COUNT(a.n_i_k) jmlh_katolik, 0 jmlh_budha, 0 jmlh_kristen, 0 jmlh_hindu, 0 jmlh_null FROM PESERTA_AKTIF a, CALON_PENERIMA_MP b WHERE a.status_peserta = 'Mantan' AND a.company = b.company AND a.n_i_k = b.n_i_k AND (b.jenis_ahli_waris = 'Peserta' OR b.no_ahli_waris = 1) AND CALON_PENERIMA_MP_API.Get_Agama(b.COMPANY, b.N_I_K, b.NO_AHLI_WARIS) = 'Katolik' UNION ALL SELECT 0 jmlh_islam, 0 jmlh_katolik, COUNT(a.n_i_k) jmlh_budha, 0 jmlh_kristen, 0 jmlh_hindu, 0 jmlh_null FROM PESERTA_AKTIF a, CALON_PENERIMA_MP b WHERE a.status_peserta = 'Mantan' AND a.company = b.company AND a.n_i_k = b.n_i_k AND (b.jenis_ahli_waris = 'Peserta' OR b.no_ahli_waris = 1) AND CALON_PENERIMA_MP_API.Get_Agama(b.COMPANY, b.N_I_K, b.NO_AHLI_WARIS) = 'Budha' UNION ALL SELECT 0 jmlh_islam, 0 jmlh_katolik, 0 jmlh_budha, COUNT(a.n_i_k) jmlh_kristen, 0 jmlh_hindu, 0 jmlh_null FROM PESERTA_AKTIF a, CALON_PENERIMA_MP b WHERE a.status_peserta = 'Mantan' AND a.company = b.company AND a.n_i_k = b.n_i_k AND (b.jenis_ahli_waris = 'Peserta' OR b.no_ahli_waris = 1) AND CALON_PENERIMA_MP_API.Get_Agama(b.COMPANY, b.N_I_K, b.NO_AHLI_WARIS) = 'Kristen' UNION ALL SELECT 0 jmlh_islam, 0 jmlh_katolik, 0 jmlh_budha, 0 jmlh_kristen, COUNT(a.n_i_k) jmlh_hindu, 0 jmlh_null FROM PESERTA_AKTIF a, CALON_PENERIMA_MP b WHERE a.status_peserta = 'Mantan' AND a.company = b.company AND a.n_i_k = b.n_i_k AND (b.jenis_ahli_waris = 'Peserta' OR b.no_ahli_waris = 1) AND CALON_PENERIMA_MP_API.Get_Agama(b.COMPANY, b.N_I_K, b.NO_AHLI_WARIS) = 'Hindu' UNION ALL SELECT 0 jmlh_islam, 0 jmlh_katolik, 0 jmlh_budha, 0 jmlh_kristen, 0 jmlh_hindu, COUNT(a.n_i_k) jmlh_null FROM PESERTA_AKTIF a, CALON_PENERIMA_MP b WHERE a.status_peserta = 'Mantan' AND a.company = b.company AND a.n_i_k = b.n_i_k AND (b.jenis_ahli_waris = 'Peserta' OR b.no_ahli_waris = 1) AND CALON_PENERIMA_MP_API.Get_Agama(b.COMPANY, b.N_I_K, b.NO_AHLI_WARIS) IS NULL)";

    $query5 = "SELECT sum(jmlh_o) jmlh_o, sum(jmlh_o_min) jmlh_o_min, sum(jmlh_ab) jmlh_ab, sum(jmlh_a) jmlh_a, sum(jmlh_b) jmlh_b, sum(jmlh_null) jmlh_goldar_null, (sum(jmlh_o) + sum(jmlh_o_min) + sum(jmlh_ab) + sum(jmlh_a) + sum(jmlh_b) + sum(jmlh_null)) total_pst_goldar FROM (SELECT COUNT(a.n_i_k) jmlh_o, 0 jmlh_o_min, 0 jmlh_ab, 0  jmlh_a, 0 jmlh_b, 0 jmlh_null FROM PESERTA_AKTIF a, CALON_PENERIMA_MP b WHERE a.status_peserta = 'Mantan' AND a.company = b.company AND a.n_i_k = b.n_i_k AND (b.jenis_ahli_waris = 'Peserta' OR b.no_ahli_waris = 1) AND CALON_PENERIMA_MP_API.Get_Gol_Darah(b.COMPANY, b.N_I_K, b.NO_AHLI_WARIS) = 'O' UNION ALL SELECT 0 jmlh_o, COUNT(a.n_i_k) jmlh_o_min, 0 jmlh_ab, 0 jmlh_a, 0 jmlh_b, 0 jmlh_null FROM PESERTA_AKTIF a, CALON_PENERIMA_MP b WHERE a.status_peserta = 'Mantan' AND a.company = b.company AND a.n_i_k = b.n_i_k AND (b.jenis_ahli_waris = 'Peserta' OR b.no_ahli_waris = 1) AND CALON_PENERIMA_MP_API.Get_Gol_Darah(b.COMPANY, b.N_I_K, b.NO_AHLI_WARIS) = 'O-' UNION ALL SELECT 0 jmlh_o, 0 jmlh_o_min, COUNT(a.n_i_k) jmlh_ab, 0 jmlh_a, 0 jmlh_b, 0 jmlh_null FROM PESERTA_AKTIF a, CALON_PENERIMA_MP b WHERE a.status_peserta = 'Mantan' AND a.company = b.company AND a.n_i_k = b.n_i_k AND (b.jenis_ahli_waris = 'Peserta' OR b.no_ahli_waris = 1) AND CALON_PENERIMA_MP_API.Get_Gol_Darah(b.COMPANY, b.N_I_K, b.NO_AHLI_WARIS) = 'AB' UNION ALL SELECT 0 jmlh_o, 0 jmlh_o_min, 0 jmlh_ab, COUNT(a.n_i_k) jmlh_a, 0 jmlh_b, 0 jmlh_null FROM PESERTA_AKTIF a, CALON_PENERIMA_MP b WHERE a.status_peserta = 'Mantan' AND a.company = b.company AND a.n_i_k = b.n_i_k AND (b.jenis_ahli_waris = 'Peserta' OR b.no_ahli_waris = 1) AND CALON_PENERIMA_MP_API.Get_Gol_Darah(b.COMPANY, b.N_I_K, b.NO_AHLI_WARIS) = 'A' UNION ALL SELECT 0 jmlh_o, 0 jmlh_o_min, 0 jmlh_ab, 0 jmlh_a, COUNT(a.n_i_k) jmlh_b, 0 jmlh_null FROM PESERTA_AKTIF a, CALON_PENERIMA_MP b WHERE a.status_peserta = 'Mantan' AND a.company = b.company AND a.n_i_k = b.n_i_k AND (b.jenis_ahli_waris = 'Peserta' OR b.no_ahli_waris = 1) AND CALON_PENERIMA_MP_API.Get_Gol_Darah(b.COMPANY, b.N_I_K, b.NO_AHLI_WARIS) = 'B' UNION ALL SELECT 0 jmlh_o, 0 jmlh_o_min, 0 jmlh_ab, 0 jmlh_a, 0 jmlh_b, COUNT(a.n_i_k) jmlh_null FROM PESERTA_AKTIF a, CALON_PENERIMA_MP b WHERE a.status_peserta = 'Mantan' AND a.company = b.company AND a.n_i_k = b.n_i_k AND (b.jenis_ahli_waris = 'Peserta' OR b.no_ahli_waris = 1) AND CALON_PENERIMA_MP_API.Get_Gol_Darah(b.COMPANY, b.N_I_K, b.NO_AHLI_WARIS) IS NULL)";

    $conn = getConnection();
    $sql1 = oci_parse($conn, $query1);
    $sql2 = oci_parse($conn, $query2);
    $sql3 = oci_parse($conn, $query3);
    $sql4 = oci_parse($conn, $query4);
    $sql5 = oci_parse($conn, $query5);

    oci_execute($sql1);
    oci_execute($sql2);
    oci_execute($sql3);
    oci_execute($sql4);
    oci_execute($sql5);
    
    $data1 = oci_fetch_array($sql1, OCI_ASSOC+OCI_RETURN_NULLS);
    $data2 = oci_fetch_array($sql2, OCI_ASSOC+OCI_RETURN_NULLS);
    $data3 = oci_fetch_array($sql3, OCI_ASSOC+OCI_RETURN_NULLS);
    $data4 = oci_fetch_array($sql4, OCI_ASSOC+OCI_RETURN_NULLS);
    $data5 = oci_fetch_array($sql5, OCI_ASSOC+OCI_RETURN_NULLS);

    $count1 = (int)$data1['JMLH_PST_MANTAN'];

    $count2 = (int)$data2['JMLH_PRA'];
    $count3 = (int)$data2['JMLH_PASCA'];
    $count4 = (int)$data2['JMLH_KD_KLMPK_NULL'];
    $count5 = (int)$data2['TOTAL_KD_KLMPK'];

    $count6 = (int)$data3['JMLH_PRIA'];
    $count7 = (int)$data3['JMLH_WANITA'];
    $count8 = (int)$data3['JMLH_JK_NULL'];
    $count9 = (int)$data3['TOTAL_JK'];

    $count10 = (int)$data4['JMLH_ISLAM'];
    $count11 = (int)$data4['JMLH_KATOLIK'];
    $count12 = (int)$data4['JMLH_BUDHA'];
    $count13 = (int)$data4['JMLH_KRISTEN'];
    $count14 = (int)$data4['JMLH_HINDU'];
    $count15 = (int)$data4['JMLH_AGAMA_NULL'];
    $count16 = (int)$data4['TOTAL_PST_AGAMA'];

    $count17 = (int)$data5['JMLH_O'];
    $count18 = (int)$data5['JMLH_O_MIN'];
    $count19 = (int)$data5['JMLH_AB'];
    $count20 = (int)$data5['JMLH_A'];
    $count21 = (int)$data5['JMLH_B'];
    $count22 = (int)$data5['JMLH_GOLDAR_NULL'];
    $count23 = (int)$data5['TOTAL_PST_GOLDAR'];

    $array1 = array("JMLH_PST_MANTAN_KLMPK_PRA"=>$count2,"JMLH_PST_MANTAN_KLMPK_PASCA"=>$count3,"JMLH_PST_MANTAN_KD_KLMPK_NULL"=>$count4,"TOTAL_PST_MANTAN_KD_KLMPK"=>$count5);
    $array2 = array("JMLH_PST_MANTAN_PRIA"=>$count6,"JMLH_PST_MANTAN_WANITA"=>$count7,"JMLH_PST_MANTAN_JK_NULL"=>$count8,"TOTAL_JK"=>$count9);
    $array3 = array("JMLH_PST_MANTAN_ISLAM"=>$count10,"JMLH_PST_MANTAN_KATOLIK"=>$count11,"JMLH_PST_MANTAN_BUDHA"=>$count12,"JMLH_PST_MANTAN_KRISTEN"=>$count13,"JMLH_PST_MANTAN_HINDU"=>$count14,"JMLH_PST_MANTAN_AGAMA_NULL"=>$count15,"TOTAL_PST_MANTAN_AGAMA"=>$count16);
    $array4 = array("JMLH_PST_MANTAN_O"=>$count17,"JMLH_PST_MANTAN_O_MIN"=>$count18,"JMLH_PST_MANTAN_AB"=>$count19,"JMLH_PST_MANTAN_A"=>$count20,"JMLH_PST_MANTAN_B"=>$count21,"JMLH_PST_MANTAN_GOLDAR_NULL"=>$count22,"TOTAL_PST_MANTAN_GOLDAR"=>$count23);
    
    $data = array("JMLH_PST_MANTAN"=>$count1,"PST_MANTAN_KD_KLMPK"=>$array1,"PST_MANTAN_JNS_KLMN"=>$array2,"PST_MANTAN_AGAMA"=>$array3,"PST_MANTAN_GOLDAR"=>$array4);

    if (!$conn) {
        $e = oci_error();
        return $response->withJson(["data" => null, "error" => $e, "status" => "success"], 200);
    } else {
        return $response->withJson(["data" => $data, "error" => null, "status" => "success"], 200);
    }
});

function selectJmlhPstDapen($param, $prm_type, $pst_type) { //exp: {PRA, PASCA, NULL}, KD_KLMPK, "... AND ..."
    $conn = getConnection();
    
    $array = array();
    $sum2 = 0;
    $pst_alias = strtoupper($pst_type);

    for ($i = 0; $i < count($param); $i++) {
        if ($param[$i] == "NULL") {
            $prm_pst = "(PESERTA_API.Get_Kd_Kelompok(a.company, a.n_i_k) IS NULL)";
            $prm_name = $pst_alias."_DAPEN_NULL";
        } else {
            if ($param[$i] == "I") {
                $prm_pst = "(PESERTA_API.Get_Kd_Kelompok(a.COMPANY,a.N_I_K) = '01' OR PESERTA_API.Get_Kd_Kelompok(a.COMPANY,a.N_I_K) = '02' OR PESERTA_API.Get_Kd_Kelompok(a.COMPANY,a.N_I_K) = '03' OR PESERTA_API.Get_Kd_Kelompok(a.COMPANY,a.N_I_K) = '05' OR PESERTA_API.Get_Kd_Kelompok(a.COMPANY,a.N_I_K) = '06' OR PESERTA_API.Get_Kd_Kelompok(a.COMPANY,a.N_I_K) = '07')";
                $prm_name = $pst_alias."_DAPEN_I";
            } elseif ($param[$i] == "II") {
                $prm_pst = "(PESERTA_API.Get_Kd_Kelompok(a.COMPANY,a.N_I_K) = '04' OR PESERTA_API.Get_Kd_Kelompok(a.COMPANY,a.N_I_K) = '08')";
                $prm_name = $pst_alias."_DAPEN_II";
            }
        }
        
        $query2 = "SELECT COUNT(a.n_i_k) JMLH_PST_$prm_name FROM PESERTA_AKTIF a WHERE a.status_peserta = '".$pst_type."' AND $prm_pst";
        
        $sql2 = oci_parse($conn, $query2);
        oci_execute($sql2);
        $data2 = oci_fetch_array($sql2, OCI_ASSOC+OCI_RETURN_NULLS);
        $count2 = (int)$data2['JMLH_PST_'.$prm_name];
        $sum2 += $count2;
        
        $array['JMLH_PST_'.$prm_name] = $count2;
    }
    $array['TOTAL_PST_'.$prm_type] = $sum2;

    return $array;
}

?>