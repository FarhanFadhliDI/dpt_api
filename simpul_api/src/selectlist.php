<?php
include "connection.php";

function selectJmlhPmpAll($param, $prm_type, $pst, $param_name) {
    $conn = getConnection();
    
    $array = array();
    $sum2 = 0;

    for ($i = 0; $i < count($param); $i++) {
        if ($param[$i] == "NULL") {
            $prm_pst = $pst." IS NULL";
            if ($prm_type == "DAPEN") {
                $prm_name = $param_name."DAPEN_NULL";
            } else {
                $prm_name = $param_name."NULL";
            }
        } else {
            if ($prm_type == "KD_JNS_PENSIUN") {
                $query1 = "SELECT a.nama_jenis_pensiun FROM JENIS_PENSIUN_TAB a WHERE kd_jenis_pensiun = '".$param[$i]."'";$sql1 = oci_parse($conn, $query1);
                oci_execute($sql1);
                $data1 = oci_fetch_array($sql1, OCI_ASSOC+OCI_RETURN_NULLS);
                $name = $data1['NAMA_JENIS_PENSIUN'];

                $prm_pst = $pst." = '".$param[$i]."'";
                $prm_name = str_replace(" ", "_", $param[$i]); //$name
            } else {
                if ($prm_type == "KD_KLMPK") {
                    $prm_pst = $pst." LIKE '%".$param[$i]."%'";
                } else {
                    $prm_pst = $pst." = '".$param[$i]."'";
                }

                if ($param[$i] == "O-") {
                    $prm_name = $param_name."O_MIN";
                } else {
                    $prm_name = $param_name.str_replace(" ", "_", strtoupper($param[$i]));
                }
            }
        }

        $query2 = "SELECT COUNT(a.n_i_k) JMLH_$prm_name FROM PENERIMA_MP2 a WHERE NVL(PERIODE_API.Get_Tanggal_Awal_Periode('DPT02', 'null'), SYSDATE) BETWEEN valid_from AND valid_to AND $prm_pst";
        
        $sql2 = oci_parse($conn, $query2);
        oci_execute($sql2);
        $data2 = oci_fetch_array($sql2, OCI_ASSOC+OCI_RETURN_NULLS);
        $count2 = (int)$data2['JMLH_'.$prm_name];
        $sum2 += $count2;
        
        $array['JMLH_'.$prm_name] = $count2;
    }
    $array['TOTAL_PMP_'.$prm_type] = $sum2;

    return $array;
}

function selectPsngnPmpAll($param, $param_name) {
    $conn = getConnection();
    
    $array = array();
    $sum2 = 0;

    for ($i = 0; $i < count($param); $i++) {
        if ($param[$i] == "NULL") {
            //$prm_pst = $pst." IS NULL";
            $prm_name = "JMLH_PMP_PSNGN_NULL";
            $query2 = "SELECT COUNT(a.n_i_k) JMLH_PMP_PSNGN_NULL FROM PENERIMA_MP2 a WHERE NVL  (PERIODE_API.Get_Tanggal_Awal_Periode('DPT02', 'null'), SYSDATE) BETWEEN valid_from AND valid_to AND PESERTA_API.Get_N_I_K_Pasangan(a.company, a.n_i_k) IS NULL";
        } elseif ($param[$i] == "PMP") {
            $prm_name = "JMLH_PMP_PSNGN_PMP";
            $query2 = "SELECT COUNT(a.n_i_k) JMLH_PMP_PSNGN_PMP FROM PENERIMA_MP2 a WHERE NVL  (PERIODE_API.Get_Tanggal_Awal_Periode('DPT02', 'null'), SYSDATE) BETWEEN valid_from AND valid_to AND PESERTA_API.Get_N_I_K_Pasangan(a.company, a.n_i_k) IS NOT NULL AND PESERTA_API.Get_N_I_K_Pasangan(a.company, a.n_i_k) IN (SELECT b.n_i_k FROM PENERIMA_MP2 b)";
        } elseif ($param[$i] == "MPS") {
            $prm_name = "JMLH_PMP_PSNGN_MPS";
            $query2 = "SELECT NULL JMLH_PMP_PSNGN_MPS FROM DUAL";
            /*$query2 = "SELECT COUNT(a.n_i_k) JMLH_PMP_PSNGN_MPS FROM PENERIMA_MP2 a WHERE NVL  (PERIODE_API.Get_Tanggal_Awal_Periode('DPT02', 'null'), SYSDATE) BETWEEN valid_from AND valid_to AND PESERTA_API.Get_N_I_K_Pasangan(a.company, a.n_i_k) IS NOT NULL AND PESERTA_API.Get_N_I_K_Pasangan(a.company, a.n_i_k) IN (SELECT b.n_i_k FROM PENERIMAAN_MP b, PENERIMA_MP2 c WHERE b.n_i_k = c.n_i_k AND NVL(PERIODE_API.Get_Tanggal_Awal_Periode('DPT02', 'null'), SYSDATE) BETWEEN c.valid_from AND c.valid_to AND b.mps IS NOT NULL)";*/
        } elseif ($param[$i] == "AKTIF") {
            $prm_name = "JMLH_PMP_PSNGN_AKTIF";
            $query2 = "SELECT COUNT(a.n_i_k) JMLH_PMP_PSNGN_AKTIF FROM PENERIMA_MP2 a WHERE NVL  (PERIODE_API.Get_Tanggal_Awal_Periode('DPT02', 'null'), SYSDATE) BETWEEN valid_from AND valid_to AND PESERTA_API.Get_N_I_K_Pasangan(a.company, a.n_i_k) IS NOT NULL AND PESERTA_API.Get_N_I_K_Pasangan(a.company, a.n_i_k) IN (SELECT b.n_i_k FROM PESERTA_AKTIF b WHERE b.status_peserta = 'Aktif')";
        } elseif ($param[$i] == "HAPUS") {
            $prm_name = "JMLH_PMP_PSNGN_HAPUS";
            $query2 = "SELECT COUNT(a.n_i_k) JMLH_PMP_PSNGN_HAPUS FROM PENERIMA_MP2 a WHERE NVL  (PERIODE_API.Get_Tanggal_Awal_Periode('DPT02', 'null'), SYSDATE) BETWEEN valid_from AND valid_to AND PESERTA_API.Get_N_I_K_Pasangan(a.company, a.n_i_k) IS NOT NULL AND PESERTA_API.Get_N_I_K_Pasangan(a.company, a.n_i_k) IN (SELECT b.n_i_k FROM HAPUS_PENERIMA_MP b)";
        }
        
        $sql2 = oci_parse($conn, $query2);
        oci_execute($sql2);
        $data2 = oci_fetch_array($sql2, OCI_ASSOC+OCI_RETURN_NULLS);
        $count2 = (int)$data2[$prm_name];
        $sum2 += $count2;
        
        $array[$prm_name] = $count2;
    }
    $array['TOTAL_PMP_PSNGN'] = $sum2;

    return $array;
}

function jmlhPmp($periode, $jmlh_aw, $jenis_aw, $kd_jenis_pensiun, $kd_kelompok, $dapen, $jenis_kelamin, $agama, $gol_darah, $n_i_k, $tgl_kerja, $tgl_lahir, $tgl_capeg, $tgl_berhenti, $kd_band, $pasangan_mp, $jns_api) {
    $conn = getConnection();
    $prm_where = "";

        $conn = getConnection();

        if ($jmlh_aw != '') {
            //$prm_where = " AND ";
        }
        elseif ($jenis_aw != '') {
            $prm_where = $prm_where." AND CALON_PENERIMA_MP_API.Get_Jenis_Ahli_Waris(a.company, a.n_i_k, a.no_ahli_waris) = INITCAP('".$jenis_aw."')";
        }
        elseif ($kd_jenis_pensiun != '') {
            $prm_where = $prm_where." AND a.kd_jenis_pensiun = '".$kd_jenis_pensiun."'";
        }
        elseif ($kd_kelompok != '') {
            $prm_where = $prm_where." AND KELOMPOK_PESERTA_API.Get_Nama_Kelompok(a.company, PESERTA_API.Get_Kd_Kelompok(a.company, a.n_i_k)) LIKE UPPER('%".$kd_kelompok."%')";
        }
        elseif ($dapen != '') {
            $prm_where = $prm_where." AND KELOMPOK_PESERTA_API.Get_Kelompok_Dapen(a.company, PESERTA_API.Get_Kd_Kelompok(a.company, a.n_i_k)) = UPPER('".$dapen."')";
        } 
        elseif ($jenis_kelamin != '') {
            $prm_where = $prm_where." AND CALON_PENERIMA_MP_API.Get_Jenis_Kelamin2(a.COMPANY, a.N_I_K, a.no_ahli_waris) = INITCAP('".$jenis_kelamin."')";
        }
        elseif ($agama != '') {
            $prm_where = $prm_where." AND CALON_PENERIMA_MP_API.Get_Agama(a.COMPANY, a.N_I_K, a.no_ahli_waris) = INITCAP('".$agama."')";
        }
        elseif ($gol_darah != '') {
            $prm_where = $prm_where." AND CALON_PENERIMA_MP_API.Get_Gol_Darah(a.COMPANY, a.N_I_K, a.no_ahli_waris) = UPPER('".$goldar."')";
        }
        elseif ($tgl_kerja != '') {
            $prm_where = $prm_where." AND CALON_PENERIMA_MP_API.Get_Tgl_Bekerja(a.COMPANY, a.N_I_K, a.no_ahli_waris) = to_date('".$tgl_kerja."', 'dd/mm/yyyy')";
        }
        elseif ($tgl_lahir != '') {
            $prm_where = $prm_where." AND CALON_PENERIMA_MP_API.Get_Tgl_Lahir(a.COMPANY, a.N_I_K, a.no_ahli_waris) = to_date('".$tgl_lahir."', 'dd/mm/yyyy')";
        }
        elseif ($tgl_capeg != '') {
            $prm_where = $prm_where." AND PESERTA_API.Get_Tgl_Capeg(a.COMPANY, a.N_I_K) = to_date('".$tgl_capeg."', 'dd/mm/yyyy')";
        }
        elseif ($tgl_berhenti != '') {
            $prm_where = $prm_where." AND PESERTA_API.Get_Tgl_Berhenti(a.COMPANY, a.N_I_K) = to_date('".$tgl_berhenti."', 'dd/mm/yyyy')";
        }
        elseif ($kd_band != '') {
            $prm_where = $prm_where." AND PESERTA_API.Get_Kd_Band(a.COMPANY, a.N_I_K) = UPPER('".$kd_band."')";
        }
        elseif ($pasangan_mp != '') {
            if (strtoupper($pasangan_mp) == "PMP") {
                $prm_where = " AND PESERTA_API.Get_N_I_K_Pasangan(a.company, a.n_i_k) IS NOT NULL AND PESERTA_API.Get_N_I_K_Pasangan(a.company, a.n_i_k) IN (SELECT b.n_i_k FROM PENERIMA_MP2 b)";
            } elseif (strtoupper($pasangan_mp) == "MPS") {
                $prm_where = "";
                /*$query2 = "SELECT COUNT(a.n_i_k) JMLH_PMP_PSNGN_MPS FROM PENERIMA_MP2 a WHERE NVL  (PERIODE_API.Get_Tanggal_Awal_Periode('DPT02', 'null'), SYSDATE) BETWEEN valid_from AND valid_to AND PESERTA_API.Get_N_I_K_Pasangan(a.company, a.n_i_k) IS NOT NULL AND PESERTA_API.Get_N_I_K_Pasangan(a.company, a.n_i_k) IN (SELECT b.n_i_k FROM PENERIMAAN_MP b, PENERIMA_MP2 c WHERE b.n_i_k = c.n_i_k AND NVL(PERIODE_API.Get_Tanggal_Awal_Periode('DPT02', 'null'), SYSDATE) BETWEEN c.valid_from AND c.valid_to AND b.mps IS NOT NULL)";*/
            } elseif (strtoupper($pasangan_mp) == "AKTIF") {
                $prm_where = " AND PESERTA_API.Get_N_I_K_Pasangan(a.company, a.n_i_k) IS NOT NULL AND PESERTA_API.Get_N_I_K_Pasangan(a.company, a.n_i_k) IN (SELECT b.n_i_k FROM PESERTA_AKTIF b WHERE b.status_peserta = 'Aktif')";
            } elseif (strtoupper($pasangan_mp) == "HAPUS") {
                $prm_where = " AND valid_to AND PESERTA_API.Get_N_I_K_Pasangan(a.company, a.n_i_k) IS NOT NULL AND PESERTA_API.Get_N_I_K_Pasangan(a.company, a.n_i_k) IN (SELECT b.n_i_k FROM HAPUS_PENERIMA_MP b)";
            }
        }

        if (strtoupper($n_i_k) == 'TRUE') {
            if ($jns_api == 'jmlh') {
                $query2 = "SELECT COUNT(DISTINCT(a.n_i_k)) JMLH_PMP FROM PENERIMA_MP2 a WHERE NVL(PERIODE_API.Get_Tanggal_Awal_Periode('DPT02', '".$periode."'), SYSDATE) BETWEEN valid_from AND valid_to$prm_where";
            }
            elseif ($jns_api == 'list') {
                $query2 = "SELECT a.n_i_k, CALON_PENERIMA_MP_API.Get_Nama_Ahli_Waris(a.company,a.n_i_k,a.no_ahli_waris) nama, CALON_PENERIMA_MP_API.Get_Jenis_Ahli_Waris(a.company, a.n_i_k, a.no_ahli_waris) jenis_aw, a.kd_jenis_pensiun, JENIS_PENSIUN_API.Get_Nama_Jenis_Pensiun(a.company,a.kd_jenis_pensiun) jenis_pensiun, PESERTA_API.Get_Kd_Kelompok(a.company,a.n_i_k) kd_kelompok, KELOMPOK_PESERTA_API.Get_Nama_Kelompok(a.company,PESERTA_API.Get_Kd_Kelompok(a.company,a.n_i_k)) nama_kelompok, KELOMPOK_PESERTA_API.Get_Kelompok_Dapen(a.company, PESERTA_API.Get_Kd_Kelompok(a.company,a.n_i_k))kelompok_dapen, CALON_PENERIMA_MP_API.Get_Jenis_Kelamin2(a.company,a.n_i_k,a.no_ahli_waris) jenis_kelamin, CALON_PENERIMA_MP_API.Get_Agama(a.company,a.n_i_k,a.no_ahli_waris) agama, CALON_PENERIMA_MP_API.Get_Gol_Darah(a.company,a.n_i_k,a.no_ahli_waris) gol_darah, CALON_PENERIMA_MP_API.Get_Tgl_Bekerja(a.company,a.n_i_k,a.no_ahli_waris) tgl_kerja, CALON_PENERIMA_MP_API.Get_Tgl_Lahir(a.company,a.n_i_k,a.no_ahli_waris) tgl_lahir, PESERTA_API.Get_Tgl_Capeg(a.company,a.n_i_k) tgl_capeg, PESERTA_API.Get_Tgl_Berhenti(a.company,a.n_i_k) tgl_berhenti, PESERTA_API.Get_Kd_Band(a.company,a.n_i_k) kd_band, PESERTA_API.Get_N_I_K_Pasangan(a.company,a.n_i_k) pasangan_pmp FROM PENERIMA_MP2 a WHERE NVL(PERIODE_API.Get_Tanggal_Awal_Periode('DPT02', $periode), SYSDATE) BETWEEN a.valid_from AND a.valid_to AND a.no_ahli_waris = 1";
            }
        }
        elseif (strtoupper($n_i_k) == 'FALSE') {
            if ($jns_api == 'jmlh') {
                $query2 = "SELECT COUNT(a.n_i_k) JMLH_PMP FROM PENERIMA_MP2 a WHERE NVL(PERIODE_API.Get_Tanggal_Awal_Periode('DPT02', '".$periode."'), SYSDATE) BETWEEN valid_from AND valid_to$prm_where";
            }
            elseif ($jns_api == 'list') {
                $query2 = "SELECT a.n_i_k, CALON_PENERIMA_MP_API.Get_Nama_Ahli_Waris(a.company,a.n_i_k,a.no_ahli_waris) nama, CALON_PENERIMA_MP_API.Get_Jenis_Ahli_Waris(a.company, a.n_i_k, a.no_ahli_waris) jenis_aw, a.kd_jenis_pensiun, JENIS_PENSIUN_API.Get_Nama_Jenis_Pensiun(a.company,a.kd_jenis_pensiun) jenis_pensiun, PESERTA_API.Get_Kd_Kelompok(a.company,a.n_i_k) kd_kelompok, KELOMPOK_PESERTA_API.Get_Nama_Kelompok(a.company,PESERTA_API.Get_Kd_Kelompok(a.company,a.n_i_k)) nama_kelompok, KELOMPOK_PESERTA_API.Get_Kelompok_Dapen(a.company, PESERTA_API.Get_Kd_Kelompok(a.company,a.n_i_k))kelompok_dapen, CALON_PENERIMA_MP_API.Get_Jenis_Kelamin2(a.company,a.n_i_k,a.no_ahli_waris) jenis_kelamin, CALON_PENERIMA_MP_API.Get_Agama(a.company,a.n_i_k,a.no_ahli_waris) agama, CALON_PENERIMA_MP_API.Get_Gol_Darah(a.company,a.n_i_k,a.no_ahli_waris) gol_darah, CALON_PENERIMA_MP_API.Get_Tgl_Bekerja(a.company,a.n_i_k,a.no_ahli_waris) tgl_kerja, CALON_PENERIMA_MP_API.Get_Tgl_Lahir(a.company,a.n_i_k,a.no_ahli_waris) tgl_lahir, PESERTA_API.Get_Tgl_Capeg(a.company,a.n_i_k) tgl_capeg, PESERTA_API.Get_Tgl_Berhenti(a.company,a.n_i_k) tgl_berhenti, PESERTA_API.Get_Kd_Band(a.company,a.n_i_k) kd_band, PESERTA_API.Get_N_I_K_Pasangan(a.company,a.n_i_k) pasangan_pmp FROM PENERIMA_MP2 a WHERE NVL(PERIODE_API.Get_Tanggal_Awal_Periode('DPT02', $periode), SYSDATE) BETWEEN a.valid_from AND a.valid_to";
            }
        }
        
        $sql2 = oci_parse($conn, $query2);
        oci_execute($sql2);
        $data2 = oci_fetch_array($sql2, OCI_ASSOC+OCI_RETURN_NULLS);
        $count2 = (int)$data2['JMLH_PMP'];

    return $count2;
}

function selectJmlhPstAll($param, $prm_type, $pst, $pst_type) { //exp: {PRA, PASCA, NULL}, KD_KLMPK, "... AND ..."
    $conn = getConnection();
    
    $array = array();
    $sum2 = 0;
    $pst_alias = strtoupper($pst_type);

    for ($i = 0; $i < count($param); $i++) {
        if ($param[$i] == "NULL") {
            $prm_pst = $pst." IS NULL";
            $prm_pst2 = $prm_pst;
            $prm_name = $pst_alias."_".$prm_type."_NULL";
        } else {
            $prm_pst = $pst." LIKE '%".$param[$i]."%'";
            $prm_pst2 = $pst." = '".$param[$i]."'";
            if ($param[$i] == "O-") {
                $prm_name = $pst_alias."_O_MIN";
            } else {
                $prm_name = $pst_alias."_".strtoupper($param[$i]);
            }

            if ($prm_type == "DAPEN") {
                $prm_name = $pst_alias."_".str_replace(" ", "_", $param[$i]);
            }
        }

        if ($prm_type == "KD_KLMPK") {
            $query2 = "SELECT COUNT(a.n_i_k) JMLH_PST_$prm_name FROM PESERTA_AKTIF a WHERE a.status_peserta = '".$pst_type."' AND $prm_pst";
        } else {
            $query2 = "SELECT COUNT(a.n_i_k) JMLH_PST_$prm_name FROM PESERTA_AKTIF a WHERE a.status_peserta = '".$pst_type."' AND $prm_pst2";
        }
        
        $sql2 = oci_parse($conn, $query2);
        oci_execute($sql2);
        $data2 = oci_fetch_array($sql2, OCI_ASSOC+OCI_RETURN_NULLS);
        $count2 = (int)$data2['JMLH_PST_'.$prm_name];
        $sum2 += $count2;
        
        $array['JMLH_PST_'.$prm_name] = $count2;
    }
    $array['TOTAL_PST_'.$prm_type] = $sum2;

    return $array;
}

function jmlhPst($jns_api, $pst_type, $kd_klmpk, $dapen, $jk, $agama, $goldar) {
    $conn = getConnection();
    $pst_alias = strtolower($pst_type);

    if ($kd_klmpk != '') {
        $kd_klmpk_query = " AND a.kd_kelompok = '".$kd_klmpk."'";
    } else {
        $kd_klmpk_query = "";
    }

    if ($dapen != '') {
        $dapen_query = " AND KELOMPOK_PESERTA_API.Get_Kelompok_Dapen(a.company, a.kd_kelompok) = UPPER('".$dapen."')";
    } else {
        $dapen_query = "";
    }

    if ($jk != '') {
        $jk_query = " AND CALON_PENERIMA_MP_API.Get_Jenis_Kelamin2(a.COMPANY, a.N_I_K, 1) = INITCAP('".$jk."')";
    } else {
        $jk_query = "";
    }

    if ($agama != '') {
        $agama_query = " AND CALON_PENERIMA_MP_API.Get_Agama(a.COMPANY, a.N_I_K, 1) = INITCAP('".$agama."')";
    } else {
        $agama_query = "";
    }

    if ($goldar != '') {
        $goldar_query = " AND CALON_PENERIMA_MP_API.Get_Gol_Darah(a.COMPANY, a.N_I_K, 1) = UPPER('".$goldar."')";
    } else {
        $goldar_query = "";
    }

    if ($jns_api == "Jumlah") {
        $query = "SELECT COUNT(*) jmlh_pst_$pst_alias FROM PESERTA_AKTIF a WHERE a.status_peserta = INITCAP('".$pst_type."')".$kd_klmpk_query.$dapen_query.$jk_query.$agama_query.$goldar_query."";

        $sql = oci_parse($conn, $query);
        oci_execute($sql);
        $data1 = oci_fetch_array($sql, OCI_ASSOC+OCI_RETURN_NULLS);
        $result = (int)$data1['JMLH_PST_'.strtoupper($pst_alias)];
        
    } elseif ($jns_api == "List") {
        if ($pst_type == "Aktif") {
            $klmpk_dapen = "";
        } elseif($pst_type == "Mantan") {
            $klmpk_dapen = " KELOMPOK_PESERTA_API.Get_Kelompok_Dapen(a.company, a.kd_kelompok) kelompok_dapen,";
        }
        $query = "SELECT a.n_i_k, a.nama_pegawai nama, a.kd_kelompok, KELOMPOK_PESERTA_API.Get_Nama_Kelompok(a.company, a.kd_kelompok) nama_kelompok,$klmpk_dapen CALON_PENERIMA_MP_API.Get_Jenis_Kelamin2(a.COMPANY, a.N_I_K, 1) jenis_kelamin, CALON_PENERIMA_MP_API.Get_Agama(a.COMPANY, a.N_I_K, 1) agama, CALON_PENERIMA_MP_API.Get_Gol_Darah(a.COMPANY, a.N_I_K, 1) gol_darah FROM PESERTA_AKTIF a WHERE a.status_peserta = INITCAP('".$pst_type."')".$kd_klmpk_query.$dapen_query.$jk_query.$agama_query.$goldar_query."";

        $sql = oci_parse($conn, $query);
        oci_execute($sql);
        
        $rows = array();
        while ($dt = oci_fetch_assoc($sql)) {
            $result[] = $dt;
        }
    }

    return $result;
}

function pmpTangguh($jns_api, $periode, $usia_tangguh, $saldo_awal, $penambahan, $sls_bln_brjln, $sls_periode_sblm, $total_penyelesaian, $saldo_akhir) {
    $conn = getConnection();

    if ($periode != '') {
        $periode_query = " AND a.periode_tangguh = '".$periode."'";
    } else {
        $periode_query = "";
    }

    if ($usia_tangguh != '') {
        $usia_tangguh_query = " AND a.usia_tangguhan_daftar = '".$usia_tangguh."'";
    } else {
        $usia_tangguh_query = "";
    }

    if ($saldo_awal != '') {
        $saldo_awal_query = " AND a.saldo_awal_tangguhan = '".$saldo_awal."'";
    } else {
        $saldo_awal_query = "";
    }

    if ($penambahan != '') {
        $penambahan_query = ""; // -- //
    } else {
        $penambahan_query = "";
    }

    if ($sls_bln_brjln != '') {
        $sls_bln_brjln_query = ""; // -- //
    } else {
        $sls_bln_brjln_query = "";
    }

    if ($sls_periode_sblm != '') {
        $sls_periode_sblm_query = "";
    } else {
        $sls_periode_sblm_query = "";
    }

    if ($total_penyelesaian != '') {
        $total_penyelesaian_query = "";
    } else {
        $total_penyelesaian_query = "";
    }

    if ($saldo_akhir != '') {
        $saldo_akhir_query = "";
    } else {
        $saldo_akhir_query = "";
    }

    if ($jns_api == "Jumlah") {
        $query = "SELECT COUNT(*) jmlh_pmp_tangguh FROM DAFTAR_PST_TANGGUH a WHERE (a.state = 'Disetujui' OR a.state = 'Diproses')/* AND a.periode_selesai IS NULL*/".$periode_query.$usia_tangguh_query.$saldo_awal_query.$penambahan_query.$sls_bln_brjln_query.$sls_periode_sblm_query.$total_penyelesaian_query.$saldo_akhir_query."";

        $sql = oci_parse($conn, $query);
        oci_execute($sql);
        $data1 = oci_fetch_array($sql, OCI_ASSOC+OCI_RETURN_NULLS);
        $result = (int)$data1['JMLH_PMP_TANGGUH'];

    } elseif ($jns_api == "List") {
        $query = "SELECT a.n_i_k, CALON_PENERIMA_MP_API.Get_Nama_Ahli_Waris(a.company,a.n_i_k,a.no_ahli_waris) nama/*, kd_bank, nama_bank, cabang_bank, no_rekening_bank, nama_pemilik_rekening, no_sppb*/ FROM DAFTAR_PST_TANGGUH a WHERE (a.state = 'Disetujui' OR a.state = 'Diproses') AND a.periode_selesai IS NULL".$periode_query.$usia_tangguh_query.$saldo_awal_query.$penambahan_query.$sls_bln_brjln_query.$sls_periode_sblm_query.$total_penyelesaian_query.$saldo_akhir_query."";

        $sql = oci_parse($conn, $query);
        oci_execute($sql);
        
        $rows = array();
        while ($dt = oci_fetch_assoc($sql)) {
            $result[] = $dt;
        }
    } elseif ($jns_api == "Nilai") {
        $query = "SELECT SUM(a.total_tangguhan_view) jmlh_nilai_tangguh FROM DAFTAR_PST_TANGGUH a WHERE (a.state = 'Disetujui' OR a.state = 'Diproses') AND a.periode_selesai IS NULL".$periode_query.$usia_tangguh_query.$saldo_awal_query.$penambahan_query.$sls_bln_brjln_query.$sls_periode_sblm_query.$total_penyelesaian_query.$saldo_akhir_query."";

        $sql = oci_parse($conn, $query);
        oci_execute($sql);
        $data1 = oci_fetch_array($sql, OCI_ASSOC+OCI_RETURN_NULLS);
        $result = (int)$data1['JMLH_NILAI_TANGGUH'];
    }

    return $result;
}

function jmlhPmpP2tel($select, $periode) {
    $conn = getConnection();
    
    $query = "SELECT COUNT(a.n_i_k) JMLH_ANGGOTA_PMP FROM PENERIMA_MP2 a WHERE NVL(PERIODE_API.Get_Tanggal_Awal_Periode('DPT02', '".$periode."'), SYSDATE) BETWEEN a.valid_from AND a.valid_to $select";
    
    $sql = oci_parse($conn, $query);
    oci_execute($sql);
    $data = oci_fetch_array($sql, OCI_ASSOC+OCI_RETURN_NULLS);
    $count = (int)$data['JMLH_ANGGOTA_PMP'];

    return $count;
}

function jmlhPengurusP2tel($periode, $kd_cabang) {
    $conn = getConnection();

    $ketua = "SELECT SUM(a.n_i_k) KETUA FROM KONTAK_MITRA_KERJA a WHERE NVL(PERIODE_API.Get_Tanggal_Awal_Periode('DPT02', '".$periode."'), SYSDATE) BETWEEN a.valid_from AND a.valid_to AND a.kd_mitra_kerja = UPPER('".$kd_cabang."') AND UPPER(a.jabatan) LIKE '%KETUA%'";
    
    $ketua_sql = oci_parse($conn, $ketua);
    oci_execute($ketua_sql);
    $ketua_data = oci_fetch_array($ketua_sql, OCI_ASSOC+OCI_RETURN_NULLS);
    $ketua_count = (int)$ketua_data['KETUA'];

    $wakil_ketua = "SELECT SUM(a.n_i_k) WAKIL_KETUA FROM KONTAK_MITRA_KERJA a WHERE NVL(PERIODE_API.Get_Tanggal_Awal_Periode('DPT02', '".$periode."'), SYSDATE) BETWEEN a.valid_from AND a.valid_to AND a.kd_mitra_kerja = UPPER('".$kd_cabang."') AND UPPER(a.jabatan) LIKE '%WAKIL KETUA%'";
    
    $wakil_ketua_sql = oci_parse($conn, $wakil_ketua);
    oci_execute($wakil_ketua_sql);
    $wakil_ketua_data = oci_fetch_array($wakil_ketua_sql, OCI_ASSOC+OCI_RETURN_NULLS);
    $wakil_ketua_count = (int)$wakil_ketua_data['WAKIL_KETUA'];

    $bendahara = "SELECT SUM(a.n_i_k) BENDAHARA FROM KONTAK_MITRA_KERJA a WHERE NVL(PERIODE_API.Get_Tanggal_Awal_Periode('DPT02', '".$periode."'), SYSDATE) BETWEEN a.valid_from AND a.valid_to AND a.kd_mitra_kerja = UPPER('".$kd_cabang."') AND UPPER(a.jabatan) LIKE '%BENDAHARA%'";
    
    $bendahara_sql = oci_parse($conn, $bendahara);
    oci_execute($bendahara_sql);
    $bendahara_data = oci_fetch_array($bendahara_sql, OCI_ASSOC+OCI_RETURN_NULLS);
    $bendahara_count = (int)$bendahara_data['BENDAHARA'];

    $sekretaris = "SELECT SUM(a.n_i_k) SEKRETARIS FROM KONTAK_MITRA_KERJA a WHERE NVL(PERIODE_API.Get_Tanggal_Awal_Periode('DPT02', '".$periode."'), SYSDATE) BETWEEN a.valid_from AND a.valid_to AND a.kd_mitra_kerja = UPPER('".$kd_cabang."') AND UPPER(a.jabatan) LIKE '%SEKRETARIS%'";
    
    $sekretaris_sql = oci_parse($conn, $sekretaris);
    oci_execute($sekretaris_sql);
    $sekretaris_data = oci_fetch_array($sekretaris_sql, OCI_ASSOC+OCI_RETURN_NULLS);
    $sekretaris_count = (int)$sekretaris_data['SEKRETARIS'];

    $bdn_pengawas = "SELECT SUM(a.n_i_k) BADAN_PENGAWAS FROM KONTAK_MITRA_KERJA a WHERE NVL(PERIODE_API.Get_Tanggal_Awal_Periode('DPT02', '".$periode."'), SYSDATE) BETWEEN a.valid_from AND a.valid_to AND a.kd_mitra_kerja = UPPER('".$kd_cabang."') AND UPPER(a.jabatan) LIKE '%BADAN PENGAWAS%'";
    
    $bdn_pengawas_sql = oci_parse($conn, $bdn_pengawas);
    oci_execute($bdn_pengawas_sql);
    $bdn_pengawas_data = oci_fetch_array($bdn_pengawas_sql, OCI_ASSOC+OCI_RETURN_NULLS);
    $bdn_pengawas_count = (int)$bdn_pengawas_data['BADAN_PENGAWAS'];
    
    $komisariat = "SELECT SUM(a.n_i_k) BADAN_PENGAWAS FROM KONTAK_MITRA_KERJA a WHERE NVL(PERIODE_API.Get_Tanggal_Awal_Periode('DPT02', '".$periode."'), SYSDATE) BETWEEN a.valid_from AND a.valid_to AND a.kd_mitra_kerja = UPPER('".$kd_cabang."') AND UPPER(a.jabatan) LIKE '%KOMISARIAT%'";
    
    $komisariat_sql = oci_parse($conn, $komisariat);
    oci_execute($komisariat_sql);
    $komisariat_data = oci_fetch_array($komisariat_sql, OCI_ASSOC+OCI_RETURN_NULLS);
    $komisariat_count = (int)$komisariat_data['BADAN_PENGAWAS'];
    
    $query1 = "SELECT '".$periode."' periode, a.kd_mitra_kerja kd_cabang, MITRA_KERJA_API.Get_Nama_Mitra_Kerja(a.company, a.kd_mitra_kerja) nama_cabang, $ketua_count ketua, $wakil_ketua_count wakil_ketua, $bendahara_count bendahara, $sekretaris_count sekretaris, $bdn_pengawas_count badan_pengawas, $komisariat_count komisariat FROM KONTAK_MITRA_KERJA a WHERE NVL(PERIODE_API.Get_Tanggal_Awal_Periode('DPT02', '".$periode."'), SYSDATE) BETWEEN a.valid_from AND a.valid_to AND a.kd_mitra_kerja = UPPER('".$kd_cabang."')";
    
    $sql1 = oci_parse($conn, $query1);
    oci_execute($sql1);
    
    $rows = array();
    while ($dt = oci_fetch_assoc($sql1)) {
        $rows[] = $dt;
    }

    return $rows;
}

// ----- Login and Get Log Functions ----- //

function getIfsUser($username) {
    $conn = getConnection();
    $query_ifs_user = "SELECT a.ifs_user FROM CTM_USER_LOGIN_API_TAB a WHERE a.username = '".$username."'";
    
    $sql_ifs_user = oci_parse($conn, $query_ifs_user);
    oci_execute($sql_ifs_user);
    $data_ifs_user = oci_fetch_array($sql_ifs_user, OCI_ASSOC+OCI_RETURN_NULLS);
    
    $ifs_user = $data_ifs_user['IFS_USER'];

    return $ifs_user;
}

function checkAccess($ifs_user, $api_id) {
    $conn = getConnection();
    $query_check_acccess = "SELECT 1 CHECK_ACCESS FROM CTM_USER_API_MAPPING a WHERE a.ifs_user = '".$ifs_user."' AND a.api_id = '".$api_id."'";
    
    $sql_check_acccess = oci_parse($conn, $query_check_acccess);
    oci_execute($sql_check_acccess);
    $data_check_acccess = oci_fetch_array($sql_check_acccess, OCI_ASSOC+OCI_RETURN_NULLS);
    $count_check_acccess = (int)$data_check_acccess['CHECK_ACCESS'];
    
    if ($count_check_acccess == 1) {
        $access = "true";
    } else {
        $access = "false";
    }

    return $access;
}

function insertUserLog($username, $api_id, $id_address) {
    $conn = getConnection();
    $query_get_log_id = "SELECT (NVL(LOG_ID,0)+1) LOG_ID FROM (SELECT LOG_ID FROM CTM_USER_LOG_API_TAB WHERE ROWNUM = 1 ORDER BY LOG_ID DESC)";
    
    $sql_get_log_id = oci_parse($conn, $query_get_log_id);
    oci_execute($sql_get_log_id);
    $data_get_log_id = oci_fetch_array($sql_get_log_id, OCI_ASSOC+OCI_RETURN_NULLS);
    $count_get_log_id = (int)$data_get_log_id['LOG_ID'];
    
    if ($count_get_log_id == '') {
        $log_id = 1;
    } else {
        $log_id = $count_get_log_id;
    }

    $query_insert_log = "INSERT INTO CTM_USER_LOG_API_TAB (LOG_ID, USERNAME, API_ID, LOG_START, LOG_FINISH, STATUS, IP_ADDRESS) VALUES (".$log_id.", '".$username."', '".$api_id."', TO_DATE(TO_CHAR(SYSDATE, 'DD/MM/YYYY HH24:MI:SS'), 'DD/MM/YYYY HH24:MI:SS'), '', 'Load!', '".$id_address."')";
    
    $sql_insert_log = oci_parse($conn, $query_insert_log);
    
    if (oci_execute($sql_insert_log)) {
        $e = "Success";
    } else {
        $e = oci_error();
    }

    return $log_id;
}

function updateUserLog($log_id, $status) {
    $conn = getConnection();
    $query_update_log = "UPDATE CTM_USER_LOG_API_TAB SET LOG_FINISH = TO_DATE(TO_CHAR(SYSDATE, 'DD/MM/YYYY HH24:MI:SS'), 'DD/MM/YYYY HH24:MI:SS'), STATUS = '".$status."' WHERE LOG_ID = ".$log_id;
    
    $sql_update_log = oci_parse($conn, $query_update_log);
    
    if (oci_execute($sql_update_log)) {
        $e = "Success";
    } else {
        $e = oci_error();
    }

    return $e;
}
?>