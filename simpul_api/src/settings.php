<?php
return [
    'settings' => [
        'displayErrorDetails' => true, // set to false in production
        'addContentLengthHeader' => false, // Allow the web server to send the content-length header

        // Renderer settings
        'renderer' => [
            'template_path' => __DIR__ . '/../templates/',
        ],

        // Monolog settings
        'logger' => [
            'name' => 'slim-app',
            'path' => isset($_ENV['docker']) ? 'php://stdout' : __DIR__ . '/../logs/app.log',
            'level' => \Monolog\Logger::DEBUG,
        ],

        // Database settings
        'db' => [
            /*'host' => '(DESCRIPTION = (ADDRESS = (PROTOCOL = TCP)(HOST = 10.15.2.68)(PORT = 1521)) (CONNECT_DATA = (SERVER = DEDICATED) (SERVICE_NAME = DEV)))',
            'user' => 'ifsapp',
            'pass' => 'ifsappdev',
            'dbname' => 'DEV',
            'driver' => 'oci8'*/
            
            'host' => '(DESCRIPTION = (ADDRESS = (PROTOCOL = TCP)(HOST = 10.15.2.68)(PORT = 1521))
            (CONNECT_DATA = (SERVER = DEDICATED) (SERVICE_NAME = DEV)))',
            'user' => 'ifsapp',
            'pass' => 'ifsappdev',
            'dbname' => 'DEV',
            'driver' => 'oci8'
        ],
    ],
];
