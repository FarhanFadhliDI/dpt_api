<?php

use Slim\App;
//use Slim\Http\Request;
//use Slim\Http\Response;
use Slim\Helper\Set;

use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Message\ResponseInterface;

use Firebase\JWT\JWT;
use Tuupola\Base62;

require '../vendor/autoload.php';

//include "connection.php";
include "selectlist.php";
include "input_type_reader.php";

$dotenv = Dotenv\Dotenv::createImmutable(__DIR__);
$dotenv->load();

return function (App $app) {
    $container = $app->getContainer();
    //$app = new App();

    //
    $app->post("/token", function (ServerRequestInterface $request, ResponseInterface $response, array $args) use ($container){
        $login_data = $request->getParsedBody();

        $conn = getConnection();

        $error_msg = "";
        $error_msg = string_type($login_data['USERNAME'], $error_msg, 'Username', 'NotNull');
        $error_msg = string_type($login_data['PASSWORD'], $error_msg, 'Password', 'NotNull');

        if ($error_msg == '') {
            /*
                $query2 = "SELECT USERNAME, PASSWORD FROM CTM_EXAMPLE_LOGIN_TAB WHERE username = '".$login_data['USERNAME']."' AND password = '".$login_data['PASSWORD']."'";
                
                $sql = oci_parse($conn, $query2);
                oci_execute($sql);
                
                $rows = array();
                while ($dt = oci_fetch_assoc($sql)) {
                    $rows[] = $dt;
                }
            */

            $data = array(
                'username' => $login_data['USERNAME'],
                'password' => $login_data['PASSWORD']
            );

            $url = 'https://apifactory.telkom.co.id:8243/hcm/auth/v1/token';
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch,CURLOPT_RETURNTRANSFER,true);
            curl_setopt($ch,CURLOPT_HEADER, false);
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                'Content-Type: application/x-www-form-urlencoded'));
            curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($data));
            
            // Fetch and return content, save it.
            $raw_data = curl_exec($ch);
            curl_close($ch);

            // If the API is JSON, use json_decode.
            $data1 = json_decode($raw_data);
            $data2 = $data1->data->auth;

            //if (oci_num_rows($sql) > 0) {
            if ($data2 != false) {
                $now = new DateTime();
                $future = new DateTime("+100 minutes");
                $user = "FAR";
                //
                $jti = (new Base62)->encode(random_bytes(16));
                $payload = [
                    "user" => $user,
                    "iat" => $now->getTimeStamp(),
                    "exp" => $future->getTimeStamp(),
                    "jti" => $jti
                ];
                $secret = getenv('SECRET_KEY');
                $token = JWT::encode($payload, $secret, "HS256");
                $data3["user"] = $login_data['USERNAME'];
                $data3["ifs_user"] = getIfsUser($login_data['USERNAME']);
                $data3["token"] = $token;
                $data3["expires"] = $future->getTimeStamp();
                $data3["message"] = "Login Successful!";

                return $response->withStatus(201)
                    ->withHeader("Content-Type", "application/json")
                    ->write(json_encode($data3, JSON_UNESCAPED_SLASHES | JSON_PRETTY_PRINT));
            } else {
                return $response->withJson(["success" => "false", "message" => "Invalid Username or Password!"], 200);
            }
        } elseif ($error_msg != '') {
            return $response->withJson(["success" => "false", "message" => $error_msg], 200);
        }
    });

    /*$app->group('/api', function (\Slim\App $app){});*/
    //

    $app->get('/[{name}]', function (ServerRequestInterface $request, ResponseInterface $response, array $args) use ($container) {
        // Sample log message
        $container->get('logger')->info("Slim-Skeleton '/' route");

        // Render index view
        return $container->get('renderer')->render($response, 'index.phtml', $args);
    });

    //----------------------------------------------------------------------------------//
    //---------------------------------// [INF_KPST] //---------------------------------//
    //----------------------------------------------------------------------------------//

    $app->get("/list_pmp/", function (ServerRequestInterface $request, ResponseInterface $response){
        $keyword = $request->getHeaderLine('KEYWORD');
        $api_id = "INF_LEBIH_BYR_08";
        
        $ipAddress = $request->getAttribute('ip_address');
        $decoded = $request->getAttribute("decoded_jwt");
        $username = $decoded['user'];
        $ifs_user = $decoded['ifs_user'];

        $log_id = insertUserLog($username, $api_id, $ipAddress);

        $conn = getConnection();

        $access = checkAccess($ifs_user, $api_id);

        $error_msg = "";
        $error_msg = string_type($keyword, $error_msg, 'Keyword', 'NotNull');

        if ($access == 'false') {
            $error_msg = "User ".$ifs_user." Does Not Have Access to This Action!";
        }

        if ($error_msg == '') {
            $query2 = "SELECT a.n_i_k, a.no_ahli_waris, CALON_PENERIMA_MP_API.Get_Nama_Ahli_Waris(a.company,a.n_i_k,a.no_ahli_waris) nama_lengkap, ALAMAT_PENERIMA_MP_API.Get_Jalan(a.company,a.no_ahli_waris,a.n_i_k,ALAMAT_PENERIMA_MP_API.Get_No_Urut_Default(a.company,a.n_i_k,a.no_ahli_waris)) jalan, ALAMAT_PENERIMA_MP_API.Get_Komplek(a.company,a.no_ahli_waris,a.n_i_k,ALAMAT_PENERIMA_MP_API.Get_No_Urut_Default(a.company,a.n_i_k,a.no_ahli_waris)) komplek, ALAMAT_PENERIMA_MP_API.Get_Blok(a.company,a.no_ahli_waris,a.n_i_k,ALAMAT_PENERIMA_MP_API.Get_No_Urut_Default(a.company,a.n_i_k,a.no_ahli_waris)) blok, ALAMAT_PENERIMA_MP_API.Get_No_Rumah(a.company,a.no_ahli_waris,a.n_i_k,ALAMAT_PENERIMA_MP_API.Get_No_Urut_Default(a.company,a.n_i_k,a.no_ahli_waris)) no, ALAMAT_PENERIMA_MP_API.Get_Rt(a.company,a.no_ahli_waris,a.n_i_k,ALAMAT_PENERIMA_MP_API.Get_No_Urut_Default(a.company,a.n_i_k,a.no_ahli_waris)) rt, ALAMAT_PENERIMA_MP_API.Get_Rw(a.company,a.no_ahli_waris,a.n_i_k,ALAMAT_PENERIMA_MP_API.Get_No_Urut_Default(a.company,a.n_i_k,a.no_ahli_waris)) rw, ALAMAT_PENERIMA_MP_API.Get_Kelurahan_Id(a.company,a.no_ahli_waris,a.n_i_k,ALAMAT_PENERIMA_MP_API.Get_No_Urut_Default(a.company,a.n_i_k,a.no_ahli_waris)) kd_kelurahan, ALAMAT_PENERIMA_MP_API.Get_Kecamatan_Id(a.company,a.no_ahli_waris,a.n_i_k,ALAMAT_PENERIMA_MP_API.Get_No_Urut_Default(a.company,a.n_i_k,a.no_ahli_waris)) kd_kecamatan, ALAMAT_PENERIMA_MP_API.Get_Kota_Id(a.company,a.no_ahli_waris,a.n_i_k,ALAMAT_PENERIMA_MP_API.Get_No_Urut_Default(a.company,a.n_i_k,a.no_ahli_waris)) kd_kota, ALAMAT_PENERIMA_MP_API.Get_Provinsi_Id(a.company,a.no_ahli_waris,a.n_i_k,ALAMAT_PENERIMA_MP_API.Get_No_Urut_Default(a.company,a.n_i_k,a.no_ahli_waris)) kd_provinsi, ALAMAT_PENERIMA_MP_API.Get_Kode_Pos(a.company,a.no_ahli_waris,a.n_i_k,ALAMAT_PENERIMA_MP_API.Get_No_Urut_Default(a.company,a.n_i_k,a.no_ahli_waris)) kd_pos, KELURAHAN_API.Get_Nama_By_Id(ALAMAT_PENERIMA_MP_API.Get_Kelurahan_Id(a.company,a.no_ahli_waris,a.n_i_k,ALAMAT_PENERIMA_MP_API.Get_No_Urut_Default(a.company,a.n_i_k,a.no_ahli_waris))) nama_kelurahan, KECAMATAN_API.Get_Nama_By_Id(ALAMAT_PENERIMA_MP_API.Get_Kecamatan_Id(a.company,a.no_ahli_waris,a.n_i_k,ALAMAT_PENERIMA_MP_API.Get_No_Urut_Default(a.company,a.n_i_k,a.no_ahli_waris))) nama_kecamatan, KOTA_API.Get_Nama_By_Id(ALAMAT_PENERIMA_MP_API.Get_Kota_Id(a.company,a.no_ahli_waris,a.n_i_k,ALAMAT_PENERIMA_MP_API.Get_No_Urut_Default(a.company,a.n_i_k,a.no_ahli_waris))) nama_kota, PROVINSI_API.Get_Nama_By_Id(ALAMAT_PENERIMA_MP_API.Get_Provinsi_Id(a.company,a.no_ahli_waris,a.n_i_k,ALAMAT_PENERIMA_MP_API.Get_No_Urut_Default(a.company,a.n_i_k,a.no_ahli_waris))) nama_provinsi, CALON_PENERIMA_MP_API.Get_No_Telp(a.company,a.n_i_k,a.nominal) no_kontak, CALON_PENERIMA_MP_API.Get_Jenis_Ahli_Waris(a.company,a.n_i_k,a.no_ahli_waris) status, ALAMAT_PENERIMA_MP_API.Get_P2TEL_default(a.company,a.n_i_k,a.no_ahli_waris,SYSDATE) kd_cabang, MITRA_KERJA_API.Get_Nama_Mitra_Kerja(a.company,ALAMAT_PENERIMA_MP_API.Get_P2TEL_default(a.company,a.n_i_k,a.no_ahli_waris,SYSDATE)) cabang FROM PENERIMAAN_MP a WHERE a.kd_periode = PERIODE_API.Get_Periode('DPT02',SYSDATE) AND (UPPER(CALON_PENERIMA_MP_API.Get_Nama_Ahli_Waris(a.company,a.n_i_k,a.no_ahli_waris)) LIKE UPPER('%".$keyword."%') OR a.n_i_k LIKE '%".$keyword."%')";
            
            $sql = oci_parse($conn, $query2);
            oci_execute($sql);
            
            $rows = array();
            while ($dt = oci_fetch_assoc($sql)) {
                $rows[] = $dt;
            }
        }

        if (!$conn) {
            $e = oci_error();
            return $response->withJson(["data" => null, "error" => $e, "status" => "success"], 200);
        } else {
            if ($error_msg != '') {
                return $response->withJson(["success" => "false", "message" => $error_msg], 200);
            } elseif (oci_num_rows($sql) > 0) {
                return $response->withJson(["success" => "true", "message" => "PMP Ditemukan!", "data" => $rows], 200);
            } else {
                return $response->withJson(["success" => "false", "message" => "PMP Tidak Ditemukan!"], 200);
            }
        }
    }); //[SIMPUL]

    $app->get("/detail_pmp/", function (ServerRequestInterface $request, ResponseInterface $response, array $args) {
        $nik = $request->getHeaderLine('NIK');
        $naw = $request->getHeaderLine('NO_AHLI_WARIS');
        
        $conn = getConnection();

        $error_msg = "";
        $error_msg = string_type($nik, $error_msg, 'NIK', 'NotNull');
        $error_msg = int_type($naw, $error_msg, 'No Ahli Waris', 'NotNull');

        if ($error_msg == '') {
            $query2 = "SELECT a.n_i_k, a.no_ahli_waris, CALON_PENERIMA_MP_API.Get_Nama_Ahli_Waris(a.company,a.n_i_k,a.no_ahli_waris) nama_lengkap, ALAMAT_PENERIMA_MP_API.Get_Jalan(a.company,a.no_ahli_waris,a.n_i_k,ALAMAT_PENERIMA_MP_API.Get_No_Urut_Default(a.company,a.n_i_k,a.no_ahli_waris)) jalan, ALAMAT_PENERIMA_MP_API.Get_Komplek(a.company,a.no_ahli_waris,a.n_i_k,ALAMAT_PENERIMA_MP_API.Get_No_Urut_Default(a.company,a.n_i_k,a.no_ahli_waris)) komplek, ALAMAT_PENERIMA_MP_API.Get_Blok(a.company,a.no_ahli_waris,a.n_i_k,ALAMAT_PENERIMA_MP_API.Get_No_Urut_Default(a.company,a.n_i_k,a.no_ahli_waris)) blok, ALAMAT_PENERIMA_MP_API.Get_No_Rumah(a.company,a.no_ahli_waris,a.n_i_k,ALAMAT_PENERIMA_MP_API.Get_No_Urut_Default(a.company,a.n_i_k,a.no_ahli_waris)) no, ALAMAT_PENERIMA_MP_API.Get_Rt(a.company,a.no_ahli_waris,a.n_i_k,ALAMAT_PENERIMA_MP_API.Get_No_Urut_Default(a.company,a.n_i_k,a.no_ahli_waris)) rt, ALAMAT_PENERIMA_MP_API.Get_Rw(a.company,a.no_ahli_waris,a.n_i_k,ALAMAT_PENERIMA_MP_API.Get_No_Urut_Default(a.company,a.n_i_k,a.no_ahli_waris)) rw, ALAMAT_PENERIMA_MP_API.Get_Kelurahan_Id(a.company,a.no_ahli_waris,a.n_i_k,ALAMAT_PENERIMA_MP_API.Get_No_Urut_Default(a.company,a.n_i_k,a.no_ahli_waris)) kd_kelurahan, ALAMAT_PENERIMA_MP_API.Get_Kecamatan_Id(a.company,a.no_ahli_waris,a.n_i_k,ALAMAT_PENERIMA_MP_API.Get_No_Urut_Default(a.company,a.n_i_k,a.no_ahli_waris)) kd_kecamatan, ALAMAT_PENERIMA_MP_API.Get_Kota_Id(a.company,a.no_ahli_waris,a.n_i_k,ALAMAT_PENERIMA_MP_API.Get_No_Urut_Default(a.company,a.n_i_k,a.no_ahli_waris)) kd_kota, ALAMAT_PENERIMA_MP_API.Get_Provinsi_Id(a.company,a.no_ahli_waris,a.n_i_k,ALAMAT_PENERIMA_MP_API.Get_No_Urut_Default(a.company,a.n_i_k,a.no_ahli_waris)) kd_provinsi, ALAMAT_PENERIMA_MP_API.Get_Kode_Pos(a.company,a.no_ahli_waris,a.n_i_k,ALAMAT_PENERIMA_MP_API.Get_No_Urut_Default(a.company,a.n_i_k,a.no_ahli_waris)) kd_pos, KELURAHAN_API.Get_Nama_By_Id(ALAMAT_PENERIMA_MP_API.Get_Kelurahan_Id(a.company,a.no_ahli_waris,a.n_i_k,ALAMAT_PENERIMA_MP_API.Get_No_Urut_Default(a.company,a.n_i_k,a.no_ahli_waris))) nama_kelurahan, KECAMATAN_API.Get_Nama_By_Id(ALAMAT_PENERIMA_MP_API.Get_Kecamatan_Id(a.company,a.no_ahli_waris,a.n_i_k,ALAMAT_PENERIMA_MP_API.Get_No_Urut_Default(a.company,a.n_i_k,a.no_ahli_waris))) nama_kecamatan, KOTA_API.Get_Nama_By_Id(ALAMAT_PENERIMA_MP_API.Get_Kota_Id(a.company,a.no_ahli_waris,a.n_i_k,ALAMAT_PENERIMA_MP_API.Get_No_Urut_Default(a.company,a.n_i_k,a.no_ahli_waris))) nama_kota, PROVINSI_API.Get_Nama_By_Id(ALAMAT_PENERIMA_MP_API.Get_Provinsi_Id(a.company,a.no_ahli_waris,a.n_i_k,ALAMAT_PENERIMA_MP_API.Get_No_Urut_Default(a.company,a.n_i_k,a.no_ahli_waris))) nama_provinsi, CALON_PENERIMA_MP_API.Get_No_Telp(a.company,a.n_i_k,a.nominal) no_kontak, CALON_PENERIMA_MP_API.Get_Jenis_Ahli_Waris(a.company,a.n_i_k,a.no_ahli_waris) status, ALAMAT_PENERIMA_MP_API.Get_P2TEL_default(a.company,a.n_i_k,a.no_ahli_waris,SYSDATE) kd_cabang, MITRA_KERJA_API.Get_Nama_Mitra_Kerja(a.company,ALAMAT_PENERIMA_MP_API.Get_P2TEL_default(a.company,a.n_i_k,a.no_ahli_waris,SYSDATE)) cabang FROM PENERIMAAN_MP a WHERE a.kd_periode = PERIODE_API.Get_Periode('DPT02',SYSDATE) AND (a.n_i_k LIKE '%".$nik."%') AND (a.no_ahli_waris = '".$naw."')";
            
            $sql = oci_parse($conn, $query2);
            oci_execute($sql);
            
            $rows = array();
            while ($dt = oci_fetch_assoc($sql)) {
                $rows[] = $dt;
            }
        }

        if (!$conn) {
            $e = oci_error();
            return $response->withJson(["data" => null, "error" => $e, "status" => "success"], 200);
        } else {
            if ($error_msg != '') {
                return $response->withJson(["success" => "false", "message" => $error_msg], 200);
            } elseif (oci_num_rows($sql) > 0) {
                return $response->withJson(["success" => "true", "message" => "PMP Ditemukan!", "data" => $rows], 200);
            } else {
                return $response->withJson(["success" => "false", "message" => "PMP Tidak Ditemukan!"], 200);
            }
        }
    }); //[SIMPUL]

    $app->get("/daftar_aw/", function (ServerRequestInterface $request, ResponseInterface $response, array $args) {
        $nik = $request->getHeaderLine('NIK');
        $naw = $request->getHeaderLine('NO_AHLI_WARIS');

        if ($naw == '') {
            $get_naw = "";
        } else {
            $get_naw = "AND a.no_ahli_waris = '".$naw."'";
        }
        
        $conn = getConnection();

        $error_msg = "";
        $error_msg = string_type($nik, $error_msg, 'NIK', 'NotNull');
        $error_msg = int_type($naw, $error_msg, 'No Ahli Waris', 'Nullable');

        if ($error_msg == '') {
            $query2 = "SELECT a.n_i_k, a.no_ahli_waris, a.nama_ahli_waris nama_lengkap, NVL(REPLACE(ALAMAT_PENERIMA_MP_API.Get_Alamat2(a.company,a.n_i_k,a.no_ahli_waris,b.no_urut),'###',' '),'-') alamat, ALAMAT_PENERIMA_MP_API.Get_P2TEL_default(a.company,a.n_i_k,a.no_ahli_waris,sysdate) kd_cabang, MITRA_KERJA_API.Get_Nama_Mitra_Kerja(a.company,ALAMAT_PENERIMA_MP_API.Get_P2TEL_default(a.company,a.n_i_k,a.no_ahli_waris,sysdate)) cabang, NVL(KONTAK_PENERIMA_MP_API.Get_Data_Kontak(a.company,a.n_i_k,a.no_ahli_waris,'1'),'-') no_kontak, (CASE a.jenis_ahli_waris WHEN 'Anak' THEN NVL(a.jenis_anak, a.jenis_ahli_waris) else a.jenis_ahli_waris END) hubungan_keluarga, c.picture_id url_foto, (CASE a.jenis_ahli_waris WHEN 'Anak' THEN NVL(a.jenis_anak, a.jenis_ahli_waris) else a.jenis_ahli_waris END) status_aw, CALON_PENERIMA_MP_API.Get_No_Ktp(a.company,a.n_i_k,a.no_ahli_waris) no_ktp, CALON_PENERIMA_MP_API.Get_Gol_Darah(a.COMPANY, a.N_I_K, 1) gol_darah, CALON_PENERIMA_MP_API.Get_Agama(a.COMPANY, a.N_I_K, 1) agama, CALON_PENERIMA_MP_API.Get_Jenis_Kelamin2(a.COMPANY, a.N_I_K, 1) jenis_kelamin, (CASE WHEN CALON_PENERIMA_MP_API.Get_Tgl_Nikah(a.company,a.n_i_k,a.no_ahli_waris) IS NULL THEN 'Belum Menikah' else 'Menikah' END) status_marital, '' url_dokumen /**/ FROM PENERIMA_MP_TAB c RIGHT OUTER JOIN SUSUNAN_AHLI_WARIS2 a ON c.n_i_k = a.n_i_k AND c.company = a.company AND c.no_ahli_waris = a.no_ahli_waris LEFT OUTER JOIN ALAMAT_PENERIMA_MP_TAB b ON a.n_i_k = b.n_i_k AND a.company = b.company AND a.no_ahli_waris = b.no_ahli_waris AND b.default1 = 'TRUE' WHERE a.tgl_meninggal IS NULL AND a.tgl_nikah_lagi IS NULL AND a.tgl_cerai IS NULL AND ((a.jenis_ahli_waris = 'Anak' AND TRUNC((months_between(sysdate, a.tgl_lahir) / 12), 0) <= 25) OR a.jenis_ahli_waris  = 'Istri' OR a.jenis_ahli_waris = 'Suami' OR a.jenis_ahli_waris = 'Peserta') AND a.n_i_k = '".$nik."' $get_naw";
            
            $sql = oci_parse($conn, $query2);
            oci_execute($sql);
            
            $rows = array();
            while ($dt = oci_fetch_assoc($sql)) {
                $rows[] = $dt;
            }
        }

        if (!$conn) {
            $e = oci_error();
            return $response->withJson(["data" => null, "error" => $e, "status" => "success"], 200);
        } else {
            if ($error_msg != '') {
                return $response->withJson(["success" => "false", "message" => $error_msg], 200);
            } elseif (oci_num_rows($sql) > 0) {
                return $response->withJson(["success" => "true", "message" => "Ahli Waris Ditemukan!", "data_ahliwaris" => $rows], 200);
            } else {
                return $response->withJson(["success" => "false", "message" => "Ahli Waris Tidak Ditemukan!"], 200);
            }
        }
    }); //[SIMPUL]

    $app->get("/status_laporan_kematian/", function (ServerRequestInterface $request, ResponseInterface $response, array $args) {
        $nik = $request->getHeaderLine('NIK');
        $naw = $request->getHeaderLine('NO_AHLI_WARIS');
        
        $conn = getConnection();

        $error_msg = "";
        $error_msg = string_type($nik, $error_msg, 'NIK', 'NotNull');
        $error_msg = int_type($naw, $error_msg, 'No Ahli Waris', 'NotNull');

        if ($error_msg == '') {
            $hapus_query = "SELECT * FROM (SELECT b.nama_event status, TRUNC(b.tgl) tanggal, b.catatan deskripsi FROM HAPUS_PENERIMA_MP a, DPT_AUDITTRAIL b WHERE a.objid = b.row_id AND a.state = b.nama_event AND a.tgl_meninggal IS NOT NULL AND a.n_i_k = '".$nik."' AND a.no_ahli_waris = '".$naw."' ORDER BY b.tgl DESC) WHERE ROWNUM = 1";
            $hapus_sql = oci_parse($conn, $hapus_query);
            oci_execute($hapus_sql);

            $hapus_rows = array();
            while ($hapus_dt = oci_fetch_assoc($hapus_sql)) {
                $hapus_rows[] = $hapus_dt;
            }

            $mutasi_pmp_query = "SELECT * FROM (SELECT b.nama_event status, TRUNC(b.tgl) tanggal, b.catatan deskripsi FROM MUTASI_PENERIMA_MP a, DPT_AUDITTRAIL b WHERE a.objid = b.row_id AND a.state = b.nama_event AND a.tgl_meninggal IS NOT NULL AND NVL(a.gabung_mp,'FALSE') != 'TRUE' AND a.n_i_k = '".$nik."' AND a.no_ahli_waris = '".$naw."' ORDER BY b.tgl DESC) WHERE ROWNUM = 1";
            $mutasi_pmp_sql = oci_parse($conn, $mutasi_pmp_query);
            oci_execute($mutasi_pmp_sql);

            $mutasi_pmp_rows = array();
            while ($mutasi_pmp_dt = oci_fetch_assoc($mutasi_pmp_sql)) {
                $mutasi_pmp_rows[] = $mutasi_pmp_dt;
            }

            $mutasi_aw_query = "SELECT * FROM (SELECT b.nama_event status, TRUNC(b.tgl) tanggal, b.catatan deskripsi FROM MUTASI_PENERIMA_MP_AW a, DPT_AUDITTRAIL b WHERE a.objid = b.row_id AND a.state = b.nama_event AND a.tgl_meninggal IS NOT NULL AND a.n_i_k = '".$nik."' AND a.no_ahli_waris = '".$naw."' ORDER BY b.tgl DESC) WHERE ROWNUM = 1";
            $mutasi_aw_sql = oci_parse($conn, $mutasi_aw_query);
            oci_execute($mutasi_aw_sql);

            $mutasi_aw_rows = array();
            while ($mutasi_aw_dt = oci_fetch_assoc($mutasi_aw_sql)) {
                $mutasi_aw_rows[] = $mutasi_aw_dt;
            }

            $mutasi_gabung_query = "SELECT * FROM (SELECT b.nama_event status, TRUNC(b.tgl) tanggal, b.catatan deskripsi FROM MUTASI_PENGGABUNGAN_MP a, DPT_AUDITTRAIL b WHERE a.objid = b.row_id AND a.state = b.nama_event AND a.tgl_meninggal IS NOT NULL AND NVL(a.gabung_mp,'FALSE') = 'TRUE' AND a.n_i_k = '".$nik."' AND a.no_ahli_waris = '".$naw."' ORDER BY b.tgl DESC) WHERE ROWNUM = 1";
            $mutasi_gabung_sql = oci_parse($conn, $mutasi_gabung_query);
            oci_execute($mutasi_gabung_sql);

            $mutasi_gabung_rows = array();
            while ($mutasi_gabung_dt = oci_fetch_assoc($mutasi_gabung_sql)) {
                $mutasi_gabung_rows[] = $mutasi_gabung_dt;
            }

            $rows = array();
            if (oci_num_rows($hapus_sql) > 0) {
                $rows = $hapus_rows;
            }
            elseif (oci_num_rows($mutasi_pmp_sql) > 0) {
                $rows = $mutasi_pmp_rows;
            }
            elseif (oci_num_rows($mutasi_aw_sql) > 0) {
                $rows = $mutasi_aw_rows;
            }
            elseif (oci_num_rows($mutasi_gabung_sql) > 0) {
                $rows = $mutasi_gabung_rows;
            }
        }

        if (!$conn) {
            $e = oci_error();
            return $response->withJson(["data" => null, "error" => $e, "status" => "success"], 200);
        } else {
            if ($error_msg != '') {
                return $response->withJson(["success" => "false", "message" => $error_msg], 200);
            } elseif (oci_num_rows($hapus_sql) > 0 || oci_num_rows($mutasi_pmp_sql) > 0 || oci_num_rows($mutasi_aw_sql) > 0 || oci_num_rows($mutasi_gabung_sql) > 0) {
                return $response->withJson(["success" => "true", "message" => "Data Laporan Kematian Ditemukan!", "Tracking Laporan" => $rows], 200);
            } else {
                return $response->withJson(["success" => "false", "message" => "Data Laporan Kematian Tidak Ditemukan!"], 200);
            }
        }
    }); //[SIMPUL]

    $app->get("/status_pmp_tangguhan/", function (ServerRequestInterface $request, ResponseInterface $response, array $args) {
        $nik = $request->getHeaderLine('NIK');
        $naw = $request->getHeaderLine('NO_AHLI_WARIS');
        
        $conn = getConnection();

        $error_msg = "";
        $error_msg = string_type($nik, $error_msg, 'NIK', 'NotNull');
        $error_msg = int_type($naw, $error_msg, 'No Ahli Waris', 'NotNull');

        if ($error_msg == '') {
            $query = "SELECT a.state, a.jns_penangguhan jenis_penangguhan, '-- not yet --' tanggal, (REGEXP_REPLACE(a.catatan,'[^ -~]','')) catatan FROM DAFTAR_PST_TANGGUH a, PENERIMA_MP2 b WHERE a.company = b.company AND a.n_i_k = b.n_i_k AND a.no_ahli_waris = b.no_ahli_waris AND (a.n_i_k LIKE '%".$nik."%') AND (a.no_ahli_waris = '".$naw."')";

            $sql = oci_parse($conn, $query);
            oci_execute($sql);
            
            $rows = array();
            while ($dt = oci_fetch_assoc($sql)) {
                $rows[] = $dt;
            }
        }

        if (!$conn) {
            $e = oci_error();
            return $response->withJson(["data" => null, "error" => $e, "status" => "success"], 200);
        } else {
            if ($error_msg != '') {
                return $response->withJson(["success" => "false", "message" => $error_msg], 200);
            } elseif (oci_num_rows($sql) > 0) {
                return $response->withJson(["success" => "true", "message" => "Data Laporan Kematian Tangguhan Ditemukan!", "tracking_laporan" => $rows], 200);
            } else {
                return $response->withJson(["success" => "false", "message" => "Data Laporan Kematian Tangguhan Tidak Ditemukan!"], 200);
            }
        }
    }); //[SIMPUL]

    $app->post("/push_tgl_kematian/", function (ServerRequestInterface $request, ResponseInterface $response){
        $new_data = $request->getParsedBody(); // 500002 2 // 500009 1

        $conn = getConnection();

        $error_msg = "";
        $error_msg = string_type($new_data['COMPANY'], $error_msg, 'Company', 'NotNull');
        $error_msg = string_type($new_data['N_I_K'], $error_msg, 'NIK', 'NotNull');
        $error_msg = string_type($new_data['KD_PERIODE'], $error_msg, 'Kd Periode', 'NotNull');
        $error_msg = int_type($new_data['NO_AHLI_WARIS'], $error_msg, 'No Ahlli Waris', 'NotNull');
        $error_msg = string_type($new_data['TGL_PELAPORAN'], $error_msg, 'Tanggal Pelaporan', 'NotNull');
        $error_msg = string_type($new_data['TGL_MENINGGAL'], $error_msg, 'Tanggal Meninggal', 'NotNull');
        $error_msg = string_type($new_data['SUMBER'], $error_msg, 'Sumber', 'NotNull');
        $error_msg = string_type($new_data['SUMBER_DATA'], $error_msg, 'Sumber Data', 'NotNull');

        if ($error_msg == '') {
            $check_pmp_query = "SELECT COUNT(a.n_i_k) JMLH_PMP FROM PENERIMA_MP_TAB c RIGHT OUTER JOIN SUSUNAN_AHLI_WARIS2 a ON c.n_i_k = a.n_i_k AND c.company = a.company AND c.no_ahli_waris = a.no_ahli_waris LEFT OUTER JOIN ALAMAT_PENERIMA_MP_TAB b ON a.n_i_k = b.n_i_k AND a.company = b.company AND a.no_ahli_waris = b.no_ahli_waris AND b.default1 = 'TRUE' WHERE a.tgl_meninggal IS NULL AND a.tgl_nikah_lagi IS NULL AND a.tgl_cerai IS NULL AND ((a.jenis_ahli_waris = 'Anak' AND TRUNC((months_between(sysdate, a.tgl_lahir) / 12), 0) <= 25) OR a.jenis_ahli_waris = 'Istri' OR a.jenis_ahli_waris = 'Suami' OR a.jenis_ahli_waris = 'Peserta') AND a.n_i_k = '".$new_data['N_I_K']."'";
            $check_pmp_sql = oci_parse($conn, $check_pmp_query);
            oci_execute($check_pmp_sql);
            $check_pmp_data = oci_fetch_array($check_pmp_sql, OCI_ASSOC+OCI_RETURN_NULLS);
            $check_pmp_count = (int)$check_pmp_data['JMLH_PMP'];

            if ($check_pmp_count > 1) { // MUTASI
                $query_oci = "
                DECLARE
                    info_       VARCHAR2(32000) := NULL; --p0
                    objid_      VARCHAR2(32000) := NULL; --p1
                    objversion_ VARCHAR2(32000) := NULL; --p2
                    attr_       VARCHAR2(32000) := NULL; --p3
                    error_log_  VARCHAR2(2000);
                BEGIN
                    Client_SYS.Clear_Attr(attr_);
                    Client_Sys.Add_To_Attr('COMPANY', '".$new_data['COMPANY']."', attr_);
                    Client_Sys.Add_To_Attr('N_I_K', '".$new_data['N_I_K']."', attr_);
                    Client_Sys.Add_To_Attr('KD_PERIODE', '".$new_data['KD_PERIODE']."', attr_);
                    Client_Sys.Add_To_Attr('NO_AHLI_WARIS', '".$new_data['NO_AHLI_WARIS']."', attr_);
                    Client_Sys.Add_To_Attr('VALID_FROM', to_date('".$new_data['TGL_PELAPORAN']."','MM/DD/YYYY'), attr_);
                    Client_Sys.Add_To_Attr('TGL_MENINGGAL', to_date('".$new_data['TGL_MENINGGAL']."','MM/DD/YYYY'), attr_);
                    Client_Sys.Add_To_Attr('SUMBER', UPPER('".$new_data['SUMBER']."'), attr_);
                    Client_Sys.Add_To_Attr('SUMBER_DATA', '".$new_data['SUMBER_DATA']."', attr_);
                    Client_Sys.Add_To_Attr('JENIS_AHLI_WARIS', CALON_PENERIMA_MP_API.Get_Jenis_Ahli_Waris('".$new_data['COMPANY']."','".$new_data['N_I_K']."','".$new_data['NO_AHLI_WARIS']."'), attr_);
                    Client_Sys.Add_To_Attr('KD_JENIS_PENSIUN', PENERIMA_MP_API.Get_KdJenisPensiun('".$new_data['COMPANY']."','".$new_data['N_I_K']."','".$new_data['NO_AHLI_WARIS']."'), attr_);
                    Client_Sys.Add_To_Attr('KD_KELOMPOK', PESERTA_API.Get_Kd_Kelompok('".$new_data['COMPANY']."','".$new_data['N_I_K']."'), attr_);
                    Client_Sys.Add_To_Attr('KD_BAND', PESERTA_API.Get_Kd_Band('".$new_data['COMPANY']."','".$new_data['N_I_K']."'), attr_);
                                                        
                    MUTASI_PENERIMA_MP_API.New__(info_, objid_, objversion_, attr_, 'DO');
                                                
                    EXCEPTION 
                    WHEN OTHERS THEN
                    error_log_ := Sqlerrm;
                END;
                ";

                $sql = oci_parse($conn, $query_oci);
                
                if (oci_execute($sql)) {
                    return $response->withJson(["success" => "true", "message" => "Manfaat Tambahan Berhasil Tersimpan! MUTASI"], 200);
                } else {
                    $e = oci_error();
                    return $response->withJson(["data" => null, "error" => $e, "status" => "success"], 200);
                }
            }
            elseif ($check_pmp_count == 1) { // HAPUS
                $query_oci = "
                DECLARE
                    info_       VARCHAR2(32000) := NULL; --p0
                    objid_      VARCHAR2(32000) := NULL; --p1
                    objversion_ VARCHAR2(32000) := NULL; --p2
                    attr_       VARCHAR2(32000) := NULL; --p3
                    error_log_  VARCHAR2(2000);
                BEGIN
                    Client_SYS.Clear_Attr(attr_);
                    Client_Sys.Add_To_Attr('COMPANY', '".$new_data['COMPANY']."', attr_);
                    Client_Sys.Add_To_Attr('N_I_K', '".$new_data['N_I_K']."', attr_);
                    Client_Sys.Add_To_Attr('KD_PERIODE', '".$new_data['KD_PERIODE']."', attr_);
                    Client_Sys.Add_To_Attr('NO_AHLI_WARIS', '".$new_data['NO_AHLI_WARIS']."', attr_);
                    Client_Sys.Add_To_Attr('VALID_FROM', to_date('".$new_data['TGL_PELAPORAN']."','MM/DD/YYYY'), attr_);
                    Client_Sys.Add_To_Attr('TGL_MENINGGAL', to_date('".$new_data['TGL_MENINGGAL']."','MM/DD/YYYY'), attr_);
                    Client_Sys.Add_To_Attr('SUMBER', UPPER('".$new_data['SUMBER']."'), attr_);
                    Client_Sys.Add_To_Attr('SUMBER_DATA', '".$new_data['SUMBER_DATA']."', attr_);
                    Client_Sys.Add_To_Attr('JENIS_AHLI_WARIS', CALON_PENERIMA_MP_API.Get_Jenis_Ahli_Waris('".$new_data['COMPANY']."','".$new_data['N_I_K']."','".$new_data['NO_AHLI_WARIS']."'), attr_);
                    Client_Sys.Add_To_Attr('KD_JENIS_PENSIUN', PENERIMA_MP_API.Get_KdJenisPensiun('".$new_data['COMPANY']."','".$new_data['N_I_K']."','".$new_data['NO_AHLI_WARIS']."'), attr_);
                    Client_Sys.Add_To_Attr('KD_KELOMPOK', PESERTA_API.Get_Kd_Kelompok('".$new_data['COMPANY']."','".$new_data['N_I_K']."'), attr_);
                    Client_Sys.Add_To_Attr('KD_BAND', PESERTA_API.Get_Kd_Band('".$new_data['COMPANY']."','".$new_data['N_I_K']."'), attr_);
                    Client_Sys.Add_To_Attr('MITRA_KERJA', ALAMAT_PENERIMA_MP_API.Get_P2TEL_default('".$new_data['COMPANY']."','".$new_data['N_I_K']."','".$new_data['NO_AHLI_WARIS']."',SYSDATE), attr_);
                                                        
                    HAPUS_PENERIMA_MP_API.New__(info_, objid_, objversion_, attr_, 'DO');
                                            
                    EXCEPTION 
                    WHEN OTHERS THEN
                    error_log_ := Sqlerrm;
                END;
                ";
                
                $sql = oci_parse($conn, $query_oci);
                
                if (oci_execute($sql)) {
                    return $response->withJson(["success" => "true", "message" => "Manfaat Tambahan Berhasil Tersimpan! HAPUS"], 200);
                } else {
                    $e = oci_error();
                    return $response->withJson(["data" => null, "error" => $e, "status" => "success"], 200);
                }
            }
            elseif ($check_pmp_count == 0 || $check_pmp_count == '') {

            }
        } elseif ($error_msg != '') {
            return $response->withJson(["success" => "false", "message" => $error_msg], 200);
        }
    }); //[SIMPUL]

    $app->get("/status_byr_kematian/", function (ServerRequestInterface $request, ResponseInterface $response, array $args) {
        $no_sppb = $request->getHeaderLine('NO_SPPB');
        
        $conn = getConnection();

        $error_msg = "";
        $error_msg = string_type($no_sppb, $error_msg, 'No. SPPb', 'NotNull');

        if ($error_msg == '') {
            $query = "SELECT SPB_HEADER_API.Get_Nomor_Sppb(a.no_spb) no_sppb, SPB_HEADER_API.Get_Tanggal_Terima_Sppb(a.no_spb) tgl_sppb, a.no_spb, SPB_HEADER_API.Get_Tanggal_Spb(a.no_spb) tgl_spb, a.no_bayar_spb FROM BAYAR_SPB_DETAIL1 a WHERE SPB_HEADER_API.Get_Nomor_Sppb(a.no_spb) = '".$no_sppb."'";

            $sql = oci_parse($conn, $query);
            oci_execute($sql);
            
            $rows = array();
            while ($dt = oci_fetch_assoc($sql)) {
                $rows[] = $dt;
            }
        }

        if (!$conn) {
            $e = oci_error();
            return $response->withJson(["data" => null, "error" => $e, "status" => "success"], 200);
        } else {
            if ($error_msg != '') {
                return $response->withJson(["success" => "false", "message" => $error_msg], 200);
            } elseif (oci_num_rows($sql) > 0) {
                return $response->withJson(["success" => "true", "message" => "Pembayaran Kematian Sudah Dibayarkan!", "tracking_laporan" => $rows], 200);
            } else {
                return $response->withJson(["success" => "false", "message" => "Belum Ada Transfer ke Rekening!"], 200);
            }
        }
    }); //[SIMPUL]

    $app->get("/slip_mp/", function (ServerRequestInterface $request, ResponseInterface $response){
        $company = $request->getHeaderLine('COMPANY');
        $nik = $request->getHeaderLine('NIK');
        $kd_periode = $request->getHeaderLine('KD_PERIODE');
        $naw = $request->getHeaderLine('NO_AHLI_WARIS');
        $sekaligus = $request->getHeaderLine('SEKALIGUS');
        $tipe_proses = $request->getHeaderLine('TIPE_PROSES');
        
        $conn = getConnection();

        $error_msg = "";
        $error_msg = string_type($company, $error_msg, 'Company', 'NotNull');
        $error_msg = string_type($nik, $error_msg, 'NIK', 'NotNull');
        $error_msg = string_type($kd_periode, $error_msg, 'Kd Periode', 'NotNull');
        $error_msg = int_type($naw, $error_msg, 'No Ahli Waris', 'NotNull');
        $error_msg = string_type($sekaligus, $error_msg, 'Sekaligus', 'NotNull');
        $error_msg = int_type($tipe_proses, $error_msg, 'Tipe Proses', 'NotNull');

        if ($error_msg == '') {
            $query1 = "SELECT pmp.company, pmp.n_i_k, calon_penerima_mp_api.Get_Nama_Ahli_Waris(pmp.company, pmp.n_i_k, pmp.no_ahli_waris) Nama, bank_koordinator_api.Get_Keterangan(rmp.company, dbk.kd_koordinator) Bank, nvl(calon_penerima_mp_api.Get_Jenis_Ahli_Waris(pmp.company, pmp.n_i_k, pmp.no_ahli_waris), '-') Jenis_Ahli_Waris, rmp.nama_pemegang_rek nama_pemegang_rek, rmp.no_rekening no_rekening, rmp.office_code Office_Code, payment_institute_office_api.Get_Description(rmp.company, rmp.institute_id, rmp.office_code) Cabang, CASE WHEN '".$sekaligus."' = 'FALSE' THEN CASE WHEN pmp.kd_jenis_proses = 1 THEN pmp.mpb else CASE WHEN '".$tipe_proses."' != 3 THEN pmp.nominal else 0 END END else pmp.mps END mpb, nvl(rapel, 0) Rapel, nvl(mp20, 0) + nvl(tht, 0) Mp20, CASE WHEN '".$tipe_proses."' = 2 THEN nvl(penerimaan_mp_api.get_nominal_transfer(pmp.company, pmp.n_i_k, pmp.no_ahli_waris, pmp.kd_periode, 2), 0) else 0 END Bankes, CASE WHEN '".$tipe_proses."' = 3 THEN nvl(penerimaan_mp_api.get_nominal_transfer(pmp.company, pmp.n_i_k, pmp.no_ahli_waris, pmp.kd_periode, 3), 0) else 0 END Datul, CASE WHEN pmp.kd_jenis_proses = 1 THEN nvl(potongan_mp_api.Get_Nominal(pmp.company, pmp.n_i_k, 'DAKEM', pmp.no_ahli_waris, pmp.kd_periode), 0) else 0 END Dakem, CASE WHEN pmp.kd_jenis_proses = 1 THEN nvl(potongan_mp_api.Get_Nominal(pmp.company, pmp.n_i_k, 'P2TEL', pmp.no_ahli_waris, pmp.kd_periode), 0) else 0 END P2tel, nvl(pajak, 0) + nvl(pajak_final, 0) Tunjangan_Pajak, nvl(denda_pajak, 0) Denda_pajak, jalan jalan, No_Rumah No_Rumah, rt rt, rw rw, kelurahan_api.Get_Nama_Kelurahan(kota_id, provinsi_id, kecamatan_id, kelurahan_id) kelurahan, kecamatan_api.Get_Nama_Kecamatan(provinsi_id, kota_id, kecamatan_id) kecamatan, kota_api.Get_Nama_Kota(provinsi_id, kota_id) Kota, amp.kode_pos kode_pos, ((CASE WHEN '".$sekaligus."' = 'FALSE' THEN CASE WHEN pmp.kd_jenis_proses = 1 THEN pmp.mpb else CASE WHEN '".$tipe_proses."' != 3 THEN pmp.nominal else 0 END END else pmp.mps END + nvl(rapel, 0) + nvl(mp20, 0) + nvl(tht, 0) + CASE WHEN '".$tipe_proses."' = 2 THEN nvl(penerimaan_mp_api.get_nominal_transfer(pmp.company, pmp.n_i_k, pmp.no_ahli_waris, pmp.kd_periode, 2), 0) else 0 END + CASE WHEN '".$tipe_proses."' = 3 THEN nvl(penerimaan_mp_api.get_nominal_transfer(pmp.company, pmp.n_i_k, pmp.no_ahli_waris, pmp.kd_periode, 3), 0) else 0 END + nvl(pajak, 0) + nvl(pajak_final, 0)) - CASE WHEN pmp.kd_jenis_proses = 1 THEN (nvl(potongan_mp_api.Get_Nominal(pmp.company, pmp.n_i_k, 'DAKEM', pmp.no_ahli_waris, pmp.kd_periode), 0) + nvl(potongan_mp_api.Get_Nominal(pmp.company, pmp.n_i_k, 'P2TEL', pmp.no_ahli_waris, pmp.kd_periode), 0) + nvl(pajak, 0) + nvl(pajak_final, 0) + nvl(denda_pajak, 0)) else nvl(pajak, 0) + nvl(pajak_final, 0) + nvl(denda_pajak, 0) END) Total, terbilang_api.Number_To_Terbilang(((CASE WHEN '".$sekaligus."' = 'FALSE' THEN CASE WHEN pmp.kd_jenis_proses = 1 THEN pmp.mpb else CASE WHEN '".$tipe_proses."' != 3 THEN pmp.nominal else 0 END END else pmp.mps END + nvl(rapel, 0) + nvl(mp20, 0) + nvl(tht, 0) + CASE WHEN '".$tipe_proses."' = 2 THEN nvl(penerimaan_mp_api.get_nominal_transfer(pmp.company, pmp.n_i_k, pmp.no_ahli_waris, pmp.kd_periode, 2), 0) else 0 END + CASE WHEN '".$tipe_proses."' = 3 THEN nvl(penerimaan_mp_api.get_nominal_transfer(pmp.company, pmp.n_i_k, pmp.no_ahli_waris, pmp.kd_periode, 3), 0) else 0 END + nvl(pajak, 0) + nvl(pajak_final, 0)) - CASE WHEN pmp.kd_jenis_proses = 1 THEN (nvl(potongan_mp_api.Get_Nominal(pmp.company, pmp.n_i_k, 'DAKEM', pmp.no_ahli_waris, pmp.kd_periode), 0) + nvl(potongan_mp_api.Get_Nominal(pmp.company, pmp.n_i_k, 'P2TEL', pmp.no_ahli_waris, pmp.kd_periode), 0) + nvl(pajak, 0) + nvl(pajak_final, 0) + nvl(denda_pajak, 0)) else nvl(pajak, 0) + nvl(pajak_final, 0) + nvl(denda_pajak, 0) END), 'IDR') Terbilang, b.keterangan, c.accounting_year, c.accounting_period, periode_api.get_indo_month(c.accounting_period) indo_period, periode_api.get_indo_month(sysdate) indo_month, CASE WHEN '".$sekaligus."' = 'TRUE' THEN 'Manfaat Sekaligus' else CASE WHEN '".$tipe_proses."' = 1 THEN 'Manfaat Pensiun' else 'Manfaat Lain' END END field_title, CASE WHEN '".$sekaligus."' = 'TRUE' THEN 'MANFAAT SEKALIGUS' else CASE WHEN '".$tipe_proses."' = 1 THEN 'MANFAAT PENSIUN  BULANAN' else 'MANFAAT LAIN' END END header_title FROM penerimaan_mp pmp LEFT JOIN alamat_penerima_mp amp ON pmp.company = amp.company AND pmp.n_i_K = amp.n_i_k AND pmp.no_ahli_waris = amp.no_ahli_waris AND periode_api.get_tanggal_awal(pmp.company, pmp.kd_periode) BETWEEN amp.valid_from AND amp.valid_to, rekening_penerima_mp rmp, dtl_bank_koordinator dbk, tipe_proses b, periode c WHERE pmp.company = b.company AND pmp.kd_jenis_proses = b.tipe_proses AND b.company = c.company AND c.company = '".$company."' AND c.kd_periode = '".$kd_periode."' AND b.tipe_proses = '".$tipe_proses."' AND pmp.company = rmp.company AND pmp.n_i_k = rmp.n_i_k AND pmp.no_ahli_waris = rmp.no_ahli_waris AND rmp.company = dbk.company AND rmp.institute_id = dbk.kd_bank AND periode_api.get_tanggal_awal(pmp.company, pmp.kd_periode) BETWEEN rmp.valid_from AND rmp.valid_to AND report_sys.parse_parameter(pmp.company, '".$company."') = 'TRUE' AND report_sys.parse_parameter(pmp.kd_periode, '".$kd_periode."') = 'TRUE' AND report_sys.parse_parameter(pmp.n_i_k, '".$nik."') = 'TRUE' AND pmp.no_ahli_waris LIKE nvl('".$naw."', '%') AND pmp.kd_jenis_proses = '".$tipe_proses."'";
            
            $sql1 = oci_parse($conn, $query1);
            oci_execute($sql1);
            
            $rows = array();
            while ($dt1 = oci_fetch_assoc($sql1)) {
                $rows[] = $dt1;
            }
        }

        if (!$conn) {
            $e = oci_error();
            return $response->withJson(["data" => null, "error" => $e, "status" => "success"], 200);
        } else {
            if ($error_msg != '') {
                return $response->withJson(["success" => "false", "message" => $error_msg], 200);
            } elseif (oci_num_rows($sql1) > 0) {
                return $response->withJson(["success" => "true", "message" => "Slip MP Ditemukan!", "data" => $rows], 200);
            } else {
                return $response->withJson(["success" => "false", "message" => "Slip MP Tidak Ditemukan!"], 200);
            }
        }
    }); //[SIMPUL]

    $app->get("/data_valid_kunjungan/", function (ServerRequestInterface $request, ResponseInterface $response){
        $nik = $request->getHeaderLine('NIK');
        $naw = $request->getHeaderLine('NO_AHLI_WARIS');
        
        $conn = getConnection();

        $error_msg = "";
        $error_msg = string_type($nik, $error_msg, 'NIK', 'NotNull');
        $error_msg = int_type($naw, $error_msg, 'No Ahli Waris', 'NotNull');

        if ($error_msg == '') {
            $query2 = "SELECT ALAMAT_PENERIMA_MP_API.Get_P2TEL_default(a.company,a.n_i_k,a.no_ahli_waris,sysdate) cabang_p2tel, MITRA_KERJA_API.Get_Nama_Mitra_Kerja(a.company,ALAMAT_PENERIMA_MP_API.Get_P2TEL_default(a.company,a.n_i_k,a.no_ahli_waris,sysdate)) nama_cab_p2tel, a.n_i_k, a.no_ahli_waris, CALON_PENERIMA_MP_API.Get_Nama_Ahli_Waris(a.company,a.n_i_k,a.no_ahli_waris) nama_penerima_mp, CALON_PENERIMA_MP_API.Get_Jenis_Ahli_Waris(a.company, a.n_i_k, a.no_ahli_waris) jenis_penerima, CALON_PENERIMA_MP_API.Get_Tgl_Lahir(a.company,a.n_i_k,a.no_ahli_waris) tgl_lahir, '-- not yet --' umur, b.jalan, b.komplek komp, b.blok, b.no_rumah nomer, b.rt, b.rw, b.kelurahan_id, (SELECT nama_kelurahan FROM KELURAHAN_TAB WHERE kelurahan_id = b.kelurahan_id) kelurahan, b.kecamatan_id, (SELECT nama_kecamatan FROM KECAMATAN_TAB WHERE kecamatan_id = b.kecamatan_id) kecamatan, b.kota_id, (SELECT nama_kota FROM KOTA_TAB WHERE kota_id = b.kota_id) kota, b.provinsi_id, (SELECT nama_provinsi FROM PROVINSI_TAB WHERE provinsi_id = b.provinsi_id) provinsi, b.kode_pos, (CASE WHEN DAFTAR_PST_TANGGUH_API.Get_NIK(a.company,a.n_i_k,a.no_ahli_waris) IS NOT NULL THEN 'TRUE' else 'FALSE' END) tangguh, KELOMPOK_PESERTA_API.Get_Kelompok_Dapen(a.company, PESERTA_API.Get_Kd_Kelompok(a.company, a.n_i_k)) kelompok_dapen, a.alamat_kontak data_kontak FROM PENERIMA_MP2 a, ALAMAT_PENERIMA_MP b WHERE a.n_i_k = b.n_i_k AND a.no_ahli_waris = b.no_ahli_waris AND (SYSDATE BETWEEN a.valid_from AND a.valid_to) AND (b.no_urut = 1) AND (a.n_i_k LIKE '%".$nik."%') AND (a.no_ahli_waris = '".$naw."')";
            
            $sql = oci_parse($conn, $query2);
            oci_execute($sql);
            
            $rows = array();
            while ($dt = oci_fetch_assoc($sql)) {
                $rows[] = $dt;
            }
        }

        if (!$conn) {
            $e = oci_error();
            return $response->withJson(["data" => null, "error" => $e, "status" => "success"], 200);
        } else {
            if ($error_msg != '') {
                return $response->withJson(["success" => "false", "message" => $error_msg], 200);
            } elseif (oci_num_rows($sql) > 0) {
                return $response->withJson(["success" => "true", "message" => "Data Kunjungan Ditemukan!", "data" => $rows], 200);
            } else {
                return $response->withJson(["success" => "false", "message" => "Data Kunjungan Tidak Ditemukan!"], 200);
            }
        }
    }); //[SIMPUL]

    $app->post("/push_create_mt/", function (ServerRequestInterface $request, ResponseInterface $response){
        $new_data = $request->getParsedBody(); //700184

        $conn = getConnection();

        $error_msg = "";
        $error_msg = string_type($new_data['COMPANY'], $error_msg, 'Company', 'NotNull');
        $error_msg = string_type($new_data['N_I_K'], $error_msg, 'NIK', 'NotNull');
        $error_msg = string_type($new_data['KD_JENIS_MANFAAT'], $error_msg, 'Kd Jenis Manfaat', 'NotNull');
        $error_msg = string_type($new_data['TIPE_PROSES'], $error_msg, 'Tipe Proses', 'NotNull');
        $error_msg = string_type($new_data['KD_PERIODE'], $error_msg, 'Kd Periode', 'NotNull');
        $error_msg = int_type($new_data['NO_AHLI_WARIS'], $error_msg, 'No Ahlli Waris', 'NotNull');
        $error_msg = int_type($new_data['NOMINAL'], $error_msg, 'Nominal', 'Nullable');
        $error_msg = string_type($new_data['KETERANGAN'], $error_msg, 'Keterangan', 'Nullable');

        if ($error_msg == '') {
            $query_oci = "
            DECLARE
                info_       VARCHAR2(32000) := NULL; --p0
                objid_      VARCHAR2(32000) := NULL; --p1
                objversion_ VARCHAR2(32000) := NULL; --p2
                attr_       VARCHAR2(32000) := NULL; --p3
                error_log_  VARCHAR2(2000);
            BEGIN
                Client_SYS.Clear_Attr(attr_);
                Client_Sys.Add_To_Attr('COMPANY', '".$new_data['COMPANY']."', attr_);
                Client_Sys.Add_To_Attr('N_I_K', '".$new_data['N_I_K']."', attr_);
                Client_Sys.Add_To_Attr('KD_JENIS_MANFAAT', UPPER('".$new_data['KD_JENIS_MANFAAT']."'), attr_);
                Client_Sys.Add_To_Attr('TIPE_PROSES', '".$new_data['TIPE_PROSES']."', attr_);
                Client_Sys.Add_To_Attr('KD_PERIODE', '".$new_data['KD_PERIODE']."', attr_);
                Client_Sys.Add_To_Attr('NO_AHLI_WARIS', '".$new_data['NO_AHLI_WARIS']."', attr_);
                Client_Sys.Add_To_Attr('NOMINAL', '".$new_data['NOMINAL']."', attr_);
                Client_Sys.Add_To_Attr('KETERANGAN', '".$new_data['KETERANGAN']."', attr_);
                                            
                MANFAAT_TAMBAHAN_API.New__(info_, objid_, objversion_, attr_, 'DO');
                                
                EXCEPTION 
                WHEN OTHERS THEN
                error_log_ := Sqlerrm;
            END;
            ";
            
            $sql = oci_parse($conn, $query_oci);

            if (oci_execute($sql)) {
                return $response->withJson(["success" => "true", "message" => "Manfaat Tambahan Berhasil Tersimpan!"], 200);
            } else {
                $e = oci_error();
                return $response->withJson(["data" => null, "error" => $e, "status" => "success"], 200);
            }
        } elseif ($error_msg != '') {
            return $response->withJson(["success" => "false", "message" => $error_msg], 200);
        }
    }); //[SIMPUL]

    $app->post("/push_create_sppb/", function (ServerRequestInterface $request, ResponseInterface $response){
        $new_data = $request->getParsedBody(); //700184

        $conn = getConnection();

        $error_msg = "";
        $error_msg = string_type($new_data['VENDOR_NO'], $error_msg, 'Vendor No', 'NotNull');

        if ($error_msg == '') {
            $query_oci = "
            DECLARE
                info_       VARCHAR2(32000) := NULL; --p0
                objid_      VARCHAR2(32000) := NULL; --p1
                objversion_ VARCHAR2(32000) := NULL; --p2
                attr_       VARCHAR2(32000) := NULL; --p3
                error_log_  VARCHAR2(2000);
                temp_       VARCHAR2(2000);
                next_number_   SERIAL_NUMBER.next_number%TYPE;
                next_       NUMBER;
                
                CURSOR get_attr_ IS
                    SELECT nvl(next_number,0)
                    FROM CTM_SERIAL_NUMBER_TAB
                    WHERE name = 'SPPB';
            BEGIN
                OPEN get_attr_;
                FETCH get_attr_ INTO next_;
                CLOSE get_attr_;
                UPDATE CTM_SERIAL_NUMBER_TAB
                SET next_number = next_ + 1
                WHERE name  = 'SPPB';
                COMMIT;
                next_number_ := next_ + 1;
                IF (Ctm_Serial_Number_API.Get_Serial_Long('SPPB') > 0) THEN
                temp_ := to_char(SYSDATE, 'YYYY') || LPAD(TO_CHAR(next_number_), Ctm_Serial_Number_API.Get_Serial_Long('SPPB'), '0');
                ELSE
                temp_ := to_char(SYSDATE, 'YYYY') || TO_CHAR(next_number_);
                END IF;
                
                Client_SYS.Clear_Attr(attr_);
                Client_Sys.Add_To_Attr('NO_SPPB', temp_, attr_);
                Client_Sys.Add_To_Attr('NO_SPPB_CETAK', '".$new_data['NO_SPPB_CETAK']."', attr_);
                Client_Sys.Add_To_Attr('TGL_SPPB', NVL('".$new_data['TGL_SPPB']."',SYSDATE), attr_);
                Client_Sys.Add_To_Attr('KETERANGAN', '".$new_data['KETERANGAN']."', attr_);
                Client_Sys.Add_To_Attr('JUMLAH_SPPB', '".$new_data['JUMLAH_SPPB']."', attr_);
                Client_Sys.Add_To_Attr('NO_REKENING', '".$new_data['NO_REKENING']."', attr_);
                Client_Sys.Add_To_Attr('ATAS_NAMA', '".$new_data['ATAS_NAMA']."', attr_);
                Client_Sys.Add_To_Attr('BEBAN_ANGGARAN', '".$new_data['BEBAN_ANGGARAN']."', attr_);
                Client_Sys.Add_To_Attr('DIAJUKAN_OLEH', '".$new_data['DIAJUKAN_OLEH']."', attr_);
                Client_Sys.Add_To_Attr('JABATAN', '".$new_data['JABATAN']."', attr_);
                Client_Sys.Add_To_Attr('DIAJUKAN_OLEH_', '".$new_data['DIAJUKAN_OLEH_1']."', attr_);
                Client_Sys.Add_To_Attr('JABATAN_', '".$new_data['JABATAN_1']."', attr_);
                Client_Sys.Add_To_Attr('CATATAN', '".$new_data['CATATAN']."', attr_);
                Client_Sys.Add_To_Attr('CETAK', 'FALSE', attr_);
                Client_Sys.Add_To_Attr('NO_BUKTI', '".$new_data['NO_BUKTI']."', attr_);
                Client_Sys.Add_To_Attr('NO_BUKTI_BATAL', '".$new_data['NO_BUKTI_BATAL']."', attr_);
                Client_Sys.Add_To_Attr('KODE_DIREKTORAT', '".$new_data['KODE_DIREKTORAT']."', attr_);
                Client_Sys.Add_To_Attr('KODE_MAPPING', '".$new_data['KODE_MAPPING']."', attr_);
                Client_Sys.Add_To_Attr('KODE_PERKIRAAN', '".$new_data['KODE_PERKIRAAN']."', attr_);
                Client_Sys.Add_To_Attr('KODE_BANK', '".$new_data['KODE_BANK']."', attr_);
                Client_Sys.Add_To_Attr('KODE_CABANG_BANK', '".$new_data['KODE_CABANG_BANK']."', attr_);
                Client_Sys.Add_To_Attr('NAMA_BANK', Cabang_Bank_API.Get_Nama_Cabang_Bank('".$new_data['KODE_BANK']."', '".$new_data['KODE_CABANG_BANK']."'), attr_);
                Client_Sys.Add_To_Attr('SUMBER', '".$new_data['SUMBER']."', attr_);
                Client_Sys.Add_To_Attr('ORDER_NO1', '".$new_data['ORDER_NO1']."', attr_);
                Client_Sys.Add_To_Attr('ORDER_NO2', '".$new_data['ORDER_NO2']."', attr_);
                Client_Sys.Add_To_Attr('ORDER_NO3', '".$new_data['ORDER_NO3']."', attr_);
                Client_Sys.Add_To_Attr('ORDER_NO4', '".$new_data['ORDER_NO4']."', attr_);
                Client_Sys.Add_To_Attr('GENERATE_JURNAL', 'TRUE', attr_);
                Client_Sys.Add_To_Attr('JENIS_SPPB', INITCAP('".$new_data['JENIS_SPPB']."'), attr_);
                Client_Sys.Add_To_Attr('NO_BUKTI_SPB', '".$new_data['NO_BUKTI_SPB']."', attr_);
                Client_Sys.Add_To_Attr('NO_SPPB_PANJAR', '".$new_data['NO_SPPB_PANJAR']."', attr_);
                Client_Sys.Add_To_Attr('PROCESS_ID', '".$new_data['PROCESS_ID']."', attr_);
                Client_Sys.Add_To_Attr('SUMBER_DIREKTORAT', '".$new_data['SUMBER_DIREKTORAT']."', attr_);
                Client_Sys.Add_To_Attr('CASH_ACCOUNT', '".$new_data['CASH_ACCOUNT']."', attr_);
                Client_Sys.Add_To_Attr('SISA_PANJAR', '".$new_data['SISA_PANJAR']."', attr_);
                Client_Sys.Add_To_Attr('AMOUNT', '".$new_data['AMOUNT']."', attr_);
                Client_Sys.Add_To_Attr('VENDOR_NO', '".$new_data['VENDOR_NO']."', attr_);
                                            
                SPPB_HEADER_API.New__(info_, objid_, objversion_, attr_, 'DO');
                                
                EXCEPTION 
                WHEN OTHERS THEN
                error_log_ := Sqlerrm;
            END;
            ";
            
            $sql = oci_parse($conn, $query_oci);
            
            if (oci_execute($sql)) {
                return $response->withJson(["success" => "true", "message" => "SPPB Berhasil Tersimpan!"], 200);
            } else {
                $e = oci_error();
                return $response->withJson(["data" => null, "error" => $e, "status" => "success"], 200);
            }
        } elseif ($error_msg != '') {
            return $response->withJson(["success" => "false", "message" => $error_msg], 200);
        }
    }); //[SIMPUL]

    $app->get("/status_sppb/", function (ServerRequestInterface $request, ResponseInterface $response, array $args) {
        $no_sppb = $request->getHeaderLine('NO_SPPB');
        
        $conn = getConnection();

        $error_msg = "";
        $error_msg = string_type($no_sppb, $error_msg, 'No. SPPb', 'NotNull');

        if ($error_msg == '') {
            $query = "SELECT a.no_sppb, a.state status_sppb, (REGEXP_REPLACE(a.catatan,'[^ -~]','')) keterangan_sppb FROM SPPB_HEADER a WHERE (a.no_sppb = '".$no_sppb."')";

            $sql = oci_parse($conn, $query);
            oci_execute($sql);
            
            $rows = array();
            while ($dt = oci_fetch_assoc($sql)) {
                $rows[] = $dt;
            }
        }

        if (!$conn) {
            $e = oci_error();
            return $response->withJson(["data" => null, "error" => $e, "status" => "success"], 200);
        } else {
            if ($error_msg != '') {
                return $response->withJson(["success" => "false", "message" => $error_msg], 200);
            } elseif (oci_num_rows($sql) > 0) {
                return $response->withJson(["success" => "true", "message" => "Data Laporan Tangguhan Ditemukan!", "tracking_laporan" => $rows], 200);
            } else {
                return $response->withJson(["success" => "false", "message" => "Data Laporan Kematian Tangguhan Tidak Ditemukan!"], 200);
            }
        }
    }); //[SIMPUL]

    $app->get("/data_alamat/", function (ServerRequestInterface $request, ResponseInterface $response, array $args) {
        $nik = $request->getHeaderLine('NIK');
        $naw = $request->getHeaderLine('NO_AHLI_WARIS');
        
        $conn = getConnection();

        $error_msg = "";
        $error_msg = string_type($nik, $error_msg, 'NIK', 'NotNull');
        $error_msg = int_type($naw, $error_msg, 'No Ahli Waris', 'NotNull');

        if ($error_msg == '') {
            $data_array = array();

            $query = "SELECT a.n_i_k, a.no_ahli_waris, CALON_PENERIMA_MP_API.Get_Nama_Ahli_Waris(a.company,a.n_i_k,a.no_ahli_waris) nama, b.no_urut, b.jalan, b.komplek komp, b.blok, b.no_rumah nomer, b.rt, b.rw, b.kelurahan_id, (SELECT nama_kelurahan FROM KELURAHAN_TAB WHERE kelurahan_id = b.kelurahan_id) kelurahan, b.kecamatan_id, (SELECT nama_kecamatan FROM KECAMATAN_TAB WHERE kecamatan_id = b.kecamatan_id) kecamatan, b.kota_id, (SELECT nama_kota FROM KOTA_TAB WHERE kota_id = b.kota_id) kota, b.provinsi_id, (SELECT nama_provinsi FROM PROVINSI_TAB WHERE provinsi_id = b.provinsi_id) provinsi, b.kode_pos, ALAMAT_PENERIMA_MP_API.Get_P2TEL_default(a.company,a.n_i_k,a.no_ahli_waris,sysdate) kd_cabang, MITRA_KERJA_API.Get_Nama_Mitra_Kerja(a.company,ALAMAT_PENERIMA_MP_API.Get_P2TEL_default(a.company,a.n_i_k,a.no_ahli_waris,sysdate)) cabang, (CASE b.no_urut WHEN 1 THEN 1 else 0 END) utama FROM PENERIMAAN_MP a, ALAMAT_PENERIMA_MP b WHERE a.company = b.company AND a.n_i_k = b.n_i_k AND a.no_ahli_waris = b.no_ahli_waris AND a.kd_periode = PERIODE_API.Get_Periode(a.company,SYSDATE) AND a.n_i_k = '".$nik."' AND a.no_ahli_waris = '".$naw."' ORDER BY b.no_urut";

            $sql = oci_parse($conn, $query);
            oci_execute($sql);
            
            $rows = array();
            while ($dt = oci_fetch_assoc($sql)) {
                $rows[] = $dt;
            }
        }

        if (!$conn) {
            $e = oci_error();
            return $response->withJson(["data" => null, "error" => $e, "status" => "success"], 200);
        } else {
            if ($error_msg != '') {
                return $response->withJson(["success" => "false", "message" => $error_msg], 200);
            } elseif (oci_num_rows($sql) > 0) {
                return $response->withJson(["success" => "true", "message" => "Alamat PMP Ditemukan!", "alamat" => $rows], 200);
            } else {
                return $response->withJson(["success" => "false", "message" => "Alamat PMP Tidak Ditemukan!"], 200);
            }
        }
    }); //[SIMPUL]

    $app->get("/data_kontak/", function (ServerRequestInterface $request, ResponseInterface $response, array $args) {
        $nik = $request->getHeaderLine('NIK');
        $naw = $request->getHeaderLine('NO_AHLI_WARIS');
        
        $conn = getConnection();

        $error_msg = "";
        $error_msg = string_type($nik, $error_msg, 'NIK', 'NotNull');
        $error_msg = int_type($naw, $error_msg, 'No Ahli Waris', 'NotNull');

        if ($error_msg == '') {
            $data_array = array();

            $query = "SELECT a.n_i_k, a.no_ahli_waris, CALON_PENERIMA_MP_API.Get_Nama_Ahli_Waris(a.company,a.n_i_k,a.no_ahli_waris) nama, c.no_urut_alamat, c.no_urut_kontak, c.data_kontak isi_kontak, c.kd_kontak id_jenis, JENIS_KONTAK_API.Get_Keterangan(c.kd_kontak) keterangan_jenis, c.catatan FROM PENERIMAAN_MP a, ALAMAT_PENERIMA_MP b, KONTAK_PENERIMA_MP c WHERE a.company = b.company AND a.n_i_k = b.n_i_k AND a.no_ahli_waris = b.no_ahli_waris AND b.company = c.company AND b.n_i_k = c.n_i_k AND b.no_ahli_waris = c.no_ahli_waris AND b.no_urut = c.no_urut_alamat AND a.kd_periode = PERIODE_API.Get_Periode(a.company,SYSDATE) AND b.n_i_k = '".$nik."' AND b.no_ahli_waris = '".$naw."' ORDER BY c.no_urut_alamat, c.no_urut_kontak";

            $sql = oci_parse($conn, $query);
            oci_execute($sql);
            
            $rows = array();
            while ($dt = oci_fetch_assoc($sql)) {
                $rows[] = $dt;
            }
        }

        if (!$conn) {
            $e = oci_error();
            return $response->withJson(["data" => null, "error" => $e, "status" => "success"], 200);
        } else {
            if ($error_msg != '') {
                return $response->withJson(["success" => "false", "message" => $error_msg], 200);
            } elseif (oci_num_rows($sql) > 0) {
                return $response->withJson(["success" => "true", "message" => "Data Kontak Ditemukan!", "kontak" => $rows], 200);
            } else {
                return $response->withJson(["success" => "false", "message" => "Data Kontak Tidak Ditemukan!"], 200);
            }
        }
    }); //[SIMPUL]

    $app->post("/push_ubah_alamat/", function (ServerRequestInterface $request, ResponseInterface $response){
        $new_data = $request->getParsedBody(); //700184

        $conn = getConnection();

        $error_msg = "";
        $error_msg = string_type($new_data['COMPANY'], $error_msg, 'Company', 'NotNull');
        $error_msg = string_type($new_data['N_I_K'], $error_msg, 'NIK', 'NotNull');
        $error_msg = string_type($new_data['KD_PERIODE'], $error_msg, 'Kd Periode', 'NotNull');
        $error_msg = int_type($new_data['NO_AHLI_WARIS'], $error_msg, 'No Ahlli Waris', 'NotNull');
        $error_msg = string_type($new_data['SUMBER'], $error_msg, 'Sumber', 'NotNull');
        $error_msg = string_type($new_data['SUMBER_DATA'], $error_msg, 'Sumber Data', 'NotNull');
        $error_msg = string_type($new_data['ALAMAT_BARU'], $error_msg, 'Alamat Baru', 'NotNull');

        if ($error_msg == '') {
            $query_oci = "
            DECLARE
                info_       VARCHAR2(32000) := NULL; --p0
                objid_      VARCHAR2(32000) := NULL; --p1
                objversion_ VARCHAR2(32000) := NULL; --p2
                attr_       VARCHAR2(32000) := NULL; --p3
                error_log_  VARCHAR2(2000);
            BEGIN
                Client_SYS.Clear_Attr(attr_);
                Client_Sys.Add_To_Attr('COMPANY', '".$new_data['COMPANY']."', attr_);
                Client_Sys.Add_To_Attr('N_I_K', '".$new_data['N_I_K']."', attr_);
                Client_Sys.Add_To_Attr('KD_PERIODE', '".$new_data['KD_PERIODE']."', attr_);
                Client_Sys.Add_To_Attr('KD_MITRA_KERJA', '".$new_data['KD_MITRA_KERJA']."', attr_);
                Client_Sys.Add_To_Attr('JENIS_AHLI_WARIS', CALON_PENERIMA_MP_API.Get_Jenis_Ahli_Waris('DPT02','700184',1), attr_);
                Client_Sys.Add_To_Attr('NAMA_AHLI_WARIS', CALON_PENERIMA_MP_API.Get_Nama_Ahli_Waris('DPT02','700184',1), attr_);
                Client_Sys.Add_To_Attr('ALAMAT_LAMA', ALAMAT_PENERIMA_MP_API.Get_Alamat2('".$new_data['COMPANY']."','".$new_data['N_I_K']."','".$new_data['NO_AHLI_WARIS']."','".$new_data['NO_URUT']."'), attr_);
                Client_Sys.Add_To_Attr('KELURAHAN_LAMA', ALAMAT_PENERIMA_MP_API.Get_Kelurahan_Id('".$new_data['COMPANY']."','".$new_data['NO_AHLI_WARIS']."','".$new_data['N_I_K']."','".$new_data['NO_URUT']."'), attr_);
                Client_Sys.Add_To_Attr('KECAMATAN_LAMA', ALAMAT_PENERIMA_MP_API.Get_Kecamatan_Id('".$new_data['COMPANY']."','".$new_data['NO_AHLI_WARIS']."','".$new_data['N_I_K']."','".$new_data['NO_URUT']."'), attr_);
                Client_Sys.Add_To_Attr('KOTA_LAMA', ALAMAT_PENERIMA_MP_API.Get_Kota_Id('".$new_data['COMPANY']."','".$new_data['NO_AHLI_WARIS']."','".$new_data['N_I_K']."','".$new_data['NO_URUT']."'), attr_);
                Client_Sys.Add_To_Attr('PROVINSI_LAMA', ALAMAT_PENERIMA_MP_API.Get_Provinsi_Id('".$new_data['COMPANY']."','".$new_data['NO_AHLI_WARIS']."','".$new_data['N_I_K']."','".$new_data['NO_URUT']."'), attr_);
                Client_Sys.Add_To_Attr('KODE_POS_LAMA', ALAMAT_PENERIMA_MP_API.Get_Kode_Pos('".$new_data['COMPANY']."','".$new_data['NO_AHLI_WARIS']."','".$new_data['N_I_K']."','".$new_data['NO_URUT']."'), attr_);
                Client_Sys.Add_To_Attr('NO_TELP_LAMA', '', attr_);
                Client_Sys.Add_To_Attr('NO_TELP_LAIN_LAMA', '', attr_);
                Client_Sys.Add_To_Attr('ALAMAT_LAIN_LAMA', '', attr_);
                Client_Sys.Add_To_Attr('NO_REFERENSI', '', attr_);
                Client_Sys.Add_To_Attr('ALAMAT_BARU', '".$new_data['ALAMAT_BARU']."', attr_);
                Client_Sys.Add_To_Attr('KELURAHAN_BARU', '".$new_data['KELURAHAN_BARU']."', attr_);
                Client_Sys.Add_To_Attr('KECAMATAN_BARU', '".$new_data['KECAMATAN_BARU']."', attr_);
                Client_Sys.Add_To_Attr('KOTA_BARU', '".$new_data['KOTA_BARU']."', attr_);
                Client_Sys.Add_To_Attr('PROVINSI_BARU', '".$new_data['PROVINSI_BARU']."', attr_);
                Client_Sys.Add_To_Attr('KODE_POS_BARU', '".$new_data['KODE_POS_BARU']."', attr_);
                Client_Sys.Add_To_Attr('NO_TELP_BARU', '".$new_data['NO_TELP_BARU']."', attr_);
                Client_Sys.Add_To_Attr('NO_TELP_LAIN_BARU', '".$new_data['NO_TELP_LAIN_BARU']."', attr_);
                Client_Sys.Add_To_Attr('ALAMAT_LAIN_BARU', '".$new_data['ALAMAT_LAIN_BARU']."', attr_);
                Client_Sys.Add_To_Attr('JALAN_LAMA', ALAMAT_PENERIMA_MP_API.Get_Jalan('".$new_data['COMPANY']."','".$new_data['NO_AHLI_WARIS']."','".$new_data['N_I_K']."','".$new_data['NO_URUT']."'), attr_);
                Client_Sys.Add_To_Attr('BLOK_LAMA', ALAMAT_PENERIMA_MP_API.Get_Blok('".$new_data['COMPANY']."','".$new_data['NO_AHLI_WARIS']."','".$new_data['N_I_K']."','".$new_data['NO_URUT']."'), attr_);
                Client_Sys.Add_To_Attr('NO_RUMAH_LAMA', ALAMAT_PENERIMA_MP_API.Get_No_Rumah('".$new_data['COMPANY']."','".$new_data['NO_AHLI_WARIS']."','".$new_data['N_I_K']."','".$new_data['NO_URUT']."'), attr_);
                Client_Sys.Add_To_Attr('KOMPLEK_LAMA', ALAMAT_PENERIMA_MP_API.Get_Komplek('".$new_data['COMPANY']."','".$new_data['NO_AHLI_WARIS']."','".$new_data['N_I_K']."','".$new_data['NO_URUT']."'), attr_);
                Client_Sys.Add_To_Attr('RT_LAMA', ALAMAT_PENERIMA_MP_API.Get_Rt('".$new_data['COMPANY']."','".$new_data['NO_AHLI_WARIS']."','".$new_data['N_I_K']."','".$new_data['NO_URUT']."'), attr_);
                Client_Sys.Add_To_Attr('RW_LAMA', ALAMAT_PENERIMA_MP_API.Get_Rw('".$new_data['COMPANY']."','".$new_data['NO_AHLI_WARIS']."','".$new_data['N_I_K']."','".$new_data['NO_URUT']."'), attr_);
                Client_Sys.Add_To_Attr('JALAN_BARU', '".$new_data['JALAN_BARU']."', attr_);
                Client_Sys.Add_To_Attr('BLOK_BARU', '".$new_data['BLOK_BARU']."', attr_);
                Client_Sys.Add_To_Attr('NO_RUMAH_BARU', '".$new_data['NO_RUMAH_BARU']."', attr_);
                Client_Sys.Add_To_Attr('KOMPLEK_BARU', '".$new_data['KOMPLEK_BARU']."', attr_);
                Client_Sys.Add_To_Attr('RT_BARU', '".$new_data['RT_BARU']."', attr_);
                Client_Sys.Add_To_Attr('RW_BARU', '".$new_data['RW_BARU']."', attr_);
                Client_Sys.Add_To_Attr('SUMBER_DATA', '".$new_data['SUMBER_DATA']."', attr_);
                Client_Sys.Add_To_Attr('SUMBER', UPPER('".$new_data['SUMBER']."'), attr_);
                Client_Sys.Add_To_Attr('UPDATE_MASTER', '".$new_data['UPDATE_MASTER']."', attr_);
                Client_Sys.Add_To_Attr('NO_AHLI_WARIS', '".$new_data['NO_AHLI_WARIS']."', attr_);
                Client_Sys.Add_To_Attr('NO_KTP_LAMA', CALON_PENERIMA_MP_API.Get_No_Ktp('".$new_data['COMPANY']."','".$new_data['N_I_K']."','".$new_data['NO_AHLI_WARIS']."'), attr_);
                Client_Sys.Add_To_Attr('NO_KTP_BARU', '".$new_data['NO_KTP_BARU']."', attr_);
                Client_Sys.Add_To_Attr('NO_URUT', '".$new_data['NO_URUT']."', attr_);
                Client_Sys.Add_To_Attr('ALAMAT_BARU_CHK', '".$new_data['ALAMAT_BARU_CHK']."', attr_);
                Client_Sys.Add_To_Attr('UBAH_ALAMAT', '".$new_data['UBAH_ALAMAT']."', attr_);
                Client_Sys.Add_To_Attr('KD_KONTAK_LAMA', '', attr_);
                Client_Sys.Add_To_Attr('KD_KONTAK_BARU', '".$new_data['KD_KONTAK_BARU']."', attr_);
                Client_Sys.Add_To_Attr('DATA_KONTAK_LAMA', '', attr_);
                Client_Sys.Add_To_Attr('DATA_KONTAK_BARU', '".$new_data['DATA_KONTAK_BARU']."', attr_);
                Client_Sys.Add_To_Attr('DEFAULT1', '".$new_data['DEFAULT1']."', attr_);
                Client_Sys.Add_To_Attr('LUAR_NEGERI', '".$new_data['LUAR_NEGERI']."', attr_);
                Client_Sys.Add_To_Attr('CATATAN', '".$new_data['CATATAN']."', attr_);
                Client_Sys.Add_To_Attr('NO_URUT_KONTAK', '".$new_data['NO_URUT_KONTAK']."', attr_);
                Client_Sys.Add_To_Attr('VALID_FROM', ALAMAT_PENERIMA_MP_API.Get_Valid_From('".$new_data['COMPANY']."','".$new_data['NO_AHLI_WARIS']."','".$new_data['N_I_K']."','".$new_data['NO_URUT']."'), attr_);
                Client_Sys.Add_To_Attr('VALID_TO', ALAMAT_PENERIMA_MP_API.Get_Valid_To('".$new_data['COMPANY']."','".$new_data['NO_AHLI_WARIS']."','".$new_data['N_I_K']."','".$new_data['NO_URUT']."'), attr_);
                Client_Sys.Add_To_Attr('TGL_TRANSAKSI', TO_DATE('".$new_data['TGL_TRANSAKSI']."','MM/DD/YYYY'), attr_);
                Client_Sys.Add_To_Attr('PERIODE_TANGGUH', '".$new_data['PERIODE_TANGGUH']."', attr_);
                                            
                TR_PERUBAHAN_ALAMAT_API.New__(info_, objid_, objversion_, attr_, 'DO');
                                
                EXCEPTION 
                WHEN OTHERS THEN
                error_log_ := Sqlerrm;
            END;
            ";
            
            $sql = oci_parse($conn, $query_oci);
            
            if (oci_execute($sql)) {
                return $response->withJson(["success" => "true", "message" => "Alamat Utama Berhasil Diperbarui!"], 200);
            } else {
                $e = oci_error();
                return $response->withJson(["data" => null, "error" => $e, "status" => "success"], 200);
            }
        } elseif ($error_msg != '') {
            return $response->withJson(["success" => "false", "message" => $error_msg], 200);
        }
    }); //[SIMPUL]

    $app->post("/push_ubah_kontak/", function (ServerRequestInterface $request, ResponseInterface $response){
        $new_data = $request->getParsedBody(); //700184

        $conn = getConnection();

        $error_msg = "";
        $error_msg = string_type($new_data['COMPANY'], $error_msg, 'Company', 'NotNull');
        $error_msg = string_type($new_data['N_I_K'], $error_msg, 'NIK', 'NotNull');
        $error_msg = int_type($new_data['NO_AHLI_WARIS'], $error_msg, 'No Ahlli Waris', 'NotNull');
        $error_msg = int_type($new_data['NO_TRANSAKSI'], $error_msg, 'No Transaksi', 'NotNull');
        $error_msg = int_type($new_data['NO_URUT_LAMA'], $error_msg, 'No Urut Lama', 'NotNull');
        $error_msg = string_type($new_data['KD_KONTAK'], $error_msg, 'Kd Kontak', 'NotNull');
        $error_msg = string_type($new_data['DATA_KONTAK_BARU'], $error_msg, 'Data Kontak Baru', 'NotNull');
        $error_msg = string_type($new_data['DIHAPUS'], $error_msg, 'Dihapus', 'NotNull');
        $error_msg = string_type($new_data['CATATAN'], $error_msg, 'Catatan', 'NotNull');

        if ($error_msg == '') {
            $query_oci = "
            DECLARE
                info_       VARCHAR2(32000) := NULL; --p0
                objid_      VARCHAR2(32000) := NULL; --p1
                objversion_ VARCHAR2(32000) := NULL; --p2
                attr_       VARCHAR2(32000) := NULL; --p3
                error_log_  VARCHAR2(2000);

                CURSOR get_data IS
                    SELECT a.objid, a.objversion
                    FROM   TR_PERUBAHAN_ALAMAT_DETIL a
                    WHERE  a.company = '".$new_data['COMPANY']."'
                    AND    a.no_transaksi = '".$new_data['NO_TRANSAKSI']."'
                    AND    a.no_urut = '".$new_data['NO_URUT_LAMA']."';
                
            BEGIN
                OPEN get_data;
                FETCH get_data INTO objid_, objversion_;
                CLOSE get_data;

                Client_SYS.Clear_Attr(attr_);
                Client_Sys.Add_To_Attr('KD_KONTAK', '".$new_data['KD_KONTAK']."', attr_);
                Client_Sys.Add_To_Attr('DATA_KONTAK_LAMA', KONTAK_PENERIMA_MP_API.Get_Data_Kontak('".$new_data['COMPANY']."','".$new_data['N_I_K']."','".$new_data['NO_AHLI_WARIS']."','".$new_data['NO_URUT_LAMA']."'), attr_);
                Client_Sys.Add_To_Attr('DATA_KONTAK_BARU', '".$new_data['DATA_KONTAK_BARU']."', attr_);
                Client_Sys.Add_To_Attr('DIHAPUS', '".$new_data['DIHAPUS']."', attr_);
                Client_Sys.Add_To_Attr('CATATAN', '".$new_data['CATATAN']."', attr_);
                                            
                TR_PERUBAHAN_ALAMAT_DETIL_API.Modify__(info_, objid_, objversion_, attr_, 'DO');
                                
                EXCEPTION 
                WHEN OTHERS THEN
                error_log_ := Sqlerrm;
                
                dbms_output.put_line(error_log_);
            END;
            ";
            
            $sql = oci_parse($conn, $query_oci);
            
            if (oci_execute($sql)) {
                return $response->withJson(["success" => "true", "message" => "Data Kontak Berhasil Diperbarui!"], 200);
            } else {
                $e = oci_error();
                return $response->withJson(["data" => null, "error" => $e, "status" => "success"], 200);
            }
        } elseif ($error_msg != '') {
            return $response->withJson(["success" => "false", "message" => $error_msg], 200);
        }
    }); //[SIMPUL]

    $app->get("/daftar_tangguhan/", function (ServerRequestInterface $request, ResponseInterface $response, array $args) {
        $keyword = $request->getHeaderLine('KEYWORD');
        $kd_cabang = $request->getHeaderLine('KD_CABANG');

        $conn = getConnection();

        $error_msg = "";
        $error_msg = string_type($keyword, $error_msg, 'Keyword', 'NotNull');
        $error_msg = string_type($kd_cabang, $error_msg, 'Kd Cabang', 'Nullable');

        if ($error_msg == '') {
            if ($kd_cabang == '') {
                $get_kd_cabang = "";
            } else {
                $get_kd_cabang = "AND (ALAMAT_PENERIMA_MP_API.Get_P2TEL_default(a.company,a.n_i_k,a.no_ahli_waris,sysdate) = '".$kd_cabang."')";
            }

            $query = "SELECT a.n_i_k, a.no_ahli_waris, CALON_PENERIMA_MP_API.Get_Nama_Ahli_Waris(a.company,a.n_i_k,a.no_ahli_waris) nama_lengkap, REPLACE(ALAMAT_PENERIMA_MP_API.Get_Alamat(a.company,1,a.n_i_k, ALAMAT_PENERIMA_MP_API.Get_No_Urut_Default(a.company,a.n_i_k,1)),'###',' ') alamat, NVL(KONTAK_PENERIMA_MP_API.Get_Data_Kontak(a.company,a.n_i_k,a.no_ahli_waris,'1'),'-') no_kontak, CALON_PENERIMA_MP_API.Get_Jenis_Ahli_Waris(a.company, a.n_i_k, a.no_ahli_waris) status, ALAMAT_PENERIMA_MP_API.Get_P2TEL_default(a.company,a.n_i_k,a.no_ahli_waris,sysdate) kd_cabang, MITRA_KERJA_API.Get_Nama_Mitra_Kerja(a.company,ALAMAT_PENERIMA_MP_API.Get_P2TEL_default(a.company,a.n_i_k,a.no_ahli_waris,sysdate)) cabang, b.picture_id url_foto, a.state, (REGEXP_REPLACE(a.catatan,'[^ -~]','')) catatan FROM DAFTAR_PST_TANGGUH a, PENERIMA_MP2 b WHERE a.company = b.company AND a.n_i_k = b.n_i_k AND a.no_ahli_waris = b.no_ahli_waris AND (a.n_i_k LIKE '%".$keyword."%' OR UPPER(CALON_PENERIMA_MP_API.Get_Nama_Ahli_Waris(a.company,a.n_i_k,a.no_ahli_waris)) LIKE UPPER('%".$keyword."%')) $get_kd_cabang";

            $sql = oci_parse($conn, $query);
            oci_execute($sql);
            
            $rows = array();
            while ($dt = oci_fetch_assoc($sql)) {
                $rows[] = $dt;
            }
        }

        if (!$conn) {
            $e = oci_error();
            return $response->withJson(["data" => null, "error" => $e, "status" => "success"], 200);
        } else {
            if ($error_msg != '') {
                return $response->withJson(["success" => "false", "message" => $error_msg], 200);
            } elseif (oci_num_rows($sql) > 0) {
                return $response->withJson(["success" => "true", "message" => "Data Kontak Ditemukan!", "kontak" => $rows], 200);
            } else {
                return $response->withJson(["success" => "false", "message" => "Data Kontak Tidak Ditemukan!"], 200);
            }
        }
    }); //[SIMPUL]

    $app->get("/status_tangguhan/", function (ServerRequestInterface $request, ResponseInterface $response, array $args) {
        $nik = $request->getHeaderLine('NIK');
        $naw = $request->getHeaderLine('NO_AHLI_WARIS');
        
        $conn = getConnection();

        $error_msg = "";
        $error_msg = string_type($nik, $error_msg, 'NIK', 'NotNull');
        $error_msg = int_type($naw, $error_msg, 'No Ahli Waris', 'NotNull');

        if ($error_msg == '') {
            $query = "SELECT a.n_i_k, a.no_ahli_waris, CALON_PENERIMA_MP_API.Get_Nama_Ahli_Waris(a.company,a.n_i_k,a.no_ahli_waris) nama_lengkap, REPLACE(ALAMAT_PENERIMA_MP_API.Get_Alamat(a.company,1,a.n_i_k, ALAMAT_PENERIMA_MP_API.Get_No_Urut_Default(a.company,a.n_i_k,1)),'###',' ') alamat, NVL(KONTAK_PENERIMA_MP_API.Get_Data_Kontak(a.company,a.n_i_k,a.no_ahli_waris,'1'),'-') no_kontak, CALON_PENERIMA_MP_API.Get_Jenis_Ahli_Waris(a.company, a.n_i_k, a.no_ahli_waris) status, ALAMAT_PENERIMA_MP_API.Get_P2TEL_default(a.company,a.n_i_k,a.no_ahli_waris,sysdate) kd_cabang, MITRA_KERJA_API.Get_Nama_Mitra_Kerja(a.company,ALAMAT_PENERIMA_MP_API.Get_P2TEL_default(a.company,a.n_i_k,a.no_ahli_waris,sysdate)) cabang, b.picture_id url_foto, a.state, (REGEXP_REPLACE(a.catatan,'[^ -~]','')) catatan FROM DAFTAR_PST_TANGGUH a, PENERIMA_MP2 b WHERE a.company = b.company AND a.n_i_k = b.n_i_k AND a.no_ahli_waris = b.no_ahli_waris AND (a.n_i_k LIKE '%".$nik."%') AND (a.no_ahli_waris = '".$naw."')";

            $sql = oci_parse($conn, $query);
            oci_execute($sql);
            
            $rows = array();
            while ($dt = oci_fetch_assoc($sql)) {
                $rows[] = $dt;
            }
        }

        if (!$conn) {
            $e = oci_error();
            return $response->withJson(["data" => null, "error" => $e, "status" => "success"], 200);
        } else {
            if ($error_msg != '') {
                return $response->withJson(["success" => "false", "message" => $error_msg], 200);
            } elseif (oci_num_rows($sql) > 0) {
                return $response->withJson(["success" => "true", "message" => "Data Kontak Ditemukan!", "kontak" => $rows], 200);
            } else {
                return $response->withJson(["success" => "false", "message" => "Data Kontak Tidak Ditemukan!"], 200);
            }
        }
    }); //[SIMPUL]

    $app->post("/push_ubah_rekening/", function (ServerRequestInterface $request, ResponseInterface $response){
        $new_data = $request->getParsedBody(); //700184

        $conn = getConnection();

        $error_msg = "";
        $error_msg = string_type($new_data['N_I_K'], $error_msg, 'NIK', 'NotNull');
        $error_msg = int_type($new_data['NO_AHLI_WARIS'], $error_msg, 'No Ahli Waris', 'NotNull');
        $error_msg = string_type($new_data['KD_PERIODE'], $error_msg, 'Kd Periode', 'NotNull');
        $error_msg = string_type($new_data['SUMBER'], $error_msg, 'Sumber', 'NotNull');
        $error_msg = string_type($new_data['SUMBER_DATA'], $error_msg, 'Sumber Data', 'NotNull');
        $error_msg = string_type($new_data['KD_BANK_BARU'], $error_msg, 'Kd Bank Baru', 'NotNull');
        $error_msg = string_type($new_data['KD_CAB_BARU'], $error_msg, 'Kd Cabang Bank Baru', 'NotNull');
        $error_msg = string_type($new_data['NO_REK_BARU'], $error_msg, 'Nomor Rekening Baru', 'NotNull');
        $error_msg = string_type($new_data['NAMA_REK_BARU'], $error_msg, 'Nama Rekening Baru', 'NotNull');

        if ($error_msg == '') {
            $query_oci = "
            DECLARE
                info_       VARCHAR2(32000) := NULL; --p0
                objid_      VARCHAR2(32000) := NULL; --p1
                objversion_ VARCHAR2(32000) := NULL; --p2
                attr_       VARCHAR2(32000) := NULL; --p3
                error_log_  VARCHAR2(2000);
                
            BEGIN    
                Client_SYS.Clear_Attr(attr_);
                Client_Sys.Add_To_Attr('COMPANY', UPPER('".$new_data['COMPANY']."'), attr_);
                Client_Sys.Add_To_Attr('N_I_K', '".$new_data['N_I_K']."', attr_);
                Client_Sys.Add_To_Attr('KD_PERIODE', '".$new_data['KD_PERIODE']."', attr_);
                Client_Sys.Add_To_Attr('KD_MITRA_KERJA', '".$new_data['KD_MITRA_KERJA']."', attr_);
                Client_Sys.Add_To_Attr('JENIS_AHLI_WARIS', CALON_PENERIMA_MP_API.Get_Jenis_Ahli_Waris(UPPER('".$new_data['COMPANY']."'),'".$new_data['N_I_K']."','".$new_data['NO_AHLI_WARIS']."'), attr_);
                Client_Sys.Add_To_Attr('NAMA_AHLI_WARIS', CALON_PENERIMA_MP_API.Get_Nama_Ahli_Waris(UPPER('".$new_data['COMPANY']."'),'".$new_data['N_I_K']."','".$new_data['NO_AHLI_WARIS']."'), attr_);
                Client_Sys.Add_To_Attr('NO_REFERENSI', '".$new_data['NO_REFERENSI']."', attr_);
                Client_Sys.Add_To_Attr('KD_BANK_LAMA', REKENING_PENERIMA_MP_API.Get_Last_Inst_id(UPPER('".$new_data['COMPANY']."'),'".$new_data['N_I_K']."','".$new_data['NO_AHLI_WARIS']."'), attr_);
                Client_Sys.Add_To_Attr('NO_REK_LAMA', REKENING_PENERIMA_MP_API.Get_Last_No_Rek(UPPER('".$new_data['COMPANY']."'),'".$new_data['N_I_K']."','".$new_data['NO_AHLI_WARIS']."'), attr_);
                Client_Sys.Add_To_Attr('NAMA_REK_LAMA', REKENING_PENERIMA_MP_API.Get_Last_Nama_Rek(UPPER('".$new_data['COMPANY']."'),'".$new_data['N_I_K']."','".$new_data['NO_AHLI_WARIS']."'), attr_);
                Client_Sys.Add_To_Attr('KD_BANK_BARU', '".$new_data['KD_BANK_BARU']."', attr_);
                Client_Sys.Add_To_Attr('NO_REK_BARU', '".$new_data['NO_REK_BARU']."', attr_);
                Client_Sys.Add_To_Attr('NAMA_REK_BARU', '".$new_data['NAMA_REK_BARU']."', attr_);
                Client_Sys.Add_To_Attr('SUMBER_DATA', '".$new_data['SUMBER_DATA']."', attr_);
                Client_Sys.Add_To_Attr('SUMBER', UPPER('".$new_data['SUMBER']."'), attr_);
                Client_Sys.Add_To_Attr('UPDATE_MASTER', '".$new_data['UPDATE_MASTER']."', attr_);
                Client_Sys.Add_To_Attr('NO_AHLI_WARIS', '".$new_data['NO_AHLI_WARIS']."', attr_);
                Client_Sys.Add_To_Attr('KD_CAB_BARU', '".$new_data['KD_CAB_BARU']."', attr_);
                Client_Sys.Add_To_Attr('KD_CAB_LAMA', REKENING_PENERIMA_MP_API.Get_Last_Office_Code(UPPER('".$new_data['COMPANY']."'),'".$new_data['N_I_K']."','".$new_data['NO_AHLI_WARIS']."'), attr_);
                Client_Sys.Add_To_Attr('VALID_FROM', NVL('".$new_data['VALID_FROM']."',SYSDATE), attr_);
                Client_Sys.Add_To_Attr('BEDA', '".$new_data['BEDA']."', attr_);
                                            
                TR_PINDAH_REK_BANK_API.New__(info_, objid_, objversion_, attr_, 'DO');
                                
                EXCEPTION 
                WHEN OTHERS THEN
                error_log_ := Sqlerrm;
                
                dbms_output.put_line(error_log_);
            END;
            ";
            
            $sql = oci_parse($conn, $query_oci);
            
            if (oci_execute($sql)) {
                return $response->withJson(["success" => "true", "message" => "SPPB Berhasil Tersimpan!"], 200);
            } else {
                $e = oci_error();
                return $response->withJson(["data" => null, "error" => $e, "status" => "success"], 200);
            }
        } elseif ($error_msg != '') {
            return $response->withJson(["success" => "false", "message" => $error_msg], 200);
        }
    }); //[SIMPUL]

    $app->post("/push_ubah_aw/", function (ServerRequestInterface $request, ResponseInterface $response){
        $new_data = $request->getParsedBody(); //700184

        $conn = getConnection();

        $error_msg = "";
        $error_msg = string_type($new_data['N_I_K'], $error_msg, 'NIK', 'NotNull');
        $error_msg = int_type($new_data['NO_AHLI_WARIS'], $error_msg, 'No Ahli Waris', 'NotNull');
        $error_msg = string_type($new_data['KD_PERIODE'], $error_msg, 'Kd Periode', 'NotNull');
        $error_msg = string_type($new_data['SUMBER'], $error_msg, 'Sumber', 'NotNull');
        $error_msg = string_type($new_data['SUMBER_DATA'], $error_msg, 'Sumber Data', 'NotNull');
        $error_msg = string_type($new_data['COMPANY'], $error_msg, 'Company', 'NotNull');
        $error_msg = string_type($new_data['NAMA_AHLI_WARIS'], $error_msg, 'Nama Ahli Waris', 'NotNull');
        $error_msg = string_type($new_data['JENIS_AHLI_WARIS'], $error_msg, 'Jenis Ahli Waris', 'NotNull');
        $error_msg = string_type($new_data['TGL_LAHIR'], $error_msg, 'Tanggal Lahir', 'NotNull');

        if ($error_msg == '') {
            $query_oci = "
            DECLARE
                info_       VARCHAR2(32000) := NULL; --p0
                objid_      VARCHAR2(32000) := NULL; --p1
                objversion_ VARCHAR2(32000) := NULL; --p2
                attr_       VARCHAR2(32000) := NULL; --p3
                error_log_  VARCHAR2(2000);
            BEGIN
                Client_SYS.Clear_Attr(attr_);
                Client_Sys.Add_To_Attr('COMPANY', '".$new_data['COMPANY']."', attr_);
                Client_Sys.Add_To_Attr('N_I_K', '".$new_data['N_I_K']."', attr_);
                Client_Sys.Add_To_Attr('NO_AHLI_WARIS', '".$new_data['NO_AHLI_WARIS']."', attr_);
                Client_Sys.Add_To_Attr('JENIS_AHLI_WARIS', INITCAP('".$new_data['JENIS_AHLI_WARIS']."'), attr_);
                Client_Sys.Add_To_Attr('NAMA_AHLI_WARIS', UPPER('".$new_data['NAMA_AHLI_WARIS']."'), attr_);
                Client_Sys.Add_To_Attr('TGL_LAHIR', TO_DATE('".$new_data['TGL_LAHIR']."', 'MM/DD/YYYY'), attr_);
                Client_Sys.Add_To_Attr('TGL_NIKAH', '".$new_data['TGL_NIKAH']."', attr_);
                Client_Sys.Add_To_Attr('TGL_BEKERJA', '".$new_data['TGL_BEKERJA']."', attr_);
                Client_Sys.Add_To_Attr('TGL_MENINGGAL', '".$new_data['TGL_MENINGGAL']."', attr_);
                Client_Sys.Add_To_Attr('TGL_CERAI', '".$new_data['TGL_CERAI']."', attr_);
                Client_Sys.Add_To_Attr('TGL_NIKAH_LAGI', '".$new_data['TGL_NIKAH_LAGI']."', attr_);
                Client_Sys.Add_To_Attr('TGL_LULUS', '".$new_data['TGL_LULUS']."', attr_);
                Client_Sys.Add_To_Attr('JENIS_KELAMIN', '".$new_data['JENIS_KELAMIN']."', attr_);
                Client_Sys.Add_To_Attr('ALAMAT', '".$new_data['ALAMAT']."', attr_);
                Client_Sys.Add_To_Attr('JALAN', '".$new_data['JALAN']."', attr_);
                Client_Sys.Add_To_Attr('BLOK', '".$new_data['BLOK']."', attr_);
                Client_Sys.Add_To_Attr('NO_RUMAH', '".$new_data['NO_RUMAH']."', attr_);
                Client_Sys.Add_To_Attr('KOMPLEK', '".$new_data['KOMPLEK']."', attr_);
                Client_Sys.Add_To_Attr('RT', '".$new_data['RT']."', attr_);
                Client_Sys.Add_To_Attr('RW', '".$new_data['RW']."', attr_);
                Client_Sys.Add_To_Attr('NAMA_LAIN', '".$new_data['NAMA_LAIN']."', attr_);
                Client_Sys.Add_To_Attr('NO_TELP_LAIN', '".$new_data['NO_TELP_LAIN']."', attr_);
                Client_Sys.Add_To_Attr('ALAMAT_LAIN', '".$new_data['ALAMAT_LAIN']."', attr_);
                Client_Sys.Add_To_Attr('KODE_POS', '".$new_data['KODE_POS']."', attr_);
                Client_Sys.Add_To_Attr('NO_TELP', '".$new_data['NO_TELP']."', attr_);
                Client_Sys.Add_To_Attr('ANAK_DARI', '".$new_data['ANAK_DARI']."', attr_);
                Client_Sys.Add_To_Attr('KOTA_ID', '".$new_data['KOTA_ID']."', attr_);
                Client_Sys.Add_To_Attr('PROVINSI_ID', '".$new_data['PROVINSI_ID']."', attr_);
                Client_Sys.Add_To_Attr('KECAMATAN_ID', '".$new_data['KECAMATAN_ID']."', attr_);
                Client_Sys.Add_To_Attr('KELURAHAN_ID', '".$new_data['KELURAHAN_ID']."', attr_);
                Client_Sys.Add_To_Attr('NO_KTP', '".$new_data['NO_KTP']."', attr_);
                Client_Sys.Add_To_Attr('KEPALA_KELUARGA', '".$new_data['KEPALA_KELUARGA']."', attr_);
                Client_Sys.Add_To_Attr('GOL_DARAH', UPPER('".$new_data['GOL_DARAH']."'), attr_);
                Client_Sys.Add_To_Attr('AGAMA', INITCAP('".$new_data['AGAMA']."'), attr_);
                Client_Sys.Add_To_Attr('NO_NPWP', '".$new_data['NO_NPWP']."', attr_);
                Client_Sys.Add_To_Attr('KD_PERIODE', '".$new_data['KD_PERIODE']."', attr_);
                Client_Sys.Add_To_Attr('KD_MITRA_KERJA', '".$new_data['KD_MITRA_KERJA']."', attr_);
                Client_Sys.Add_To_Attr('SUMBER_DATA', '".$new_data['SUMBER_DATA']."', attr_);
                Client_Sys.Add_To_Attr('SUMBER', UPPER('".$new_data['SUMBER']."'), attr_);
                Client_Sys.Add_To_Attr('JENIS_ANAK', INITCAP('".$new_data['JENIS_ANAK']."'), attr_);
                Client_Sys.Add_To_Attr('JENIS_AHLI_WARIS_LAMA', CALON_PENERIMA_MP_API.Get_Jenis_Ahli_Waris('".$new_data['COMPANY']."','".$new_data['N_I_K']."','".$new_data['NO_AHLI_WARIS']."'), attr_);
                Client_Sys.Add_To_Attr('NAMA_AHLI_WARIS_LAMA', CALON_PENERIMA_MP_API.Get_Nama_Ahli_Waris('".$new_data['COMPANY']."','".$new_data['N_I_K']."','".$new_data['NO_AHLI_WARIS']."'), attr_);
                Client_Sys.Add_To_Attr('TGL_LAHIR_LAMA', CALON_PENERIMA_MP_API.Get_Tgl_Lahir('".$new_data['COMPANY']."','".$new_data['N_I_K']."','".$new_data['NO_AHLI_WARIS']."'), attr_);
                Client_Sys.Add_To_Attr('TGL_NIKAH_LAMA', CALON_PENERIMA_MP_API.Get_Tgl_Nikah('".$new_data['COMPANY']."','".$new_data['N_I_K']."','".$new_data['NO_AHLI_WARIS']."'), attr_);
                Client_Sys.Add_To_Attr('TGL_BEKERJA_LAMA', CALON_PENERIMA_MP_API.Get_Tgl_Bekerja('".$new_data['COMPANY']."','".$new_data['N_I_K']."','".$new_data['NO_AHLI_WARIS']."'), attr_);
                Client_Sys.Add_To_Attr('TGL_MENINGGAL_LAMA', CALON_PENERIMA_MP_API.Get_Tgl_Meninggal('".$new_data['COMPANY']."','".$new_data['N_I_K']."','".$new_data['NO_AHLI_WARIS']."'), attr_);
                Client_Sys.Add_To_Attr('TGL_CERAI_LAMA', CALON_PENERIMA_MP_API.Get_Tgl_Cerai('".$new_data['COMPANY']."','".$new_data['N_I_K']."','".$new_data['NO_AHLI_WARIS']."'), attr_);
                Client_Sys.Add_To_Attr('TGL_NIKAH_LAGI_LAMA', CALON_PENERIMA_MP_API.Get_Tgl_Nikah_Lagi('".$new_data['COMPANY']."','".$new_data['N_I_K']."','".$new_data['NO_AHLI_WARIS']."'), attr_);
                Client_Sys.Add_To_Attr('TGL_LULUS_LAMA', CALON_PENERIMA_MP_API.Get_Tgl_Lulus('".$new_data['COMPANY']."','".$new_data['N_I_K']."','".$new_data['NO_AHLI_WARIS']."'), attr_);
                Client_Sys.Add_To_Attr('JENIS_KELAMIN_LAMA', CALON_PENERIMA_MP_API.Get_Jenis_Kelamin('".$new_data['COMPANY']."','".$new_data['N_I_K']."','".$new_data['NO_AHLI_WARIS']."'), attr_);
                Client_Sys.Add_To_Attr('ANAK_DARI_LAMA', CALON_PENERIMA_MP_API.Get_Anak_Dari('".$new_data['COMPANY']."','".$new_data['N_I_K']."','".$new_data['NO_AHLI_WARIS']."'), attr_);
                Client_Sys.Add_To_Attr('GOL_DARAH_LAMA', CALON_PENERIMA_MP_API.Get_Gol_Darah('".$new_data['COMPANY']."','".$new_data['N_I_K']."','".$new_data['NO_AHLI_WARIS']."'), attr_);
                Client_Sys.Add_To_Attr('AGAMA_LAMA', CALON_PENERIMA_MP_API.Get_Agama('".$new_data['COMPANY']."','".$new_data['N_I_K']."','".$new_data['NO_AHLI_WARIS']."'), attr_);
                Client_Sys.Add_To_Attr('JENIS_ANAK_LAMA', CALON_PENERIMA_MP_API.Get_Jenis_Anak('".$new_data['COMPANY']."','".$new_data['N_I_K']."','".$new_data['NO_AHLI_WARIS']."'), attr_);
                                    
                TR_UBAH_AHLI_WARIS_API.New__(info_, objid_, objversion_, attr_, 'DO');
                        
                EXCEPTION 
                WHEN OTHERS THEN
                error_log_ := Sqlerrm;
            END;
            ";
            
            $sql = oci_parse($conn, $query_oci);
            
            if (oci_execute($sql)) {
                return $response->withJson(["success" => "true", "message" => "Data Berhasil Tersimpan!"], 200);
            } else {
                $e = oci_error();
                return $response->withJson(["data" => null, "error" => $e, "status" => "success"], 200);
            }
        } elseif ($error_msg != '') {
            return $response->withJson(["success" => "false", "message" => $error_msg], 200);
        }
    }); //[SIMPUL]

    $app->post("/push_aw_baru/", function (ServerRequestInterface $request, ResponseInterface $response){
        $new_data = $request->getParsedBody(); //700184

        $conn = getConnection();

        $error_msg = "";
        $error_msg = string_type($new_data['COMPANY'], $error_msg, 'Company', 'NotNull');
        $error_msg = string_type($new_data['N_I_K'], $error_msg, 'NIK', 'NotNull');
        $error_msg = string_type($new_data['SUMBER'], $error_msg, 'Sumber', 'NotNull');
        $error_msg = string_type($new_data['SUMBER_DATA'], $error_msg, 'Sumber Data', 'NotNull');
        $error_msg = string_type($new_data['NAMA_AHLI_WARIS'], $error_msg, 'Nama Ahli Waris', 'NotNull');
        $error_msg = string_type($new_data['JENIS_AHLI_WARIS'], $error_msg, 'Jenis Ahli Waris', 'NotNull');
        $error_msg = string_type($new_data['TGL_LAHIR'], $error_msg, 'Tanggal Lahir', 'NotNull');
        $error_msg = string_type($new_data['JENIS_KELAMIN'], $error_msg, 'Jenis Kelamin', 'NotNull');

        if ($error_msg == '') {
            $query_oci = "
            DECLARE
                info_       VARCHAR2(32000) := NULL; --p0
                objid_      VARCHAR2(32000) := NULL; --p1
                objversion_ VARCHAR2(32000) := NULL; --p2
                attr_       VARCHAR2(32000) := NULL; --p3
                error_log_  VARCHAR2(2000);
            BEGIN
                Client_SYS.Clear_Attr(attr_);
                Client_Sys.Add_To_Attr('COMPANY', '".$new_data['COMPANY']."', attr_);
                Client_Sys.Add_To_Attr('N_I_K', '".$new_data['N_I_K']."', attr_);
                Client_Sys.Add_To_Attr('JENIS_AHLI_WARIS', INITCAP('".$new_data['JENIS_AHLI_WARIS']."'), attr_);
                Client_Sys.Add_To_Attr('NAMA_AHLI_WARIS', UPPER('".$new_data['NAMA_AHLI_WARIS']."'), attr_);
                Client_Sys.Add_To_Attr('TGL_LAHIR', TO_DATE('".$new_data['TGL_LAHIR']."', 'MM/dd/yyyy'), attr_);

                Client_Sys.Add_To_Attr('TGL_NIKAH', TO_DATE('".$new_data['TGL_NIKAH']."', 'MM/dd/yyyy'), attr_);
                Client_Sys.Add_To_Attr('TGL_BEKERJA', TO_DATE('".$new_data['TGL_BEKERJA']."', 'MM/dd/yyyy'), attr_);
                Client_Sys.Add_To_Attr('TGL_NIKAH_LAGI', TO_DATE('".$new_data['TGL_NIKAH_LAGI']."', 'MM/dd/yyyy'), attr_);
                Client_Sys.Add_To_Attr('TGL_MENINGGAL', TO_DATE('".$new_data['TGL_MENINGGAL']."', 'MM/dd/yyyy'), attr_);
                Client_Sys.Add_To_Attr('TGL_CERAI', TO_DATE('".$new_data['TGL_CERAI']."', 'MM/dd/yyyy'), attr_);
                
                Client_Sys.Add_To_Attr('JENIS_KELAMIN', INITCAP('".$new_data['JENIS_KELAMIN']."'), attr_);

                Client_Sys.Add_To_Attr('ALAMAT', '".$new_data['ALAMAT']."', attr_);
                Client_Sys.Add_To_Attr('JALAN', '".$new_data['JALAN']."', attr_);
                Client_Sys.Add_To_Attr('NO_RUMAH', '".$new_data['NO_RUMAH']."', attr_);
                Client_Sys.Add_To_Attr('BLOK', '".$new_data['BLOK']."', attr_);
                Client_Sys.Add_To_Attr('KOMPLEK', '".$new_data['KOMPLEK']."', attr_);
                Client_Sys.Add_To_Attr('RT', '".$new_data['RT']."', attr_);
                Client_Sys.Add_To_Attr('RW', '".$new_data['RW']."', attr_);
                Client_Sys.Add_To_Attr('NAMA_LAIN', '".$new_data['NAMA_LAIN']."', attr_);
                Client_Sys.Add_To_Attr('NO_TELP_LAIN', '".$new_data['NO_TELP_LAIN']."', attr_);
                Client_Sys.Add_To_Attr('ALAMAT_LAIN', '".$new_data['ALAMAT_LAIN']."', attr_);
                Client_Sys.Add_To_Attr('KODE_POS', '".$new_data['KODE_POS']."', attr_);
                Client_Sys.Add_To_Attr('NO_TELP', '".$new_data['NO_TELP']."', attr_);

                Client_Sys.Add_To_Attr('ANAK_DARI', '".$new_data['ANAK_DARI']."', attr_);

                Client_Sys.Add_To_Attr('KOTA_ID', '".$new_data['KOTA_ID']."', attr_);
                Client_Sys.Add_To_Attr('PROVINSI_ID', '".$new_data['PROVINSI_ID']."', attr_);
                Client_Sys.Add_To_Attr('KECAMATAN_ID', '".$new_data['KECAMATAN_ID']."', attr_);
                Client_Sys.Add_To_Attr('KELURAHAN_ID', '".$new_data['KELURAHAN_ID']."', attr_);
                Client_Sys.Add_To_Attr('NO_KTP', '".$new_data['NO_KTP']."', attr_);
                Client_Sys.Add_To_Attr('KEPALA_KELUARGA', '".$new_data['KEPALA_KELUARGA']."', attr_);

                Client_Sys.Add_To_Attr('GOL_DARAH', UPPER('".$new_data['GOL_DARAH']."'), attr_);
                Client_Sys.Add_To_Attr('AGAMA', INITCAP('".$new_data['AGAMA']."'), attr_);

                Client_Sys.Add_To_Attr('NO_NPWP', '".$new_data['NO_NPWP']."', attr_);
                Client_Sys.Add_To_Attr('KD_PERIODE', '".$new_data['KD_PERIODE']."', attr_);
                Client_Sys.Add_To_Attr('KD_MITRA_KERJA', '".$new_data['KD_MITRA_KERJA']."', attr_);

                Client_Sys.Add_To_Attr('SUMBER_DATA', '".$new_data['SUMBER_DATA']."', attr_);
                Client_Sys.Add_To_Attr('SUMBER', UPPER('".$new_data['SUMBER']."'), attr_);
                
                Client_Sys.Add_To_Attr('JENIS_ANAK', INITCAP('".$new_data['JENIS_ANAK']."'), attr_);
                            
                TR_TAMBAH_AHLI_WARIS_API.New__(info_, objid_, objversion_, attr_, 'DO');
                
                EXCEPTION 
                WHEN OTHERS THEN
                error_log_ := Sqlerrm;
            END;
            ";
            
            $sql = oci_parse($conn, $query_oci);
            
            if (oci_execute($sql)) {
                return $response->withJson(["success" => "true", "message" => "Data Berhasil Tersimpan!"], 200);
            } else {
                $e = oci_error();
                return $response->withJson(["data" => null, "error" => $e, "status" => "success"], 200);
            }
        } elseif ($error_msg != '') {
            return $response->withJson(["success" => "false", "message" => $error_msg], 200);
        }
    }); //[SIMPUL]

    $app->get("/list_pegawai/", function (ServerRequestInterface $request, ResponseInterface $response){
        $keyword = $request->getHeaderLine('KEYWORD');

        $conn = getConnection();

        $error_msg = "";
        $error_msg = string_type($keyword, $error_msg, 'Keyword', 'NotNull');

        if ($error_msg == '') {
            $query2 = "SELECT a.n_i_k, a.nama_pegawai nama, REPLACE(ALAMAT_PENERIMA_MP_API.Get_Alamat(a.company,1,a.n_i_k, ALAMAT_PENERIMA_MP_API.Get_No_Urut_Default(a.company,a.n_i_k,1)),'###',' ') alamat, (ALAMAT_PENERIMA_MP_API.Get_Kode_Pos(a.company,1,a.n_i_k, ALAMAT_PENERIMA_MP_API.Get_No_Urut_Default(a.company,a.n_i_k,1))) kode_pos, NVL(KONTAK_PENERIMA_MP_API.Get_Data_Kontak(a.company,a.n_i_k,1,'1'),'-') no_telp, CALON_PENERIMA_MP_API.Get_No_Ktp(a.company,a.n_i_k,1) no_ktp, CALON_PENERIMA_MP_API.Get_Gol_Darah(a.COMPANY,a.N_I_K,1) gol_darah, CALON_PENERIMA_MP_API.Get_Agama(a.COMPANY,a.N_I_K,1) agama, CALON_PENERIMA_MP_API.Get_Jenis_Kelamin2(a.COMPANY,a.N_I_K,1) jenis_kelamin, (CASE WHEN CALON_PENERIMA_MP_API.Get_Tgl_Nikah(a.company,a.n_i_k,1) IS NULL THEN 'Belum Menikah' else 'Menikah' END) status_marital, '-- not yet --' no_rekening, '-- not yet --' nama_bank, '-- not yet --' cabang_bank, a.status_peserta FROM PESERTA_AKTIF a WHERE (a.status_peserta = 'Aktif' OR a.status_peserta = 'Mantan') AND (UPPER(CALON_PENERIMA_MP_API.Get_Nama_Ahli_Waris(a.company,a.n_i_k,1)) LIKE UPPER('%".$keyword."%') OR a.n_i_k LIKE '%".$keyword."%')";
            
            $sql = oci_parse($conn, $query2);
            oci_execute($sql);
            
            $rows = array();
            while ($dt = oci_fetch_assoc($sql)) {
                $rows[] = $dt;
            }
        }
        
        if (!$conn) {
            $e = oci_error();
            return $response->withJson(["data" => null, "error" => $e, "status" => "success"], 200);
        } else {
            if ($error_msg != '') {
                return $response->withJson(["success" => "false", "message" => $error_msg], 200);
            } elseif (oci_num_rows($sql) > 0) {
                return $response->withJson(["success" => "true", "message" => "Pegawai Ditemukan!", "data" => $rows], 200);
            } else {
                return $response->withJson(["success" => "false", "message" => "Pegawai Tidak Ditemukan!"], 200);
            }
        }
    }); //[SIMPUL]

    $app->get("/data_pegawai/", function (ServerRequestInterface $request, ResponseInterface $response){
        $nik = $request->getHeaderLine('NIK');

        $conn = getConnection();

        $error_msg = "";
        $error_msg = string_type($nik, $error_msg, 'NIK', 'NotNull');

        if ($error_msg == '') {
            $query2 = "SELECT a.n_i_k, a.nama_pegawai nama, REPLACE(ALAMAT_PENERIMA_MP_API.Get_Alamat(a.company,1,a.n_i_k, ALAMAT_PENERIMA_MP_API.Get_No_Urut_Default(a.company,a.n_i_k,1)),'###',' ') alamat, (ALAMAT_PENERIMA_MP_API.Get_Kode_Pos(a.company,1,a.n_i_k, ALAMAT_PENERIMA_MP_API.Get_No_Urut_Default(a.company,a.n_i_k,1))) kode_pos, NVL(KONTAK_PENERIMA_MP_API.Get_Data_Kontak(a.company,a.n_i_k,1,'1'),'-') no_telp, CALON_PENERIMA_MP_API.Get_No_Ktp(a.company,a.n_i_k,1) no_ktp, CALON_PENERIMA_MP_API.Get_Gol_Darah(a.COMPANY,a.N_I_K,1) gol_darah, CALON_PENERIMA_MP_API.Get_Agama(a.COMPANY,a.N_I_K,1) agama, CALON_PENERIMA_MP_API.Get_Jenis_Kelamin2(a.COMPANY,a.N_I_K,1) jenis_kelamin, (CASE WHEN CALON_PENERIMA_MP_API.Get_Tgl_Nikah(a.company,a.n_i_k,1) IS NULL THEN 'Belum Menikah' else 'Menikah' END) status_marital, '-- not yet --' no_rekening, '-- not yet --' nama_bank, '-- not yet --' cabang_bank, a.status_peserta FROM PESERTA_AKTIF a WHERE (a.status_peserta = 'Aktif' OR a.status_peserta = 'Mantan') AND a.n_i_k = '".$nik."'";
            
            $sql = oci_parse($conn, $query2);
            oci_execute($sql);
            
            $rows = array();
            while ($dt = oci_fetch_assoc($sql)) {
                $rows[] = $dt;
            }
        }
        
        if (!$conn) {
            $e = oci_error();
            return $response->withJson(["data" => null, "error" => $e, "status" => "success"], 200);
        } else {
            if ($error_msg != '') {
                return $response->withJson(["success" => "false", "message" => $error_msg], 200);
            } elseif (oci_num_rows($sql) > 0) {
                return $response->withJson(["success" => "true", "message" => "Pegawai Ditemukan!", "data" => $rows], 200);
            } else {
                return $response->withJson(["success" => "false", "message" => "Pegawai Tidak Ditemukan!"], 200);
            }
        }
    }); //[SIMPUL]

    $app->get("/status_sk/", function (ServerRequestInterface $request, ResponseInterface $response){
        $nik = $request->getHeaderLine('NIK');

        $conn = getConnection();

        $error_msg = "";
        $error_msg = string_type($nik, $error_msg, 'NIK', 'NotNull');

        if ($error_msg == '') {
            $query2 = "SELECT a.state, b.catatan FROM PROSES_SK a, DPT_AUDITTRAIL b WHERE a.objid = b.row_id AND a.state = b.nama_event AND a.n_i_k = '".$nik."'";
            
            $sql = oci_parse($conn, $query2);
            oci_execute($sql);
            
            $rows = array();
            while ($dt = oci_fetch_assoc($sql)) {
                $rows[] = $dt;
            }
        }
        
        if (!$conn) {
            $e = oci_error();
            return $response->withJson(["data" => null, "error" => $e, "status" => "success"], 200);
        } else {
            if ($error_msg != '') {
                return $response->withJson(["success" => "false", "message" => $error_msg], 200);
            } elseif (oci_num_rows($sql) > 0) {
                return $response->withJson(["success" => "true", "message" => "SK Ditemukan!", "data" => $rows], 200);
            } else {
                return $response->withJson(["success" => "false", "message" => "SK Tidak Ditemukan!"], 200);
            }
        }
    }); //[SIMPUL]

    $app->get("/data_pegawai_all/", function (ServerRequestInterface $request, ResponseInterface $response){
        $company = $request->getHeaderLine('COMPANY');
        $nik = $request->getHeaderLine('NIK');

        $conn = getConnection();

        $error_msg = "";
        $error_msg = string_type($company, $error_msg, 'Company', 'NotNull');
        $error_msg = string_type($nik, $error_msg, 'NIK', 'NotNull');

        if ($error_msg == '') {
            $query2 = "SELECT * FROM PESERTA_TAB a WHERE a.company = '".$company."' AND a.n_i_k = '".$nik."'";
            
            $sql = oci_parse($conn, $query2);
            oci_execute($sql);
            
            $rows = array();
            while ($dt = oci_fetch_assoc($sql)) {
                $rows[] = $dt;
            }
        }
        
        if (!$conn) {
            $e = oci_error();
            return $response->withJson(["data" => null, "error" => $e, "status" => "success"], 200);
        } else {
            if ($error_msg != '') {
                return $response->withJson(["success" => "false", "message" => $error_msg], 200);
            } elseif (oci_num_rows($sql) > 0) {
                return $response->withJson(["success" => "true", "message" => "Data Pegawai Ditemukan!", "data" => $rows], 200);
            } else {
                return $response->withJson(["success" => "false", "message" => "Data Pegawai Tidak Ditemukan!"], 200);
            }
        }
    }); //[SIMPUL]

    $app->post("/push_ubah_data_pegawai/", function (ServerRequestInterface $request, ResponseInterface $response){
        $new_data = $request->getParsedBody(); //

        $conn = getConnection();

        $error_msg = "";
        $error_msg = string_type($new_data['COMPANY'], $error_msg, 'Company', 'NotNull');
        $error_msg = string_type($new_data['N_I_K'], $error_msg, 'NIK', 'NotNull');

        if ($error_msg == '') {
            $query_oci = "
            DECLARE
                info_       VARCHAR2(32000) := NULL; --p0
                objid_      VARCHAR2(32000) := NULL; --p1
                objversion_ VARCHAR2(32000) := NULL; --p2
                attr_       VARCHAR2(32000) := NULL; --p3
                error_log_  VARCHAR2(2000);
                nik_        VARCHAR2(2000);

                CURSOR get_data IS
                    SELECT a.objid, a.objversion
                    FROM   PESERTA a
                    WHERE  a.company = UPPER('".$new_data['COMPANY']."')
                    AND    a.n_i_k = '".$new_data['N_I_K']."';
                        
            BEGIN
                OPEN get_data;
                FETCH get_data INTO objid_, objversion_;
                CLOSE get_data;
                
                Client_SYS.Clear_Attr(attr_);
                Client_Sys.Add_To_Attr('COMPANY', UPPER('".$new_data['COMPANY']."'), attr_);
                Client_Sys.Add_To_Attr('N_I_K', '".$new_data['N_I_K']."', attr_);
                Client_Sys.Add_To_Attr('KD_KELOMPOK', '".$new_data['KD_KELOMPOK']."', attr_);
                Client_Sys.Add_To_Attr('N_I_K_PASANGAN', '".$new_data['N_I_K_PASANGAN']."', attr_);
                Client_Sys.Add_To_Attr('NAMA_PEGAWAI', '".$new_data['NAMA_PEGAWAI']."', attr_);
                Client_Sys.Add_To_Attr('TGL_LAHIR', TO_DATE('".$new_data['TGL_LAHIR']."','MM/DD/YYYY'), attr_);
                Client_Sys.Add_To_Attr('TGL_KERJA', TO_DATE('".$new_data['TGL_KERJA']."','MM/DD/YYYY'), attr_);
                Client_Sys.Add_To_Attr('PENERIMA', '".$new_data['PENERIMA']."', attr_);
                Client_Sys.Add_To_Attr('TGL_PINDAH', TO_DATE('".$new_data['TGL_PINDAH']."','MM/DD/YYYY'), attr_);
                Client_Sys.Add_To_Attr('KETERANGAN', '".$new_data['KETERANGAN']."', attr_);
                Client_Sys.Add_To_Attr('JENIS_KELAMIN', '".$new_data['JENIS_KELAMIN']."', attr_);
                Client_Sys.Add_To_Attr('STATUS_PESERTA', UPPER('".$new_data['STATUS_PESERTA']."'), attr_);
                Client_Sys.Add_To_Attr('TIPE_PESERTA', UPPER('".$new_data['TIPE_PESERTA']."'), attr_);
                Client_Sys.Add_To_Attr('GADAS_THT', '".$new_data['GADAS_THT']."', attr_);
                Client_Sys.Add_To_Attr('GOLONGAN', '".$new_data['GOLONGAN']."', attr_);
                Client_Sys.Add_To_Attr('TGL_CAPEG', TO_DATE('".$new_data['TGL_CAPEG']."','MM/DD/YYYY'), attr_);
                Client_Sys.Add_To_Attr('TGL_PEGPRUS', TO_DATE('".$new_data['TGL_PEGPRUS']."','MM/DD/YYYY'), attr_);
                Client_Sys.Add_To_Attr('NILAI_SEKARANG', '".$new_data['NILAI_SEKARANG']."', attr_);
                Client_Sys.Add_To_Attr('NILAI_PENGURANG', '".$new_data['NILAI_PENGURANG']."', attr_);
                Client_Sys.Add_To_Attr('JABATAN', UPPER('".$new_data['JABATAN']."'), attr_);
                Client_Sys.Add_To_Attr('GADAS_PENSIUN', '".$new_data['GADAS_PENSIUN']."', attr_);
                Client_Sys.Add_To_Attr('ALAMAT', '".$new_data['ALAMAT']."', attr_);
                Client_Sys.Add_To_Attr('KOTA_ID', '".$new_data['KOTA_ID']."', attr_);
                Client_Sys.Add_To_Attr('PROVINSI_ID', '".$new_data['PROVINSI_ID']."', attr_);
                Client_Sys.Add_To_Attr('KECAMATAN_ID', '".$new_data['KECAMATAN_ID']."', attr_);
                Client_Sys.Add_To_Attr('KELURAHAN_ID', '".$new_data['KELURAHAN_ID']."', attr_);
                Client_Sys.Add_To_Attr('KD_JENIS_KPST', '".$new_data['KD_JENIS_KPST']."', attr_);
                Client_Sys.Add_To_Attr('KD_KATEGORI', '".$new_data['KD_KATEGORI']."', attr_);
                Client_Sys.Add_To_Attr('KD_UNIT_KERJA', '".$new_data['KD_UNIT_KERJA']."', attr_);
                Client_Sys.Add_To_Attr('KD_BAND', UPPER('".$new_data['KD_BAND']."'), attr_);
                Client_Sys.Add_To_Attr('NAMA_JALAN', '".$new_data['NAMA_JALAN']."', attr_);
                Client_Sys.Add_To_Attr('NO_RUMAH', '".$new_data['NO_RUMAH']."', attr_);
                Client_Sys.Add_To_Attr('KOMPLEK_RUMAH', '".$new_data['KOMPLEK_RUMAH']."', attr_);
                Client_Sys.Add_To_Attr('BLOK_RUMAH', '".$new_data['BLOK_RUMAH']."', attr_);
                Client_Sys.Add_To_Attr('RT', '".$new_data['RT']."', attr_);
                Client_Sys.Add_To_Attr('RW', '".$new_data['RW']."', attr_);
                Client_Sys.Add_To_Attr('BULAN_CLTP', '".$new_data['BULAN_CLTP']."', attr_);
                Client_Sys.Add_To_Attr('PANGKAT', UPPER('".$new_data['PANGKAT']."'), attr_);
                Client_Sys.Add_To_Attr('TGL_BERHENTI', TO_DATE('".$new_data['TGL_BERHENTI']."','MM/DD/YYYY'), attr_);
                                                    
                PESERTA_API.Modify__(info_, objid_, objversion_, attr_, 'DO');
                                        
                EXCEPTION 
                WHEN OTHERS THEN
                error_log_ := Sqlerrm;
                        
                dbms_output.put_line(nik_);
            END;
            ";
            
            $sql = oci_parse($conn, $query_oci);
            
            if (oci_execute($sql)) {
                return $response->withJson(["success" => "true", "message" => "Data Kontak Berhasil Diperbarui!"], 200);
            } else {
                $e = oci_error();
                return $response->withJson(["data" => null, "error" => $e, "status" => "success"], 200);
            }
        } elseif ($error_msg != '') {
            return $response->withJson(["success" => "false", "message" => $error_msg], 200);
        }
    }); //[SIMPUL]
};
?>