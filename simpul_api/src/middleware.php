<?php

use Slim\App;

$dotenv = Dotenv\Dotenv::createImmutable(__DIR__);
$dotenv->load();

return function (App $app) {
    // e.g: $app->add(new \Slim\Csrf\Guard);

    $app->add(new Tuupola\Middleware\JwtAuthentication([
        "secure" => false,
        "path" => "/",
        "ignore" => "/token",
        "attribute" => "decoded_jwt",
        "secret" => getenv('SECRET_KEY'),
        "algorithm" => ["HS256"],
        "error" => function ($response, $arguments) {
            $data["status"] = "error";
            $data["message"] = $arguments["message"];
            return $response
                ->withHeader("Content-Type", "application/json")
                ->getBody()->write(json_encode($data, JSON_UNESCAPED_SLASHES | JSON_PRETTY_PRINT));
        }
    ]));

    $checkProxyHeaders = true; // Note: Never trust the IP address for security processes!
    $trustedProxies = ['10.0.0.1', '10.0.0.2']; // Note: Never trust the IP address for security processes!
    $app->add(new RKA\Middleware\IpAddress($checkProxyHeaders, $trustedProxies));
};
